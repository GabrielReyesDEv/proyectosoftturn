/* eslint-disable react/display-name */
import React, { Fragment, useState, forwardRef } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import moment from 'moment';
import uuid from 'uuid/v1';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardContent,
  CardActions,
  Typography,
  TextField,
  Button,
  IconButton,
  Divider,
  colors
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/DeleteOutlined';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import formatDate from 'utils/formatDate';
import PerfectScrollbar from 'react-perfect-scrollbar';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';

const useStyles = makeStyles(theme => ({
  root: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    outline: 'none',
    boxShadow: theme.shadows[20],
    width: 700,
    maxHeight: '100%',
    overflowY: 'auto',
    maxWidth: '100%'
  },
  field: {
    marginTop: theme.spacing(3)
  },
  cancelButton: {
    marginLeft: 'auto'
  },
  confirmButton: {
    color: theme.palette.white,
    backgroundColor: colors.green[600],
    '&:hover': {
      backgroundColor: colors.green[900]
    }
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  }
}));

const AddEditActividad = forwardRef((props, ref) => {
  const {
    event,
    onDelete,
    onCancel,
    onAdd,
    onEdit,
    className,
    onDisabled,
    ...rest
  } = props;
  const classes = useStyles();

  const defaultActividad = {
    status: 'Actividad estado',
    message: 'Actividad descripción',
    deadlineDate: moment().toDate(),
  };

  const [values, setValues] = useState(event || defaultActividad);
  const [expanded, setExpanded] = useState(false);

  const mode = event ? 'edit' : 'add';

  const handleFieldChange = e => {
    e.persist();
    setValues(values => ({
      ...values,
      [e.target.name]:
        e.target.type === 'checkbox' ? e.target.checked : e.target.value
    }));
  };

  const handleDelete = () => {
    onDelete && onDelete(event);
  };

  const handleAdd = () => {
    if (!values.status || !values.message) {
      return;
    }

    onAdd({ ...values, id: uuid() });
  };

  const handleEdit = () => {
    if (!values.status || !values.message) {
      return;
    }

    onEdit(values);
  };

  const handleChangeMore = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
      ref={ref}
    >
      <form>
        <CardContent>
          <Typography
            align="center"
            gutterBottom
            variant="h3"
          >
            {mode === 'add' ? 'Agregar' : 
              onDisabled ? 'Ver' : 'Editar'}
            {values.activityId && values.activityId !== '' && ` Actividad ${values.activityId}`}
            {values.projectId && values.projectId !== '' && ` Proyecto ${values.projectId}`}
          </Typography>
          <TextField
            className={classes.field}
            disabled={onDisabled}
            fullWidth
            label="Titulo"
            name="status"
            onChange={handleFieldChange}
            value={values.status.split('#')[0]}
            variant="outlined"
          />
          <TextField
            className={classes.field}
            disabled={onDisabled}
            fullWidth
            label="Descripción"
            name="message"
            onChange={handleFieldChange}
            value={values.message}
            variant="outlined"
          />
          <TextField
            className={classes.field}
            defaultValue={moment(values.deadlineDate).format('YYYY-MM-DDThh:mm:ss')}
            disabled={onDisabled}
            fullWidth
            label="Fecha de notificación"
            name="deadlineDate"
            onChange={handleFieldChange}
            type="datetime-local"
            variant="outlined"
          />
          {values.logs && values.logs.length > 0 && (
            <Fragment>
              <Typography
                align="start"
                className={classes.field}
                gutterBottom
                variant="h6"
              >
                Estados Anteriores
              </Typography>
              <PerfectScrollbar options={{ suppressScrollX: true }}>
                {values.logs.map((file, i) => (
                  <Accordion
                    expanded={expanded === `panel${i}`}
                    onChange={handleChangeMore(`panel${i}`)}
                  >
                    <AccordionSummary
                      aria-controls="panel1bh-content"
                      expandIcon={<ExpandMoreIcon />}
                      id="panel1bh-header"
                    >
                      <Typography className={classes.heading}>{`${formatDate(file.createdAt).date} ${formatDate(file.createdAt).time}`}</Typography>
                      <Typography className={classes.secondaryHeading}>{`${file.author.split('#')[0]} / ${file.status}`}</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                      <Typography>
                        {file.description}
                      </Typography>
                    </AccordionDetails>
                  </Accordion>
                ))}
              </PerfectScrollbar>
            </Fragment>
          )}
        </CardContent>
        <Divider />
        <CardActions>
          {!onDisabled && (
            <IconButton
              edge="start"
              onClick={handleDelete}
            >
              <DeleteIcon />
            </IconButton>
          )
          }
          <Button
            className={classes.cancelButton}
            onClick={onCancel}
            variant="contained"
          >
            {onDisabled ? 'Cerrar' : 'Cancelar'}
          </Button>
          {!onDisabled && (
            mode === 'add' ? (
              <Button
                className={classes.confirmButton}
                onClick={handleAdd}
                variant="contained"
              >
              Agregar
              </Button>
            ) : (
              <Button
                className={classes.confirmButton}
                onClick={handleEdit}
                variant="contained"
              >
              Guardar
              </Button>
            )
          )
          }
        </CardActions>
      </form>
    </Card>
  );
});

AddEditActividad.propTypes = {
  className: PropTypes.string,
  event: PropTypes.object,
  onAdd: PropTypes.func,
  onCancel: PropTypes.func,
  onDelete: PropTypes.func,
  onEdit: PropTypes.func
};

export default AddEditActividad;
