import React, { useState, useRef, useEffect } from 'react';
import moment from 'moment';
import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import listPlugin from '@fullcalendar/list';
import timelinePlugin from '@fullcalendar/timeline';
import getAuthorization from 'utils/getAuthorization';
import { makeStyles } from '@material-ui/styles';
import {
  Modal,
  Card,
  CardContent,
  colors,
  useTheme,
  useMediaQuery
} from '@material-ui/core';
import '@fullcalendar/core/main.css';
import '@fullcalendar/daygrid/main.css';
import '@fullcalendar/timegrid/main.css';
import '@fullcalendar/list/main.css';
import allLocales from '@fullcalendar/core/locales-all';
import Cookies from 'js-cookie';
import { render } from 'react-dom';
import axios from 'utils/axios';
import { Page } from 'components';
import { AddEditEvent, Toolbar } from './components';

const useStyles = makeStyles(theme => ({
  root: {
    height: '100%',
    padding: theme.spacing(3),
    '& .fc-unthemed td': {
      borderColor: theme.palette.divider
    },
    '& .fc-widget-header': {
      backgroundColor: colors.grey[50]
    },
    '& .fc-axis': {
      ...theme.typography.body2
    },
    '& .fc-list-item-time': {
      ...theme.typography.body2
    },
    '& .fc-list-item-title': {
      ...theme.typography.body1
    },
    '& .fc-list-heading-main': {
      ...theme.typography.h6
    },
    '& .fc-list-heading-alt': {
      ...theme.typography.h6
    },
    '& .fc th': {
      borderColor: theme.palette.divider
    },
    '& .fc-day-header': {
      ...theme.typography.subtitle2,
      fontWeight: 500,
      color: theme.palette.text.secondary,
      padding: theme.spacing(1),
      backgroundColor: colors.grey[50]
    },
    '& .fc-day-top': {
      ...theme.typography.body2
    },
    '& .fc-highlight': {
      backgroundColor: colors.blueGrey[50]
    },
    '& .fc-event': {
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.primary.contrastText,
      borderWidth: 2,
      opacity: 0.9,
      '& .fc-time': {
        ...theme.typography.h6,
        color: 'inherit'
      },
      '& .fc-title': {
        ...theme.typography.body1,
        color: 'inherit'
      }
    },
    '& .fc-list-empty': {
      ...theme.typography.subtitle1
    }
  },
  card: {
    marginTop: theme.spacing(3)
  },
  done: {
    textDecoration: 'line-through'
  }
}));

const Calendar = () => {
  const classes = useStyles();
  const calendarRef = useRef(null);
  const theme = useTheme();
  const mobileDevice = useMediaQuery(theme.breakpoints.down('sm'));
  const [view, setView] = useState(mobileDevice ? 'listWeek' : 'dayGridMonth');
  const [date, setDate] = useState(moment().toDate());
  const [events, setEvents] = useState([]);
  const [eventModal, setEventModal] = useState({
    open: false,
    event: null
  });
  const [disabledEvent, setDisabledEvent] = useState(false);
  const [eventId, setEventId] = useState('');

  let UserRoles = '';

  // if (Cookies.get('roles') !== undefined) {
  //   const findUser = JSON.parse(Cookies.get('roles')).find((x) => x.group !== undefined);
  //   if (findUser !== '') { 
  //     UserRoles = findUser; 
  //   } else {
  //     const aux = Cookies.get('roles');
  //     UserRoles = aux[0];
  //   }
  // }

  useEffect(() => {
    let mounted = true;

    const fetchEvents = async () => {
      const startOfMonth = moment().clone().startOf('month').format('YYYY-MM-DD hh:mm');
      const endOfMonth   = moment().clone().endOf('month').format('YYYY-MM-DD hh:mm');
      if (mounted) {
        await axios
          .post(
            'tasks/pagination',
            {
              'eq': [{ 'field': 'author', 'values': [`${Cookies.get('firstName')} ${Cookies.get('lastName')}#${Cookies.get('identification')}`] }],
              'filterFechas': [{'field': 'deadlineDate', 'values': [`${startOfMonth}`, `${endOfMonth}`]}]},
            getAuthorization()
          )
          .then(response => setEvents(response.data.data));
      }
    };

    fetchEvents();

    return () => {
      mounted = false;
    };
  }, []);

  useEffect(() => {
    const calendarApi = calendarRef.current.getApi();
    const newView = mobileDevice ? 'listWeek' : 'dayGridMonth';

    calendarApi.changeView(newView);
    setView(newView);
  }, [mobileDevice]);

  const handleEventClick = info => {
    const selected = events.find(event => event._id === info.event._def.publicId);
    if (selected.type === 'Calendar') {
      setDisabledEvent(false);
    } else {
      setDisabledEvent(true);
    }
    setEventId(selected._id);

    setEventModal({
      open: true,
      event: selected
    });
  };

  const setCalendarEvents = async (calendarApi, today = '') => {
    const typeConsult = today === '' ? view : today;

    await axios
      .post(
        '/tasks/pagination',
        {
          'eq': [{ 'field': 'author', 'values': [`${Cookies.get('firstName')} ${Cookies.get('lastName')}#${Cookies.get('identification')}`] }],
          'filterFechas': typeConsult === 'timeGridDay' ? [{ 'field': 'deadlineDate', 'values': [moment(calendarApi.state.dateProfile.activeRange.end).format('YYYY-MM-DD'), moment(calendarApi.state.dateProfile.activeRange.end).format('YYYY-MM-DD')] }] :
            [{ 'field': 'deadlineDate', 'values': [calendarApi.state.dateProfile.activeRange.start, calendarApi.state.dateProfile.activeRange.end] }],
        },
        getAuthorization()
      )
      .then(response => setEvents(response.data.data));
  }

  const handleEventNew = () => {
    setDisabledEvent(false);
    setEventModal({
      open: true,
      event: null
    });
  };

  const handleEventDelete = async() => {
    const calendarApi = calendarRef.current.getApi();
    await axios
      .delete(
        `/tasks/${eventId}`,
        getAuthorization()
      )
      .then(response => setEvents(response.data.data));

    setCalendarEvents(calendarApi);
    setEventModal({
      open: false,
      event: null
    });
  };

  const handleModalClose = () => {
    setEventModal({
      open: false,
      event: null
    });
  };

  const handleEventAdd = async (event) => {
    let newEvent = {
      status: `${event.status}#null`,
      area: UserRoles.area,
      group: UserRoles.group ? UserRoles.group : 'Sin Grupo',
      type: 'Calendar',
      message: event.message,
      author: `${Cookies.get('firstName')} ${Cookies.get('lastName')}#${Cookies.get('identification')}`,
      deadlineDays: moment(moment(event.deadlineDate).format('YYYY-MM-DD HH:mm')).diff(moment().format('YYYY-MM-DD HH:mm'), 'd'),
      deadlineDate: moment(event.deadlineDate).format('YYYY-MM-DD HH:mm'),
      logs: [{
        status: 'Created',
        description: event.message,
        author: `${Cookies.get('firstName')} ${Cookies.get('lastName')}#${Cookies.get('identification')}`,
        createdAt: moment().format('YYYY-MM-DD HH:mm')
      }],
      createdAt: moment().format('YYYY-MM-DD HH:mm'),
      updatedAt: moment().format('YYYY-MM-DD HH:mm'),
    }
    await axios
      .post(
        '/tasks/',
        newEvent,
        getAuthorization()
      )
      .then(response => setEvents(response.data.data));
    const calendarApi = calendarRef.current.getApi();
    setCalendarEvents(calendarApi);
    setEventModal({
      open: false,
      event: null
    });
  };

  const handleEventEdit = async (event) => {
    delete event._id;
    const eventUpdated = {
      message: event.message,
      author: `${Cookies.get('firstName')} ${Cookies.get('lastName')}#${Cookies.get('identification')}`,
      deadlineDays: moment(moment(event.deadlineDate).format('YYYY-MM-DD HH:mm')).diff(moment().format('YYYY-MM-DD HH:mm'), 'd'),
      deadlineDate: moment(event.deadlineDate).format('YYYY-MM-DD HH:mm'),
      logs: event.logs,
      updatedAt: moment().format('YYYY-MM-DD HH:mm'),
    };
    await axios
      .put(
        `/tasks/${eventId}`,
        eventUpdated,
        getAuthorization()
      )
      .then(response => setEvents(response.data.data));
    const calendarApi = calendarRef.current.getApi();
    setCalendarEvents(calendarApi);
    setEventModal({
      open: false,
      event: null
    });
  };

  const handleDateToday = () => {
    const calendarApi = calendarRef.current.getApi();

    calendarApi.today();
    setDate(calendarApi.getDate());
    setCalendarEvents(calendarApi);
  };

  const handleViewChange = view => {
    const calendarApi = calendarRef.current.getApi();
    calendarApi.changeView(view);
    setView(view);
  };

  const handleDatePrev = () => {
    const calendarApi = calendarRef.current.getApi();

    calendarApi.prev();
    setDate(calendarApi.getDate());
    setCalendarEvents(calendarApi);
  };

  const handleDateNext = () => {
    const calendarApi = calendarRef.current.getApi();

    calendarApi.next();
    setDate(calendarApi.getDate());
    setCalendarEvents(calendarApi);
  };

  return (
    <Page
      className={classes.root}
      title="Calendar"
    >
      <Toolbar
        date={date}
        onDateNext={handleDateNext}
        onDatePrev={handleDatePrev}
        onDateToday={handleDateToday}
        onEventAdd={handleEventNew}
        onViewChange={handleViewChange}
        view={view}
      />
      <Card className={classes.card}>
        <CardContent>
          <FullCalendar
            allDayMaintainDuration
            defaultDate={date}
            defaultView={view}
            droppable
            editable
            eventClick={handleEventClick}
            eventDataTransform={function( json ) {
              let color
              if (json.type === 'Primary') {
                color = '#e53835'
              } else if (json.type === 'Secondary') {
                color = '#ffa524'
              } else {
                color = '#388e3c'
              }

              let newData = {
                id: json._id,
                // title: `${json.status.split('#')[0]} de ${json.orderType} ${json.activityId}`,
                title: `${json.status.split('#')[0]}`,
                start: moment(json.deadlineDate, 'YYYY-MM-DD HH:mm').toDate(),
                end: moment(json.deadlineDate, 'YYYY-MM-DD HH:mm').toDate(),
                desc: json.message,
                className: (json.zolidStatus === 'Terminated') && classes.done,
                zolidStatus: json.zolidStatus,
                color
              }
              return newData;
            }}
            eventRender={(info) => {
              if(view!=='listWeek'){
                render(
                  <div className="fc-content">
                    <span className={info.event.classNames.length > 0 && classes.done}>{info.event.title}</span>
                  </div>,
                  info.el,
                );
                return info.el
              }
            }}
            eventResizableFromStart
            events={events}
            header={false}
            height={800}
            locale={'es'}
            locales={allLocales}
            plugins={[
              dayGridPlugin,
              timeGridPlugin,
              interactionPlugin,
              listPlugin,
              timelinePlugin
            ]}
            ref={calendarRef}
            rerenderDelay={10}
            selectable
            weekends
          />
        </CardContent>
      </Card>
      <Modal
        onClose={handleModalClose}
        open={eventModal.open}
      >
        <AddEditEvent
          event={eventModal.event}
          onAdd={handleEventAdd}
          onCancel={handleModalClose}
          onDelete={handleEventDelete}
          onDisabled={disabledEvent}
          onEdit={handleEventEdit}
        />
      </Modal>
    </Page>
  );
};

export default Calendar;
