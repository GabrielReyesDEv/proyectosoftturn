import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { Typography, Grid, Button } from '@material-ui/core';
import { Activity } from './components';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';

const useStyles = makeStyles(theme => ({
  root: {},
  title: {
    marginBottom: theme.spacing(3)
  },
  group: {
    '& + &': {
      marginTop: theme.spacing(4)
    }
  },
  activity: {
    position: 'relative',
    '& + &': {
      marginTop: theme.spacing(3),
      '&:before': {
        position: 'absolute',
        content: '" "',
        height: 20,
        width: 1,
        top: -20,
        left: 20,
        backgroundColor: theme.palette.divider
      }
    }
  },
  learnMoreButton: {
    marginLeft: theme.spacing(2),
  },
  learnMoreDiv: {
    marginTop: '18px',
    display: 'flex',
    alignContent: 'center',
    alignItems: 'center',
    flexFlow: 'column',
  }
}));

const Activities = props => {
  const { activity, className, ...rest } = props;
  const [page, setPage] = useState(0);
  const [totalPags, setTotalPags] = useState(0);
  const [taskRegisters, setTaskRegisters] = useState([]);

  const fetchTaskRegisters = async (body) => {
    await axios
      .post(
        '/tasks/extractArrays',
        body,
        getAuthorization(),
      )
      .then((response) => {
        setTaskRegisters(response.data.data)
        setTotalPags(response.data.total_pages);
        setPage(response.data.page);
      });
  };

  const handleShowMorePosts = async () => {
    const body = {
      sort: {'column': 'createdAt', 'value': -1},
      limit: 10,
      page: page +1,
      'eq': [{ 'field': 'activityId', 'values': [activity.activityId] }],
      'neq': [{ 'field': 'logs', 'values': [null] }],
      'filter1': 'logs',
      'mergeObjects': [{ 'projectId': '$projectId'}, { 'activityId': '$activityId'}, {'area': '$area'}],
    }
    await axios
      .post(
        '/tasks/extractArrays',
        body,
        getAuthorization(),
      )
      .then((response) => {
        const dataNueva = taskRegisters.concat(response.data.data);
        setTaskRegisters(dataNueva)
        setTotalPags(response.data.total_pages);
        setPage(response.data.page);
      });
  }

  useEffect(() => {
    fetchTaskRegisters({
      limit: 10,
      page: 1,
      'eq': [{ 'field': 'activityId', 'values': [activity.activityId] }],
      'neq': [{ 'field': 'logs', 'values': [null] }],
      'filter1': 'logs',
      'mergeObjects': [{ 'projectId': '$projectId'}, { 'activityId': '$activityId'}, {'area': '$area'}],
      'sort': {'column': 'createdAt', 'value': -1}
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const classes = useStyles();

  return (
    <div
      {...rest}
      className={clsx(classes.root, className)}
    >
      <Typography
        className={classes.title}
        variant="h3"
      >
        Descripción
      </Typography>
      <div className={classes.group}>

        {taskRegisters.map(activity => (
          <Activity
            activity={activity}
            className={classes.activity}
            key={activity.id}
          />
        ))}
      </div>
      <div className={classes.learnMoreDiv}>
        {page !== totalPags && taskRegisters.length !== 0 && (
          <Grid item>
            <Button
              className={classes.learnMoreButton}
              onClick={handleShowMorePosts}
              size="small"
            >
              Mostrar más
            </Button>
          </Grid>
        )}
      </div>
    </div>
  );
};

Activities.propTypes = {
  activity: PropTypes.object.isRequired,
  className: PropTypes.string,
};

export default Activities;
