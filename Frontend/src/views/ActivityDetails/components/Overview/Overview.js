import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';

import { Brief, Deliverables, Holder, Members, Details } from './components';

const useStyles = makeStyles(theme => ({
  root: {},
  deliverables: {
    marginTop: theme.spacing(3)
  },
  members: {
    marginTop: theme.spacing(3)
  }
}));

const Overview = props => {
  const { activity, className, ...rest } = props;

  const classes = useStyles();

  return (
    <Grid
      {...rest}
      className={clsx(classes.root, className)}
      container
      spacing={3}
    >
      <Grid
        item
        lg={8}
        xl={9}
        xs={12}
      >
        {activity && (
          <Details activity={activity} />
        )}
        {activity.brief && activity.brief !== '' && (
          <Brief brief={activity.brief?.replaceAll('####', '\n')} />
        )}
        {activity.notes && activity.notes !== '' && (
          <Deliverables
            className={classes.deliverables}
            notes={activity.notes}
          />
        )}
      </Grid>
      <Grid
        item
        lg={4}
        xl={3}
        xs={12}
      >
        <Holder activity={activity} />
        <Members
          activity={activity}
          className={classes.members}
          members={activity.members ? activity.members : []}
        />
      </Grid>
    </Grid>
  );
};

Overview.propTypes = {
  activity: PropTypes.object.isRequired,
  className: PropTypes.string,
};

export default Overview;
