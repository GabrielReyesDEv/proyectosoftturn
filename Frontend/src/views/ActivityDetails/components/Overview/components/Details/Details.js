import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { Card, CardContent, Typography } from '@material-ui/core';

import { Markdown } from 'components';

const useStyles = makeStyles(() => ({
  root: {},
  detailProject: {
    marginTop: '18px'
  }
}));

const Details = props => {
  const { activity, className, ...rest } = props;
  const classes = useStyles();
  const priority = ['Baja', 'Media', 'Alta'];

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardContent>
        <Markdown source={activity.familia} />
        {activity.resolucion15 && (
          <>
            <Typography variant="h4">Prioridad:</Typography>
            <Typography variant="body1">
              {priority[activity.priority]}
            </Typography>
          </>
        )}
        {activity.resolucion15 && (
          <>
            <Typography 
              className={classes.detailProject}
              variant="h4"
            >
                Resolucion15:</Typography>
            <Typography variant="body1">
              {activity.resolucion15}
            </Typography>
          </>
        )}
        {activity.resolucion26 && (
          <>
            <Typography
              className={classes.detailProject}
              variant="h4"
            >Resolucion26:</Typography>
            <Typography variant="body1">
              {`${activity.resolucion26} $`}
            </Typography>
          </>
        )}
        {activity.resolucion37 && (
          <>
            <Typography
              className={classes.detailProject}
              variant="h4"
            >Resolucion37:</Typography>
            <Typography variant="body1">
              {activity.resolucion37}
            </Typography>
          </>
        )}
        {activity.resolucion48 && (
          <>
            <Typography
              className={classes.detailProject}
              variant="h4"
            >Resolucion48:</Typography>
            <Typography variant="body1">
              {activity.resolucion48}
            </Typography>
          </>
        )}
      </CardContent>
    </Card>
  );
};

Details.propTypes = {
  activity: PropTypes.object.isRequired,
  className: PropTypes.string
};

export default Details;
