import React, { useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Button,
  Dialog,
  TextField,
  Typography,
  colors,
  Grid, 
  List, 
  ListItem, 
  Collapse, 
  ListItemText, 
  ListItemIcon,
  Tooltip,
} from '@material-ui/core';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import ExpandMore from '@material-ui/icons/ExpandMore';
import AddIcon from '@material-ui/icons/Add';
import ExpandLess from '@material-ui/icons/ExpandLess';
import RemoveIcon from '@material-ui/icons/Remove';

const useStyles = makeStyles(theme => ({
  root: {
    width: 960
  },
  header: {
    padding: theme.spacing(3),
    maxWidth: 720,
    margin: '0 auto'
  },
  content: {
    padding: theme.spacing(0, 2),
    maxWidth: 720,
    margin: '0 auto'
  },
  helperText: {
    textAlign: 'right',
    marginRight: 0
  },
  author: {
    margin: theme.spacing(4, 0),
    display: 'flex'
  },
  avatar: {
    marginRight: theme.spacing(2)
  },
  actions: {
    backgroundColor: colors.grey[100],
    padding: theme.spacing(2),
    display: 'flex',
    justifyContent: 'center'
  },
  applyButton: {
    color: theme.palette.white,
    backgroundColor: colors.green[600],
    '&:hover': {
      backgroundColor: colors.green[900]
    }
  },
  formGroup: {
    marginBottom: theme.spacing(3)
  },
  fieldGroup: {
    display: 'flex',
    alignItems: 'center'
  },
  fieldGroupSpaced: {
    display: 'flex',
    alignItems: 'center',
    '& > * + *': {
      marginLeft: theme.spacing(2)
    }
  },
  dateField: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  },
  boton: {
    margin: '12px',
    width: '-webkit-fill-available',
  },
  cont1: {
    width: '100%',
    display: 'flex',
    flexFlow: 'row nowrap',
    justifyContent: 'space-between',
    marginBottom: '25px',
    padding: theme.spacing(0, 2),
    maxWidth: 720,
    margin: '0 auto'
  },
  cont2: {
    width: '100%',
    display: 'flex',
    flexFlow: 'row nowrap',
    justifyContent: 'space-between',
    alignItems: 'baseline',
    padding: theme.spacing(0, 2),
    maxWidth: 720,
    margin: '0 auto'
  },
}));

const Application = props => {
  const { create, member, membersInfo, open, onClose, onApply, className, ...rest } = props;
  const [values, setValues] = useState({members: member});

  const classes = useStyles();
  const [collapseMateriales, setCollapseMateriales] = React.useState({col: [false * member.length]});

  membersInfo(values);

  const handleChangeItem = (e, i = null, nombre, object) => {
    const { target } = e;
    const elementos = values.members;
    elementos[i] = {
      ...elementos[i],
      [nombre]: target.value,
    }
    setValues({members: elementos});
  };
  
  const nuevoItem = (nombre, obj) => {
    setValues({
      ...values,
      [nombre]: [
        ...obj,
        {
          name: '',
          mail: '',
          phone: '',
        },
      ],
    });
  };

  const borrarItem = (i, nombre, obj) => {
    const arr = obj;
    arr.splice(i, 1);
    setValues({
      ...values,
      [nombre]: arr,
    });
  };

  const handleClickDev = (estado, index) => {
    let arr = collapseMateriales;
    arr[index] = estado === undefined ? true : (estado === true ? false : true);
    setCollapseMateriales({col: arr});
  };

  return (
    <Dialog
      maxWidth="lg"
      onClose={onClose}
      open={open}
    >
      <div
        {...rest}
        className={clsx(classes.root, className)}
      >
        <div className={classes.header}>
          <Typography
            align="center"
            className={classes.title}
            gutterBottom
            variant="h3"
          >
            Actualizar Contactos
          </Typography>
          <Typography
            align="center"
            className={classes.subtitle}
            variant="subtitle2"
          >
            Debe llenar todos los campos para agregar un contacto.
          </Typography>
        </div>
        <div className={classes.cont1}>
            <div>
                <Tooltip title='Agregar Contacto' placement='right'>
                    <Button variant='outlined' color='primary' onClick={() => nuevoItem('members', values.members)}><AddIcon /></Button>
                </Tooltip>
            </div>
            <div className={classes.boton}>
                <Typography variant='h5'>
                    {values.members ? values.members.length : '0' }
                    {' '}
                    Contactos Agregados
                </Typography>
            </div>
        </div>
        {values.members?.length > 0 ? values.members?.map((obj, i) => (
          <Grid
            item
            key={i + 1}
            sm={12}
            xs={12}
          >
            <div className={classes.cont2}>
                <div className={classes.boton}>
              <List>
                <Tooltip
                  placement="top"
                  title="Visualizar Contacto"
                >
                  <ListItem
                    button
                    onClick={() => handleClickDev(collapseMateriales.col[i], i)}
                  >
                    <ListItemIcon>
                      <AccountCircleIcon 
                      style={create && (values.members[i].name === '' || values.members[i].name === undefined ||
                      values.members[i].mail === '' || values.members[i].mail === undefined ||
                      values.members[i].phone === '' || values.members[i].phone === undefined) ? 
                      {color: '#d33734', borderColor: '#d33734'} :
                      {color: '#546e7a', borderColor: '#546e7a'}}
                      />
                    </ListItemIcon>
                    <ListItemText
                      primary={`Visualizar Contacto ${values.members[i].name}`}
                    />
                    {collapseMateriales.col[i] ? <ExpandLess /> : <ExpandMore />}
                  </ListItem>
                </Tooltip>
                <Collapse
                  in={collapseMateriales.col[i]}
                  item
                  key={i + 1}
                  // eslint-disable-next-line react/jsx-props-no-spreading
                  {...(collapseMateriales.col[i] ? { timeout: 1000 } : {})}
                  style={{ transformOrigin: '0 0 0' }}
                  unmountOnExit
                >
                  <div className={classes.formGroup}>
                    <div className={classes.fieldGroupSpaced}>
                      <TextField
                        error={create && (values.members[i].name === '' || values.members[i].name === undefined)}
                        fullWidth
                        helperText={create && (values.members[i].name === '' || values.members[i].name === undefined) ? '*Campo Requerido' : false}
                        label="Nombre"
                        margin="dense"
                        multiline
                        name="name"
                        onChange={event =>
                          handleChangeItem(event, i, 'name', values.members)
                        }
                        rows="2"
                        value={values.members[i].name || ''}
                        variant="outlined"
                      />
                      <TextField
                        error={create && (values.members[i].mail === '' || values.members[i].mail === undefined)}
                        fullWidth
                        helperText={create && (values.members[i].mail === '' || values.members[i].mail === undefined) ? '*Campo Requerido' : false}
                        label="Correo"
                        margin="dense"
                        multiline
                        name="mail"
                        onChange={event =>
                          handleChangeItem(event, i, 'mail', values.members)
                        }
                        rows="2"
                        value={values.members[i].mail}
                        variant="outlined"
                      />
                      <TextField
                        error={create && (values.members[i].phone === '' || values.members[i].phone === undefined)}
                        fullWidth
                        helperText={create && (values.members[i].phone === '' || values.members[i].phone === undefined) ? '*Campo Requerido' : false}
                        label="Telefono"
                        margin="dense"
                        multiline
                        name="phone"
                        onChange={event =>
                          handleChangeItem(event, i, 'phone', values.members)
                        }
                        rows="2"
                        value={values.members[i].phone}
                        variant="outlined"
                      />
                    </div>
                  </div>
                </Collapse>
              </List>
              </div>
              <div>
                <Tooltip title='Borrar Contacto' placement='top'>
                   <Button variant='outlined' style={{color: '#ff8581', borderColor: '#ff8581'}} onClick={() => borrarItem(i, 'members', values.members)}><RemoveIcon /></Button>
                 </Tooltip>
                </div>
            </div>
          </Grid>
        )) : ''}
        <div className={classes.actions}>
          <Button
            className={classes.applyButton}
            onClick={onApply}
            variant="contained"
          >
            Editar Contactos
          </Button>
        </div>
      </div>
    </Dialog>
  );
};

Application.propTypes = {
  className: PropTypes.string,
  create: PropTypes.bool,
  member: PropTypes.object.isRequired,
  membersInfo: PropTypes.func,
  onApply: PropTypes.func,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired,
};

export default Application;
