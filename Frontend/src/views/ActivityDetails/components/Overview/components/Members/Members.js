import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Avatar,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  colors
} from '@material-ui/core';
import moment from 'moment';
import getInitials from 'utils/getInitials';
import { Application } from './components';
import { useSnackbar } from 'notistack';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';

const useStyles = makeStyles(() => ({
  root: {},
  header: {
    paddingBottom: 0
  },
  content: {
    paddingTop: 0
  },
  actions: {
    backgroundColor: colors.grey[50]
  },
  manageButton: {
    width: '100%'
  }
}));

const Members = props => {
  const { activity, members, className, ...rest } = props;
  const [openApplication, setOpenApplication] = useState(false);
  const [membersInfo, setMembersInfo] = useState({});
  const [crear, setCrear] = useState(false);
  // eslint-disable-next-line no-unused-vars
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const classes = useStyles();

  const handleChangeMembers = (values) => {
    setMembersInfo(values);
  }

  const handleApplicationOpen = () => {
    setOpenApplication(true);
  };

  const handleApplicationClose = () => {
    setOpenApplication(false);
  };

  const validacionErrores = (camposReq, info) => {
    let errorVacios = 0;
    for (let index = 0; index < camposReq.length; index++) {
      for (let j = 0; j < info.length; j++) {
        const auxCamposReq = camposReq[index];
        const aux = Object.fromEntries(Object.entries(info[j]).filter(([key]) => key === auxCamposReq));
        if (Object.values(aux)[0] === '' || Object.values(aux)[0] === undefined) {
          errorVacios = 1;
          index = camposReq.length;
          j = info.length;
        }
      }
    }
    return errorVacios;
  }

  const fetchQuery = async (data, url, message = '') => {
    let result = 0;
    await axios
      .put(
        url,
        data,
        getAuthorization(),
      )
      .then((response) => {
        result = response.data.data.length;
        if (message !== '') {
          enqueueSnackbar(message.msg, {
            variant: message.variant,
          });
        }
      })
      .catch((err) => {
        enqueueSnackbar(`error: ${err}`, {
          variant: 'error',
        });
        result = -1;
      })
    return result; 
  };

  const handleEditButton = async () => {
    setCrear(true);
    const idObject = activity._id;
    const camposReqActivity = [
      'name',
      'mail',
      'phone',
    ];
    let errorVacios = validacionErrores(camposReqActivity, membersInfo.members);
    if (errorVacios === 0) {
      let url = `/activities/${idObject}`;
      const finalActivity = {
        ...membersInfo, 
        field: '_id',
        updatedAt: moment().format('YYYY-MM-DD HH:mm'),
      }
      await fetchQuery(
        finalActivity, 
        url, 
        {
          msg: `Actividad ${activity.activityId} Actualizada con exito`,
          variant: 'success',
        });
      window.location.href = `/activities/${activity.activityId}/overview`;
    } else {
      enqueueSnackbar('Algunos campos estan vacios', {
        variant: 'error',
      });
    }
  }

  return (
    <div>
      <Card
        {...rest}
        className={clsx(classes.root, className)}
      >
        <CardHeader
          className={classes.header}
          title="Contactos del Proyecto"
          titleTypographyProps={{
            variant: 'overline'
          }}
        />
        <CardContent className={classes.content}>
          <List>
            {members.map(member => (
              <ListItem
                disableGutters
                key={member.id}
              >
                <ListItemAvatar>
                  <Avatar
                    alt="Author"
                    className={classes.avatar}
                    src={member.avatar}
                  >
                    {getInitials(member.name)}
                  </Avatar>
                </ListItemAvatar>
                <ListItemText
                  primary={member.name}
                  primaryTypographyProps={{ variant: 'h6' }}
                  secondary={member.bio}
                />
              </ListItem>
            ))}
          </List>
        </CardContent>
        <CardActions className={classes.actions}>
          <Button
            className={classes.manageButton}
            onClick={handleApplicationOpen}
          >
            Editar Contactos
          </Button>
        </CardActions>
      </Card>
      <Application
        create={crear}
        member={members}
        membersInfo={handleChangeMembers}
        onApply={handleEditButton}
        onClose={handleApplicationClose}
        open={openApplication}
      />
    </div>
  );
};

Members.propTypes = {
  activity: PropTypes.object,
  className: PropTypes.string,
  members: PropTypes.array.isRequired,
};

export default Members;
