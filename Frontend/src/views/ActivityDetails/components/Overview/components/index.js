export { default as Brief } from './Brief';
export { default as Deliverables } from './Deliverables';
export { default as Holder } from './Holder';
export { default as Members } from './Members';
export { default as Details } from './Details';
