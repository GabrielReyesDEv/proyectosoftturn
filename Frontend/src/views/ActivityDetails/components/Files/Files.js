import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { Card, CardContent, Grid, Button } from '@material-ui/core';
import { FilesDropzone } from 'components';
import { FileCard } from './components';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';

const useStyles = makeStyles(theme => ({
  root: {},
  activity: {
    marginTop: theme.spacing(3)
  }
}));

const Files = props => {
  const { activity, className, ...rest } = props;
  const [page, setPage] = useState(0);
  const [totalPags, setTotalPags] = useState(0);
  const [taskRegisters, setTaskRegisters] = useState([]);

  const fetchTaskFiles = async (body) => {
    await axios
      .post(
        '/tasks/extractArrays',
        body,
        getAuthorization(),
      )
      .then((response) => {
        setTaskRegisters(response.data.data)
        setTotalPags(response.data.total_pages);
        setPage(response.data.page);
      });
  };

  const handleShowMorePosts = async () => {
    const body = {
      limit: 10,
      page: page +1,
      'eq': [{ 'field': 'activityId', 'values': [activity.activityId] }],
      'neq': [{ 'field': 'evidences', 'values': [null] }],
      'filter1': 'evidences',
      'mergeObjects': [{ 'projectId': '$projectId'}, { 'activityId': '$activityId'}],
      'sort': {'column': 'createdAt', 'value': -1}
    }
    await axios
      .post(
        '/tasks/extractArrays',
        body,
        getAuthorization(),
      )
      .then((response) => {
        const dataNueva = taskRegisters.concat(response.data.data);
        setTaskRegisters(dataNueva)
        setTotalPags(response.data.total_pages);
        setPage(response.data.page);
      });
  }

  useEffect(() => {
    fetchTaskFiles({
      limit: 2,
      page: 1,
      'eq': [{ 'field': 'activityId', 'values': [activity.activityId] }],
      'neq': [{ 'field': 'evidences', 'values': [null] }],
      'filter1': 'evidences',
      'mergeObjects': [{ 'projectId': '$projectId'}, { 'activityId': '$activityId'}],
      'sort': {'column': 'createdAt', 'value': -1}
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const classes = useStyles();

  return (
    <div>
      <div
        {...rest}
        className={clsx(classes.root, className)}
      >
        <Card>
          <CardContent>
            <FilesDropzone />
          </CardContent>
        </Card>
        <Grid
          className={classes.activity}
          container
          spacing={3}
        >
          {taskRegisters.map(file => (
            <Grid
              item
              key={file.id}
              lg={4}
              md={4}
              sm={6}
              xs={12}
            >
              <FileCard file={file} />
            </Grid>
          ))}
        </Grid>
      </div>
      <div className={classes.learnMoreDiv}>
        {page !== totalPags && taskRegisters.length !== 0 && (
          <Grid item>
            <Button
              className={classes.learnMoreButton}
              onClick={handleShowMorePosts}
              size="small"
            >
              Mostrar más
            </Button>
          </Grid>
        )}
      </div>
    </div>
  );
};

Files.propTypes = {
  activity: PropTypes.array.isRequired,
  className: PropTypes.string,
};

export default Files;
