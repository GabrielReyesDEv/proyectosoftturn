import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';

import { Results } from '../../../ActivityList/components';

const useStyles = makeStyles(() => ({
  root: {}
}));

const Subscribers = props => {
  const { idActivity, activity, subscribers, className, ...rest } = props;
  const classes = useStyles();
  return (
    <Grid
      {...rest}
      className={clsx(classes.root, className)}
      container
      spacing={3}
    >
      <Results
        activity={activity}
        className={classes.results}
        idActivity={idActivity}
      />
      {/* {subscribers.map(subscriber => (
        <Grid
          item
          key={subscriber.id}
          lg={4}
          xs={12}
        >
          <SubscriberCard subscriber={subscriber} />
        </Grid>
      ))} */}
    </Grid>
  );
};

Subscribers.propTypes = {
  activity: PropTypes.object.isRequired,
  className: PropTypes.string,
  idActivity: PropTypes.string,
  subscribers: PropTypes.array.isRequired
};

export default Subscribers;
