import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Avatar,
  Button,
  Dialog,
  TextField,
  Typography,
  colors,
  Checkbox,
  FormControlLabel,
  FormLabel,
  FormControl,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Grid,
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Autocomplete from '@material-ui/lab/Autocomplete';
import getInitials from 'utils/getInitials';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';

const useStyles = makeStyles(theme => ({
  root: {
    width: 960
  },
  header: {
    padding: theme.spacing(3),
    maxWidth: 720,
    margin: '0 auto'
  },
  content: {
    padding: theme.spacing(0, 2),
    maxWidth: 720,
    margin: '0 auto'
  },
  helperText: {
    textAlign: 'right',
    marginRight: 0
  },
  author: {
    margin: theme.spacing(4, 0),
    display: 'flex'
  },
  avatar: {
    marginRight: theme.spacing(2)
  },
  actions: {
    backgroundColor: colors.grey[100],
    padding: theme.spacing(2),
    display: 'flex',
    justifyContent: 'center'
  },
  applyButton: {
    color: theme.palette.white,
    backgroundColor: colors.green[600],
    '&:hover': {
      backgroundColor: colors.green[900]
    }
  },
  formGroup: {
    marginBottom: theme.spacing(3)
  },
  fieldGroup: {
    display: 'flex',
    alignItems: 'center'
  },
  fieldGroupSpaced: {
    display: 'flex',
    alignItems: 'center',
    '& > * + *': {
      marginLeft: theme.spacing(2)
    }
  },
  dateField: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  },
}));

const Application = props => {
  const { create, activity, valuesCheck, areas, handleChange, activityInfo, open, onClose, onApply, className, ...rest } = props;
  const [values, setValues] = useState({ ...activity });
  const [users, setUsers] = useState({});
  const [role, setSelectRole] = useState(null);
  const area = areas.find((area) => area.name === activity.area);
  const [defaultSel, setDefault] = useState({
    type:{
      default: { title: values.type, value: values.type }
    },
    status:{
      default: { title: values.status, value: values.status }
    },
    priority:{
      default: { title: `${values.priority}`, value: parseInt(values.priority) }
    },
    area:{
      default: { title: `${values.area}`, value: values.area }
    }
  });

  const classes = useStyles();

  const [arrAreas, setArrAreas] = useState([]);
  const [valuesCheck2, setValuesCheck2] = React.useState({});

  const handleSelectionTask = (e) => {
    setValuesCheck2({
      ...valuesCheck2,
      [e.target.name]: e.target.checked
    });
    const selectedRole = area.tasks[e.target.id].role
    setSelectRole({ ...role, [`${e.target.id}`]: selectedRole })
    getUsers(selectedRole)
  }

  useEffect(() => {
    let mounted = true;
    
    const fetchStatus = () => {
      const group = 'BO'
      axios.get(`/areas?limit=100&page=1&group=${group}`, getAuthorization()).then(response => {
        if (mounted) {
          for (let index = 0; index < response.data.data.length; index++) {
            arrAreas.push({ title: response.data.data[index].name, value: response.data.data[index].name },);
          }
          setArrAreas(arrAreas)
        }
      });
      
    };
    
    fetchStatus();
    getUsers();
    

    return () => {
      mounted = false;
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  activityInfo(values);
  const handleFieldChange = (event, field, value, type = '') => {
    event.persist && event.persist();
    setValues(values => ({
      ...values,
      [field]: value
    }));
    if (type === 'autoComplete') {
      setDefault(defaultSel => ({
        ...defaultSel,
        [field]: { title: `${value}`, value: value }
      }));
    }
  };

  const [dispatchers, setDisparcher] =  React.useState([]);


  const getUsers = (role) => {
    // 'area': task.area,
    // '$elemMatch': { 'group': 'BO', 'role' : 'Dispatcher' }
    axios.post('/users/pagination', {
      // 'status': 'Active',
      'roles': {
        '$elemMatch': { 'role' : role, 'area': activity.area }
      }
    }, getAuthorization()).then(response => {
      console.log('response', response);
      setUsers({ ...users, [role]: response.data?.data && response.data?.data.length > 0 && response.data.data[0].data ? response.data.data[0].data : []})
    })
  }

  return (
    <Dialog
      maxWidth="lg"
      onClose={onClose}
      open={open}
    >
      <div
        {...rest}
        className={clsx(classes.root, className)}
      >
        <div className={classes.header}>
          <Typography
            align="center"
            className={classes.title}
            gutterBottom
            variant="h3"
          >
            {`Actualizar Actividad ${values.activityId}`}
          </Typography>
          <Typography
            align="center"
            className={classes.subtitle}
            variant="subtitle2"
          >
            Los siguientes campos pueden ser actualizados 
          </Typography>
        </div>
        <div className={classes.content}>
          <div className={classes.formGroup}>
            <div className={classes.fieldGroupSpaced}>
              <Autocomplete
                fullWidth
                getOptionLabel={(option) => (option.title ? option.title : values.status)}
                id="status"
                // eslint-disable-next-line no-use-before-define
                name="status"
                onChange={event => (
                  handleFieldChange(event, 'status', event.target.innerText, 'autoComplete')
                )
                }
                options={optionsStatus}
                renderInput={(params) => (
                  // eslint-disable-next-line react/jsx-props-no-spreading
                  <TextField
                    {...params}
                    error={create && (values.status === '' || values.status === undefined)}
                    fullWidth
                    helperText={create && (values.status === ''  || values.status === undefined) ? '*Campo Requerido' : false}
                    label="Estado del Proyecto"
                    required
                    variant="outlined"
                  />
                )}
                value={defaultSel.status}
              />
              <Autocomplete
                fullWidth
                getOptionLabel={(option) => (option.title ? option.title : `${values.priority}`)}
                id="priority"
                // eslint-disable-next-line no-use-before-define
                name="priority"
                onChange={event => (
                  handleFieldChange(event, 'priority', (isNaN(parseInt(event.target.innerText)) ? '' : parseInt(event.target.innerText)), 'autoComplete')
                )
                }
                options={optionsPriority}
                renderInput={(params) => (
                  // eslint-disable-next-line react/jsx-props-no-spreading
                  <TextField
                    {...params}
                    error={create && (values.priority === '' || values.priority === undefined)}
                    fullWidth
                    helperText={create && (values.priority === '' || values.priority === undefined) ? '*Campo Requerido' : false}
                    label="Prioridad del Proyecto"
                    required
                    variant="outlined"
                  />
                )}
                required
                type="number"
                value={defaultSel.priority}
              />
            </div>
          </div>
        </div>
        <div className={classes.content}>
          <div className={classes.formGroup}>
            <div className={classes.fieldGroupSpaced}>
              <TextField
                fullWidth
                label="Resolucion 15"
                name="resolucion15"
                onChange={event =>
                  handleFieldChange(event, 'resolucion15', event.target.value !== '' ? event.target.value : undefined)
                }
                value={values.resolucion15}
                variant="outlined"
              />
              <TextField
                className={classes.dateField}
                fullWidth
                label="Resolucion 26"
                name="resolucion26"
                onChange={event =>
                  handleFieldChange(event, 'resolucion26', event.target.value !== '' ? event.target.value : undefined)
                }
                value={values.resolucion26}
                variant="outlined"
              />
            </div>
          </div>
        </div>
        <div className={classes.content}>
          <div className={classes.formGroup}>
            <div className={classes.fieldGroupSpaced}>
              <TextField
                fullWidth
                label="Resolucion 37"
                name="resolucion37"
                onChange={event =>
                  handleFieldChange(event, 'resolucion37', event.target.value !== '' ? event.target.value : undefined)
                }
                value={values.resolucion37}
                variant="outlined"
              />
              <TextField
                className={classes.dateField}
                fullWidth
                label="Resolucion 48"
                name="resolucion48"
                onChange={event =>
                  handleFieldChange(event, 'resolucion48', event.target.value !== '' ? event.target.value : undefined)
                }
                value={values.resolucion48}
                variant="outlined"
              />
            </div>
          </div>
        </div>
        <div className={classes.content}>
          <div className={classes.formGroup}>
            <div className={classes.fieldGroupSpaced}>
              <Autocomplete
                fullWidth
                getOptionLabel={(option) => (option.title ? option.title : values.area)}
                id="area"
                // eslint-disable-next-line no-use-before-define
                name="area"
                onChange={event =>
                  handleFieldChange(event, 'area', event.target.innerText)
                }
                options={arrAreas}
                renderInput={(params) => (
                  // eslint-disable-next-line react/jsx-props-no-spreading
                  <TextField
                    {...params}
                    error={create && (values.area === '' || values.area === undefined)}
                    fullWidth
                    helperText={create && (values.area === '' || values.area === undefined) ? '*Campo Requerido' : false}
                    label="Area"
                    required
                    variant="outlined"
                  />
                )}
                value={defaultSel.area}
              />
            </div>
            <div className={classes.author}>
              <Typography variant="h6">Disparar actividades:</Typography>
            </div>
            <FormControl fullWidth>
              {area && area.tasks.map((data, index) => {
                // const type = data.name;
                // const optionsDispatch = getDispatchers(type)

                return (
                  <div>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={valuesCheck2 && valuesCheck2[data.type]}
                          color="primary"
                          id={index} 
                          name={data.type}
                          onChange={handleSelectionTask}
                        />}
                      fullWidth
                      label={data.type?.split('#')[0]}
                    />
                    {valuesCheck2[data.type] === true ? (
                      <Accordion fullWidth>
                        <AccordionSummary
                          expandIcon={<ExpandMoreIcon />}
                          fullWidth
                        >
                          <Typography
                            className={classes.heading}
                            fullWidth
                          >
                                Requerimientos:
                          </Typography>
                        </AccordionSummary>
                        <AccordionDetails fullWidth>
                          <Grid
                            container
                            fullWidth
                            id={`cont_${data.type}`}
                            spacing={4}
                          >
                            <Grid
                              item
                              md={6}
                              xs={12}
                            >
                              <TextField
                                fullWidth
                                label="Tipo de tarea"
                                name="task"
                                select
                                SelectProps={{ native: true }}
                                variant="outlined"
                              >
                                <option
                                  value=""
                                />
                                {area.activities.map(option => (
                                  <option
                                    key={option}
                                    value={option}
                                  >
                                    {option}
                                  </option>
                                ))}
                              </TextField>
                            </Grid>
                            <Grid
                              item
                              md={6}
                              xs={12}
                            >
                              <TextField
                                fullWidth
                                label={role[`${index}`]}
                                name="dispatcher"
                                select
                                SelectProps={{ native: true }}
                                variant="outlined"
                              >
                                <option
                                  value=""
                                />
                                {users[role[`${index}`]]?.map(option => (
                                  <option
                                    key={`${option.firstName} ${option.lastName}`}
                                    value={`${option.firstName} ${option.lastName}`}
                                  >
                                    {`${option.firstName} ${option.lastName}`}
                                  </option>
                                ))}
                              </TextField>
                            </Grid>
                          </Grid>
                        </AccordionDetails>
                      </Accordion>
                    ): ''} 
                  </div>
                )
              })}

            </FormControl>
          </div>
        </div>
        <div className={classes.content}>
          <div className={classes.author}>
            <Typography variant="h6">{'Creado por: '}</Typography>
          </div>
          <div className={classes.author}>
            <Avatar
              alt="Author"
              className={classes.avatar}
              src={''}
            >
              {getInitials(values.author?.split('#')[0])}
            </Avatar>
            <div>
              <Typography variant="h3">{values.author?.split('#')[0]}</Typography>
              <Typography variant="subtitle2">{values.author?.split('#')[1]}</Typography>
            </div>
          </div>
        </div>
        <div className={classes.actions}>
          <Button
            className={classes.applyButton}
            onClick={onApply}
            variant="contained"
          >
            Editar Actividad
          </Button>
        </div>
      </div>
    </Dialog>
  );
};

const optionsStatus = [
  { title: '1- Generada', value: '1- Generada' },
  { title: '2- En ejecución', value: '2- En ejecución' },
  { title: '3- Terminada', value: '3- Terminada' },
  { title: 'Alistamiento', value: 'Alistamiento' },
  { title: 'Caja OB', value: 'Caja OB' },
  { title: 'Detalle Solución', value: 'Detalle Solución' },
  { title: 'Diseño', value: 'Diseño' },
  { title: 'Ejecución ruta', value: 'Ejecución ruta' },
  { title: 'Entrega', value: 'Entrega' },
  { title: 'Factibilidad', value: 'Factibilidad' },
  { title: 'Instalación', value: 'Instalación' },
  { title: 'Liberación Recursos', value: 'Liberación Recursos' },
  { title: 'Pendiente Cliente', value: 'Pendiente Cliente' },
  { title: 'Pendiente Telmex', value: 'Pendiente Telmex' },
];

const optionsPriority = [
  {title: '0', value: 0},
  {title: '1', value: 1},
  {title: '2', value: 2},
  {title: '3', value: 3},
  {title: '4', value: 4},
  {title: '5', value: 5},
  {title: '6', value: 6},
  {title: '7', value: 7},
  {title: '8', value: 8},
  {title: '9', value: 9},  
]

Application.propTypes = {
  activity: PropTypes.object.isRequired,
  activityInfo: PropTypes.func,
  areas: PropTypes.array,
  className: PropTypes.string,
  create: PropTypes.bool,
  handleChange: PropTypes.func,
  onApply: PropTypes.func,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired,
  valuesCheck: PropTypes.object,
};

export default Application;
