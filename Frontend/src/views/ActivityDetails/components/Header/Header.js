import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { Typography, Grid, Button, colors } from '@material-ui/core';
import moment from 'moment';
import { Label } from 'components';
import { Application } from './components';
import { useSnackbar } from 'notistack';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';
import Cookies from 'js-cookie';

const useStyles = makeStyles(theme => ({
  root: {},
  label: {
    marginTop: theme.spacing(1)
  },
  shareButton: {
    marginRight: theme.spacing(2)
  },
  shareIcon: {
    marginRight: theme.spacing(1)
  },
  applyButton: {
    color: theme.palette.white,
    backgroundColor: colors.green[600],
    '&:hover': {
      backgroundColor: colors.green[900]
    }
  }
}));

const Header = props => {
  const { activity, className, ...rest } = props;
  const classes = useStyles();
  const firstName = Cookies.get('firstName');
  const lastName = Cookies.get('lastName');
  // const phoneNumber = Cookies.get('phone');
  const identification = Cookies.get('identification');

  const [activityInfo, setActivityInfo] = useState({});
  const [crear, setCrear] = useState(false);
  const [openApplication, setOpenApplication] = useState(false);
  // eslint-disable-next-line no-unused-vars
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const handleChangeActivity = (values) => {
    setActivityInfo(values);
  }

  const handleApplicationOpen = () => {
    setOpenApplication(true);
  };

  const handleApplicationClose = () => {
    setOpenApplication(false);
  };

  const validacionErrores = (camposReq, info) => {
    let errorVacios = 0;
    for (let index = 0; index < camposReq.length; index++) {
      const aux = Object.fromEntries(Object.entries(info).filter(([key]) => key === camposReq[index]));
      if (Object.values(aux)[0] === '' || Object.values(aux)[0] === undefined) {
        errorVacios = 1;
        index = camposReq.length
      }
    }
    return errorVacios;
  }

  const fetchQuery = async (data, url, message = '') => {
    let result = 0;
    await axios
      .put(
        url,
        data,
        getAuthorization(),
      )
      .then((response) => {
        result = response.data.data.length;
        if (message !== '') {
          enqueueSnackbar(message.msg, {
            variant: message.variant,
          });
        }
      })
      .catch((err) => {
        enqueueSnackbar(`error: ${err}`, {
          variant: 'error',
        });
        result = -1;
      })
    return result; 
  };

  React.useEffect(() => {

    axios.post('/areas/pagination', { 'eq': [{ 'field': 'group', 'values': ['BO']}] }, getAuthorization()).then(response => {    
      setAreas(response.data.data);
    });
  }, []);


  const [valuesCheck, setValuesCheck] = React.useState({});
  const [areas, setAreas] = React.useState([]);

  const handleChange = (e) => {
    setValuesCheck({
      ...valuesCheck,
      [e.target.name]: e.target.checked
    });
  };

  const submitTasks = async(tasks) => {


  }
  const handleEditButton = async() => {

    console.log('activityInfo', activityInfo);
    const array = []
    for (let key in valuesCheck) {
      if ( valuesCheck[key] === true ) {
        const data = areas.find((val) => val.name === key);
        if (data) {
          const divElement = document.getElementById(`cont_${key}`).querySelectorAll('input, select')
          const typeValue = divElement[0].value;
          const nameDispatch = divElement[1].value;
          const infoArea = await areas.filter((data) => data.name === key);
          console.log('infoArea', infoArea);
          const infoAreaTask = await infoArea[0].tasks.filter((data) => data.type === typeValue);
          let info = {
            projectId: activityInfo.projectId,
            status: typeValue,
            activityId: activityInfo.activityId,
            area: key,
            author: nameDispatch,
            createdAt: moment().format('YYYY-MM-DD HH:mm'),
            deadlineDate: moment().add(infoAreaTask[0].days,'days').format('YYYY-MM-DD HH:mm'),
            deadlineDays: infoAreaTask[0].days,
            group: 'BO',
            logs: [
              {
                status: typeValue,
                description: `Tarea asignada a ${infoAreaTask[0].role} ${nameDispatch} othId: ${activityInfo.activityId}`,
                createdAt: moment().format('YYYY-MM-DD HH:mm'),
                author: `${firstName} ${lastName}#${identification}`
              }
            ],
            orderType: '',
            priority: infoAreaTask[0].order,
            role: infoAreaTask[0].role,
            type: 'Primary',
            updatedAt: moment().format('YYYY-MM-DD HH:mm'),
            zolidStatus: 'Created'
          }
          // info.USUARIO_ASIGNADO = userOnyx;
          delete info.undefined

          array.push(info)
        }
      }
    }

    console.log('array', array);

    // await submitTasks(array);

    // setCrear(true);
    // const idObject = activityInfo._id;
    // const camposReqActivity = [
    //   'activityId',
    //   'status',
    //   'priority',
    //   'area',
    // ];
    // let errorVacios = validacionErrores(camposReqActivity, activityInfo);
    // if (errorVacios === 0) {
    //   let url = `/activities/${idObject}`;
    //   delete activityInfo._id;
    //   const finalActivity = {
    //     ...activityInfo, 
    //     field: '_id',
    //     resolucion15: activityInfo.resolucion15 ? activityInfo.resolucion15 : 'SIN RESOLUCION15',
    //     updatedAt: moment().format('YYYY-MM-DD HH:mm'),
    //   }
    //   await fetchQuery(
    //     finalActivity, 
    //     url, 
    //     {
    //       msg: `Actividad ${activityInfo.activityId} Actualizada con exito`,
    //       variant: 'success',
    //     });
    //   window.location.href = `/activities/${activityInfo.activityId}/overview`;
    // } else {
    //   enqueueSnackbar('Algunos campos estan vacios', {
    //     variant: 'error',
    //   });
    // }
  };

  return (
    <div
      {...rest}
      className={clsx(classes.root, className)}
    >
      <Grid
        alignItems="flex-end"
        container
        justify="space-between"
        spacing={3}
      >
        <Grid item>
          <Typography
            component="h2"
            gutterBottom
            variant="overline"
          >
            Explorar Actividades
          </Typography>
          <Typography
            component="h1"
            gutterBottom
            variant="h3"
          >
            {activity.title}
          </Typography>
          <Label
            className={classes.label}
            color={colors.green[600]}
            variant="outlined"
          >
            Activo
          </Label>
        </Grid>
        <Grid item>
          <Button
            className={classes.applyButton}
            onClick={handleApplicationOpen}
            variant="contained"
          >
            Editar
          </Button>
        </Grid>
      </Grid>
      {areas !== [] ? 
        <Application
          activity={activity}
          activityInfo={handleChangeActivity}
          areas={areas}
          create={crear}
          handleChange={handleChange}
          onApply={handleEditButton}
          onClose={handleApplicationClose}
          open={openApplication}
          valuesCheck={valuesCheck}
        />
        : ''
      }
    </div>
  );
};

Header.propTypes = {
  activity: PropTypes.object.isRequired,
  className: PropTypes.string,
};

Header.defaultProps = {};

export default Header;
