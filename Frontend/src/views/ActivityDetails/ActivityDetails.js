import React, { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Tabs, Tab, Divider, colors } from '@material-ui/core';
import { Page } from 'components';
import { Header, Overview, Files, Activities, Subscribers } from './components';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';

const useStyles = makeStyles(theme => ({
  root: {
    width: theme.breakpoints.values.lg,
    maxWidth: '100%',
    margin: '0 auto',
    padding: theme.spacing(3)
  },
  tabs: {
    marginTop: theme.spacing(3)
  },
  divider: {
    backgroundColor: colors.grey[300]
  },
  alert: {
    marginTop: theme.spacing(3)
  },
  content: {
    marginTop: theme.spacing(3)
  }
}));

const ActivityDetails = props => {
  const { match, history } = props;
  const classes = useStyles();
  const { id, tab } = match.params;
  const [activity, setActivity] = useState(null);

  const fetchActivitys = async (body) => {
    await axios
      .post(
        '/activities/pagination',
        body,
        getAuthorization(),
      )
      .then((response) => {
        setActivity(response.data.data[0])
      });
  };

  // let userOnyx = '';
  // if (Cookies.get('roles') !== undefined) {
  //   const findUser = JSON.parse(Cookies.get('roles')).find((x) => x.role === 'Ingeniero Gestion');
  //   if (findUser !== '') { userOnyx = findUser.userOnyx; }
  // }

  useEffect(() => {
    fetchActivitys({
      limit: 10,
      page: 1,
      'eq': [{ 'field': 'activityId', 'values': [id] }],
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleTabsChange = (event, value) => {
    history.push(value);
  };

  const tabs = [
    { value: 'overview', label: 'Detalle' },
    { value: 'subscribers', label: 'Actividades' },
    { value: 'files', label: 'Archivos' },
    { value: 'activity', label: 'Registros' }
  ];

  if (!tab) {
    return <Redirect to={`/activities/${id}/overview`} />;
  }

  if (!tabs.find(t => t.value === tab)) {
    return <Redirect to="/errors/error-404" />;
  }

  if (!activity) {
    return null;
  }

  return (
    <Page
      className={classes.root}
      title="Activity Details"
    >
      <Header activity={activity} />
      <Tabs
        className={classes.tabs}
        onChange={handleTabsChange}
        scrollButtons="auto"
        value={tab}
        variant="scrollable"
      >
        {tabs.map(tab => (
          <Tab
            key={tab.value}
            label={tab.label}
            value={tab.value}
          />
        ))}
      </Tabs>
      <Divider className={classes.divider} />
      {/* {openAlert && (
        <Alert
          className={classes.alert}
          message="The content holder has extended the deadline! Good luck"
          onClose={handleAlertClose}
        />
      )} */}
      <div className={classes.content}>
        {tab === 'overview' && <Overview activity={activity} />}
        {tab === 'files' && <Files activity={activity} />}
        {tab === 'activity' && <Activities activity={activity} />}
        {tab === 'subscribers' && (
          <Subscribers
            activity={activity}
            idActivity={id}
            subscribers={activity && activity.subscribers ? activity.subscribers : []}
          />
        )}
      </div>
    </Page>
  );
};

ActivityDetails.propTypes = {
  history: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired
};

export default ActivityDetails;
