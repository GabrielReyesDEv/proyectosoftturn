import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { useSelector, useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/styles';
import moment from 'moment';
import { useSnackbar } from 'notistack';
import AccountTreeIcon from '@material-ui/icons/AccountTree'; //Icono para project
import AssignmentIcon from '@material-ui/icons/Assignment'; // icono para activity
import ListAltIcon from '@material-ui/icons/ListAlt'; // icono para task
import SendIcon from '@material-ui/icons/Send'; // icono para envio del email
import { 
  Button,
  Dialog,
  List,
  ListItem,
  Avatar,
  ListItemAvatar,
  ListItemText,
  Divider,
  CircularProgress
} from '@material-ui/core';
import { ModalFormat, StartTask } from './components';
import { saveCheckKickOff } from 'actions';
import Cookies from 'js-cookie';
import getAuthorization from 'utils/getAuthorization';
import axios from 'utils/axios';

const useStyles = makeStyles((theme) => ({
  actions: {
    marginTop: theme.spacing(3)
  },
  root: {
    width: '100%',
    maxWidth: 400,
    backgroundColor: theme.palette.background.paper,
  },

  completeLoader: {
    backgroundColor: 'green',
  }

}));


const InputSave = props => {
  // eslint-disable-next-line react/prop-types
  const { dataTask, activityId, projectId, taskId, className, ...rest } = props;
  const classes = useStyles();
  const { formKickoff, otpInfo } = useSelector(state => state.formKickoff);
  const { service } = useSelector(state => state.serviceReducer);
  const dispatch = useDispatch();
  const email = Cookies.get('email');
  const firstName = Cookies.get('firstName');
  const lastName = Cookies.get('lastName');
  const phoneNumber = Cookies.get('phone');
  const identification = Cookies.get('identification');
  const { enqueueSnackbar } = useSnackbar();
  const [open, setOpen] = React.useState(false);
  const [openFormat, setOpenFormat] = React.useState(false);
  const [openActivity, setOpenActivity] = React.useState(false);

  const [loaderProject, setLoaderProject] = React.useState({
    loader: false,
    complete: false
  });
  const [loaderActivity, setLoaderActivity] = React.useState({
    loader: false,
    complete: false
  });
  const [loaderTask, setLoaderTask] = React.useState({
    loader: false,
    complete: false
  });
  const [loaderEmail, setLoaderEmail] = React.useState({
    loader: false,
    complete: false
  });



  const otpChange = async(inputs) => {
    let inputsChange = {}

    for (let i = 0; i < inputs.length; i++) {
      if (inputs[i].getElementsByTagName('select')[0] !== undefined) {
        const name = await inputs[i].getElementsByTagName('select')[0].name;
        const value = await inputs[i].getElementsByTagName('select')[0].value;
        inputsChange = {...inputsChange, [name]: value}
      } else {
        const name = await inputs[i].getElementsByTagName('input')[0].name;
        const value = await inputs[i].getElementsByTagName('input')[0].value
        inputsChange = {...inputsChange, [name]: value}
      }
    }
    return inputsChange;
  }

  const constructionTask = () => {

    const engineersContacts = {
      engineer1: {
        name: `${firstName} ${lastName}`,
        phoneNumber: phoneNumber,
        email: email,
      },
      engineer2: {
        name: 'Melani Gisella Viveros Moreno',
        phoneNumber: 3102129290,
        email: 'melani.viverosm.ext@claro.com.co',
      },
      engineer3: {
        name: 'Cristian Camilo Ramirez Ramirez',
        phoneNumber: 3102852816,
        email: 'cristian.ramirezr.ext@claro.com.co',
      },
    }

    const logTask = dataTask.logs;
    logTask.push({
      status:'Terminated',
      description: `Tarea Terminada por ${firstName} ${lastName}`,
      createdAt: moment().format('YYYY-MM-DD HH:mm'),
      author: `${firstName} ${lastName}#${identification}`,

    })

    const payloadTask = {
      message: `Tarea terminada por ${firstName} ${lastName}`,
      author: `${firstName} ${lastName}#${identification}`,
      updatedAt: moment().format('YYYY-MM-DD HH:mm'),
      zolidStatus: 'Terminated',
      finishDate: moment().format('YYYY-MM-DD HH:mm'),
      data: {
        form: formKickoff,
        serviceType: service,
        engineersContacts,
      },
      logs: logTask,
    }

    return  payloadTask;
  }

  const handleSubmit = async () => {

    const inputs = document.getElementsByClassName('selected_error');
    const inputsOtp = document.getElementsByClassName('otp_change');
    const errors = [];


    for (let i = 0; i < inputs.length; i++) {
      if (inputs[i].getElementsByTagName('select')[0] !== undefined) {
        const element = inputs[i].getElementsByTagName('select')[0].name;
        errors.push(element)
      } else {
        const element = inputs[i].getElementsByTagName('input')[0].name;
        errors.push(element)
      }
    }

    dispatch(saveCheckKickOff(errors)); 
     
    if (errors.length === 0) {

      handleClickOpen()
      const dataTask = constructionTask();

      setLoaderTask({...loaderTask, loader: true})
      axios.put(`/tasks/${taskId}`, { field: '_id', ...dataTask}, getAuthorization()).then(async (res1) => {  
        setLoaderTask({complete: true , loader: false})
        if (inputsOtp.length > 0) {
          const dataOtp = await otpChange(inputsOtp)
          setLoaderProject({...loaderProject, loader: true}) //Start loader project
          return axios.put(`/projects/${projectId}`, { field: 'projectId',  ...dataOtp }, getAuthorization())
        } 
        return true
      }).then(res2 => {
        (inputsOtp.length > 0) && setLoaderProject({complete: true ,loader: false}) //Finish loader project and complete project
        setLoaderActivity({...loaderActivity, loader: true}) //Start loader activity
        return axios.put(`/activities/${activityId}`, { field: 'activityId', status: 'Terminated', updatedAt:moment().format('YYYY-MM-DD HH:mm') }, getAuthorization())

      }).then(res3 => {
        setLoaderActivity({complete: true ,loader: false}) //Finish loader activity and complete activity
        setLoaderEmail({...loaderEmail, loader: true}) //Start loader email
        return axios.post('/email', { 'eq': [{ 'field': '_id', 'values': `${taskId}`}], email }, getAuthorization());
         
      }).then(async (res3) => {
        setLoaderEmail({complete: true ,loader: false}) //Finish loader email and complete email
        await enqueueSnackbar('Guardado Completado', {
          variant: 'success',
        });

        handleOpenDrawer()

      }).catch(error => {
        enqueueSnackbar('Error de conexion', {
          variant: 'Error',
        });
        console.log('este es el error', error);
      });
    } else {
      enqueueSnackbar('Faltan campos por llenar', {
        variant: 'Error',
      });
    }

  }

  React.useEffect(() => {
    window.onbeforeunload = () => {
      return '¿Desea recargar la página web?';
    };
  }, []);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpenDrawer = () => {
    handleClose()
    setOpenFormat(true)
  }

  const handleCloseDrawer = () => {
    setOpenFormat(false)
  }

  const handleOpenActivity = () => {
    setOpenActivity(true)
  }

  const handleCloseActivity = () => {
    setOpenActivity(false)
  }

  return (
    <div
      {...rest}
      className={clsx(classes.root, className)}
    >
      <div className={classes.actions}>
        <Button
          color="primary"
          onClick={handleSubmit}
          variant="contained"
        >
              Cerrar KickOff
        </Button>
        <Dialog
          fullWidth="xs"
          maxWidth="xs"
          onClose={handleClose}
          open={open}
        //   TransitionComponent={Transition}
        >
          <List className={classes.root}>
            <ListItem>
              <ListItemAvatar>
                <Avatar className={loaderTask.complete === true ?  classes.completeLoader : ''}>
                  {loaderTask.loader === true ? 
                    <CircularProgress /> 
                    : 
                    <ListAltIcon />
                  }
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary="TAREA"
                secondary={loaderProject.complete === false ? 'Guaradando tarea...' : 'Tarea guardada'}
              />
            </ListItem>
            <Divider
              component="li"
              variant="inset"
            />
            <ListItem>
              <ListItemAvatar>
                <Avatar className={loaderProject.complete === true ? classes.completeLoader : ''}>
                  {loaderProject.loader === true ? 
                    <CircularProgress /> 
                    : 
                    <AccountTreeIcon />
                  }
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary="OTP"
                secondary={loaderProject.complete === false ? 'Actualizando otp...' : 'Otp actualizada'}
              />
            </ListItem>
            <Divider
              component="li"
              variant="inset"
            />
            <ListItem>
              <ListItemAvatar>
                <Avatar className={loaderActivity.complete === true ?  classes.completeLoader : ''}>
                  {loaderActivity.loader === true ? 
                    <CircularProgress /> 
                    : 
                    <AssignmentIcon />
                  }
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary="OTH"
                secondary={loaderProject.complete === false ? 'Actualizando oth...' : 'Oth actualizada'}
              />
            </ListItem>
            <Divider
              component="li"
              variant="inset"
            />
            <ListItem>
              <ListItemAvatar>
                <Avatar className={loaderEmail.complete === true ?  classes.completeLoader : ''}>
                  {loaderEmail.loader === true ? 
                    <CircularProgress /> 
                    : 
                    <SendIcon />
                  }
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary="Evidencia"
                secondary={loaderProject.complete === false ? 'Enviando Correo...' : 'Correo enviado'}

              />
            </ListItem>
          </List>
        </Dialog>
        {service ? (
          <ModalFormat
            formKickoff={formKickoff}
            handleCloseDrawer={() => handleCloseDrawer()}
            handleOpenActivity={() => handleOpenActivity()}
            openFormat={openFormat}
            service={service}
          />
        ): ''}

        {openActivity === true ? (
          <StartTask
            formKickoff={formKickoff}
            handleCloseActivity={() => handleCloseActivity()}
            openActivity={openActivity}
            otpInfo={otpInfo}
            projectId={projectId}
            service={service}
          />
        ): ''}


      </div>
    </div>
  );
};

InputSave.propTypes = {
  className: PropTypes.string
};

export default InputSave;