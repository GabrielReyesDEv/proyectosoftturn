import React from 'react';

const Format1 = (props) => {

  const { formKickoff } = props;

  return (
    <div>
      **************************************************   DATOS BASICOS   **************************************************
      <br />
      CIUDAD: ............................................
      {formKickoff?.basicData?.city}
      <br />
      DIRECCIÓN: .........................................
      {formKickoff?.basicData?.sourceAddress}
      <br />
      TIPO PREDIO: .......................................
      {formKickoff?.basicData?.predio_type}      
      <br />
      NIT del cliente: ...................................
      {formKickoff?.basicData?.nit}      
      <br />
      ALIAS DEL LUGAR: ...................................
      {formKickoff?.basicData?.alias}      
      <br />
      OTP: ...............................................
      {formKickoff?.basicData?.projectId}      
      <br />
      OTP ASOCIADAS: .....................................
      {formKickoff?.basicData?.otp_asociada}      
      <br />
      TIPO DE INTERNET: ..................................
      {formKickoff?.basicData?.internet_type}      
      <br />
      ANCHO DE BANDA : ...................................
      {formKickoff?.basicData?.ancho_banda}      
      <br />
      TIPO DE INSTALACIÓN: ...............................
      {formKickoff?.basicData?.instalation_type}
      <br />
      ID SERVICIO ACTUAL (Aplica para UM Existente):......
      {formKickoff?.basicData?.id_current_service}      
      <br />
      <br />
      **********************************************   INFORMACIÓN  ULTIMA MILLA    ******************************************
      <br />
      <br />
      ¿ESTA OT REQUIERE INSTALACION DE UM? :..............
      {formKickoff?.uMInfo?.ot_requiere_instalacion_um}
      <br />
      ¿ESTA ULTIMA MILLA ES UN BACKUP? :..............
      {formKickoff?.uMInfo?.last_milla_backup}
      <br />
      PROVEEDOR :.........................................
      {formKickoff?.uMInfo?.provider}      
      <br />
      MEDIO :.............................................
      {formKickoff?.uMInfo?.medium}      
      <br />
      {/* eslint-disable-next-line react/no-unescaped-entities */}
      RESPUESTA FACTIBILIDAD BW > =100 MEGAS :............
      {formKickoff?.uMInfo?.feasibility_response}      
      <br />
      TIPO DE CONECTOR :..................................
      {formKickoff?.uMInfo?.type_conector}      
      <br />
      ACCESO SDS DESTINO UNIFILAR.............................................
      {formKickoff?.uMInfo?.destination_sds_access}      
      <br />
      ACCESO OLT (GPON) ................................................
      {formKickoff?.uMInfo?.access_olt}      
      <br />
      INTERFACE DE ENTREGA AL CLIENTE :...................
      {formKickoff?.uMInfo?.client_delivery_interface}      
      <br />
      REQUIERE VOC :......................................
      {formKickoff?.uMInfo?.requires_visit_civil_work}      
      <br />
      PROGRAMACIÓN DE VOC :...............................
      {formKickoff?.uMInfo?.schedule_visit_civil_work}      
      <br />
      <br />
      *****************************************  REQUERIMIENTOS PARA ENTREGA DEL SERVICIO  ************************************
      <br />
      REQUIERE RFC........................................
      {formKickoff?.deliveryServiceRequirement?.requires_rfc}      
      <br />
      Conversor Medio :...................................
      {formKickoff?.deliveryServiceRequirement?.media_converter}      
      <br />
      Referencia Router :.................................
      {formKickoff?.deliveryServiceRequirement?.router_reference}      
      <br />
      Modulos o Tarjetas :................................
      {formKickoff?.deliveryServiceRequirement?.modules_or_cards}      
      <br />
      Licencias :.........................................
      {formKickoff?.deliveryServiceRequirement?.licensing}      
      <br />
      Equipos Adicionales :...............................
      {formKickoff?.deliveryServiceRequirement?.additional_equipment}      
      <br />
      Consumibles :.......................................
      {formKickoff?.deliveryServiceRequirement?.consumables}      
      <br />
      REGISTRO DE IMPORTACIÓN Y CARTA VALORIZADA :........
      {formKickoff?.deliveryServiceRequirement?.import_registration_and_valued_letter}      
      <br />
      <br />
      *****************************************   DATOS DEL CONTACTO PARA COMUNICACIÓN   **************************************
      <br />
      ****************************  APRUEBA COSTOS DE OC E INICIO DE FACTURACIÓN DE ORDEN DE TRABAJO  *************************
      <br />
      NOMBRE :............................................
      {formKickoff?.contactInfoWorkOrder?.full_name_CW}      
      <br />
      TELEFONO :..........................................
      {formKickoff?.contactInfoWorkOrder?.phone_CW}      
      <br />
      CELULAR :...........................................
      {formKickoff?.contactInfoWorkOrder?.mobile_number_CW}      
      <br />
      CORREO ELECTRONICO :................................
      {formKickoff?.contactInfoWorkOrder?.email_CW}      
      <br />
      <br />
      ************************************************  DATOS CONTACTO TÉCNICO   **********************************************
      <br />
      NOMBRE :............................................
      {formKickoff?.technicalContactDetails?.full_name_TC}      
      <br />
      TELEFONO :..........................................
      {formKickoff?.technicalContactDetails?.phone_TC}      
      <br />
      CELULAR :...........................................
      {formKickoff?.technicalContactDetails?.mobile_number_TC}      
      <br />
      CORREO ELECTRONICO :................................
      {formKickoff?.technicalContactDetails?.email_TC}      
      <br />
      OBSERVACIONES :.....................................
      {formKickoff?.technicalContactDetails?.observations}
      <br />
      <br />
      ***************************************************  KIKOFF TECNICO     *************************************************
      <br />
      Ancho de banda Exclusivo NAP :......................
      {formKickoff?.technicalKickoff?.NAP_bandwidth}      
      <br />
      Ancho de banda de Internet :........................
      {formKickoff?.technicalKickoff?.internet_bandwidth}      
      <br />
      Direcciones IP :....................................
      {formKickoff?.technicalKickoff?.ip_address}      
      <br />
      Activación correo :.................................
      {formKickoff?.technicalKickoff?.email_activation}      
      <br />
      Activación WEB Hosting :............................
      {formKickoff?.technicalKickoff?.web_hosting_activation}
      <br />
      Dominio existente :.................................
      {formKickoff?.technicalKickoff?.existing_domain}
      <br />
      Dominio a comprar :.................................
      {formKickoff?.technicalKickoff?.domain_to_buy}
      <br />
      Cantidad cuentas de correo :........................
      {formKickoff?.technicalKickoff?.emails_quantity}      
      <br />
      Espacio de correo (GB) :............................
      {formKickoff?.technicalKickoff?.email_storage}      
      <br />
      Plataforma de WEB Hosting :..........................
      {formKickoff?.technicalKickoff?.web_hosting_platform}      
      <br />
      Almacenamiento Web Hosting (MB) :..................................
      {formKickoff?.technicalKickoff?.web_hosting_storage}      
      <br />
      APLICA A ALGUNA PROMOCION VIGENTE :.................
      {formKickoff?.technicalKickoff?.current_promotion}      
      <br />
    </div>
  );

};


export default Format1;
