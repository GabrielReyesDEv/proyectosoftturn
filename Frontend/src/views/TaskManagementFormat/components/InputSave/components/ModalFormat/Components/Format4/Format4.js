import React from 'react';

const Format4 = (props) => {

  const { formKickoff } = props;
    
  return (
    <div>
      **************************************************   DATOS BASICOS   **************************************************
      <br />
      CIUDAD: ............................................
      {formKickoff?.basicData?.city}      
      <br />
      DIRECCIÓN UBICACIÓN ACTUAL DEL SERVICIO:............
      {formKickoff?.basicData?.sourceAddress}      
      <br />
      DIRECCIÓN DONDE SE TRASLADARA EL SERVICIO:..........
      {formKickoff?.basicData?.destinationAddress}      
      <br />
      TIPO PREDIO :.......................................
      {formKickoff?.basicData?.predio_type}      
      <br />
      NIT del cliente : ..................................
      {formKickoff?.basicData?.nit}      
      <br />
      ALIAS DEL LUGAR:.........
      {formKickoff?.basicData?.alias}      
      <br />
      OTP :...............................................
      {formKickoff?.basicData?.projectId}      
      <br />
      OTP ASOCIADAS :.....................................
      {formKickoff?.basicData?.otp_asociada}      
      <br />
      CANTIDAD DE SERVICIOS A TRASLADAR :.................
      {formKickoff?.basicData?.amount_service_transfer}      
      <br />
      CODIGOS DE SERVICIO  A TRASLADAR :..................
      {formKickoff?.basicData?.transfer_service_code}      
      <br />
      TIPO DE TRASLADO EXTERNO :..........................
      {formKickoff?.basicData?.external_transfer_type}      
      <br />
      TIPO DE SERVICIO:...................................
      {formKickoff?.basicData?.service_type}      
      <br />
      ANCHO DE BANDA : ...................................
      {formKickoff?.basicData?.ancho_banda}      
      <br />
      TIPO DE ACTIVIDAD:..................................
      {formKickoff?.basicData?.activity_type}      
      <br />
      ID SERVICIO ACTUAL (Aplica para UM Existente):......
      {formKickoff?.basicData?.id_current_service}      
      <br />
      <br />
      REQUIERE LIBERACIÓN DE RECURSOS
      <br />
      DE ULTIMA MILLA SEDE ANTIGUA(PROVEEDOR TERCERO):....
      {formKickoff?.basicData?.requires_release_reources}      
      <br />
      <br />
      **********************************************   INFORMACIÓN  ULTIMA MILLA    ******************************************
      <br />
      <br />
      ¿ESTA OT REQUIERE INSTALACION DE  UM? :..............
      {formKickoff?.uMInfo?.ot_requiere_instalacion_um}      
      <br />
      PROVEEDOR :.........................................
      {formKickoff?.uMInfo?.provider}      
      <br />
      MEDIO :.............................................
      {formKickoff?.uMInfo?.medium}      
      <br />
      RESPUESTA FACTIBILIDAD BW
      {' > '}
      =100 MEGAS :............
      {formKickoff?.uMInfo?.feasibility_response}      
      <br />
      <br />
      ACCESO (Solo Aplica para Canales
      {' > '}
      100 MEGAS   =======
      <br />
      SDS DESTINO (Unifilar) :............................
      {formKickoff?.uMInfo?.destination_sds_access}      
      <br />
      OLT (GPON) :........................................
      {formKickoff?.uMInfo?.access_olt}      
      <br />
      INTERFACE DE ENTREGA AL CLIENTE : ..................
      {formKickoff?.uMInfo?.client_delivery_interface}      
      <br />
      REQUIERE VOC :......................................
      {formKickoff?.uMInfo?.requires_visit_civil_work}      
      <br />
      PROGRAMACIÓN DE VOC :...............................
      {formKickoff?.uMInfo?.schedule_visit_civil_work}      
      <br />
      <br />
      REQUIERE LIBERACIÓN DE RECURSOS
      <br />
      DE ULTIMA MILLA (FO) EN SEDE ANTIGUA :..............
      {formKickoff?.uMInfo?.requires_release_reources}      
      <br />
      <br />
      *****************************************  REQUERIMIENTOS PARA ENTREGA DEL SERVICIO  ************************************
      <br />
      REQUIERE VENTANA DE MTTO :..........................
      {formKickoff?.deliveryServiceRequirement?.requires_window_mito}      
      <br />
      REQUIERE RFC :......................................
      {formKickoff?.deliveryServiceRequirement?.requires_rfc}      
      <br />
      Conversor Medio :...................................
      {formKickoff?.deliveryServiceRequirement?.media_converter}      
      <br />
      Referencia Router :.................................
      {formKickoff?.deliveryServiceRequirement?.router_reference}      
      <br />
      Modulos o Tarjetas :................................
      {formKickoff?.deliveryServiceRequirement?.modules_or_cards}      
      <br />
      Licencias :.........................................
      {formKickoff?.deliveryServiceRequirement?.licensing}      
      <br />
      Equipos Adicionales :...............................
      {formKickoff?.deliveryServiceRequirement?.additional_equipment}      
      <br />
      Consumibles :.......................................
      {formKickoff?.deliveryServiceRequirement?.consumables}      
      <br />
      REGISTRO DE IMPORTACIÓN Y CARTA VALORIZADA :........
      {formKickoff?.deliveryServiceRequirement?.import_registration_and_valued_letter}      <br />
      <br />
      *****************************************   DATOS DEL CONTACTO PARA COMUNICACIÓN   **************************************
      <br />
      ****************************  APRUEBA COSTOS DE OC E INICIO DE FACTURACIÓN DE ORDEN DE TRABAJO  *************************
      <br />
      NOMBRE :............................................
      {formKickoff?.contactInfoWorkOrder?.full_name_CW}      
      <br />
      TELEFONO :..........................................
      {formKickoff?.contactInfoWorkOrder?.phone_CW}      
      <br />
      CELULAR :...........................................
      {formKickoff?.contactInfoWorkOrder?.mobile_number_CW}      
      <br />
      CORREO ELECTRONICO :................................
      {formKickoff?.contactInfoWorkOrder?.email_CW}      
      <br />
      <br />
      ************************************************  DATOS CONTACTO TÉCNICO   **********************************************
      <br />
      NOMBRE :............................................
      {formKickoff?.technicalContactDetails?.full_name_TC}      
      <br />
      TELEFONO :..........................................
      {formKickoff?.technicalContactDetails?.phone_TC}      
      <br />
      CELULAR :...........................................
      {formKickoff?.technicalContactDetails?.mobile_number_TC}      
      <br />
      CORREO ELECTRONICO :................................
      {formKickoff?.technicalContactDetails?.email_TC}      
      <br />
      OBSERVACIONES :.....................................
      {formKickoff?.technicalContactDetails?.observations}      
      <br />
      <br />
    </div>
  );

};

export default Format4;
