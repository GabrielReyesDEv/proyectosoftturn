import React from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button
} from '@material-ui/core';
import { 
  Format1,
  Format2,
  Format3,
  Format4,
  Format5,
  Format6,
  Format7,
  Format8,
  Format9,
  Format10
} from './Components';
import { Redirect } from 'react-router-dom';

const ModalFormat = (props) => {

  const { openFormat, service, handleCloseDrawer, formKickoff, handleOpenActivity } = props;

  const formatType = () => {
    switch (service) {
      case 'Internet Dedicado Empresarial':
      case 'Internet Dedicado':
      case 'PL ETHERNET':
        return(<Format1 formKickoff={formKickoff}/>)

      case 'Instalación Servicio Telefonia Fija PBX Distribuida Linea E1':
      case 'Instalación Servicio Telefonia Fija PBX Distribuida Linea SIP':
      case 'Instalación Servicio Telefonia Fija PBX Distribuida Linea SIP con Gateway de Voz':
      case 'Instalación Telefonía Publica Básica - Internet Dedicado':
        return(<Format7 formKickoff={formKickoff}/>)

      case 'PRIVATE LINE':
        return(<Format9 formKickoff={formKickoff}/>)

      case 'MPLS Avanzado Intranet':
      case 'MPLS Avanzado Intranet - Varios Puntos':
      case 'MPLS Avanzado Intranet con Backup de Ultima Milla - NDS 2':
      case 'MPLS Avanzado Intranet con Backup de Ultima Milla y Router - NDS1':
      case 'MPLS Avanzado Extranet':
      case 'Backend MPLS':
      case 'MPLS Avanzado con Componente Datacenter Claro':
      case 'MPLS Transaccional 3G':
        return(<Format2 formKickoff={formKickoff}/>)

      case 'Punta DATACENTER':
        return(<Format8 formKickoff={formKickoff}/>) //pending

      case 'LAN ADMINISTRADA':
        return(<Format10 formKickoff={formKickoff}/>)

      case 'SOLUCIONES ADMINISTRATIVAS - COMUNICACIONES UNIFICADAS PBX ADMINISTRADA':
        return(<Format6 formKickoff={formKickoff}/>)

      case 'Traslado Externo Servicio':
        return(<Format4 formKickoff={formKickoff}/>)

      case 'Traslado Interno Servicio':
        return(<Format5 formKickoff={formKickoff}/>)

      case 'Cambio de Equipos Servicio':
      case 'Cambio de Servicio Telefonia Fija Pública Linea Basica a Linea E1':
      case 'Cambio de Servicio Telefonia Fija Pública Linea SIP a PBX Distribuida Linea SIP':
      case 'Cambio de Última Milla':
      case 'Cambio de Equipo':
        return(<Format3 formKickoff={formKickoff}/>)

      default:
        return '';
    }
  };

  const exitModal = () => {
    handleCloseDrawer();
    // return (<Redirect to="/management/tasks" />);
  }

  const startActivities = () => {
    handleCloseDrawer();
    handleOpenActivity();
  }
  


  return (
    <div>
      <Dialog
        maxWidth="md"
        open={openFormat}
      >
        <DialogTitle>Formato TXT KO</DialogTitle>
        <DialogContent>
          <DialogContentText>
            {formatType()}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            autoFocus
            color="primary"
            fullWidth="true"
            onClick={() => exitModal()}
            variant="contained"
          >
            Salir
          </Button>

          <Button
            autoFocus
            color="primary"
            fullWidth="true"
            onClick={() => startActivities()}
            variant="contained"
          >
            Disparar actividades
          </Button>
        </DialogActions>
      </Dialog>

    </div>
  );
};


export default ModalFormat;
