import React from 'react';

const Format8 = (props) => {

  const { formKickoff } = props;
    
  return (
    <div>
      **************************************************   DATOS BASICOS   **************************************************
      <br />
      CIUDAD: ............................................
      {formKickoff?.basicData?.city}      
      <br />
      DIRECCIÓN UBICACIÓN ACTUAL DEL SERVICIO:............
      {formKickoff?.basicData?.sourceAddress}      
      <br />
      TIPO PREDIO :.......................................
      {formKickoff?.basicData?.predio_type}      
      <br />
      NIT del cliente : ..................................
      {formKickoff?.basicData?.nit}      
      <br />
      ALIAS DEL LUGAR:.........
      {formKickoff?.basicData?.alias}      
      <br />
      OTP :...............................................
      {formKickoff?.basicData?.projectId}      
      <br />
      OTP ASOCIADAS :.....................................
      {formKickoff?.basicData?.otp_asociada}      
      <br />
      TIPO DE SERVICIO:...................................
      {formKickoff?.basicData?.service_type}      
      <br />
      ANCHO DE BANDA : ...................................
      {formKickoff?.basicData?.ancho_banda}      
      <br />
      TIPO DE INSTALACION:..................................
      {formKickoff?.basicData?.instalation_type}      
      <br />
      ID SERVICIO ACTUAL (Aplica para UM Existente):......
      {formKickoff?.basicData?.id_current_service}      
      <br />
      <br />
      **********************************************   INFORMACIÓN  ULTIMA MILLA    ******************************************
      <br />
      <br />
      ¿ESTA OT REQUIERE INSTALACION DE  UM? :..............
      {formKickoff?.uMInfo?.ot_requiere_instalacion_um}     
      <br />
      PROVEEDOR :.........................................
      {formKickoff?.uMInfo?.provider}     
      <br />
      MEDIO :.............................................
      {formKickoff?.uMInfo?.medium}     
      <br />
      RESPUESTA FACTIBILIDAD BW
      {' > '}
      100 MEGAS :............
      {formKickoff?.uMInfo?.feasibility_response}     
      <br />
      <br />
      ACCESO (Solo Aplica para Canales
      {' > '}
      100 MEGAS   =======
      <br />
      <br />
      TIPO DE CONECTOR
      <br />
      APLICA PARA (FO) CLARO :..............
      {formKickoff?.uMInfo?.requires_release_reources}      
      <br />
      SDS DESTINO (Unifilar) :............................
      {formKickoff?.uMInfo?.destination_sds_access}      
      <br />
      OLT (GPON) :........................................
      {formKickoff?.uMInfo?.access_olt}      
      <br />
      INTERFACE DE ENTREGA AL CLIENTE : ..................
      {formKickoff?.uMInfo?.client_delivery_interface}      
      <br />
      <br />
      *****************************************  REQUERIMIENTOS PARA ENTREGA DEL SERVICIO  ************************************
      <br />
      REQUIERE RFC :......................................
      {formKickoff?.deliveryServiceRequirement?.requires_rfc}      
      <br />
      Conversor Medio :...................................
      {formKickoff?.deliveryServiceRequirement?.media_converter}      
      <br />
      Referencia Router :.................................
      {formKickoff?.deliveryServiceRequirement?.router_reference}      
      <br />
      Modulos o Tarjetas :................................
      {formKickoff?.deliveryServiceRequirement?.modules_or_cards}      
      <br />
      Licencias :.........................................
      {formKickoff?.deliveryServiceRequirement?.licensing}      
      <br />
      Equipos Adicionales :...............................
      {formKickoff?.deliveryServiceRequirement?.additional_equipment}      
      <br />
      Consumibles :.......................................
      {formKickoff?.deliveryServiceRequirement?.consumables}      
      <br />
      REGISTRO DE IMPORTACIÓN Y CARTA VALORIZADA :........
      {formKickoff?.deliveryServiceRequirement?.import_registration_and_valued_letter}      <br />
      <br />
      *****************************************   DATOS DEL CONTACTO PARA COMUNICACIÓN   **************************************
      <br />
      ************************************************  DATOS CONTACTO TÉCNICO   **********************************************
      <br />
      NOMBRE :............................................
      {formKickoff?.technicalContactDetails?.full_name_TC}      
      <br />
      TELEFONO :..........................................
      {formKickoff?.technicalContactDetails?.phone_TC}      
      <br />
      CELULAR :...........................................
      {formKickoff?.technicalContactDetails?.mobile_number_TC}      
      <br />
      CORREO ELECTRONICO :................................
      {formKickoff?.technicalContactDetails?.email_TC}      
      <br />
      OBSERVACIONES :.....................................
      {formKickoff?.technicalContactDetails?.observations}      
      <br />
      <br />
      ******************************************  PUNTO A - EQUIPO DE ACCESO: ALCATEL TRIARA   ****************************************
      <br />
      EQUIPO DE ACCESO :............................................
      {formKickoff?.dataCenterConnectionInformation?.access_computer}      
      <br />
      PUERTO EQUIPO DE ACCESO :..........................................
      {formKickoff?.dataCenterConnectionInformation?.access_computer_port}      
      <br />
      TIPO DE INTERFACE :...........................................
      {formKickoff?.dataCenterConnectionInformation?.type_interfaces} 
      <br />
      <br />
      ******************************************  PUNTO B EQUIPO CLARO - RACK: APPLIANCE   ****************************************
      <br />
      EQUIPO CLARO :............................................
      {formKickoff?.dataCenterConnectionInformation?.claro_computer}      
      <br />
      CONECTOR WAN :..........................................
      {formKickoff?.dataCenterConnectionInformation?.connector_wan}      
      <br />
      PUERTO LAN :...........................................
      {formKickoff?.dataCenterConnectionInformation?.port_lan}
      <br />
      CONECTOR LAN :..........................................
      {formKickoff?.dataCenterConnectionInformation?.connector_lan}      
      <br />
      TOMAS REGULADAS REQUERIDAS :..........................................
      {formKickoff?.dataCenterConnectionInformation?.required_regulated_outlets_B}          
      <br />
      <br />
      ******************************************  PUNTO C EQUIPO CLIENTE - RACK Y UNIDAD DE CONEXION   ****************************************
      <br />
      EQUIPO DE CONEXION :............................................
      {formKickoff?.dataCenterConnectionInformation?.connection_computer}      
      <br />
      PUERTO CLIENTE :..........................................
      {formKickoff?.dataCenterConnectionInformation?.client_port}      
      <br />
      TIPO DE INTERFACE :...........................................
      {formKickoff?.dataCenterConnectionInformation?.type_interface}
      <br />
      TOMAS REGULADAS REQUERIDAS :..........................................
      {formKickoff?.dataCenterConnectionInformation?.required_regulated_outlets_C}
      <br />
      TOTAL UNIDADES DE RACK :..........................................
      {formKickoff?.dataCenterConnectionInformation?.total_unidades_rack}     
      <br />   
      <br />
    </div>
  );

};

export default Format8;
