import React from 'react';

const Format6 = (props) => {

  const { formKickoff } = props;

  return (
    <div>
      **************************************************   DATOS BASICOS   **************************************************
      <br />
      CIUDAD: ............................................
      {formKickoff?.basicData?.city}     
      <br />
      DIRECCIÓN: .........................................
      {formKickoff?.basicData?.sourceAddress}     
      <br />
      TIPO PREDIO: .......................................
      {formKickoff?.basicData?.predio_type}     
      <br />
      NIT del cliente: ...................................
      {formKickoff?.basicData?.nit}     
      <br />
      ALIAS DEL LUGAR: ...................................
      {formKickoff?.basicData?.alias}     
      <br />
      OTP: ...............................................
      {formKickoff?.basicData?.projectId}     
      <br />
      OTP ASOCIADAS: .....................................
      {formKickoff?.basicData?.otp_asociada}     
      <br />
      TIPO DE PBX ADMINISTRADA: ..........................
      {formKickoff?.basicData?.managed_pbx_type}     
      <br />
      TIPO DE INSTALACIÓN: ...............................
      {formKickoff?.basicData?.instalation_type}     
      <br />
      ID SERVICIO ACTUAL (Aplica para UM Existente):......
      {formKickoff?.basicData?.id_current_service}     
      <br />
      <br />
      **********************************************   INFORMACIÓN  ULTIMA MILLA    ******************************************
      <br />
      <br />
      ¿ESTA OT REQUIERE INSTALACION DE  UM? :..............
      {formKickoff?.uMInfo?.ot_requiere_instalacion_um}      
      <br />
      PROVEEDOR :.........................................
      {formKickoff?.uMInfo?.provider}      
      <br />
      MEDIO :.............................................
      {formKickoff?.uMInfo?.medium}      
      <br />
      REQUIERE VOC :......................................
      {formKickoff?.uMInfo?.requires_visit_civil_work}      
      <br />
      PROGRAMACIÓN DE VOC :...............................
      {formKickoff?.uMInfo?.schedule_visit_civil_work}      
      <br />
      <br />
      *****************************************  REQUERIMIENTOS PARA ENTREGA DEL SERVICIO  ************************************
      <br />
      REQUIERE RFC........................................
      {formKickoff?.deliveryServiceRequirement?.requires_rfc}      
      <br />
      Conversor Medio :...................................
      {formKickoff?.deliveryServiceRequirement?.media_converter}      
      <br />
      Referencia Router :.................................
      {formKickoff?.deliveryServiceRequirement?.router_reference}      
      <br />
      Modulos o Tarjetas :................................
      {formKickoff?.deliveryServiceRequirement?.modules_or_cards}      
      <br />
      Licencias :.........................................
      {formKickoff?.deliveryServiceRequirement?.licensing}      
      <br />
      Equipos Adicionales :...............................
      {formKickoff?.deliveryServiceRequirement?.additional_equipment}      
      <br />
      <br />
      ********* Teléfonos *********
      <br />
      Teléfonos:........................................
      {formKickoff?.deliveryServiceRequirement?.phones}
      <br />
      Referencias:........................................
      {formKickoff?.deliveryServiceRequirement?.phone_reference}      
      <br />
      Cantidad :..........................................
      {formKickoff?.deliveryServiceRequirement?.quantity}      
      <br />
      Fuentes de Teléfonos :..............................
      {formKickoff?.deliveryServiceRequirement?.phone_sources}      
      <br />
      Diademas :..........................................
      {formKickoff?.deliveryServiceRequirement?.headbands}      
      <br />
      Arañas de Conferencia :.............................
      {formKickoff?.deliveryServiceRequirement?.conference_spiders}      
      <br />
      Botoneras :.........................................
      {formKickoff?.deliveryServiceRequirement?.keypads}      
      <br />
      Modulo Expansión Botonera :.........................
      {formKickoff?.deliveryServiceRequirement?.module_expanansion_buttons}      
      <br />
      Fuente Botoneras :..................................
      {formKickoff?.deliveryServiceRequirement?.button_source}      
      <br />
      Consumibles :.......................................
      {formKickoff?.deliveryServiceRequirement?.consumables}      
      <br />
      REGISTRO DE IMPORTACIÓN Y CARTA VALORIZADA :........
      {formKickoff?.deliveryServiceRequirement?.import_registration_and_valued_letter}      <br />
      *****************************************   DATOS DEL CONTACTO PARA COMUNICACIÓN   **************************************
      <br />
      ****************************  APRUEBA COSTOS DE OC E INICIO DE FACTURACIÓN DE ORDEN DE TRABAJO  *************************
      <br />
      NOMBRE :............................................
      {formKickoff?.contactInfoWorkOrder?.full_name_CW}      
      <br />
      TELEFONO :..........................................
      {formKickoff?.contactInfoWorkOrder?.phone_CW}      
      <br />
      CELULAR :...........................................
      {formKickoff?.contactInfoWorkOrder?.mobile_number_CW}      
      <br />
      CORREO ELECTRONICO :................................
      {formKickoff?.contactInfoWorkOrder?.email_CW}      
      <br />
      <br />
      ************************************************  DATOS CONTACTO TÉCNICO   **********************************************
      <br />
      NOMBRE :............................................
      {formKickoff?.technicalContactDetails?.full_name_TC}     
      <br />
      TELEFONO :..........................................
      {formKickoff?.technicalContactDetails?.phone_TC}     
      <br />
      CELULAR :...........................................
      {formKickoff?.technicalContactDetails?.mobile_number_TC}     
      <br />
      CORREO ELECTRONICO :................................
      {formKickoff?.technicalContactDetails?.email_TC}     
      <br />
      OBSERVACIONES :.....................................
      {formKickoff?.technicalContactDetails?.observations}     
      <br />
      <br />
      ***************************************************  KIKOFF TECNICO     *************************************************
      <br />
      TELEFONIA FIJA CLARO :......................
      {formKickoff?.technicalKickoff?.fixed_phone_claro}      
      <br />
      CANTIDAD DE EXTENSIONES :........................
      {formKickoff?.technicalKickoff?.quantity_extensions}      
      <br />
      CANTIDAD DE BUZONES VOZ :....................................
      {formKickoff?.technicalKickoff?.number_voice_boxes}      
      <br />
      INCLUYE GRABACIÓN DE VOZ :.................................
      {formKickoff?.technicalKickoff?.includes_goat_voice}      
      <br />
      INCLUYE LAN ADMINISTRADA :............................
      {formKickoff?.technicalKickoff?.include_lan_managed}      
      <br />
      <br />
    </div>
  );

};

export default Format6;
