import React from 'react';

const Format10 = (props) => {
    
  const { formKickoff } = props;
    
  return (
    <div>
      **************************************************   DATOS BASICOS   **************************************************
      <br />
      CIUDAD: ............................................
      {formKickoff?.basicData?.city}      
      <br />
      DIRECCIÓN: .........................................
      {formKickoff?.basicData?.sourceAddress}      
      <br />
      TIPO PREDIO: .......................................
      {formKickoff?.basicData?.predio_type}      
      <br />
      NIT del cliente: ...................................
      {formKickoff?.basicData?.nit}      
      <br />
      ALIAS DEL LUGAR: ...................................
      {formKickoff?.basicData?.alias}      
      <br />
      OTP: ...............................................
      {formKickoff?.basicData?.projectId}      
      <br />
      OTP ASOCIADAS: .....................................
      {formKickoff?.basicData?.otp_asociada}      
      <br />
      TOPOLOGIA :......
      {formKickoff?.basicData?.topology}      
      <br />
      ID SERVICIO ACTUAL (Aplica para UM Existente):......
      {formKickoff?.basicData?.id_current_service}      
      <br />
      <br />
      *****************************************  REQUERIMIENTOS PARA ENTREGA DEL SERVICIO  ************************************
      <br />
      REQUIERE RFC........................................
      {formKickoff?.deliveryServiceRequirement?.requires_rfc}      
      <br />
      Conversor Medio :...................................
      {formKickoff?.deliveryServiceRequirement?.media_converter}      
      <br />
      Referencia Router :.................................
      {formKickoff?.deliveryServiceRequirement?.router_reference}      
      <br />
      Modulos o Tarjetas :................................
      {formKickoff?.deliveryServiceRequirement?.modules_or_cards}      
      <br />
      Licencias :.........................................
      {formKickoff?.deliveryServiceRequirement?.licensing}      
      <br />
      Equipos Adicionales :...............................
      {formKickoff?.deliveryServiceRequirement?.additional_equipment}      
      <br />
      Consumibles :.......................................
      {formKickoff?.deliveryServiceRequirement?.consumables}      
      <br />
      REGISTRO DE IMPORTACIÓN Y CARTA VALORIZADA :........
      {formKickoff?.deliveryServiceRequirement?.import_registration_and_valued_letter}      
      <br />
      <br />
      *****************************************   DATOS DEL CONTACTO PARA COMUNICACIÓN   **************************************
      <br />
      ****************************  APRUEBA COSTOS DE OC E INICIO DE FACTURACIÓN DE ORDEN DE TRABAJO  *************************
      <br />
      NOMBRE :............................................
      {formKickoff?.contactInfoWorkOrder?.full_name_CW}      
      <br />
      TELEFONO :..........................................
      {formKickoff?.contactInfoWorkOrder?.phone_CW}      
      <br />
      CELULAR :...........................................
      {formKickoff?.contactInfoWorkOrder?.mobile_number_CW}      
      <br />
      CORREO ELECTRONICO :................................
      {formKickoff?.contactInfoWorkOrder?.email_CW}      
      <br />
      <br />
      ************************************************  DATOS CONTACTO TÉCNICO   **********************************************
      <br />
      NOMBRE :............................................
      {formKickoff?.technicalContactDetails?.full_name_TC}     
      <br />
      TELEFONO :..........................................
      {formKickoff?.technicalContactDetails?.phone_TC}     
      <br />
      CELULAR :...........................................
      {formKickoff?.technicalContactDetails?.mobile_number_TC}     
      <br />
      CORREO ELECTRONICO :................................
      {formKickoff?.technicalContactDetails?.email_TC}     
      <br />
      OBSERVACIONES :.....................................
      {formKickoff?.technicalContactDetails?.observations}     
      <br />
      <br />
      ***************************************************  KIKOFF TECNICO     *************************************************
      <br />
      TIPO PROTOCOLO :......................
      {formKickoff?.technicalKickoff?.activation_plan_ld}      
      <br />
      <br />
    </div>
  );
};


export default Format10;
