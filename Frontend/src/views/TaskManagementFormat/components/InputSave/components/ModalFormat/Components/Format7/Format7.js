import React from 'react';

const Format7 = (props) => {

  const { formKickoff } = props;

  return (
    <div>
      **************************************************   DATOS BASICOS   **************************************************
      <br />
      CIUDAD: ............................................
      {formKickoff?.basicData?.city}      
      <br />
      DIRECCIÓN UBICACIÓN ACTUAL DEL SERVICIO:............
      {formKickoff?.basicData?.sourceAddress}      
      <br />
      TIPO PREDIO :.......................................
      {formKickoff?.basicData?.predio_type}      
      <br />
      NIT del cliente: ...................................
      {formKickoff?.basicData?.nit}      
      <br />
      ALIAS DEL LUGAR: ...................................
      {formKickoff?.basicData?.alias}      
      <br />
      OTP :...............................................
      {formKickoff?.basicData?.projectId}      
      <br />
      OTP ASOCIADAS: .....................................
      {formKickoff?.basicData?.otp_asociada}      
      <br />
      TIPO DE TELEFONIA FIJA : ...............................
      {formKickoff?.basicData?.landline_type}      
      <br />
      ANCHO DE BANDA : ...................................
      {formKickoff?.basicData?.ancho_banda}      
      <br />
      TIPO DE INSTALACION :.......................
      {formKickoff?.basicData?.instalation_type}      
      <br />
      ID SERVICIO ACTUAL (Aplica para UM Existente) :..................................
      {formKickoff?.basicData?.id_current_service}      
      <br />
      <br />
      **********************************************   INFORMACIÓN  ULTIMA MILLA    ******************************************
      <br />
      <br />
      ¿ESTA OT REQUIERE INSTALACION DE  UM? :..............
      {formKickoff?.uMInfo?.ot_requiere_instalacion_um}      
      <br />
      PROVEEDOR :.........................................
      {formKickoff?.uMInfo?.provider}      
      <br />
      MEDIO :.............................................
      {formKickoff?.uMInfo?.medium}      
      <br />
      TIPO DE CONECTOR *** (Aplica para FO Claro) :............
      {formKickoff?.uMInfo?.type_conector}      
      <br />
      INTERFACE DE ENTREGA AL CLIENTE : ..................
      {formKickoff?.uMInfo?.client_delivery_interface}      
      <br />
      REQUIERE VOC :......................................
      {formKickoff?.uMInfo?.requires_visit_civil_work}      
      <br />
      PROGRAMACIÓN DE VOC :...............................
      {formKickoff?.uMInfo?.schedule_visit_civil_work}      
      <br />
      <br />
      *****************************************  REQUERIMIENTOS PARA ENTREGA DEL SERVICIO  ************************************
      <br />
      REQUIERE RFC :......................................
      {formKickoff?.deliveryServiceRequirement?.requires_rfc}      
      <br />
      Conversor Medio :...................................
      {formKickoff?.deliveryServiceRequirement?.media_converter}      
      <br />
      Referencia Router :.................................
      {formKickoff?.deliveryServiceRequirement?.router_reference}      
      <br />
      Modulos o Tarjetas :................................
      {formKickoff?.deliveryServiceRequirement?.modules_or_cards}      
      <br />
      Licencias :.........................................
      {formKickoff?.deliveryServiceRequirement?.licensing}      
      <br />
      Equipos Adicionales :...............................
      {formKickoff?.deliveryServiceRequirement?.additional_equipment}      
      <br />
      Consumibles :.......................................
      {formKickoff?.deliveryServiceRequirement?.consumables}      
      <br />
      REGISTRO DE IMPORTACIÓN Y CARTA VALORIZADA :........
      {formKickoff?.deliveryServiceRequirement?.import_registration_and_valued_letter}      <br />
      <br />
      *****************************************   DATOS DEL CONTACTO PARA COMUNICACIÓN   **************************************
      <br />
      ****************************  APRUEBA COSTOS DE OC E INICIO DE FACTURACIÓN DE ORDEN DE TRABAJO  *************************
      <br />
      NOMBRE :............................................
      {formKickoff?.contactInfoWorkOrder?.full_name_CW}      
      <br />
      TELEFONO :..........................................
      {formKickoff?.contactInfoWorkOrder?.phone_CW}      
      <br />
      CELULAR :...........................................
      {formKickoff?.contactInfoWorkOrder?.mobile_number_CW}      
      <br />
      CORREO ELECTRONICO :................................
      {formKickoff?.contactInfoWorkOrder?.email_CW}      
      <br />
      <br />
      ************************************************  DATOS CONTACTO TÉCNICO   **********************************************
      <br />
      NOMBRE :............................................
      {formKickoff?.technicalContactDetails?.full_name_TC}     
      <br />
      TELEFONO :..........................................
      {formKickoff?.technicalContactDetails?.phone_TC}     
      <br />
      CELULAR :...........................................
      {formKickoff?.technicalContactDetails?.mobile_number_TC}     
      <br />
      CORREO ELECTRONICO :................................
      {formKickoff?.technicalContactDetails?.email_TC}     
      <br />
      OBSERVACIONES :.....................................
      {formKickoff?.technicalContactDetails?.observations}     
      <br />
      <br />
      ***************************************************  KIKOFF TECNICO     *************************************************
      <br />
      Activación de PLAN LD CON COSTO (0 $):..............
      {formKickoff?.technicalKickoff?.activation_plan_ld}      
      <br />
      Equipo Cliente :....................................
      {formKickoff?.technicalKickoff?.client_team}      
      <br />
      Interfaz Equipos Cliente :..........................
      {formKickoff?.technicalKickoff?.client_equipment_interface}      
      <br />
      <br />
      Cantidad Lineas Básicas a Adicionar
      <br />
      (Solo Telefonia Pública Líneas Análogas) :..........
      {formKickoff?.technicalKickoff?.quantities_basic_lines}      
      <br />
      <br />
      Conformación PBX
      <br />
      (Solo Telefonia Pública Líneas Análogas) :..........
      {formKickoff?.technicalKickoff?.conformation_pbx}      
      <br />
      Cantidad de DID a Adicionar :.......................
      {formKickoff?.technicalKickoff?.quantity_did_requested}      
      <br />
      Cantidad Canales a Adicionar :......................
      {formKickoff?.technicalKickoff?.number_channels}      
      <br />
      Numero Cabecera PBX :..................
      {formKickoff?.technicalKickoff?.header_number_pbx}      
      <br />
      FAX TO MAIL :..................
      {formKickoff?.technicalKickoff?.fax_to_mail}      
      <br />
      TELEFONO VIRTUAL :................
      {formKickoff?.technicalKickoff?.virtual_phone}      
      <br />
      Requiere Permisos para Larga Distancia Nacional :...
      {formKickoff?.technicalKickoff?.requires_national_permissions}      
      <br />
      Requiero Larga  Para Distancia  Internacional :.....
      {formKickoff?.technicalKickoff?.requires_long_distance}      
      <br />
      Requiere Permisos para Móviles :....................
      {formKickoff?.technicalKickoff?.requires_mobile_permissions}      
      <br />
      Requiere Permisos para Local Extendida :....................
      {formKickoff?.technicalKickoff?.requires_local_permissions}
      <br />
      <br />
      *********************************************   NUMERACIÓN SOLO DILIGENCIAR PARA LA OPCIÓN  PBX DISTRIBUIDO  *********************************************
      { formKickoff?.numberingForDistributedPBX?.bogota ? (
        
        <div>
          ****  Ciudad Bogotá  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.bogota?.requires}
          <br />
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.bogota?.numbering_assigned_TAB}         
          <br />
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.bogota?.quantity_did}         
          <br />
          <br />
        </div>
      ) : '' }

      { formKickoff?.numberingForDistributedPBX?.tunja ? (        
        <div>
          ****  Ciudad Tunja  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.tunja?.requires}
          <br />
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.tunja?.numbering_assigned_TAB} 
          <br />
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.tunja?.quantity_did}
          <br />
          <br />
        </div>
      ) : '' }

      { formKickoff?.numberingForDistributedPBX?.villavicencio ? (        
        <div>
          ****  Ciudad Villavicencio  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.villavicencio?.requires}
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.villavicencio?.numbering_assigned_TAB} 
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.villavicencio?.quantity_did}
          <br />
        </div>
      ) : '' }

      { formKickoff?.numberingForDistributedPBX?.facatativa ? (        
        <div>
          ****  Ciudad Facatativa  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.facatativa?.requires}
          <br />
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.facatativa?.numbering_assigned_TAB} 
          <br />
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.facatativa?.quantity_did}
          <br />
          <br />
        </div>
      ) : '' }

      { formKickoff?.numberingForDistributedPBX?.girardot ? (        
        <div>
          ****  Ciudad Girardot  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.girardot?.requires}
          <br />
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.girardot?.numbering_assigned_TAB} 
          <br />
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.girardot?.quantity_did}
          <br />
          <br />
        </div>
      ) : '' }

      { formKickoff?.numberingForDistributedPBX?.yopal ? (        
        <div>
          ****  Ciudad Yopal  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.yopal?.requires}
          <br />
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.yopal?.numbering_assigned_TAB} 
          <br />
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.yopal?.quantity_did}
          <br />
          <br />
        </div>
      ) : '' }

      { formKickoff?.numberingForDistributedPBX?.cali ? (        
        <div>
          ****  Ciudad Cali  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.cali?.requires}
          <br />
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.cali?.numbering_assigned_TAB} 
          <br />
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.cali?.quantity_did}
          <br />
          <br />
        </div>
      ) : '' }

      { formKickoff?.numberingForDistributedPBX?.buenaventura ? (        
        <div>
          ****  Ciudad Buenaventura  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.buenaventura?.requires}
          <br />
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.buenaventura?.numbering_assigned_TAB} 
          <br />
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.buenaventura?.quantity_did}
          <br />
          <br />
        </div>
      ) : '' }

      { formKickoff?.numberingForDistributedPBX?.pasto ? (        
        <div>
          ****  Ciudad Pasto  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.pasto?.requires}
          <br />
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.pasto?.numbering_assigned_TAB} 
          <br />
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.pasto?.quantity_did}
          <br />
          <br />
        </div>
      ) : '' }

      { formKickoff?.numberingForDistributedPBX?.popayan ? (        
        <div>
          ****  Ciudad Popayán  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.popayan?.requires}
          <br />
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.popayan?.numbering_assigned_TAB} 
          <br />
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.popayan?.quantity_did}
          <br />
          <br />
        </div>
      ) : '' }

      { formKickoff?.numberingForDistributedPBX?.neiva ? (        
        <div>
          ****  Ciudad Neiva  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.neiva?.requires}
          <br />
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.neiva?.numbering_assigned_TAB} 
          <br />
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.neiva?.quantity_did}
          <br />
          <br />
        </div>
      ) : '' }

      { formKickoff?.numberingForDistributedPBX?.medellin ? (        
        <div>
          ****  Ciudad Medellín  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.medellin?.requires}
          <br />
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.medellin?.numbering_assigned_TAB} 
          <br />
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.medellin?.quantity_did}
          <br />
          <br />
        </div>
      ) : '' }

      { formKickoff?.numberingForDistributedPBX?.barranquilla ? (        
        <div>
          ****  Ciudad Barranquilla  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.barranquilla?.requires}
          <br />
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.barranquilla?.numbering_assigned_TAB} 
          <br />
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.barranquilla?.quantity_did}
          <br />
          <br />
        </div>
      ) : '' }

      { formKickoff?.numberingForDistributedPBX?.cartagena ? (        
        <div>
          ****  Ciudad Cartagena  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.cartagena?.requires}
          <br />
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.cartagena?.numbering_assigned_TAB} 
          <br />
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.cartagena?.quantity_did}
          <br />
          <br />
        </div>
      ) : '' }

      { formKickoff?.numberingForDistributedPBX?.santaMarta ? (        
        <div>
          ****  Ciudad Santa Marta  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.santaMarta?.requires}
          <br />
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.santaMarta?.numbering_assigned_TAB} 
          <br />
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.santaMarta?.quantity_did}
          <br />
          <br />
        </div>
      ) : '' }

      { formKickoff?.numberingForDistributedPBX?.monteria ? (        
        <div>
          ****  Ciudad Monteria  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.monteria?.requires}
          <br />
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.monteria?.numbering_assigned_TAB} 
          <br />
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.monteria?.quantity_did}
          <br />
          <br />
        </div>
      ) : '' }

      { formKickoff?.numberingForDistributedPBX?.valledupar ? (        
        <div>
          ****  Ciudad Valledupar  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.valledupar?.requires}
          <br />
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.valledupar?.numbering_assigned_TAB} 
          <br />
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.valledupar?.quantity_did}
          <br />
          <br />
        </div>
      ) : '' }

      { formKickoff?.numberingForDistributedPBX?.sincelejo ? (        
        <div>
          ****  Ciudad Sincelejo  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.sincelejo?.requires}
          <br />
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.sincelejo?.numbering_assigned_TAB} 
          <br />
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.sincelejo?.quantity_did}
          <br />
          <br />
        </div>
      ) : '' }

      { formKickoff?.numberingForDistributedPBX?.pereira ? (        
        <div>
          ****  Ciudad Pereira  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.pereira?.requires}
          <br />
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.pereira?.numbering_assigned_TAB} 
          <br />
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.pereira?.quantity_did}
          <br />
          <br />
        </div>
      ) : '' }

      { formKickoff?.numberingForDistributedPBX?.armenia ? (        
        <div>
          ****  Ciudad Armenia  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.armenia?.requires}
          <br />
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.armenia?.numbering_assigned_TAB} 
          <br />
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.armenia?.quantity_did}
          <br />
          <br />
        </div>
      ) : '' }

      { formKickoff?.numberingForDistributedPBX?.manizales ? (        
        <div>
          ****  Ciudad Manizales  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.manizales?.requires}
          <br />
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.manizales?.numbering_assigned_TAB} 
          <br />
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.manizales?.quantity_did}
          <br />
          <br />
        </div>
      ) : '' }

      { formKickoff?.numberingForDistributedPBX?.ibague ? (        
        <div>
          ****  Ciudad Ibagué  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.ibague?.requires}
          <br />
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.ibague?.numbering_assigned_TAB} 
          <br />
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.ibague?.quantity_did}
          <br />
          <br />
        </div>
      ) : '' }

      { formKickoff?.numberingForDistributedPBX?.cucuta ? (        
        <div>
          ****  Ciudad Cucutá  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.cucuta?.requires}
          <br />
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.cucuta?.numbering_assigned_TAB} 
          <br />
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.cucuta?.quantity_did}
          <br />
          <br />
        </div>
      ) : '' }

      { formKickoff?.numberingForDistributedPBX?.bucaramanga ? (        
        <div>
          ****  Ciudad Bucaramanga  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.bucaramanga?.requires}
          <br />
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.bucaramanga?.numbering_assigned_TAB} 
          <br />
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.bucaramanga?.quantity_did}
          <br />
          <br />
        </div>
      ) : '' }

      { formKickoff?.numberingForDistributedPBX?.duitama ? (        
        <div>
          ****  Ciudad Duitama  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.duitama?.requires}
          <br />
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.duitama?.numbering_assigned_TAB} 
          <br />
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.duitama?.quantity_did}
          <br />
          <br />
        </div>
      ) : '' }

      { formKickoff?.numberingForDistributedPBX?.sogamoso ? (        
        <div>
          ****  Ciudad Sogamoso  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.sogamoso?.requires}
          <br />
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.sogamoso?.numbering_assigned_TAB} 
          <br />
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.sogamoso?.quantity_did}
          <br />
          <br />
        </div>
      ) : '' }

      { formKickoff?.numberingForDistributedPBX?.flandes ? (        
        <div>
          ****  Ciudad Flandes  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.flandes?.requires}
          <br />
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.flandes?.numbering_assigned_TAB} 
          <br />
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.flandes?.quantity_did}
          <br />
          <br />
        </div>
      ) : '' }

      { formKickoff?.numberingForDistributedPBX?.rivera ? (        
        <div>
          ****  Ciudad Rivera  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.rivera?.requires}
          <br />
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.rivera?.numbering_assigned_TAB} 
          <br />
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.rivera?.quantity_did}
          <br />
          <br />
        </div>
      ) : '' }

      { formKickoff?.numberingForDistributedPBX?.aipe ? (        
        <div>
          ****  Ciudad Aipe  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.aipe?.requires}
          <br />
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.aipe?.numbering_assigned_TAB} 
          <br />
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.aipe?.quantity_did}
          <br />
          <br />
        </div>
      ) : '' }

      { formKickoff?.numberingForDistributedPBX?.lebrija ? (        
        <div>
          ****  Ciudad Lebrija  ****
          <br />
          Requiere:.......................................
          {formKickoff?.numberingForDistributedPBX?.lebrija?.requires}
          <br />
          NUMERACIÓN ASIGNADA EN TAB:.....................
          {formKickoff?.numberingForDistributedPBX?.lebrija?.numbering_assigned_TAB} 
          <br />
          CANTIDAD DID:...................................
          {formKickoff?.numberingForDistributedPBX?.lebrija?.quantity_did}
          <br />
          <br />
        </div>
      ) : '' }

    </div>
  );

};

export default Format7;
