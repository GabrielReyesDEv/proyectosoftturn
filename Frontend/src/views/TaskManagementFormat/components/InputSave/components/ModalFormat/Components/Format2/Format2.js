import React from 'react';

const Format2 = (props) => {

  const { formKickoff } = props;

  return (
    <div>
      ***************************************************   PUNTO DESTINO       ************************************************
      <br />
      *******************************************    DATOS BÁSICOS DE INSTALACION      *****************************************
      <br />
      CIUDAD :......................................................
      {formKickoff?.destinationPoint?.basicData?.destination_city}      
      <br />
      DIRECCIÓN :...................................................
      {formKickoff?.destinationPoint?.basicData?.destinationAddress}      
      <br />
      TIPO PREDIO :.................................................
      {formKickoff?.destinationPoint?.basicData?.destination_predio_type}      
      <br />
      NIT del cliente :.............................................
      {formKickoff?.destinationPoint?.basicData?.destination_nit}      
      <br />
      ALIAS DEL LUGAR :.............................................
      {formKickoff?.destinationPoint?.basicData?.destination_alias}      
      <br />
      OTP :.........................................................
      {formKickoff?.destinationPoint?.basicData?.projectId}      
      <br />
      OTP ASOCIADAS :...............................................
      {formKickoff?.destinationPoint?.basicData?.destination_otp_asociada}      
      <br />
      TIPO MPLS :...................................................
      {formKickoff?.destinationPoint?.basicData?.destination_mlps_type}      
      <br />
      ANCHO DE BANDA :..............................................
      {formKickoff?.destinationPoint?.basicData?.destination_ancho_banda}      
      <br />
      TIPO DE INSTALACIÓN :.........................................
      {formKickoff?.destinationPoint?.basicData?.destination_instalation_type}      
      <br />
      ID SERVICIO ACTUAL :..........................................
      {formKickoff?.destinationPoint?.basicData?.destination_id_current_service}      
      <br />
      ID SERVICIO PRINCIPAL :.......................................
      {formKickoff?.destinationPoint?.basicData?.destination_id_principal_service}      
      <br />     
      <br />
      *******************************************  INFORMACIÓN  ULTIMA MILLA DESTINO  ******************************************
      ¿ESTA OT REQUIERE INSTALACION DE  UM? :.......................
      {formKickoff?.destinationPoint?.uMInfo?.destination_ot_requiere_instalacion_um}      
      <br />
      ESTA ULTIMA MILLA ES UN BACKUP? :.............................
      {formKickoff?.destinationPoint?.uMInfo?.destination_last_milla_backup}      
      <br />
      PROVEEDOR :...................................................
      {formKickoff?.destinationPoint?.uMInfo?.destination_provider}      
      <br />
      MEDIO :.......................................................
      {formKickoff?.destinationPoint?.uMInfo?.destination_medium}      
      <br />
      RESPUESTA FACTIBILIDAD BW
      {' > '}
      100 MEGAS :........................
      {formKickoff?.destinationPoint?.uMInfo?.destination_feasibility_response}      
      <br />
      TIPO DE CONECTOR *** (Aplica para FO Claro) :.................
      {formKickoff?.destinationPoint?.uMInfo?.destination_type_conector}      
      <br />
      ACCESO (Solo Aplica para Canales
      {' > '}
      100 MEGAS    
      <br />
      SDS DESTINO :.................................................
      {formKickoff?.destinationPoint?.uMInfo?.destination_unifilar_sds_access}      
      <br />
      INTERFACE DE ENTREGA AL CLIENTE :.............................
      {formKickoff?.destinationPoint?.uMInfo?.destination_client_delivery_interface}      
      <br />
      REQUIERE VOC :................................................
      {formKickoff?.destinationPoint?.uMInfo?.destination_requires_visit_civil_work}      
      <br />
      PROGRAMACIÓN DE VOC :.........................................
      {formKickoff?.destinationPoint?.uMInfo?.destination_schedule_visit_civil_work}      
      <br />     
      <br />
      *****************************************  REQUERIMIENTOS PARA ENTREGA DEL SERVICIO  ************************************
      
      <br />
      REQUIERE RFC :......................................
      {formKickoff?.destinationPoint?.deliveryServiceRequirement?.destination_requires_rfc}      
      <br />
      Conversor Medio :...................................
      {formKickoff?.destinationPoint?.deliveryServiceRequirement?.destination_media_converter}      
      <br />
      Referencia Router :.................................
      {formKickoff?.destinationPoint?.deliveryServiceRequirement?.destination_router_reference}      
      <br />
      Modulos o Tarjetas :................................
      {formKickoff?.destinationPoint?.deliveryServiceRequirement?.destination_modules_or_cards}      
      <br />
      Licencias :.........................................
      {formKickoff?.destinationPoint?.deliveryServiceRequirement?.destination_licensing}      
      <br />
      Equipos Adicionales :...............................
      {formKickoff?.destinationPoint?.deliveryServiceRequirement?.destination_additional_equipment}      
      <br />
      Consumibles :.......................................
      {formKickoff?.destinationPoint?.deliveryServiceRequirement?.destination_consumables}      
      <br />
      REGISTRO DE IMPORTACIÓN Y CARTA VALORIZADA :........
      {formKickoff?.destinationPoint?.deliveryServiceRequirement?.destination_import_registration_and_valued_letter}      
      <br />
      <br />

      *********************************************  DATOS DEL CONTACTO PARA COMUNICACIÓN     **************************************
      <br />
      ****************************************  APRUEBA COSTOS DE OC Y CIERRE DE ORDEN DE TRABAJO  *********************************
      <br />
      NOMBRE :......................................................
      {formKickoff?.destinationPoint?.contactInfoWorkOrder?.destination_full_name_CW}      
      <br />
      TELEFONO :....................................................
      {formKickoff?.destinationPoint?.contactInfoWorkOrder?.destination_phone_CW}      
      <br />
      CELULAR :.....................................................
      {formKickoff?.destinationPoint?.contactInfoWorkOrder?.destination_mobile_number_CW}      
      <br />
      CORREO ELECTRONICO :..........................................
      {formKickoff?.destinationPoint?.contactInfoWorkOrder?.destination_email_CW}      
      <br />
      <br />
      ***********************************************   DATOS CONTACTO TÉCNICO   ***************************************************
      <br />
      NOMBRE :......................................................
      {formKickoff?.destinationPoint?.technicalContactDetails?.destination_full_name_TC}      
      <br />
      TELEFONO :....................................................
      {formKickoff?.destinationPoint?.technicalContactDetails?.destination_phone_TC}      
      <br />
      CELULAR :.....................................................
      {formKickoff?.destinationPoint?.technicalContactDetails?.destination_mobile_number_TC}      
      <br />
      CORREO ELECTRONICO :..........................................
      {formKickoff?.destinationPoint?.technicalContactDetails?.destination_email_TC}      
      <br />
      OBSERVACIONES:................................................
      {formKickoff?.destinationPoint?.technicalContactDetails?.destination_observations}      
      <br />
      <br />
      <br />

      <div>
          ***********************************************     PUNTO ORIGEN       ************************************************
        <br />
          ****************************************     DATOS BÁSICOS DE INSTALACION      *****************************************
        <br />
          CIUDAD :......................................................
        {formKickoff?.originPoint?.basicData?.origin_city}        
        <br />
          DIRECCIÓN :...................................................
        {formKickoff?.originPoint?.basicData?.sourceAddress}        
        <br />
          TIPO PREDIO :.................................................
        {formKickoff?.originPoint?.basicData?.origin_predio_type}        
        <br />
          NIT del cliente :.............................................
        {formKickoff?.originPoint?.basicData?.origin_nit}        
        <br />
          ALIAS DEL LUGAR :.............................................
        {formKickoff?.originPoint?.basicData?.origin_alias}        
        <br />
          OTP :...............................................
        {formKickoff?.originPoint?.basicData?.projectId}          
          OTP ASOCIADAS :...............................................
        {formKickoff?.originPoint?.basicData?.origin_otp_asociada}        
        <br />
          TIPO MPLS :...................................................
        {formKickoff?.originPoint?.basicData?.origin_mlps_type}        
        <br />
          ANCHO DE BANDA :..............................................
        {formKickoff?.originPoint?.basicData?.origin_ancho_banda}        
        <br />
          TIPO DE INSTALACIÓN :.........................................
        {formKickoff?.originPoint?.basicData?.origin_instalation_type}        
        <br />
          ID SERVICIO ACTUAL :..........................................
        {formKickoff?.originPoint?.basicData?.origin_id_current_service}        
        <br />
          ID SERVICIO PRINCIPAL :.......................................
        {formKickoff?.originPoint?.basicData?.origin_id_principal_service}        
        <br />       
        <br />
          *******************************************  INFORMACIÓN  ULTIMA MILLA  ******************************************       
        <br />
          ¿ESTA OT REQUIERE INSTALACION DE  UM? :.......................
        {formKickoff?.originPoint?.uMInfo?.origin_ot_requiere_instalacion_um}        
        <br />
          ESTA ULTIMA MILLA ES UN BACKUP? :.............................
        {formKickoff?.originPoint?.uMInfo?.origin_last_milla_backup}        
        <br />
          PROVEEDOR :...................................................
        {formKickoff?.originPoint?.uMInfo?.origin_provider}        
        <br />
          MEDIO :.......................................................
        {formKickoff?.originPoint?.uMInfo?.origin_medium}        
        <br />
          RESPUESTA FACTIBILIDAD BW
        {' > '}
          100 MEGAS :........................
        {formKickoff?.originPoint?.uMInfo?.origin_feasibility_response}        
        <br />
          TIPO DE CONECTOR *** (Aplica para FO Claro) :.................
        {formKickoff?.originPoint?.uMInfo?.origin_type_conector}        
        <br />
          ACCESO (Solo Aplica para Canales
        {' > '}
          100 MEGAS
        <br />
          SDS DESTINO :.................................................
        {formKickoff?.originPoint?.uMInfo?.origin_destination_sds_access}        
        <br />
          INTERFACE DE ENTREGA AL CLIENTE :.............................
        {formKickoff?.originPoint?.uMInfo?.origin_client_delivery_interface}        
        <br />
          REQUIERE VOC :................................................
        {formKickoff?.originPoint?.uMInfo?.origin_requires_visit_civil_work}        
        <br />
          PROGRAMACIÓN DE VOC :.........................................
        {formKickoff?.originPoint?.uMInfo?.origin_schedule_visit_civil_work}        
        <br />
        
        <br />
          *******************************************    REQUERIMIENTOS PARA ENTREGA DEL SERVICIO  ************************************
        
        <br />
          REQUIERE RFC: ................................................
        {formKickoff?.originPoint?.deliveryServiceRequirement?.origin_requires_rfc}   
        <br />       
        EQUIPOS   (VER LISTA COMPLETA):
        <br />
          Conversor Medio :.............................................
        {formKickoff?.originPoint?.deliveryServiceRequirement?.origin_media_converter}        
        <br />
          Referencia Router :...........................................
        {formKickoff?.originPoint?.deliveryServiceRequirement?.origin_router_reference}        
        <br />
          Modulos o Tarjetas :..........................................
        {formKickoff?.originPoint?.deliveryServiceRequirement?.origin_modules_or_cards}        
        <br />
          Licencias :...................................................
        {formKickoff?.originPoint?.deliveryServiceRequirement?.origin_licensing}        
        <br />
          Equipos Adicionales :.........................................
        {formKickoff?.originPoint?.deliveryServiceRequirement?.origin_additional_equipment}        
        <br />
          Consumibles :.................................................
        {formKickoff?.originPoint?.deliveryServiceRequirement?.origin_consumables}        
        <br />
          REGISTRO DE IMPORTACIÓN Y CARTA VALORIZADA :..................
        {formKickoff?.originPoint?.deliveryServiceRequirement?.origin_import_registration_and_valued_letter}        
        <br />
        <br />
        *********************************************  DATOS DEL CONTACTO PARA COMUNICACIÓN     **************************************
        <br />
          ****************************************  APRUEBA COSTOS DE OC Y CIERRE DE ORDEN DE TRABAJO  *********************************
        <br />
          NOMBRE :......................................................
        {formKickoff?.originPoint?.contactInfoWorkOrder?.origin_full_name_CW}        
        <br />
          TELEFONO :....................................................
        {formKickoff?.originPoint?.contactInfoWorkOrder?.origin_phone_CW}        
        <br />
          CELULAR :.....................................................
        {formKickoff?.originPoint?.contactInfoWorkOrder?.origin_mobile_number_CW}        
        <br />
          CORREO ELECTRONICO :..........................................
        {formKickoff?.originPoint?.contactInfoWorkOrder?.origin_email_CW}        
        <br />
        <br />
          ***********************************************   DATOS CONTACTO TÉCNICO   ***************************************************
        <br />
          NOMBRE :......................................................
        {formKickoff?.originPoint?.technicalContactDetails?.origin_full_name_TC}        
        <br />
          TELEFONO :....................................................
        {formKickoff?.originPoint?.technicalContactDetails?.origin_phone_TC}        
        <br />
          CELULAR :.....................................................
        {formKickoff?.originPoint?.technicalContactDetails?.origin_mobile_number_TC}        
        <br />
          CORREO ELECTRONICO :..........................................
        {formKickoff?.originPoint?.technicalContactDetails?.origin_email_TC}        
        <br />
          OBSERVACIONES:................................................
        {formKickoff?.originPoint?.technicalContactDetails?.origin_observations}        
        <br />
        <br />
      </div>
    </div>
  );

};


export default Format2;
