import React from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  Checkbox,
  FormControlLabel,
  FormLabel,
  FormControl,
  DialogActions,
  Button,
  Accordion,
  AccordionSummary,
  Typography,
  AccordionDetails,
  Grid,
  TextField
} from '@material-ui/core';
import { useSnackbar } from 'notistack';
import { makeStyles } from '@material-ui/core/styles';
import getAuthorization from 'utils/getAuthorization';
import axios from 'utils/axios';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Cookies from 'js-cookie';
import moment from 'moment';
import { Redirect } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
}));

const StartTask = (props) => {

  const classes = useStyles();
    
  const { 
    openActivity,
    handleCloseActivity, 
    otpInfo, 
    formKickoff,
    service
  } = props;

  const [areas, setAreas] = React.useState([]);
  const firstName = Cookies.get('firstName');
  const lastName = Cookies.get('lastName');
  const userOnyx = Cookies.get('userOnyx');
  const [redit, setRedit] = React.useState(false);
  const { enqueueSnackbar } = useSnackbar();
  const [values, setValues] = React.useState({});


  const exitModal = () => {
    handleCloseActivity();
    setRedit(true);
  }

  const handleChange = (e) => {
    setValues({
      ...values,
      [e.target.name]: e.target.checked
    });
  };


  const startActivity = async () => {
    const array = []
    for (let key in values) {
      if ( values[key] === true ) {
        const data = areas.find((val) => val.name === key)
        if (data) {
          const user = areas.filter((data) => data.name === key);
          const divElement = document.getElementById(`cont_${key}`).querySelectorAll('input, select')
          const typeValue = divElement[0].value;
          const idValue = divElement[1].value;
          let info = {
            OT_HIJA: typeValue,
            ID_ORDEN_TRABAJO_HIJA: idValue,
            FECHA_CREACION_OT_HIJA: moment().format('YYYY-MM-DD HH:mm'),
            ESTADO_ORDEN_TRABAJO_HIJA: '1- Generada',
            USUARIO_ASIGNADO4: user[0]?.userOnyx ? user[0].userOnyx : `${firstName} ${lastName}`,
            RESOLUCION_15: 'SIN RESOLUCION1',
            RESOLUCION_26: 'SIN RESOLUCION2',
            RESOLUCION_37: 'SIN RESOLUCION3',
            RESOLUCION_48: 'SIN RESOLUCION4',
            FEC_ACTUALIZACION_ONYX_HIJA: moment().format('YYYY-MM-DD HH:mm'),
          }
          const projectObject = { projectId: 'NRO_OT_ONYX', 
            author: 'USUARIO_ASIGNADO', 
            segment: 'SEGMENTO', 
            group: 'GRUPO', 
            family: 'FAMILIA', 
            product: 'PRODUCTO', 
            description: 'DESCRIPCION', 
            resolucion1: 'RESOLUCION_1', 
            resolucion2: 'RESOLUCION_2', 
            resolucion3: 'RESOLUCION_3', 
            resolucion4: 'RESOLUCION_4', 
            incidentCity: 'CIUDAD_INCIDENTE', 
            incidentCity3: 'CIUDAD_INCIDENTE3', 
            client: 'NOMBRE_CLIENTE', 
            orderType: 'ORDEN_TRABAJO', 
            service: 'SERVICIO', 
            serviceCode: 'ID_ENLACE', 
            status: 'ESTADO_ORDEN_TRABAJO', 
            createdAt: 'FECHA_CREACION', 
            incidentDate: 'TIEMPO_INCIDENTE', 
            commitmentDate: 'FECHA_COMPROMISO', 
            programmingDate: 'FECHA_PROGRAMACION', 
            city: 'CIUDAD', 
            department: 'DEPARTAMENTO', 
            destinationAddress: 'DIRECCION_DESTINO', 
            sourceAddress: 'DIRECCION_ORIGEN', 
            finishDate: 'FECHA_REALIZACION', 
            mrc: 'MONTO_MONEDA_LOCAL_CARGO_MENSUAL'
          }
          for (let key in otpInfo) {
            info[projectObject[key]] = otpInfo[key]
          }

          if (
            service === 'MPLS Avanzado Intranet' ||
            service === 'MPLS Avanzado Intranet - Varios Puntos' ||
            service === 'MPLS Avanzado Intranet con Backup de Ultima Milla - NDS 2' ||
            service === 'MPLS Avanzado Intranet con Backup de Ultima Milla y Router - NDS1' ||
            service === 'Backend MPLS' ||
            service === 'MPLS Avanzado con Componente Datacenter Claro' ||
            service === 'MPLS Transaccional 3G' ||
            service === 'PRIVATE LINE') {
            info.CIUDAD = formKickoff?.originPoint?.basicData?.origin_city || otpInfo?.city
            info.DIRECCION_DESTINO = formKickoff?.destinationPoint?.basicData?.destinationAddress || otpInfo?.destinationAddress
            info.DIRECCION_ORIGEN = formKickoff?.originPoint?.basicData?.sourceAddress || otpInfo?.sourceAddress
            info.NOMBRE_CLIENTE = formKickoff?.initialReport?.client || otpInfo?.client
          } else {
            info.CIUDAD = formKickoff?.basicData?.city || otpInfo?.city
            info.DIRECCION_DESTINO = formKickoff?.basicData?.sourceAddress || otpInfo?.destinationAddress
            info.DIRECCION_ORIGEN = formKickoff?.basicData?.sourceAddress || otpInfo?.sourceAddress
            info.NOMBRE_CLIENTE = formKickoff?.initialReport?.client || otpInfo?.client
          }

          info.USUARIO_ASIGNADO = userOnyx;
          delete info.undefined

          array.push(info)
        }
      }
    }

    // console.log('array final', array);
    try {
      const consult = await axios({
        url: 'activities/importUpdt',
        method: 'post',
        data: array,
      });
  
      enqueueSnackbar('Se crearon las actividades correctamente', {
        variant: 'success',
      });
      setRedit(true);
      return consult.data.data.totalUsers;
    } catch (e) {
      enqueueSnackbar('Error de conexion', {
        variant: 'Error',
      });
      console.error('Failure!');
      console.error('Import error: ', e);
      console.error(e && e.response && e.response.status);
    }
  }

  React.useEffect(() => {

    axios.post('/areas/pagination', { 'eq': [{ 'field': 'group', 'values': ['BO']}] }, getAuthorization()).then(response => {    
      setAreas(response.data.data);
    });
  }, []);

  return (
    <div>
      <Dialog
        fullWidth
        maxWidth="sm"
        open={openActivity}
      >
        <DialogTitle>Disparar tareas</DialogTitle>
        <DialogContent fullWidth>
          <FormControl fullWidth>
            <FormLabel >Area:</FormLabel>
            {areas.length > 0 && areas.map((data) => {
              const type = data.name;
              return (
                <div>
                  <FormControlLabel
                    control={
                      <Checkbox 
                        checked={values && values.type}
                        color="primary"
                        name={type}
                        onChange={handleChange}
                      />}
                    fullWidth
                    label={type}
                  />
                  {values[type] === true ? (
                    <Accordion fullWidth>
                      <AccordionSummary
                        expandIcon={<ExpandMoreIcon />}
                        fullWidth
                      >
                        <Typography
                          className={classes.heading}
                          fullWidth
                        >
                                Requerimientos:
                        </Typography>
                      </AccordionSummary>
                      <AccordionDetails fullWidth>
                        <Grid
                          container
                          fullWidth
                          id={`cont_${type}`}
                          spacing={4}
                        >
                          <Grid
                            item
                            md={6}
                            xs={12}
                          >
                            <TextField
                              fullWidth
                              label="Tipo de actividad"
                              name="OT_HIJA"
                              select
                              SelectProps={{ native: true }}
                              variant="outlined"
                            >
                              <option
                                value=""
                              />
                              {data.activities.map(option => (
                                <option
                                  key={option}
                                  value={option}
                                >
                                  {option}
                                </option>
                              ))}
                            </TextField>
                          </Grid>
                          <Grid
                            item
                            md={6}
                            xs={12}
                          >
                            <TextField
                              fullWidth
                              label="ID de la actividad"
                              name="ID_ORDEN_TRABAJO_HIJA"
                              required
                              type="number"
                              variant="outlined"
                            />
                          </Grid>
                        </Grid>
                      </AccordionDetails>
                    </Accordion>
                  ): ''} 
                </div>
              )
            })}

          </FormControl>
        </DialogContent>
        <DialogActions>
          <Button
            autoFocus
            color="secondary"
            fullWidth="true"
            onClick={() => exitModal()}
            variant="contained"
          >
            Salir
          </Button>
          <Button
            autoFocus
            color="primary"
            fullWidth="true"
            onClick={() => startActivity()}
            variant="contained"
          >
            Guardar
          </Button>
        </DialogActions>
      </Dialog>
      {redit === true ? <Redirect to="/management/tasks" /> : '' }
    </div>
  );
};


export default StartTask;
