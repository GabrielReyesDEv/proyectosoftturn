import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { 
  HeaderSelect,
  DestinationPoint,
  OriginPoint
} from './components';
import { Alert } from 'components';
import { useDispatch, useSelector } from 'react-redux';
import { saveFormKickoff } from 'actions';

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(3)
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}));

const Mpls = (props) => {

  const { oth } = props; 
  const classes = useStyles();
  const { originDestination } = useSelector(state => state.destinationOriginReducer);
  const dispatch = useDispatch();
  const { formKickoff } = useSelector(state => state.formKickoff);
  
  useEffect(() => {
    dispatch(saveFormKickoff({
      ...formKickoff,
      destinationPoint:{
        basicData: {},
        contactInfoWorkOrder: {},
        deliveryServiceRequirement: {},
        technicalContactDetails: {},
        uMInfo: {},
      },
      originPoint: {
        basicData: {},
        contactInfoWorkOrder: {},
        deliveryServiceRequirement: {},
        technicalContactDetails: {},
        uMInfo: {},
      }
    }));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <Alert
        className={classes.root}
        message={`Servicio MPLS - OTH ${oth}`}
        loader={true}
      />
      <HeaderSelect className={classes.root} />
      {originDestination === 'Origen' ? <OriginPoint oth={oth}/> : <DestinationPoint oth={oth}/> }

    </>
  );
};

Mpls.propTypes = {
  className: PropTypes.string
};

export default Mpls;
