import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { useDispatch, useSelector } from 'react-redux';
import MyLocationIcon from '@material-ui/icons/MyLocation';
import {
  Card,
  RadioGroup,
  CardContent,
  FormControlLabel,
  Radio,
  FormLabel,
  Typography,
  FormControl
} from '@material-ui/core';
import { saveOriginDestination } from 'actions';

const useStyles = makeStyles(theme => ({
  root: {},
  icon: {
    fontSize: 20,
    marginRight: theme.spacing(1)
  },
  addButton: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
}));

const HeaderSelect = props => {
  const { className, ...rest } = props;

  const classes = useStyles();

  const { originDestination } = useSelector(state => state.destinationOriginReducer);

  const dispatch = useDispatch();

  const handleChange = (event) => {
    dispatch(saveOriginDestination(event.target.value));
  };

  useEffect(() => {
    dispatch(saveOriginDestination('Origen'));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardContent className={classes.addButton}> 
        <Typography
          className={classes.addButton}
          gutterBottom
          variant="h4"
        >

          <span className={classes.icon}><MyLocationIcon /></span>
          
          {' '}
          {`Punto de ${originDestination}`}

        </Typography>
        <FormControl component="fieldset">
          <FormLabel component="legend">Punto de:</FormLabel>
          <RadioGroup
            aria-label="position"
            defaultValue="Origen"
            name="position"
            onChange={handleChange}
            row
            value={originDestination}
          >
            <FormControlLabel
              control={<Radio color="primary" />}
              label="Origen"
              value="Origen"
            />
            <FormControlLabel 
              control={<Radio color="primary" />} 
              label="Destino" 
              value="Destino"
            />
          </RadioGroup>
        </FormControl>
      </CardContent>
    </Card>
  );
};

HeaderSelect.propTypes = {
  className: PropTypes.string
};

export default HeaderSelect;
