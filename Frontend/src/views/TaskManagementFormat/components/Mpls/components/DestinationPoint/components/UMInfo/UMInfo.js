import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  TextField,
  Grid
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { saveFormKickoff } from 'actions';

const useStyles = makeStyles(theme => ({
  root: {},
  alert: {
    marginBottom: theme.spacing(3)
  },
  formGroup: {
    marginBottom: theme.spacing(3)
  },
  fieldGroup: {
    display: 'flex',
    alignItems: 'center'
  },
  addIcon: {
    margin: theme.spacing(1)
  },
  fieldHint: {
    margin: theme.spacing(1, 0)
  },
  addButton: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  tags: {
    marginTop: theme.spacing(1),
    '& > * + *': {
      marginLeft: theme.spacing(1)
    }
  },
  flexGrow: {
    flexGrow: 1
  },
  dateField: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  }
}));

const Baseline = props => {
  const { className, ...rest } = props;

  const classes = useStyles();
  const dispatch = useDispatch();
  const { formKickoff, checkKickOff } = useSelector(state => state.formKickoff);

  const statusOptions = ['Si', 'No', 'Existente'];
  const providers = ['N/A', 'Existente', 'Claro', 'Axesat', 'Comcel', 'Tigo', 'Media Commerce', 'Diveo', 'Edatel', 'UNE', 'ETB', 'IBM', 'IFX', 'Level 3 Colombia', 'Mercanet', 'Metrotel', 'Promitel', 'Skynet', 'Telebucaramanga', 'Telecom', 'Terremark', 'Sol Cable Vision', 'Sistelec', 'Opain', 'Airplan - (Información y Tecnología)', 'TV Azteca']
  const media = ['N/A', 'Existente', 'Fibra', 'Cobre', 'Satelital', 'Radio Enlace', '3G', 'UTP']
  const connectors = ['LC', 'SC', 'ST', 'FC']
  const interfaces = ['N/A', 'Ethernet', 'Serial V.35', 'Giga (óptico)', 'Giga Ethernet (Eléctrico)', 'STM-1', 'RJ45 - 120 OHM', 'G703 BNC']
  const yesNo = ['N/A', 'Si', 'No']
  const programming = ['No requiere programación', 'Programada', 'No Programada. Otra Ciudad', 'No programada. Cliente Solicita ser contactado en fecha posterior y/o con otro contacto']
  
  const handleFieldChange = (event, field) => {
    event.persist && event.persist();
    document.getElementsByName(field)[0].parentNode.parentNode.classList.remove('selected_error');

    dispatch(saveFormKickoff({
      ...formKickoff,
      destinationPoint: {
        ...formKickoff.destinationPoint,
        uMInfo: {
          ...formKickoff.destinationPoint.uMInfo,
          [field]: event.target.value,
        }
      }
    }));
  
  };

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader title="Información de Ultima Milla" />
      <CardContent>
        <Grid
          container
          spacing={4}
        >
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('destination_ot_requiere_instalacion_um') !== -1 ? true : false}
              fullWidth
              label="¿ESTA OT REQUIERE INSTALACION DE  UM?"
              name="destination_ot_requiere_instalacion_um"
              onChange={event =>
                handleFieldChange(
                  event,
                  'destination_ot_requiere_instalacion_um',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.destinationPoint?.uMInfo?.destination_ot_requiere_instalacion_um}
              variant="outlined"
            >
              <option
                value=""
              />
              {statusOptions.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('destination_last_milla_backup') !== -1 ? true : false}
              fullWidth
              label="¿ESTA ULTIMA MILLA ES UN BACKUP?"
              name="destination_last_milla_backup"
              onChange={event =>
                handleFieldChange(
                  event,
                  'destination_last_milla_backup',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.destinationPoint?.uMInfo?.destination_last_milla_backup}
              variant="outlined"
            >
              <option
                value=""
              />
              {statusOptions.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
         
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('destination_provider') !== -1 ? true : false}
              fullWidth
              label="PROVEEDOR"
              name="destination_provider"
              onChange={event =>
                handleFieldChange(
                  event,
                  'destination_provider',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.destinationPoint?.uMInfo?.destination_provider}
              variant="outlined"
            >
              <option
                value=""
              />
              {providers.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('destination_medium') !== -1 ? true : false}
              fullWidth
              label="Medio"
              name="destination_medium"
              onChange={event =>
                handleFieldChange(
                  event,
                  'destination_medium',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.destinationPoint?.uMInfo?.destination_medium}
              variant="outlined"
            >
              <option
                value=""
              />
              {media.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              helperText="BW > 100 MEGAS"
              label="Respuesta Factibilidad"
              name="destination_feasibility_response"
              onChange={event =>
                handleFieldChange(event, 'destination_feasibility_response', event.target.value)
              }
              required
              value={formKickoff?.destinationPoint?.uMInfo?.destination_feasibility_response}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('destination_type_conector') !== -1 ? true : false}
              fullWidth
              helperText="Aplica para FO Claro"
              label="Tipo de Conector"
              name="destination_type_conector"
              onChange={event =>
                handleFieldChange(
                  event,
                  'destination_type_conector',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.destinationPoint?.uMInfo?.destination_type_conector}
              variant="outlined"
            >
              <option
                value=""
              />
              {connectors.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              helperText="Solo Aplica para Canales > 100 MEGAS"
              label="ACCESO SDS DESTINO (UNIFILAR)"
              name="destination_unifilar_sds_access"
              onChange={event =>
                handleFieldChange(event, 'destination_unifilar_sds_access', event.target.value)
              }
              required
              value={formKickoff?.destinationPoint?.uMInfo?.destination_unifilar_sds_access}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('destination_client_delivery_interface') !== -1 ? true : false}
              fullWidth
              label="Interface de Entrega al Cliente"
              name="destination_client_delivery_interface"
              onChange={event =>
                handleFieldChange(
                  event,
                  'destination_client_delivery_interface',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.destinationPoint?.uMInfo?.destination_client_delivery_interface}
              variant="outlined"
            >
              <option
                value=""
              />
              {interfaces.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('destination_requires_visit_civil_work') !== -1 ? true : false}
              fullWidth
              label="¿Requiere Visita Obra Civil (VOC)?"
              name="destination_requires_visit_civil_work"
              onChange={event =>
                handleFieldChange(
                  event,
                  'destination_requires_visit_civil_work',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.destinationPoint?.uMInfo?.destination_requires_visit_civil_work}
              variant="outlined"
            >
              <option
                value=""
              />
              {yesNo.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('destination_schedule_visit_civil_work') !== -1 ? true : false}
              fullWidth
              label="Programación Visita Obra Civil (VOC)"
              name="destination_schedule_visit_civil_work"
              onChange={event =>
                handleFieldChange(
                  event,
                  'destination_schedule_visit_civil_work',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.destinationPoint?.uMInfo?.destination_schedule_visit_civil_work}
              variant="outlined"
            >
              <option
                value=""
              />
              {programming.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

Baseline.propTypes = {
  className: PropTypes.string
};

export default Baseline;
