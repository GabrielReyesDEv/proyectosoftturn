import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  TextField,
  Grid
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { saveFormKickoff } from 'actions';

const useStyles = makeStyles(theme => ({
  root: {},
  field: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  }
}));


const ContactInfoWorkOrder = props => {
  const { className, ...rest } = props;
  const classes = useStyles();
  const dispatch = useDispatch();
  const { formKickoff, checkKickOff } = useSelector(state => state.formKickoff);

  const handleFieldChange = (event, field) => {
    event.persist && event.persist();
    document.getElementsByName(field)[0].parentNode.parentNode.classList.remove('selected_error');

    dispatch(saveFormKickoff({
      ...formKickoff,
      originPoint: {
        ...formKickoff.originPoint,
        contactInfoWorkOrder: {
          ...formKickoff.originPoint.contactInfoWorkOrder,
          [field]: event.target.value,
        }
      }
    }));
  };

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader title="Aprueba costos de OC e inicio de facturación de orden de trabajo" />
      <CardContent>
        <Grid
          container
          spacing={4}
        >
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('origin_full_name_CW') !== -1 ? true : false}
              fullWidth
              label="Nombre Completo"
              name="origin_full_name_CW"
              onChange={event =>
                handleFieldChange(event, 'origin_full_name_CW', event.target.value)
              }
              value={formKickoff?.originPoint?.contactInfoWorkOrder?.origin_full_name_CW}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              helperText="Ej. (1) 6500800 Ext 1213"
              label="Telefono Fijo"
              name="origin_phone_CW"
              onChange={event =>
                handleFieldChange(event, 'origin_phone_CW', event.target.value)
              }
              value={formKickoff?.originPoint?.contactInfoWorkOrder?.origin_phone_CW}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('origin_mobile_number_CW') !== -1 ? true : false}
              fullWidth
              label="Celular"
              name="origin_mobile_number_CW"
              onChange={event =>
                handleFieldChange(event, 'origin_mobile_number_CW', event.target.value)
              }
              type="number"
              value={formKickoff?.originPoint?.contactInfoWorkOrder?.origin_mobile_number_CW}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('origin_email_CW') !== -1 ? true : false}
              fullWidth
              helperText="Agrega correos separados por punto y coma (;)"
              label="Correo(s)"
              name="origin_email_CW"
              onChange={event =>
                handleFieldChange(event, 'origin_email_CW', event.target.value)
              }
              value={formKickoff?.originPoint?.contactInfoWorkOrder?.origin_email_CW}
              variant="outlined"
            />
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

ContactInfoWorkOrder.propTypes = {
  className: PropTypes.string
};

export default ContactInfoWorkOrder;
