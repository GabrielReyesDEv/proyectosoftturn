import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  TextField,
  Grid
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { saveFormKickoff } from 'actions';

const useStyles = makeStyles(theme => ({
  root: {},
  alert: {
    marginBottom: theme.spacing(3)
  },
  formGroup: {
    marginBottom: theme.spacing(3)
  },
  fieldGroup: {
    display: 'flex',
    alignItems: 'center'
  },
  addIcon: {
    margin: theme.spacing(1)
  },
  fieldHint: {
    margin: theme.spacing(1, 0)
  },
  addButton: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  tags: {
    marginTop: theme.spacing(1),
    '& > * + *': {
      marginLeft: theme.spacing(1)
    }
  },
  flexGrow: {
    flexGrow: 1
  },
  dateField: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  }
}));

const BasicData = props => {
  const { className, ...rest } = props;

  const classes = useStyles();
  const dispatch = useDispatch();
  const { formKickoff, checkKickOff, inputsOtp, otpInfo } = useSelector(state => state.formKickoff);

  React.useEffect(() => {
    if (!formKickoff?.originPoint?.basicData?.sourceAddress) {
      document.getElementsByName('origin_city')[0].parentNode.parentNode.classList.remove('selected_error');
      document.getElementsByName('sourceAddress')[0].parentNode.parentNode.classList.remove('selected_error');
      dispatch(saveFormKickoff({
        originPoint: {
          basicData: {
            origin_city : otpInfo.city,
            sourceAddress: otpInfo.sourceAddress,
            projectId: otpInfo.projectId,
          }
        }
      }));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const statusOptions = ['Edificio', 'Casa'];
  const mlps_type = ['MPLS Avanzado INTRANET  NDS5 +  Monitoreo CPE (Gestión Proactiva)','MPLS Avanzado INTRANET NDS4 +  Monitoreo CPE (Gestión Proactiva)','MPLS Avanzado INTRANET NDS3 +  Monitoreo CPE (Gestión Proactiva)','MPLS Avanzado INTRANET  NDS2 +  Monitoreo CPE (Gestión Proactiva)','MPLS Avanzado INTRANET  NDS1 +  Monitoreo CPE (Gestión Proactiva)','MPLS Avanzado EXTRANET  NDS6 +  Monitoreo CPE (Gestión Proactiva)','MPLS Avanzado EXTRANET  NDS5 +  Monitoreo CPE (Gestión Proactiva)','MPLS Avanzado EXTRANET  NDS4 +  Monitoreo CPE (Gestión Proactiva)','MPLS Avanzado EXTRANET  NDS3 +  Monitoreo CPE (Gestión Proactiva)','MPLS Avanzado EXTRANET  NDS2 +  Monitoreo CPE (Gestión Proactiva)','MPLS Avanzado EXTRANET  NDS1 +  Monitoreo CPE (Gestión Proactiva)','MPLS Avanzado Solución Punta Backend Triara','MPLS Avanzado Solución Punta Backend Ortezal','MPLS Avanzado Componente Datacenter (con Punta en Rack de Appliance) ','MPLS Avanzado Claro Connect (con Punta Cloud)','MPLS Transaccional - Solución Fibra','MPLS Transaccional - Solución HFC','MPLS Transaccional - Solución 3G','IP Data Internacional','Backup de Ultima Milla Fibra','Backup de Ultima Milla Fibra  + Router','Backup de Ultima Milla HFC','Backup de Ultima Milla 3G','Backup de Ultima Milla Terceros','SI - Requiere Factibilidad de Preventa','MPLS Avanzado INTRANET  NDS6 +  Monitoreo CPE (Gestión Proactiva)']
  const instalationType = ['Instalar UM con PE','Instalar UM con PE sobre OTP de Pymes','Instalar UM con CT','Instalar UM con HFC','Instalar UM con 3G','Instalar UM en Datacenter Claro- Implementación','UM existente. Requiere Cambio de equipo','UM existente. Requiere Adición de equipo','UM existente. Solo configuración'];

  const handleFieldChange = (event, field) => {
    event.persist && event.persist();
    var input = inputsOtp.includes(field);
    if (input === true) document.getElementsByName(field)[0].parentNode.parentNode.classList.add('otp_change');
    document.getElementsByName(field)[0].parentNode.parentNode.classList.remove('selected_error');

    dispatch(saveFormKickoff({
      ...formKickoff,
      originPoint: {
        ...formKickoff.originPoint,
        basicData: {
          ...formKickoff.originPoint.basicData,
          [field]: event.target.value,
        }
      }
    }));

  };

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader title="Datos Basicos de instalacion" />
      <CardContent>
        <Grid
          container
          spacing={4}
        >
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('origin_city') !== -1 ? true : false}
              fullWidth
              label="CIUDAD"
              name="origin_city"
              onChange={event =>
                handleFieldChange(event, 'origin_city', event.target.value)
              }
              value={formKickoff?.originPoint?.basicData?.origin_city}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={6}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('sourceAddress') !== -1 ? true : false}
              fullWidth
              helperText="Especificar barrio, piso u oficina"
              label="Direccion"
              name="sourceAddress"
              onChange={event =>
                handleFieldChange(event, 'sourceAddress', event.target.value)
              }
              value={formKickoff?.originPoint?.basicData?.sourceAddress}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('origin_predio_type') !== -1 ? true : false}
              fullWidth
              label="TIPO PREDIO"
              name="origin_predio_type"
              onChange={event =>
                handleFieldChange(
                  event,
                  'origin_predio_type',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.originPoint?.basicData?.origin_predio_type}
              variant="outlined"
            >
              <option
                value=""
              />
              {statusOptions.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('origin_nit') !== -1 ? true : false}
              fullWidth
              label="Nit del Cliente"
              name="origin_nit"
              onChange={event =>
                handleFieldChange(event, 'origin_nit', event.target.value)
              }
              value={formKickoff?.originPoint?.basicData?.origin_nit}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('origin_alias') !== -1 ? true : false}
              fullWidth
              helperText="CODIGO DE SERVICIO/CIUDAD/SERVICIO/COMERCIO O SEDE DEL CLIENTE"
              label="Alias del Lugar"
              name="origin_alias"
              onChange={event =>
                handleFieldChange(event, 'origin_alias', event.target.value)
              }
              value={formKickoff?.originPoint?.basicData?.origin_alias}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              disabled
              fullWidth
              label="OTP"
              name="projectId"
              onChange={event =>
                handleFieldChange(event, 'projectId', event.target.value)
              }
              value={formKickoff?.originPoint?.basicData?.projectId}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('origin_otp_asociada') !== -1 ? true : false}
              fullWidth
              label="OTP Asociada"
              name="origin_otp_asociada"
              onChange={event =>
                handleFieldChange(event, 'origin_otp_asociada', event.target.value)
              }
              value={formKickoff?.originPoint?.basicData?.origin_otp_asociada}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={6}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('origin_mlps_type') !== -1 ? true : false}
              fullWidth
              label="Tipo MPLS"
              name="origin_mlps_type"
              onChange={event =>
                handleFieldChange(
                  event,
                  'origin_mlps_type',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.originPoint?.basicData?.origin_mlps_type}
              variant="outlined"
            >
              <option
                value=""
              />
              {mlps_type.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={6}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('origin_instalation_type') !== -1 ? true : false}
              fullWidth
              label="Tipo de Instalación"
              name="origin_instalation_type"
              onChange={event =>
                handleFieldChange(
                  event,
                  'origin_instalation_type',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.originPoint?.basicData?.origin_instalation_type}
              variant="outlined"
            >
              <option
                value=""
              />
              {instalationType.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('origin_ancho_banda') !== -1 ? true : false}
              fullWidth
              label="Ancho de Banda"
              name="origin_ancho_banda"
              onChange={event =>
                handleFieldChange(event, 'origin_ancho_banda', event.target.value)
              }
              value={formKickoff?.originPoint?.basicData?.origin_ancho_banda}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              helperText="(Aplica para UM Existente)"
              label="ID del servicio actual"
              name="origin_id_current_service"
              onChange={event =>
                handleFieldChange(event, 'origin_id_current_service', event.target.value)
              }
              required
              value={formKickoff?.originPoint?.basicData?.origin_id_current_service}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              helperText="(Aplica solo para enlaces Backup)"
              label="ID del servicio Principal"
              name="origin_id_principal_service"
              onChange={event =>
                handleFieldChange(event, 'origin_id_principal_service', event.target.value)
              }
              required
              value={formKickoff?.originPoint?.basicData?.origin_id_principal_service}
              variant="outlined"
            />
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

BasicData.propTypes = {
  className: PropTypes.string
};

export default BasicData;
