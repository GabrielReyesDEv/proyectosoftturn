import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import moment from 'moment';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  TextField,
  Grid
} from '@material-ui/core';
import { DatePicker } from '@material-ui/pickers';
import { Mpls } from './components'
import { useDispatch, useSelector } from 'react-redux';
import { saveFormKickoff } from 'actions';

const useStyles = makeStyles(theme => ({
  root: {},
  alert: {
    marginBottom: theme.spacing(3)
  },
  formGroup: {
    marginBottom: theme.spacing(3)
  },
  fieldGroup: {
    display: 'flex',
    alignItems: 'center'
  },
  addIcon: {
    margin: theme.spacing(1)
  },
  fieldHint: {
    margin: theme.spacing(1, 0)
  },
  addButton: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  tags: {
    marginTop: theme.spacing(1),
    '& > * + *': {
      marginLeft: theme.spacing(1)
    }
  },
  flexGrow: {
    flexGrow: 1
  },
  dateField: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  }
}));

const Baseline = props => {
  const { className, ...rest } = props;

  const classes = useStyles();
  const dispatch = useDispatch();
  const { formKickoff, otpInfo, inputsOtp, checkKickOff } = useSelector(state => state.formKickoff);
  const { service } = useSelector(state => state.serviceReducer);


  const [calendarTrigger, setCalendarTrigger] = useState(null);

  const handleFieldChange = (event, field) => {
    event.persist && event.persist();
    document.getElementsByName(field)[0].parentNode.parentNode.classList.remove('selected_error');
    var input = inputsOtp.includes(field);
    if (input === true) document.getElementsByName(field)[0].parentNode.parentNode.classList.add('otp_change');
    dispatch(saveFormKickoff({
      ...formKickoff,
      initialReport: {
        ...formKickoff.initialReport,
        [field]: event.target.value,
      }
    }));
  
  };

  const handleCalendarOpen = trigger => {
    setCalendarTrigger(trigger);
  };

  useEffect(() => {
    dispatch(saveFormKickoff({
      initialReport: {
        ...formKickoff?.initialReport,
        client: otpInfo?.client,
      }
    }));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleCalendarChange = () => {};

  const handleCalendarAccept = date => {
    dispatch(saveFormKickoff({
      ...formKickoff,
      initialReport: {
        ...formKickoff.initialReport,
        [calendarTrigger]: date
      }
    }));
  };

  const handleCalendarClose = () => {
    setCalendarTrigger(false);
  };
  const calendarOpen = Boolean(calendarTrigger);
 

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader title="Información" />
      <CardContent>
        <Grid
          container
          spacing={4}
        >
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('fullName') !== -1 ? true : false}
              fullWidth
              label="Nombre"
              name="fullName"
              onChange={event =>
                handleFieldChange(event, 'fullName', event.target.value)
              }
              value={formKickoff?.initialReport?.fullName}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              disabled
              fullWidth
              label="Servicio"
              name="service"
              value={service}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cliente"
              name="client"
              onChange={event =>
                handleFieldChange(event, 'client', event.target.value)
              }
              value={formKickoff?.initialReport?.client}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              disabled
              fullWidth
              label="Fecha Inicio"
              name="startDateIR"
              onClick={() => handleCalendarOpen('startDateIR')}
              value={moment(formKickoff?.baseLine?.startDate).format('DD/MM/YYYY')}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              disabled
              fullWidth
              label="Fecha Entrega Ampliación"
              name="delivery_date_extension"
              onClick={() => handleCalendarOpen('delivery_date_extension')}
              value={moment(formKickoff?.baseLine?.between_service).format('DD/MM/YYYY')}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={6}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              disabled
              fullWidth
              label="Direccion Punto Central"
              name="address_point_center"
              onChange={event =>
                handleFieldChange(event, 'address_point_center', event.target.value)
              }
              value={formKickoff?.basicData?.sourceAddress}
              variant="outlined"
            />
          </Grid>
          {
            (service === 'LAN Administrada' || service === 'PBX Administrada' || service === 'PL ETHERNET') && (
              <>
                <Grid
                  item
                  md={3}
                  xs={12}
                >
                  <TextField
                    className={`${classes.dateField} selected_error`}
                    error={checkKickOff.indexOf('existing') !== -1 ? true : false}
                    fullWidth
                    label="Existente"
                    name="existing"
                    onChange={event =>
                      handleFieldChange(event, 'existing', event.target.value)
                    }
                    value={formKickoff?.initialReport?.existing}
                    variant="outlined"
                  />
                </Grid>
                <Grid
                  item
                  md={3}
                  xs={12}
                >
                  <TextField
                    className={`${classes.dateField} selected_error`}
                    error={checkKickOff.indexOf('new') !== -1 ? true : false}
                    fullWidth
                    label="Nuevo"
                    name="new"
                    onChange={event =>
                      handleFieldChange(event, 'new', event.target.value)
                    }
                    value={formKickoff?.initialReport?.new}
                    variant="outlined"
                  />
                </Grid>
              </>
            )
          }
        </Grid>
        
      </CardContent>
      <DatePicker
        onAccept={handleCalendarAccept}
        onChange={handleCalendarChange}
        onClose={handleCalendarClose}
        open={calendarOpen}
        style={{ display: 'none' }}
        variant="dialog"
      />
    </Card>
  );
};

Baseline.propTypes = {
  className: PropTypes.string
};

export default Baseline;
