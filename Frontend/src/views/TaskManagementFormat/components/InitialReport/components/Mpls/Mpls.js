import React, { useState } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { makeStyles } from '@material-ui/styles';
import {
  TextField,
  Grid
} from '@material-ui/core';
import { DatePicker } from '@material-ui/pickers';

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  actions: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    '& > * + *': {
      marginLeft: 0
    }
  },
  buttonIcon: {
    marginRight: theme.spacing(1)
  },
  action: {
    margin: 0
  }
}));

const Mpls = () => {

  const classes = useStyles();
  const initialValues = {
    name: '',
    tag: '',
    tags: ['Full-Time', 'ReactJS'],
    startDate: moment(),
    endDate: moment().add(1, 'day')
  };

  const [values, setValues] = useState({ ...initialValues });
  const [calendarTrigger, setCalendarTrigger] = useState(null);

  const handleFieldChange = (event, field, value) => {
    event.persist && event.persist();
    setValues(values => ({
      ...values,
      [field]: value
    }));
  };

  const handleCalendarChange = () => {};

  const handleCalendarAccept = date => {
    setValues(values => ({
      ...values,
      [calendarTrigger]: date
    }));
  };

  const handleCalendarClose = () => {
    setCalendarTrigger(false);
  };

  const calendarOpen = Boolean(calendarTrigger);
  const calendarMinDate =
    calendarTrigger === 'startDate'
      ? moment()
      : moment(values.startDate).add(1, 'day');
  const calendarValue = values[calendarTrigger];

  return (
    <>
      {/* <Grid
        item
        md={6}
        xs={12}
      >
        <TextField
          className={classes.dateField}
          fullWidth
          label="Direccion"
          name="addressMPLS"
          onChange={event =>
            handleFieldChange(event, 'addressMPLS', event.target.value)
          }
          value={values.addressMPLS}
          variant="outlined"
        />
      </Grid>
      <Grid
        item
        md={3}
        xs={12}
      >
        <TextField
          className={classes.dateField}
          fullWidth
          label="Interfaz de Entrega"
          name="delivery_interface"
          onChange={event =>
            handleFieldChange(event, 'delivery_interface', event.target.value)
          }
          value={values.delivery_interface}
          variant="outlined"
        />

      </Grid> */}
      {/* <DatePicker
        minDate={calendarMinDate}
        onAccept={handleCalendarAccept}
        onChange={handleCalendarChange}
        onClose={handleCalendarClose}
        open={calendarOpen}
        style={{ display: 'none' }} // Temporal fix to hide the input element
        value={calendarValue}
        variant="dialog"
      /> */}
    </>
  );
};

Mpls.propTypes = {
  className: PropTypes.string
};

export default Mpls;
