import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  TextField,
  Grid
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { saveFormKickoff } from 'actions';

const useStyles = makeStyles(theme => ({
  root: {},
  alert: {
    marginBottom: theme.spacing(3)
  },
  formGroup: {
    marginBottom: theme.spacing(3)
  },
  fieldGroup: {
    display: 'flex',
    alignItems: 'center'
  },
  addIcon: {
    margin: theme.spacing(1)
  },
  fieldHint: {
    margin: theme.spacing(1, 0)
  },
  addButton: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  tags: {
    marginTop: theme.spacing(1),
    '& > * + *': {
      marginLeft: theme.spacing(1)
    }
  },
  flexGrow: {
    flexGrow: 1
  },
  dateField: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  }
}));

const BasicData = props => {
  const { className, ...rest } = props;

  const classes = useStyles();
  const dispatch = useDispatch();
  const { formKickoff, checkKickOff, inputsOtp, otpInfo } = useSelector(state => state.formKickoff);

  React.useEffect(() => {
    document.getElementsByName('city')[0].parentNode.parentNode.classList.remove('selected_error');
    document.getElementsByName('sourceAddress')[0].parentNode.parentNode.classList.remove('selected_error');
    dispatch(saveFormKickoff({
      basicData: {
        ...formKickoff,
        city : otpInfo.city,
        sourceAddress: otpInfo.sourceAddress,
        projectId: otpInfo.projectId,
      }
    }));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const statusOptions = ['Edificio', 'Casa'];
  const topologyType = [
    'INTERNET DEDICADO (Solución Diferenciación de tráfico (Internet / NAP))',
    'INTERNET DEDICADO (VLR AGRE -Monitoreo CPE (Gestion Proactiva))',
    'INTERNET DEDICADO ADMINISTRADO (VLR AGRE -Monitoreo CPE (Gestion Proactiva))',
    'INTERNET EMPRESARIAL',
    'INTERNET BANDA ANCHA (Solución FO)',
  ];

  const handleFieldChange = (event, field) => {
    event.persist && event.persist();
    var input = inputsOtp.includes(field);
    if (input === true) document.getElementsByName(field)[0].parentNode.parentNode.classList.add('otp_change');
    document.getElementsByName(field)[0].parentNode.parentNode.classList.remove('selected_error');

    dispatch(saveFormKickoff({
      ...formKickoff,
      basicData: {
        ...formKickoff.basicData,
        [field]: event.target.value,
      }
    }));
  };

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader title="Datos Basicos de instalacion" />
      <CardContent>
        <Grid
          container
          spacing={4}
        >
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('city') !== -1 ? true : false}
              fullWidth
              label="CIUDAD"
              name="city"
              onChange={event =>
                handleFieldChange(event, 'city', event.target.value)
              }
              value={formKickoff?.basicData?.city}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={6}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('sourceAddress') !== -1 ? true : false}
              fullWidth
              helperText="Especificar barrio, piso u oficina"
              label="Direccion"
              name="sourceAddress"
              onChange={event =>
                handleFieldChange(event, 'sourceAddress', event.target.value)
              }
              value={formKickoff?.basicData?.sourceAddress}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('predio_type') !== -1 ? true : false}
              fullWidth
              label="TIPO PREDIO"
              name="predio_type"
              onChange={event =>
                handleFieldChange(
                  event,
                  'predio_type',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.basicData?.predio_type}
              variant="outlined"
            >
              <option
                value=""
              />
              {statusOptions.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('nit') !== -1 ? true : false}
              fullWidth
              label="Nit del Cliente"
              name="nit"
              onChange={event =>
                handleFieldChange(event, 'nit', event.target.value)
              }
              value={formKickoff?.basicData?.nit}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('alias') !== -1 ? true : false}
              fullWidth
              helperText="CODIGO DE SERVICIO/CIUDAD/SERVICIO/COMERCIO O SEDE DEL CLIENTE"
              label="Alias del Lugar"
              name="alias"
              onChange={event =>
                handleFieldChange(event, 'alias', event.target.value)
              }
              value={formKickoff?.basicData?.alias}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              disabled
              fullWidth
              label="OTP"
              name="projectId"
              onChange={event =>
                handleFieldChange(event, 'projectId', event.target.value)
              }
              value={formKickoff?.basicData?.projectId}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('otp_asociada') !== -1 ? true : false}
              fullWidth
              label="OTP Asociada"
              name="otp_asociada"
              onChange={event =>
                handleFieldChange(event, 'otp_asociada', event.target.value)
              }
              value={formKickoff?.basicData?.otp_asociada}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('id_current_service') !== -1 ? true : false}
              fullWidth
              helperText="(Aplica para UM Existente)"
              label="ID del servicio actual"
              name="id_current_service"
              onChange={event =>
                handleFieldChange(event, 'id_current_service', event.target.value)
              }
              value={formKickoff?.basicData?.id_current_service}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={6}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('topology') !== -1 ? true : false}
              fullWidth
              label="Topologia"
              name="topology"
              onChange={event =>
                handleFieldChange(
                  event,
                  'topology',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.basicData?.topology}
              variant="outlined"
            >
              <option
                value=""
              />
              {topologyType.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>

        </Grid>
      </CardContent>
    </Card>
  );
};

BasicData.propTypes = {
  className: PropTypes.string
};

export default BasicData;
