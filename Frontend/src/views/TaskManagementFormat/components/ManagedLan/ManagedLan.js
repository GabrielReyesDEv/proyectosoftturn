import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { 
  BasicData, 
  ContactInfoWorkOrder,
  DeliveryServiceRequirement,
  TechnicalContactDetails,
  TechnicalKickoff ,
} from './components';
import { Alert } from 'components';

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(3)
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}));

const ManagedLan = (props) => {

  const { oth } = props; 
  const classes = useStyles();

  return (
    <>
      <Alert
        className={classes.root}
        loader
        message={`Servicio LAN Administrada - OTH ${oth}`}
      />
      <BasicData className={classes.root} />
      <DeliveryServiceRequirement className={classes.root} />
      <Alert
        className={classes.root}
        message={`Datos de contacto para comunicación - OTH ${oth}`}
      />
      <ContactInfoWorkOrder className={classes.root} />
      <TechnicalContactDetails className={classes.root} />
      <Alert
        className={classes.root}
        message={`Kickoff Técnico - OTH ${oth}`}
      />
      <TechnicalKickoff className={classes.root} />
    </>
  );
};

ManagedLan.propTypes = {
  className: PropTypes.string,
  oth: PropTypes.string
};

export default ManagedLan;
