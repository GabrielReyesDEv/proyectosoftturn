import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/styles';
import { Card, CardContent, Grid, TextField, Typography } from '@material-ui/core';
import { saveService, saveFormKickoff, saveCheckKickOff } from 'actions';

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(3)
  },
  title: {
    marginBottom: theme.spacing(3),
    color: '#3f51b5',
    fontWeight: 'bold'
  }
}));

const Header = props => {
  const { className, id, ...rest } = props;

  const { checkKickOff } = useSelector(state => state.formKickoff);

  const dispatch = useDispatch();
  const classes = useStyles();
  const [values, setValues] = useState({});
  const service_facilities = [
    'Internet Dedicado Empresarial',
    'Internet Dedicado',
    'PL ETHERNET',
    'MPLS Avanzado Intranet',
    'MPLS Avanzado Intranet - Varios Puntos',
    'MPLS Avanzado Intranet con Backup de Ultima Milla - NDS 2',
    'MPLS Avanzado Intranet con Backup de Ultima Milla y Router - NDS1',
    'MPLS Avanzado Extranet',
    'Backend MPLS',
    'MPLS Avanzado con Componente Datacenter Claro',
    'MPLS Transaccional 3G',
    'Instalación Servicio Telefonia Fija PBX Distribuida Linea E1',
    'Instalación Servicio Telefonia Fija PBX Distribuida Linea SIP',
    'Instalación Servicio Telefonia Fija PBX Distribuida Linea SIP con Gateway de Voz',
    'Instalación Telefonía Publica Básica - Internet Dedicado',
    'PRIVATE LINE',
    'Punta DATACENTER',
    'LAN ADMINISTRADA',
    'SOLUCIONES ADMINISTRATIVAS - COMUNICACIONES UNIFICADAS PBX ADMINISTRADA',
    'Traslado Externo Servicio',
    'Traslado Interno Servicio',
    'Cambio de Equipos Servicio',
    'Cambio de Servicio Telefonia Fija Pública Linea Basica a Linea E1',
    'Cambio de Servicio Telefonia Fija Pública Linea SIP a PBX Distribuida Linea SIP',
    'Cambio de Última Milla',
    'Cambio de Equipo',
  ];

  const handleFieldChange = (event, field, value) => {
    dispatch(saveCheckKickOff([])); 
    dispatch(saveFormKickoff({}));
    dispatch(saveService(value));
    event.persist && event.persist();
    setValues(values => ({
      ...values,
      [field]: value
    }));
  };

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardContent>
        <Typography
          className={classes.title}
          color="textSecondary"
          gutterBottom
        >
          OTH ID: {id}
        </Typography>
        <Grid
          container
          spacing={4}
        >
          <Grid
            item
            xs={12}
          >
            <TextField
              fullWidth
              label="Seleccione una instalación de servicio"
              name="service_facility"
              onChange={event =>
                handleFieldChange(
                  event,
                  'name',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={values.service_facility}
              variant="outlined"
            >
              <option
                value=""
              />
              {service_facilities.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

Header.propTypes = {
  className: PropTypes.string
};

export default Header;
