import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import moment from 'moment';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  TextField,
  Grid
} from '@material-ui/core';
import { DatePicker } from '@material-ui/pickers';
import { useDispatch, useSelector } from 'react-redux';
import { saveFormKickoff } from 'actions';

const useStyles = makeStyles(theme => ({
  root: {},
  alert: {
    marginBottom: theme.spacing(3)
  },
  formGroup: {
    marginBottom: theme.spacing(3)
  },
  fieldGroup: {
    display: 'flex',
    alignItems: 'center'
  },
  addIcon: {
    margin: theme.spacing(1)
  },
  fieldHint: {
    margin: theme.spacing(1, 0)
  },
  addButton: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  tags: {
    marginTop: theme.spacing(1),
    '& > * + *': {
      marginLeft: theme.spacing(1)
    }
  },
  flexGrow: {
    flexGrow: 1
  },
  dateField: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  }
}));

const Baseline = props => {
  const { className, ...rest } = props;

  const classes = useStyles();
  const dispatch = useDispatch();
  const { formKickoff } = useSelector(state => state.formKickoff);

  // const [values, setValues] = useState({});
  const [calendarTrigger, setCalendarTrigger] = useState(null);

  useEffect(() => {
    dispatch(saveFormKickoff({
      baseLine: {
        ...formKickoff,
        compromise: moment().add(18, 'd').locale('es').format('YYYY-MM-DD'),
        programming: moment().add(18, 'd').locale('es').format('YYYY-MM-DD'),
        setting: moment().add(18, 'd').locale('es').format('YYYY-MM-DD'),
        startDate: moment().format('YYYY-MM-DD'),
        civil_work_view: moment().add(3, 'd').locale('es').format('YYYY-MM-DD'),
        quotation_documentation: moment().add(4, 'd').locale('es').format('YYYY-MM-DD'),
        engineering_detail: moment().add(5, 'd').locale('es').format('YYYY-MM-DD'),
        teams: moment().add(5, 'd').locale('es').format('YYYY-MM-DD'),
        approval_quotation: moment().add(10, 'd').locale('es').format('YYYY-MM-DD'),
        execution_view: moment().add(12, 'd').locale('es').format('YYYY-MM-DD'),
        splices: moment().add(14, 'd').locale('es').format('YYYY-MM-DD'),
        between_service: moment().add(18, 'd').locale('es').format('YYYY-MM-DD'),
        days: '20',
      }
    }));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleCalendarOpen = trigger => {
    setCalendarTrigger(trigger);
  };

  const handleCalendarChange = () => {};


  const handleCalculation = (event) => {
    if (event.target.name === 'startDate' || event.target.name === 'days') {
      let fechaEntrega = '';
      const fechavoc = moment(event.target.name === 'days' ? formKickoff?.baseLine?.startDate : event.target.value).add(3, 'd').locale('es').format('YYYY-MM-DD');
      const fechadcoc = moment(event.target.name === 'days' ? formKickoff?.baseLine?.startDate : event.target.value).add(4, 'd').locale('es').format('YYYY-MM-DD');
      const fechaequipos = moment(event.target.name === 'days' ? formKickoff?.baseLine?.startDate : event.target.value).add(5, 'd').locale('es').format('YYYY-MM-DD');
      const fechaaprobacioncoc = moment(event.target.name === 'days' ? formKickoff?.baseLine?.startDate : event.target.value).add(10, 'd').locale('es').format('YYYY-MM-DD');
      const fechaejecucionobracivil = moment(event.target.name === 'days' ? formKickoff?.baseLine?.startDate : event.target.value).add(12, 'd').locale('es').format('YYYY-MM-DD');
      const fechaempalmes = moment(event.target.name === 'days' ? formKickoff?.baseLine?.startDate : event.target.value).add(14, 'd').locale('es').format('YYYY-MM-DD');
      if (event.target.name === 'startDate') {
        fechaEntrega = moment(event.target.value).add(formKickoff?.baseLine?.days ? formKickoff.baseLine.days - 2 : 18, 'd').locale('es').format('YYYY-MM-DD');
      }
      if (event.target.name === 'days') {
        fechaEntrega = moment(formKickoff?.baseLine?.startDate ? formKickoff.baseLine.startDate : moment()).add(event.target.value - 2, 'd').locale('es').format('YYYY-MM-DD');
      }

      dispatch(saveFormKickoff({
        ...formKickoff,
        baseLine: {
          ...formKickoff.baseLine,
          [event.target.name]: event.target.value,
          between_service: fechaEntrega,
          compromise: fechaEntrega,
          programming: fechaEntrega,
          setting: fechaEntrega,
          civil_work_view: fechavoc,
          quotation_documentation: fechadcoc,
          engineering_detail: fechaequipos,
          teams: fechaequipos,
          approval_quotation: fechaaprobacioncoc,
          execution_view: fechaejecucionobracivil,
          splices: fechaempalmes,
        }
      }));
    } 
  }

  const handleCalendarAccept = date => {

    dispatch(saveFormKickoff({
      ...formKickoff,
      baseLine: {
        ...formKickoff.baseLine,
        [calendarTrigger]: date,
      }
    }));
  };

  const handleCalendarClose = () => {
    setCalendarTrigger(false);
  };

  const calendarOpen = Boolean(calendarTrigger);

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader title="Linea Base" />
      <CardContent>
        <div className={classes.formGroup}>
          <Grid
            container
            spacing={4}
          >
            <Grid
              item
              md={5}
              xs={12}
            >
              <TextField
                fullWidth
                label="Días"
                name="days"
                onChange={handleCalculation}
                type="number"
                value={formKickoff?.baseLine?.days}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={5}
              xs={12}
            >
              <TextField
                className={classes.dateField}
                fullWidth
                InputLabelProps={{
                  shrink: true,
                }}
                label="Inicio"
                name="startDate"
                onChange={handleCalculation}
                type="date"
                value={moment(formKickoff?.baseLine?.startDate).format('YYYY-MM-DD')}
                variant="outlined"
              />
            </Grid>
          </Grid>
        </div>
        <div className={classes.formGroup}>
          <Grid
            container
            spacing={4}
          >
            <Grid
              item
              md={3}
              xs={12}
            >
              <TextField
                className={classes.dateField}
                fullWidth
                label="COMPROMISO"
                name="compromise"
                onClick={() => handleCalendarOpen('compromise')}
                value={moment(formKickoff?.baseLine?.compromise).format('DD/MM/YYYY')}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={3}
              xs={12}
            >
              <TextField
                className={classes.dateField}
                fullWidth
                label="VISITA OBRA CIVIL (VOC)" 
                name="civil_work_view"
                onClick={() => handleCalendarOpen('civil_work_view')}
                value={moment(formKickoff?.baseLine?.civil_work_view).format('DD/MM/YYYY')}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={3}
              xs={12}
            >
              <TextField
                className={classes.dateField}
                fullWidth
                label="PROGRAMACIÓN"
                name="programming"
                onClick={() => handleCalendarOpen('programming')}
                value={moment(formKickoff?.baseLine?.programming).format('DD/MM/YYYY')}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={3}
              xs={12}
            >
              <TextField
                className={classes.dateField}
                fullWidth
                label="DOCUMENTACIÓN COTIZACIÓN OBRA CIVIL"
                name="quotation_documentation"
                onClick={() => handleCalendarOpen('quotation_documentation')}
                value={moment(formKickoff?.baseLine?.quotation_documentation).format('DD/MM/YYYY')}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={3}
              xs={12}
            >
              <TextField
                className={classes.dateField}
                fullWidth
                label="APROBACIÓN COTIZACIÓN OC"
                name="approval_quotation"
                onClick={() => handleCalendarOpen('approval_quotation')}
                value={moment(formKickoff?.baseLine?.approval_quotation).format('DD/MM/YYYY')}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={3}
              xs={12}
            >
              <TextField
                className={classes.dateField}
                fullWidth
                label="CONFIGURACIÓN"
                name="setting"
                onClick={() => handleCalendarOpen('setting')}
                value={moment(formKickoff?.baseLine?.setting).format('DD/MM/YYYY')}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={3}
              xs={12}
            >
              <TextField
                className={classes.dateField}
                fullWidth
                label="INGENIERÍA DETALLE"
                name="engineering_detail"
                onClick={() => handleCalendarOpen('engineering_detail')}
                value={moment(formKickoff?.baseLine?.engineering_detail).format('DD/MM/YYYY')}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={3}
              xs={12}
            >
              <TextField
                className={classes.dateField}
                fullWidth
                label="EQUIPOS"
                name="teams"
                onClick={() => handleCalendarOpen('teams')}
                value={moment(formKickoff?.baseLine?.teams).format('DD/MM/YYYY')}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={3}
              xs={12}
            >
              <TextField
                className={classes.dateField}
                fullWidth
                label="VISITA EJECUCION OBRA CIVIL (EOC)"
                name="execution_view"
                onClick={() => handleCalendarOpen('execution_view')}
                value={moment(formKickoff?.baseLine?.execution_view).format('DD/MM/YYYY')}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={3}
              xs={12}
            >
              <TextField
                className={classes.dateField}
                fullWidth
                label="ENTREGA DE SERVICIO"
                name="between_service"
                onClick={() => handleCalendarOpen('between_service')}
                value={moment(formKickoff?.baseLine?.between_service).format('DD/MM/YYYY')}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={3}
              xs={12}
            >
              <TextField
                className={classes.dateField}
                fullWidth
                label="EMPALMES"
                name="splices"
                onClick={() => handleCalendarOpen('splices')}
                value={moment(formKickoff?.baseLine?.splices).format('DD/MM/YYYY')}
                variant="outlined"
              />
            </Grid>
          </Grid>
        </div>
      </CardContent>
      <DatePicker
        onAccept={handleCalendarAccept}
        onChange={handleCalendarChange}
        onClose={handleCalendarClose}
        open={calendarOpen}
        style={{ display: 'none' }}
        variant="dialog"
      />
    </Card>
  );
};

Baseline.propTypes = {
  className: PropTypes.string
};

export default Baseline;
