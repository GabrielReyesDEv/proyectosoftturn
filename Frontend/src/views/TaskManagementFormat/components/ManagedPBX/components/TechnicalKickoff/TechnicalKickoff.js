import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardContent,
  TextField,
  Grid
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { saveFormKickoff } from 'actions';

const useStyles = makeStyles(theme => ({
  root: {},
  field: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  }
}));

const TechnicalKickoff = props => {
  const { className, ...rest } = props;

  const classes = useStyles();
  const dispatch = useDispatch();
  const { formKickoff, checkKickOff } = useSelector(state => state.formKickoff);

  const number_voice_boxes = ['Cantidad IPs: 2 - Mascara: /30','Cantidad IPs 6 - Mascara: /29','Cantidad IPs 14 - Mascara: /28 - Requiere Viabilidad Preventa','Cantidad Ips: 30 - Mascara: /27 - Requiere Viabilidad Preventa']
  const yes_no = ['No', 'Si'];
  const fixed_phone_claro = ['EXISTENTE','A IMPLEMENTAR'];


  const handleFieldChange = (event, field) => {
    event.persist && event.persist();
    document.getElementsByName(field)[0].parentNode.parentNode.classList.remove('selected_error');

    dispatch(saveFormKickoff({
      ...formKickoff,
      technicalKickoff: {
        ...formKickoff.technicalKickoff,
        [field]: event.target.value,
      }
    }));
                                                                                                                                         
  };

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardContent>
        <Grid
          container
          spacing={4}
        >
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('fixed_phone_claro') !== -1 ? true : false}
              fullWidth
              label="TELEFONIA FIJA CLARO"
              name="fixed_phone_claro"
              onChange={event =>
                handleFieldChange(
                  event,
                  'fixed_phone_claro',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.technicalKickoff?.fixed_phone_claro}
              variant="outlined"
            >
              <option
                value=""
              />
              {fixed_phone_claro.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('quantity_extensions') !== -1 ? true : false}
              fullWidth
              label="CANTIDAD DE EXTENSIONES"
              name="quantity_extensions"
              onChange={event =>
                handleFieldChange(event, 'quantity_extensions', event.target.value)
              }
              value={formKickoff?.technicalKickoff?.quantity_extensions}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('number_voice_boxes') !== -1 ? true : false}
              fullWidth
              label="CANTIDAD DE BUZONES VOZ"
              name="number_voice_boxes"
              onChange={event =>
                handleFieldChange(
                  event,
                  'number_voice_boxes',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.technicalKickoff?.number_voice_boxes}
              variant="outlined"
            >
              <option
                value=""
              />
              {number_voice_boxes.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('includes_goat_voice') !== -1 ? true : false}
              fullWidth
              label="INCLUYE GRABACIÓN DE VOZ"
              name="includes_goat_voice"
              onChange={event =>
                handleFieldChange(
                  event,
                  'includes_goat_voice',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.technicalKickoff?.includes_goat_voice}
              variant="outlined"
            >
              <option
                value=""
              />
              {yes_no.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('include_lan_managed') !== -1 ? true : false}
              fullWidth
              label="INCLUYE LAN ADMINISTRADA"
              name="include_lan_managed"
              onChange={event =>
                handleFieldChange(
                  event,
                  'include_lan_managed',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.technicalKickoff?.include_lan_managed}
              variant="outlined"
            >
              <option
                value=""
              />
              {yes_no.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

TechnicalKickoff.propTypes = {
  className: PropTypes.string
};

export default TechnicalKickoff;
