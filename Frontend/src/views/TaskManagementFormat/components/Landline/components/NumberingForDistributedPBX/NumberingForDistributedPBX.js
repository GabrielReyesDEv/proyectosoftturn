import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  TextField,
  Grid,
  Typography
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { saveFormKickoff } from 'actions';

const useStyles = makeStyles(theme => ({
  root: {},
  alert: {
    marginBottom: theme.spacing(3)
  },
  formGroup: {
    marginBottom: theme.spacing(3)
  },
  fieldGroup: {
    display: 'flex',
    alignItems: 'center'
  },
  addIcon: {
    margin: theme.spacing(1)
  },
  fieldHint: {
    margin: theme.spacing(1, 0)
  },
  addButton: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  tags: {
    marginTop: theme.spacing(1),
    '& > * + *': {
      marginLeft: theme.spacing(1)
    }
  },
  flexGrow: {
    flexGrow: 1
  },
  dateField: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  }
}));

const NumberingForDistributedPBX = props => {
  const { className, ...rest } = props;

  const classes = useStyles();
  const dispatch = useDispatch();
  const { formKickoff } = useSelector(state => state.formKickoff);

  const numbering_assigned = ['Si', 'NO - Escalar a Soporte Comercial', 'NO - Escalar a Soporte Comercial'];

  const handleFieldChange = (event, field, value, city, cityVal) => {
    event.persist && event.persist();

    dispatch(saveFormKickoff({
      ...formKickoff,
      numberingForDistributedPBX: {
        ...formKickoff.numberingForDistributedPBX,
        [city]: {...cityVal, [field]: value}
      }
    }));

  };

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader title="Numeracion Solo Diligenciar Para La Opcion de PBX Distribuido" />
      <CardContent>
        {/* ***************************************BOGOTA********************************************/}
        <Grid
          container
          spacing={4}
        >
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              Bogotá:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'bogota',
                  formKickoff?.numberingForDistributedPBX?.bogota,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.bogota?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'bogota',
                  formKickoff?.numberingForDistributedPBX?.bogota,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.bogota?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(event, 
                  'quantity_did', 
                  event.target.value,
                  'bogota',
                  formKickoff?.numberingForDistributedPBX?.bogota,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.bogota?.quantity_did}
              variant="outlined"
            />
          </Grid>
          {/* ***************************************TUNJA********************************************/}
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              Tunja:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'tunja',
                  formKickoff?.numberingForDistributedPBX?.tunja,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.tunja?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'tunja',
                  formKickoff?.numberingForDistributedPBX?.tunja,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.tunja?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(
                  event, 
                  'quantity_did',
                  event.target.value,
                  'tunja',
                  formKickoff?.numberingForDistributedPBX?.tunja,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.tunja?.quantity_did}
              variant="outlined"
            />
          </Grid>
          {/* ***************************************VILLAVUICENCIO********************************************/}
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              Villavicencio:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'villavicencio',
                  formKickoff?.numberingForDistributedPBX?.villavicencio,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.villavicencio?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'villavicencio',
                  formKickoff?.numberingForDistributedPBX?.villavicencio,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.villavicencio?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(
                  event, 
                  'quantity_did',
                  event.target.value,
                  'villavicencio',
                  formKickoff?.numberingForDistributedPBX?.villavicencio,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.villavicencio?.quantity_did}
              variant="outlined"
            />
          </Grid>
          {/* ***************************************FACATATIVA********************************************/}
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              Facatativa:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'facatativa',
                  formKickoff?.numberingForDistributedPBX?.facatativa,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.facatativa?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'facatativa',
                  formKickoff?.numberingForDistributedPBX?.facatativa,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.facatativa?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(
                  event, 
                  'quantity_did',
                  event.target.value,
                  'facatativa',
                  formKickoff?.numberingForDistributedPBX?.facatativa,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.facatativa?.quantity_did}
              variant="outlined"
            />
          </Grid>
          {/* ***************************************GIRARDOT********************************************/}
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              Girardot:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'girardot',
                  formKickoff?.numberingForDistributedPBX?.girardot,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.girardot?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'girardot',
                  formKickoff?.numberingForDistributedPBX?.girardot,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.girardot?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(
                  event, 
                  'quantity_did',
                  event.target.value,
                  'girardot',
                  formKickoff?.numberingForDistributedPBX?.girardot,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.girardot?.quantity_did}
              variant="outlined"
            />
          </Grid>
          {/* ***************************************YOPAL********************************************/}
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              Yopal:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'yopal',
                  formKickoff?.numberingForDistributedPBX?.yopal,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.yopal?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'yopal',
                  formKickoff?.numberingForDistributedPBX?.yopal,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.yopal?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(
                  event, 
                  'quantity_did',
                  event.target.value,
                  'yopal',
                  formKickoff?.numberingForDistributedPBX?.yopal,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.yopal?.quantity_did}
              variant="outlined"
            />
          </Grid>
          {/* ***************************************CALI********************************************/}
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              Cali:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'cali',
                  formKickoff?.numberingForDistributedPBX?.cali,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.cali?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'cali',
                  formKickoff?.numberingForDistributedPBX?.cali,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.cali?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(
                  event, 
                  'quantity_did',
                  event.target.value,
                  'cali',
                  formKickoff?.numberingForDistributedPBX?.cali,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.cali?.quantity_did}
              variant="outlined"
            />
          </Grid>
          {/* ***************************************BUENABENTURA********************************************/}
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              Buenaventura:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'buenaventura',
                  formKickoff?.numberingForDistributedPBX?.buenaventura,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.buenaventura?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'buenaventura',
                  formKickoff?.numberingForDistributedPBX?.buenaventura,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.buenaventura?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(
                  event, 
                  'quantity_did',
                  event.target.value,
                  'buenaventura',
                  formKickoff?.numberingForDistributedPBX?.buenaventura,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.buenaventura?.quantity_did}
              variant="outlined"
            />
          </Grid>
          {/* ***************************************PASTO********************************************/}
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              Pasto:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'pasto',
                  formKickoff?.numberingForDistributedPBX?.pasto,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.pasto?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'pasto',
                  formKickoff?.numberingForDistributedPBX?.pasto,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.pasto?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(
                  event, 
                  'quantity_did',
                  event.target.value,
                  'pasto',
                  formKickoff?.numberingForDistributedPBX?.pasto,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.pasto?.quantity_did}
              variant="outlined"
            />
          </Grid>
          {/* ***************************************POPAYAN********************************************/}
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              Popayán:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'popayan',
                  formKickoff?.numberingForDistributedPBX?.popayan,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.popayan?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'popayan',
                  formKickoff?.numberingForDistributedPBX?.popayan,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.popayan?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(
                  event, 
                  'quantity_did',
                  event.target.value,
                  'popayan',
                  formKickoff?.numberingForDistributedPBX?.popayan,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.popayan?.quantity_did}
              variant="outlined"
            />
          </Grid>
          {/* ***************************************NEIVA********************************************/}
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              Neiva:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'neiva',
                  formKickoff?.numberingForDistributedPBX?.neiva,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.neiva?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'neiva',
                  formKickoff?.numberingForDistributedPBX?.neiva,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.neiva?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(
                  event, 
                  'quantity_did',
                  event.target.value,
                  'neiva',
                  formKickoff?.numberingForDistributedPBX?.neiva,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.neiva?.quantity_did}
              variant="outlined"
            />
          </Grid>
          {/* ***************************************MEDELLIN********************************************/}
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              MedellÍn:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'medellin',
                  formKickoff?.numberingForDistributedPBX?.medellin,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.medellin?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'medellin',
                  formKickoff?.numberingForDistributedPBX?.medellin,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.medellin?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(
                  event, 
                  'quantity_did',
                  event.target.value,
                  'medellin',
                  formKickoff?.numberingForDistributedPBX?.medellin,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.medellin?.quantity_did}
              variant="outlined"
            />
          </Grid>
          {/* ***************************************BARRANQUILLA********************************************/}
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              Barranquilla:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'barranquilla',
                  formKickoff?.numberingForDistributedPBX?.barranquilla,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.barranquilla?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'barranquilla',
                  formKickoff?.numberingForDistributedPBX?.barranquilla,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.barranquilla?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(
                  event, 
                  'quantity_did',
                  event.target.value,
                  'barranquilla',
                  formKickoff?.numberingForDistributedPBX?.barranquilla,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.barranquilla?.quantity_did}
              variant="outlined"
            />
          </Grid>
          {/* ***************************************CARTAGENA********************************************/}
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              Cartagena:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'cartagena',
                  formKickoff?.numberingForDistributedPBX?.cartagena,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.cartagena?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'cartagena',
                  formKickoff?.numberingForDistributedPBX?.cartagena,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.cartagena?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(
                  event, 
                  'quantity_did',
                  event.target.value,
                  'cartagena',
                  formKickoff?.numberingForDistributedPBX?.cartagena,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.cartagena?.quantity_did}
              variant="outlined"
            />
          </Grid>
          {/* ***************************************SANTA MARTA********************************************/}
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              Santa Marta:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'santaMarta',
                  formKickoff?.numberingForDistributedPBX?.santaMarta,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.santaMarta?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'santaMarta',
                  formKickoff?.numberingForDistributedPBX?.santaMarta,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.santaMarta?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(
                  event, 
                  'quantity_did',
                  event.target.value,
                  'santaMarta',
                  formKickoff?.numberingForDistributedPBX?.santaMarta,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.santaMarta?.quantity_did}
              variant="outlined"
            />
          </Grid>
          {/* ***************************************MONTERIA********************************************/}
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              Montería:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'monteria',
                  formKickoff?.numberingForDistributedPBX?.monteria,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.monteria?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'monteria',
                  formKickoff?.numberingForDistributedPBX?.monteria,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.monteria?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(
                  event, 
                  'quantity_did',
                  event.target.value,
                  'monteria',
                  formKickoff?.numberingForDistributedPBX?.monteria,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.monteria?.quantity_did}
              variant="outlined"
            />
          </Grid>
          {/* ***************************************VALLEDUPAR********************************************/}
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              Valledupar:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'valledupar',
                  formKickoff?.numberingForDistributedPBX?.valledupar,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.valledupar?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'valledupar',
                  formKickoff?.numberingForDistributedPBX?.valledupar,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.valledupar?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(
                  event, 
                  'quantity_did',
                  event.target.value,
                  'valledupar',
                  formKickoff?.numberingForDistributedPBX?.valledupar,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.valledupar?.quantity_did}
              variant="outlined"
            />
          </Grid>
          {/* ***************************************SINCELEJO********************************************/}
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              Sincelejo:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'sincelejo',
                  formKickoff?.numberingForDistributedPBX?.sincelejo,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.sincelejo?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'sincelejo',
                  formKickoff?.numberingForDistributedPBX?.sincelejo,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.sincelejo?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(
                  event, 
                  'quantity_did',
                  event.target.value,
                  'sincelejo',
                  formKickoff?.numberingForDistributedPBX?.sincelejo,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.sincelejo?.quantity_did}
              variant="outlined"
            />
          </Grid>
          {/* ***************************************PEREIRA********************************************/}
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              Pereira:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'pereira',
                  formKickoff?.numberingForDistributedPBX?.pereira,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.pereira?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'pereira',
                  formKickoff?.numberingForDistributedPBX?.pereira,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.pereira?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(
                  event, 
                  'quantity_did',
                  event.target.value,
                  'pereira',
                  formKickoff?.numberingForDistributedPBX?.pereira,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.pereira?.quantity_did}
              variant="outlined"
            />
          </Grid>
          {/* ***************************************ARMENIA********************************************/}
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              Armenia:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'armenia',
                  formKickoff?.numberingForDistributedPBX?.armenia,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.armenia?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'armenia',
                  formKickoff?.numberingForDistributedPBX?.armenia,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.armenia?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(
                  event, 
                  'quantity_did',
                  event.target.value,
                  'armenia',
                  formKickoff?.numberingForDistributedPBX?.armenia,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.armenia?.quantity_did}
              variant="outlined"
            />
          </Grid>
          {/* ***************************************MANIZALES********************************************/}
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              Manizales:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'manizales',
                  formKickoff?.numberingForDistributedPBX?.manizales,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.manizales?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'manizales',
                  formKickoff?.numberingForDistributedPBX?.manizales,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.manizales?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(
                  event, 
                  'quantity_did',
                  event.target.value,
                  'manizales',
                  formKickoff?.numberingForDistributedPBX?.manizales,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.manizales?.quantity_did}
              variant="outlined"
            />
          </Grid>
          {/* ***************************************IBAGUE********************************************/}
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              Ibagué:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'ibague',
                  formKickoff?.numberingForDistributedPBX?.ibague,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.ibague?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'ibague',
                  formKickoff?.numberingForDistributedPBX?.ibague,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.ibague?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(
                  event, 
                  'quantity_did',
                  event.target.value,
                  'ibague',
                  formKickoff?.numberingForDistributedPBX?.ibague,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.ibague?.quantity_did}
              variant="outlined"
            />
          </Grid>
          {/* ***************************************CUCUTA********************************************/}
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              Cucutá:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'cucuta',
                  formKickoff?.numberingForDistributedPBX?.cucuta,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.cucuta?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'cucuta',
                  formKickoff?.numberingForDistributedPBX?.cucuta,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.cucuta?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(
                  event, 
                  'quantity_did',
                  event.target.value,
                  'cucuta',
                  formKickoff?.numberingForDistributedPBX?.cucuta,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.cucuta?.quantity_did}
              variant="outlined"
            />
          </Grid>
          {/* ***************************************BUCARAMANGA********************************************/}
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              Bucaramanga:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'bucaramanga',
                  formKickoff?.numberingForDistributedPBX?.bucaramanga,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.bucaramanga?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'bucaramanga',
                  formKickoff?.numberingForDistributedPBX?.bucaramanga,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.bucaramanga?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(
                  event, 
                  'quantity_did',
                  event.target.value,
                  'bucaramanga',
                  formKickoff?.numberingForDistributedPBX?.bucaramanga,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.bucaramanga?.quantity_did}
              variant="outlined"
            />
          </Grid>
          {/* ***************************************DUITAMA********************************************/}
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              Duitama:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'duitama',
                  formKickoff?.numberingForDistributedPBX?.duitama,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.duitama?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'duitama',
                  formKickoff?.numberingForDistributedPBX?.duitama,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.duitama?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(
                  event, 
                  'quantity_did',
                  event.target.value,
                  'duitama',
                  formKickoff?.numberingForDistributedPBX?.duitama,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.duitama?.quantity_did}
              variant="outlined"
            />
          </Grid>
          {/* ***************************************SOGAMOSO********************************************/}
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              Sogamoso:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'sogamoso',
                  formKickoff?.numberingForDistributedPBX?.sogamoso,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.sogamoso?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'sogamoso',
                  formKickoff?.numberingForDistributedPBX?.sogamoso,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.sogamoso?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(
                  event, 
                  'quantity_did',
                  event.target.value,
                  'sogamoso',
                  formKickoff?.numberingForDistributedPBX?.sogamoso,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.sogamoso?.quantity_did}
              variant="outlined"
            />
          </Grid>
          {/* ***************************************FLANDES********************************************/}
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              Flandes:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'flandes',
                  formKickoff?.numberingForDistributedPBX?.flandes,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.flandes?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'flandes',
                  formKickoff?.numberingForDistributedPBX?.flandes,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.flandes?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(
                  event, 
                  'quantity_did',
                  event.target.value,
                  'flandes',
                  formKickoff?.numberingForDistributedPBX?.flandes,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.flandes?.quantity_did}
              variant="outlined"
            />
          </Grid>
          {/* ***************************************RIVERA********************************************/}
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              Rivera:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'rivera',
                  formKickoff?.numberingForDistributedPBX?.rivera,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.rivera?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'rivera',
                  formKickoff?.numberingForDistributedPBX?.rivera,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.rivera?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(
                  event, 
                  'quantity_did',
                  event.target.value,
                  'rivera',
                  formKickoff?.numberingForDistributedPBX?.rivera,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.rivera?.quantity_did}
              variant="outlined"
            />
          </Grid>
          {/* ***************************************AIPE********************************************/}
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              Aipe:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'aipe',
                  formKickoff?.numberingForDistributedPBX?.aipe,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.aipe?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'aipe',
                  formKickoff?.numberingForDistributedPBX?.aipe,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.aipe?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(
                  event, 
                  'quantity_did',
                  event.target.value,
                  'aipe',
                  formKickoff?.numberingForDistributedPBX?.aipe,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.aipe?.quantity_did}
              variant="outlined"
            />
          </Grid>
          {/* ***************************************LEBRIJA********************************************/}
          <Grid
            className={classes.addButton}
            item
            md={2}
            xs={12}
          >
            <Typography
              gutterBottom
              variant="h4"
            >
              Lebrija:
            </Typography>

          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="REQUIERE"
              name="requires"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires',
                  event.target.value,
                  'lebrija',
                  formKickoff?.numberingForDistributedPBX?.lebrija,
                )
              }
              select
              value={formKickoff?.numberingForDistributedPBX?.lebrija?.requires}
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numeracion Asignada en TAB"
              name="numbering_assigned_TAB"
              onChange={event =>
                handleFieldChange(
                  event,
                  'numbering_assigned_TAB',
                  event.target.value,
                  'lebrija',
                  formKickoff?.numberingForDistributedPBX?.lebrija,
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.numberingForDistributedPBX?.lebrija?.numbering_assigned_TAB}
              variant="outlined"
            >
              <option
                value=""
              />
              {numbering_assigned.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.dateField}
              fullWidth
              label="Cantidad DID"
              name="quantity_did"
              onChange={event =>
                handleFieldChange(
                  event, 
                  'quantity_did',
                  event.target.value,
                  'lebrija',
                  formKickoff?.numberingForDistributedPBX?.lebrija,
                )
              }
              value={formKickoff?.numberingForDistributedPBX?.lebrija?.quantity_did}
              variant="outlined"
            />
          </Grid>
                                                                                                                                                                                                                                                                                                  
        </Grid>
      </CardContent>
    </Card>
  );
};

NumberingForDistributedPBX.propTypes = {
  className: PropTypes.string
};

export default NumberingForDistributedPBX;
