import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardContent,
  TextField,
  Grid
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { saveFormKickoff } from 'actions';

const useStyles = makeStyles(theme => ({
  root: {},
  field: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  }
}));

const TechnicalKickoff = props => {
  const { className, ...rest } = props;

  const classes = useStyles();
  const dispatch = useDispatch();
  const { formKickoff, checkKickOff } = useSelector(state => state.formKickoff);

  const client_team = ['Teléfonos analogos','Planta E1','Planta IP']
  const yes_no = ['No', 'Si'];
  const client_equipment_interface = ['FXS','RJ11','RJ45','RJ48','BNC'];
  const requeriments = ['SI','NO','No hay Survey Adjunto - En espera de Respuesta a reporte de Inicio de Kickoff'];


  const handleFieldChange = (event, field) => {
    event.persist && event.persist();
    document.getElementsByName(field)[0].parentNode.parentNode.classList.remove('selected_error');
    
    dispatch(saveFormKickoff({
      ...formKickoff,
      technicalKickoff: {
        ...formKickoff.technicalKickoff,
        [field]: event.target.value,
      }
    }));
                                                                                                                                         
  };

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardContent>
        <Grid
          container
          spacing={4}
        >
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              helperText="CON COSTO (0 $)"
              label="Activación de PLAN LD "
              name="activation_plan_ld"
              onChange={event =>
                handleFieldChange(
                  event,
                  'activation_plan_ld',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.technicalKickoff?.activation_plan_ld}
              variant="outlined"
            >
              <option
                value=""
              />
              {yes_no.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="Equipo Cliente "
              name="client_team"
              onChange={event =>
                handleFieldChange(
                  event,
                  'client_team',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.technicalKickoff?.client_team}
              variant="outlined"
            >
              <option
                value=""
              />
              {client_team.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="Interfaz Equipos Cliente"
              name="client_equipment_interface"
              onChange={event =>
                handleFieldChange(
                  event,
                  'client_equipment_interface',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.technicalKickoff?.client_equipment_interface}
              variant="outlined"
            >
              <option
                value=""
              />
              {client_equipment_interface.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              helperText="(Solo Telefonia Pública Líneas Análogas)"
              label="Cantidad Lineas Básicas"
              name="quantities_basic_lines"
              onChange={event =>
                handleFieldChange(event, 'quantities_basic_lines', event.target.value)
              }
              value={formKickoff?.technicalKickoff?.quantities_basic_lines}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              helperText="(Solo Telefonia Pública Líneas Análogas)"
              label="Conformación PBX"
              name="conformation_pbx"
              onChange={event =>
                handleFieldChange(event, 'conformation_pbx', event.target.value)
              }
              value={formKickoff?.technicalKickoff?.conformation_pbx}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('quantity_did_requested') !== -1 ? true : false}
              fullWidth
              label="Cantidad de DID Solicitados"
              name="quantity_did_requested"
              onChange={event =>
                handleFieldChange(event, 'quantity_did_requested', event.target.value)
              }
              value={formKickoff?.technicalKickoff?.quantity_did_requested}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="Cantidad Canales"
              name="number_channels"
              onChange={event =>
                handleFieldChange(event, 'number_channels', event.target.value)
              }
              value={formKickoff?.technicalKickoff?.number_channels}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="Numero Cabecera PBX"
              name="header_number_pbx"
              onChange={event =>
                handleFieldChange(
                  event,
                  'header_number_pbx',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.technicalKickoff?.header_number_pbx}
              variant="outlined"
            >
              <option
                value=""
              />
              {yes_no.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="FAX TO MAIL"
              name="fax_to_mail"
              onChange={event =>
                handleFieldChange(
                  event,
                  'fax_to_mail',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.technicalKickoff?.fax_to_mail}
              variant="outlined"
            >
              <option
                value=""
              />
              {yes_no.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="TELEFONO VIRTUAL"
              name="virtual_phone"
              onChange={event =>
                handleFieldChange(
                  event,
                  'virtual_phone',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.technicalKickoff?.virtual_phone}
              variant="outlined"
            >
              <option
                value=""
              />
              {['SI (SOLICITAR LICENCIA A LIDER TECNICO GRUPO ASE)', 'NO'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              helperText="Larga Distancia Nacional"
              label="Requiere Permisos"
              name="requires_national_permissions"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires_national_permissions',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.technicalKickoff?.requires_national_permissions}
              variant="outlined"
            >
              <option
                value=""
              />
              {requeriments.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              helperText="Distancia  Internacional"
              label="Requiero Larga"
              name="requires_long_distance"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires_long_distance',
                  event.target.value
                )
              }
              select
              SelectProps={{ native: true }}
              // eslint-disable-next-line react/jsx-sort-props
              type="number"
              value={formKickoff?.technicalKickoff?.requires_long_distance}
              variant="outlined"
            >
              <option
                value=""
              />
              {requeriments.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              helperText="Móviles"
              label="Requiere Permisos"
              name="requires_mobile_permissions"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires_mobile_permissions',
                  event.target.value
                )
              }
              select
              SelectProps={{ native: true }}
              // eslint-disable-next-line react/jsx-sort-props
              type="number"
              value={formKickoff?.technicalKickoff?.requires_mobile_permissions}
              variant="outlined"
            >
              <option
                value=""
              />
              {requeriments.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              helperText="Local Extendida"
              label="Requiere Permisos"
              name="requires_local_permissions"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires_local_permissions',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.technicalKickoff?.requires_local_permissions}
              variant="outlined"
            >
              <option
                value=""
              />
              {requeriments.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

TechnicalKickoff.propTypes = {
  className: PropTypes.string
};

export default TechnicalKickoff;
