import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardContent,
  TextField,
  Grid,
  CardHeader
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { saveFormKickoff } from 'actions';

const useStyles = makeStyles(theme => ({
  root: {},
  field: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  }
}));

const TechnicalKickoff = props => {
  const { className, ...rest } = props;

  const classes = useStyles();
  const dispatch = useDispatch();
  const { formKickoff } = useSelector(state => state.formKickoff);


  const client_team = ['Teléfonos analogos','Planta E1','Planta IP']
  const yes_no = ['No', 'Si'];
  const client_equipment_interface = ['FXS','RJ11','RJ45','RJ48','BNC'];
  const requeriments = ['SI (Debe esta firmado por el Cliente en el Survey o AOS)','NO'];


  const handleFieldChange = (event, field) => {
    event.persist && event.persist();

    dispatch(saveFormKickoff({
      ...formKickoff,
      technicalKickoff: {
        ...formKickoff.technicalKickoff,
        [field]: event.target.value,
      }
    }));

  };

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardContent>
        <Grid
          container
          spacing={4}
        >
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="Equipo Cliente "
              name="client_team"
              onChange={event =>
                handleFieldChange(
                  event,
                  'client_team',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.technicalKickoff?.client_team}
              variant="outlined"
            >
              <option
                value=""
              />
              {client_team.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="Interfaz Equipos Cliente"
              name="client_equipment_interface"
              onChange={event =>
                handleFieldChange(
                  event,
                  'client_equipment_interface',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.technicalKickoff?.client_equipment_interface}
              variant="outlined"
            >
              <option
                value=""
              />
              {client_equipment_interface.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.field}
              fullWidth
              helperText="(Solo Telefonia Pública Líneas Análogas)"
              label="Cantidad Lineas Básicas a adicionar"
              name="quantities_basic_lines"
              onChange={event =>
                handleFieldChange(event, 'quantities_basic_lines', event.target.value)
              }
              value={formKickoff?.technicalKickoff?.quantities_basic_lines}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.field}
              fullWidth
              helperText="(Solo Telefonia Pública Líneas Análogas)"
              label="Conformación PBX"
              name="conformation_pbx"
              onChange={event =>
                handleFieldChange(event, 'conformation_pbx', event.target.value)
              }
              value={formKickoff?.technicalKickoff?.conformation_pbx}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.field}
              fullWidth
              label="Cantidad de DID a Adicionar"
              name="quantity_did_add"
              onChange={event =>
                handleFieldChange(event, 'quantity_did_add', event.target.value)
              }
              value={formKickoff?.technicalKickoff?.quantity_did_add}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={classes.field}
              fullWidth
              label="Cantidad Canales a Adicionar"
              name="number_channels_add"
              onChange={event =>
                handleFieldChange(event, 'number_channels_add', event.target.value)
              }
              value={formKickoff?.technicalKickoff?.number_channels_add}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="Adicon de lineas FAX TO MAIL"
              name="fax_to_mail_add_line"
              onChange={event =>
                handleFieldChange(
                  event,
                  'fax_to_mail_add_line',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.technicalKickoff?.fax_to_mail_add_line}
              variant="outlined"
            >
              <option
                value=""
              />
              {yes_no.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="Adicion de lineas TELEFONO VIRTUAL"
              name="virtual_phone_add_line"
              onChange={event =>
                handleFieldChange(
                  event,
                  'virtual_phone_add_line',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.technicalKickoff?.virtual_phone_add_line}
              variant="outlined"
            >
              <option
                value=""
              />
              {['SI (SOLICITAR LICENCIA A LIDER TECNICO GRUPO ASE)', 'NO'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={12}
            xs={12}
          >
            <CardHeader title="Cambio de Telefonia Pública a PBX Distribuida" />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              helperText="Larga Distancia Nacional"
              label="Requiere Permisos"
              name="requires_national_permissions"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires_national_permissions',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.technicalKickoff?.requires_national_permissions}
              variant="outlined"
            >
              <option
                value=""
              />
              {requeriments.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              helperText="Distancia  Internacional"
              label="Requiero Larga"
              name="requires_long_distance"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires_long_distance',
                  event.target.value
                )
              }
              select
              SelectProps={{ native: true }}
              // eslint-disable-next-line react/jsx-sort-props
              type="number"
              value={formKickoff?.technicalKickoff?.requires_long_distance}
              variant="outlined"
            >
              <option
                value=""
              />
              {requeriments.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              helperText="Móviles"
              label="Requiere Permisos"
              name="requires_mobile_permissions"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires_mobile_permissions',
                  event.target.value
                )
              }
              select
              SelectProps={{ native: true }}
              // eslint-disable-next-line react/jsx-sort-props
              type="number"
              value={formKickoff?.technicalKickoff?.requires_mobile_permissions}
              variant="outlined"
            >
              <option
                value=""
              />
              {requeriments.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              helperText="Local Extendida"
              label="Requiere Permisos"
              name="requires_local_permissions"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires_local_permissions',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.technicalKickoff?.requires_local_permissions}
              variant="outlined"
            >
              <option
                value=""
              />
              {requeriments.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

TechnicalKickoff.propTypes = {
  className: PropTypes.string
};

export default TechnicalKickoff;
