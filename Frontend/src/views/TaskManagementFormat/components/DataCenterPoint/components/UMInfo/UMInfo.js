import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  TextField,
  Grid
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { saveFormKickoff } from 'actions';

const useStyles = makeStyles(theme => ({
  root: {},
  alert: {
    marginBottom: theme.spacing(3)
  },
  formGroup: {
    marginBottom: theme.spacing(3)
  },
  fieldGroup: {
    display: 'flex',
    alignItems: 'center'
  },
  addIcon: {
    margin: theme.spacing(1)
  },
  fieldHint: {
    margin: theme.spacing(1, 0)
  },
  addButton: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  tags: {
    marginTop: theme.spacing(1),
    '& > * + *': {
      marginLeft: theme.spacing(1)
    }
  },
  flexGrow: {
    flexGrow: 1
  },
  dateField: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  }
}));

const Baseline = props => {
  const { className, ...rest } = props;

  const classes = useStyles();
  const dispatch = useDispatch();
  const { formKickoff, checkKickOff } = useSelector(state => state.formKickoff);

  const statusOptions = ['Si', 'No', 'Existente'];
  const providers = ['N/A', 'Existente', 'Claro', 'Axesat', 'Comcel', 'Tigo', 'Media Commerce', 'Diveo', 'Edatel', 'UNE', 'ETB', 'IBM', 'IFX', 'Level 3 Colombia', 'Mercanet', 'Metrotel', 'Promitel', 'Skynet', 'Telebucaramanga', 'Telecom', 'Terremark', 'Sol Cable Vision', 'Sistelec', 'Opain', 'Airplan - (Información y Tecnología)', 'TV Azteca']
  const media = ['N/A', 'Existente', 'Fibra', 'Cobre', 'Satelital', 'Radio Enlace', '3G', 'UTP']
  const connectors = ['LC', 'SC', 'ST', 'FC']
  const interfaces = ['N/A', 'Ethernet', 'Serial V.35', 'Giga (óptico)', 'Giga Ethernet (Eléctrico)', 'STM-1', 'RJ45 - 120 OHM', 'G703 BNC']
  
  const handleFieldChange = (event, field) => {
    event.persist && event.persist();
    document.getElementsByName(field)[0].parentNode.parentNode.classList.remove('selected_error');

    dispatch(saveFormKickoff({
      ...formKickoff,
      uMInfo: {
        ...formKickoff.uMInfo,
        [field]: event.target.value,
      }
    }));

  };

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader title="Información de Ultima Milla" />
      <CardContent>
        <Grid
          container
          spacing={4}
        >
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('ot_requiere_instalacion_um') !== -1 ? true : false}
              fullWidth
              label="¿ESTA OT REQUIERE INSTALACION DE  UM?"
              name="ot_requiere_instalacion_um"
              onChange={event =>
                handleFieldChange(
                  event,
                  'ot_requiere_instalacion_um',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.uMInfo?.ot_requiere_instalacion_um}
              variant="outlined"
            >
              <option
                value=""
              />
              {statusOptions.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('provider') !== -1 ? true : false}
              fullWidth
              label="PROVEEDOR"
              name="provider"
              onChange={event =>
                handleFieldChange(
                  event,
                  'provider',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.uMInfo?.provider}
              variant="outlined"
            >
              <option
                value=""
              />
              {providers.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('medium') !== -1 ? true : false}
              fullWidth
              label="Medio"
              name="medium"
              onChange={event =>
                handleFieldChange(
                  event,
                  'medium',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.uMInfo?.medium}
              variant="outlined"
            >
              <option
                value=""
              />
              {media.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('feasibility_response') !== -1 ? true : false}
              fullWidth
              helperText="BW > 100 MEGAS"
              label="Respuesta Factibilidad"
              name="feasibility_response"
              onChange={event =>
                handleFieldChange(event, 'feasibility_response', event.target.value)
              }
              value={formKickoff?.uMInfo?.feasibility_response}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('type_conector') !== -1 ? true : false}
              fullWidth
              helperText="Aplica para FO Claro"
              label="Tipo de Conector"
              name="type_conector"
              onChange={event =>
                handleFieldChange(
                  event,
                  'type_conector',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.uMInfo?.type_conector}
              variant="outlined"
            >
              <option
                value=""
              />
              {connectors.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('destination_sds_access') !== -1 ? true : false}
              fullWidth
              helperText="Solo Aplica para Canales > 100 MEGAS"
              label="ACCESO SDS DESTINO (UNIFILAR)"
              name="destination_sds_access"
              onChange={event =>
                handleFieldChange(event, 'destination_sds_access', event.target.value)
              }
              value={formKickoff?.uMInfo?.destination_sds_access}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('access_olt') !== -1 ? true : false}
              fullWidth
              helperText="Solo Aplica para Canales > 100 MEGAS"
              label="ACCESO OLT (GPON)"
              name="access_olt"
              onChange={event =>
                handleFieldChange(event, 'access_olt', event.target.value)
              }
              value={formKickoff?.uMInfo?.access_olt}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('client_delivery_interface') !== -1 ? true : false}
              fullWidth
              label="Interface de Entrega al Cliente"
              name="client_delivery_interface"
              onChange={event =>
                handleFieldChange(
                  event,
                  'client_delivery_interface',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.uMInfo?.client_delivery_interface}
              variant="outlined"
            >
              <option
                value=""
              />
              {interfaces.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

Baseline.propTypes = {
  className: PropTypes.string
};

export default Baseline;
