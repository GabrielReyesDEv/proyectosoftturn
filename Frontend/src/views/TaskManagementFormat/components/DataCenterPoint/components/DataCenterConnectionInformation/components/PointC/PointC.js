import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  TextField,
  Grid
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { saveFormKickoff } from 'actions';

const useStyles = makeStyles(theme => ({
  root: {},
  field: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  }
}));

const PointC = props => {
  const { className, ...rest } = props;

  const classes = useStyles();
  const dispatch = useDispatch();
  const { formKickoff, checkKickOff } = useSelector(state => state.formKickoff);

  const handleFieldChange = (event, field) => {
    event.persist && event.persist();
    document.getElementsByName(field)[0].parentNode.parentNode.classList.remove('selected_error');
  
    dispatch(saveFormKickoff({
      ...formKickoff,
      dataCenterConnectionInformation: {
        ...formKickoff.dataCenterConnectionInformation,
        [field]: event.target.value,
      }
    }));

  };

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader title="PUNTO C Equipo Cliente - Rack y Unidad de conexión" />
      <CardContent>
        <Grid
          container
          spacing={4}
        >
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('connection_computer') !== -1 ? true : false}
              fullWidth
              label="Equipo de conexión"
              name="connection_computer"
              onChange={event =>
                handleFieldChange(event, 'connection_computer', event.target.value)
              }
              value={formKickoff?.dataCenterConnectionInformation?.connection_computer}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('client_port') !== -1 ? true : false}
              fullWidth
              label="Puerto Cliente"
              name="client_port"
              onChange={event =>
                handleFieldChange(event, 'client_port', event.target.value)
              }
              value={formKickoff?.dataCenterConnectionInformation?.client_port}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('type_interface') !== -1 ? true : false}
              fullWidth
              label="Tipo de Interface"
              name="type_interface"
              onChange={event =>
                handleFieldChange(event, 'type_interface', event.target.value)
              }
              value={formKickoff?.dataCenterConnectionInformation?.type_interface}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('required_regulated_outlets_C') !== -1 ? true : false}
              fullWidth
              label="TOMAS REGULADAS REQUERIDAS"
              name="required_regulated_outlets_C"
              onChange={event =>
                handleFieldChange(event, 'required_regulated_outlets_C', event.target.value)
              }
              value={formKickoff?.dataCenterConnectionInformation?.required_regulated_outlets_C}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('total_unidades_rack') !== -1 ? true : false}
              fullWidth
              label="TOTAL UNIDADES DE RACK"
              name="total_unidades_rack"
              onChange={event =>
                handleFieldChange(event, 'total_unidades_rack', event.target.value)
              }
              value={formKickoff?.dataCenterConnectionInformation?.total_unidades_rack}
              variant="outlined"
            />
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

PointC.propTypes = {
  className: PropTypes.string
};

export default PointC;
