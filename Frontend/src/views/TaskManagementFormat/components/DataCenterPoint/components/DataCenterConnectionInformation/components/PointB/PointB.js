import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  TextField,
  Grid
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { saveFormKickoff } from 'actions';

const useStyles = makeStyles(theme => ({
  root: {},
  field: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  }
}));

const PointB = props => {
  const { className, ...rest } = props;

  const classes = useStyles();
  const dispatch = useDispatch();
  const { formKickoff, checkKickOff } = useSelector(state => state.formKickoff);

  const handleFieldChange = (event, field) => {
    event.persist && event.persist();
    document.getElementsByName(field)[0].parentNode.parentNode.classList.remove('selected_error');
  
    dispatch(saveFormKickoff({
      ...formKickoff,
      dataCenterConnectionInformation: {
        ...formKickoff.dataCenterConnectionInformation,
        [field]: event.target.value,
      }
    }));

  };

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader title="PUNTO B Equipo Claro - Rack: Appliance" />
      <CardContent>
        <Grid
          container
          spacing={4}
        >
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('claro_computer') !== -1 ? true : false}
              fullWidth
              label="Equipo Claro"
              name="claro_computer"
              onChange={event =>
                handleFieldChange(event, 'claro_computer', event.target.value)
              }
              value={formKickoff?.dataCenterConnectionInformation?.claro_computer}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('connector_wan') !== -1 ? true : false}
              fullWidth
              label="Conector WAN"
              name="connector_wan"
              onChange={event =>
                handleFieldChange(event, 'connector_wan', event.target.value)
              }
              value={formKickoff?.dataCenterConnectionInformation?.connector_wan}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('port_lan') !== -1 ? true : false}
              fullWidth
              label="Puerto LAN"
              name="port_lan"
              onChange={event =>
                handleFieldChange(event, 'port_lan', event.target.value)
              }
              value={formKickoff?.dataCenterConnectionInformation?.port_lan}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('connector_lan') !== -1 ? true : false}
              fullWidth
              label="Conector LAN"
              name="connector_lan"
              onChange={event =>
                handleFieldChange(event, 'connector_lan', event.target.value)
              }
              value={formKickoff?.dataCenterConnectionInformation?.connector_lan}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('required_regulated_outlets_B') !== -1 ? true : false}
              fullWidth
              label="TOMAS REGULADAS REQUERIDAS"
              name="required_regulated_outlets_B"
              onChange={event =>
                handleFieldChange(event, 'required_regulated_outlets_B', event.target.value)
              }
              value={formKickoff?.dataCenterConnectionInformation?.required_regulated_outlets_B}
              variant="outlined"
            />
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

PointB.propTypes = {
  className: PropTypes.string
};

export default PointB;
