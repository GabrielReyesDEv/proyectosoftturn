import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  TextField,
  Grid
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { saveFormKickoff } from 'actions';

const useStyles = makeStyles(theme => ({
  root: {},
  field: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  }
}));

const PointA = props => {
  const { className, ...rest } = props;

  const classes = useStyles();
  const dispatch = useDispatch();
  const { formKickoff, checkKickOff } = useSelector(state => state.formKickoff);

  const handleFieldChange = (event, field) => {
    event.persist && event.persist();
    document.getElementsByName(field)[0].parentNode.parentNode.classList.remove('selected_error');
  
    dispatch(saveFormKickoff({
      ...formKickoff,
      dataCenterConnectionInformation: {
        ...formKickoff.dataCenterConnectionInformation,
        [field]: event.target.value,
      }
    }));

  };

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader title="PUNTO A - Equipo de acceso: Alcatel Triara" />
      <CardContent>
        <Grid
          container
          spacing={4}
        >
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('access_computer') !== -1 ? true : false}
              fullWidth
              label="Equipo de Acceso"
              name="access_computer"
              onChange={event =>
                handleFieldChange(event, 'access_computer', event.target.value)
              }
              value={formKickoff?.dataCenterConnectionInformation?.access_computer}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('access_computer_port') !== -1 ? true : false}
              fullWidth
              label="Puerto Equipo de Acceso"
              name="access_computer_port"
              onChange={event =>
                handleFieldChange(event, 'access_computer_port', event.target.value)
              }
              value={formKickoff?.dataCenterConnectionInformation?.access_computer_port}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('type_interfaces') !== -1 ? true : false}
              fullWidth
              label="Tipo de Interface"
              name="type_interfaces"
              onChange={event =>
                handleFieldChange(event, 'type_interfaces', event.target.value)
              }
              value={formKickoff?.dataCenterConnectionInformation?.type_interfaces}
              variant="outlined"
            />
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

PointA.propTypes = {
  className: PropTypes.string
};

export default PointA;
