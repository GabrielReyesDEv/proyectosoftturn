import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { 
  PointA,
  PointB,
  PointC
} from './components';

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(3)
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}));

const DataCenterConnectionInformation = (props) => {
  const classes = useStyles();

  return (
    <>
      <PointA className={classes.root} />
      <PointB className={classes.root}/>
      <PointC className={classes.root} />
    </>
  );
};

DataCenterConnectionInformation.propTypes = {
  className: PropTypes.string
};

export default DataCenterConnectionInformation;
