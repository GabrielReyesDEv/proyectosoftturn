import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { 
  HeaderSelect,
  DestinationPoint,
  OriginPoint
} from './components';
import { Alert } from 'components';
import { useSelector } from 'react-redux';

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(3)
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}));

const PrivateLine = (props) => {

  const { oth } = props; 
  const classes = useStyles();
  const { originDestination } = useSelector(state => state.destinationOriginReducer);
  

  return (
    <>
      <Alert
        className={classes.root}
        loader
        message={`Servicio Private Line - OTH ${oth}`}
      />
      <HeaderSelect className={classes.root} />
      {originDestination === 'Origen' ? <OriginPoint oth={oth}/> : <DestinationPoint oth={oth}/> }

    </>
  );
};

PrivateLine.propTypes = {
  className: PropTypes.string
};

export default PrivateLine;
