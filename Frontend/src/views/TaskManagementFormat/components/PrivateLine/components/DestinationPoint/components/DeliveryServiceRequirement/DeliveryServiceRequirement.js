import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  TextField,
  Grid
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { saveFormKickoff } from 'actions';

const useStyles = makeStyles(theme => ({
  root: {},
  field: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  }
}));

const DeliveryServiceRequirement = props => {
  const { className, ...rest } = props;

  const classes = useStyles();

  const dispatch = useDispatch();

  const { formKickoff, checkKickOff } = useSelector(state => state.formKickoff);

  const macs_quantities = ['0 - 250 Estándar', '250 en Adelante - Solicitar autorización a CORE']
  const transmission_mode = ['Troncal - Especifique VLAN', 'Acceso (Null)']
  const rfc = ['SI => Cliente Critico Punto Central', 'SI => Servicio Critico (Listado)', 'SI => Cliente Critico', 'SI => RFC Estándar Saturación', 'SI => Cliente Critico Punto Central - RFC Estándar Saturación', 'NO' ];
  const consumables = ['N/A', 'Bandeja', 'Cables de Poder', 'Clavijas de Conexión', 'Accesorios para rackear (Orejas)', 'Balum']
  
  const handleFieldChange = (event, field) => {
    event.persist && event.persist();
    document.getElementsByName(field)[0].parentNode.parentNode.classList.remove('selected_error');

    dispatch(saveFormKickoff({
      ...formKickoff,
      destinationPoint: {
        ...formKickoff.destinationPoint,
        deliveryServiceRequirement: {
          ...formKickoff.destinationPoint.deliveryServiceRequirement,
          [field]: event.target.value,
        }
      }
    }));
  
  };

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader title="Requerimientos para entrega del Servicio" />
      <CardContent>
        <Grid
          container
          spacing={4}
        >
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="Requiere RFC"
              name="destination_requires_rfc"
              onChange={event =>
                handleFieldChange(
                  event,
                  'destination_requires_rfc',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.destinationPoint?.deliveryServiceRequirement?.destination_requires_rfc}
              variant="outlined"
            >
              <option
                value=""
              />
              {rfc.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="Conversor de Medio"
              name="destination_media_converter"
              onChange={event =>
                handleFieldChange(event, 'destination_media_converter', event.target.value)
              }
              value={formKickoff?.destinationPoint?.deliveryServiceRequirement?.destination_media_converter}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="Equipos adicionales"
              name="destination_additional_equipment"
              onChange={event =>
                handleFieldChange(event, 'destination_additional_equipment', event.target.value)
              }
              value={formKickoff?.destinationPoint?.deliveryServiceRequirement?.destination_additional_equipment}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="Consumibles"
              name="destination_consumables"
              onChange={event =>
                handleFieldChange(
                  event,
                  'destination_consumables',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.destinationPoint?.deliveryServiceRequirement?.destination_consumables}
              variant="outlined"
            >
              <option
                value=""
              />
              {consumables.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('destination_import_registration_and_valued_letter') !== -1 ? true : false}
              fullWidth
              label="Registro Importanción y carta valorizada"
              name="destination_import_registration_and_valued_letter"
              onChange={event =>
                handleFieldChange(
                  event,
                  'destination_import_registration_and_valued_letter',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.destinationPoint?.deliveryServiceRequirement?.destination_import_registration_and_valued_letter}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('destination_channel_delivery_transmission_mode') !== -1 ? true : false}
              fullWidth
              label="Modo transmision entrega canal"
              name="destination_channel_delivery_transmission_mode"
              onChange={event =>
                handleFieldChange(
                  event,
                  'destination_channel_delivery_transmission_mode',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.destinationPoint?.deliveryServiceRequirement?.destination_channel_delivery_transmission_mode}
              variant="outlined"
            >
              <option
                value=""
              />
              {transmission_mode.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="Cantidades MACS"
              name="destination_macs_quantities"
              onChange={event =>
                handleFieldChange(
                  event,
                  'destination_macs_quantities',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.destinationPoint?.deliveryServiceRequirement?.destination_macs_quantities}
              variant="outlined"
            >
              <option
                value=""
              />
              {macs_quantities.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

DeliveryServiceRequirement.propTypes = {
  className: PropTypes.string
};

export default DeliveryServiceRequirement;
