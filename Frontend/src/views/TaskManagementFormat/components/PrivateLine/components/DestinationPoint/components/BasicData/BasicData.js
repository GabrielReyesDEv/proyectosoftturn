import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  TextField,
  Grid
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { saveFormKickoff } from 'actions';

const useStyles = makeStyles(theme => ({
  root: {},
  alert: {
    marginBottom: theme.spacing(3)
  },
  formGroup: {
    marginBottom: theme.spacing(3)
  },
  fieldGroup: {
    display: 'flex',
    alignItems: 'center'
  },
  addIcon: {
    margin: theme.spacing(1)
  },
  fieldHint: {
    margin: theme.spacing(1, 0)
  },
  addButton: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  tags: {
    marginTop: theme.spacing(1),
    '& > * + *': {
      marginLeft: theme.spacing(1)
    }
  },
  flexGrow: {
    flexGrow: 1
  },
  dateField: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  }
}));

const BasicData = props => {
  const { className, ...rest } = props;

  const classes = useStyles();
  const dispatch = useDispatch();

  const { formKickoff, checkKickOff, inputsOtp, otpInfo } = useSelector(state => state.formKickoff);

  React.useEffect(() => {
    if (!formKickoff?.destinationPoint?.basicData?.destinationAddress) {

      document.getElementsByName('destination_city')[0].parentNode.parentNode.classList.remove('selected_error');
      document.getElementsByName('destinationAddress')[0].parentNode.parentNode.classList.remove('selected_error');

      dispatch(saveFormKickoff({
        destinationPoint: {
          basicData: {
            destination_city : otpInfo.city,
            destinationAddress: otpInfo.destinationAddress,
            projectId: otpInfo.projectId,
          }
        }
      }));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const statusOptions = ['Edificio', 'Casa'];
  const instalationType = ['Instalar UM con PE','Instalar UM con CT (No Estándar - Requiere Validación Solución No Estándar)','Instalar UM en Datacenter Claro- Cableado','Instalar UM en Datacenter Claro- Implementación','Instalar UM en Datacenter Tercero','UM existente. Requiere Cambio de equipo'];
  const privateLine_type = ['Local - P2P','Local - P2MP','Nacional - P2P','Nacional - P2MP','VPRN','Private Line Service (SDH)']

  const handleFieldChange = (event, field) => {
    event.persist && event.persist();
    var input = inputsOtp.includes(field);
    if (input === true) document.getElementsByName(field)[0].parentNode.parentNode.classList.add('otp_change');
    document.getElementsByName(field)[0].parentNode.parentNode.classList.remove('selected_error');
    
    dispatch(saveFormKickoff({
      ...formKickoff,
      destinationPoint: {
        ...formKickoff.destinationPoint,
        basicData: {
          ...formKickoff.destinationPoint.basicData,
          [field]: event.target.value,
        }
      }
    }));

  };

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader title="Datos Basicos de instalacion" />
      <CardContent>
        <Grid
          container
          spacing={4}
        >
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('destination_city') !== -1 ? true : false}
              fullWidth
              label="CIUDAD"
              name="destination_city"
              onChange={event =>
                handleFieldChange(event, 'destination_city', event.target.value)
              }
              value={formKickoff?.destinationPoint?.basicData?.destination_city}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={6}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('destinationAddress') !== -1 ? true : false}
              fullWidth
              helperText="Especificar barrio, piso u oficina"
              label="Direccion"
              name="destinationAddress"
              onChange={event =>
                handleFieldChange(event, 'destinationAddress', event.target.value)
              }
              value={formKickoff?.destinationPoint?.basicData?.destinationAddress}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('destination_predio_type') !== -1 ? true : false}
              fullWidth
              label="TIPO PREDIO"
              name="destination_predio_type"
              onChange={event =>
                handleFieldChange(
                  event,
                  'destination_predio_type',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.destinationPoint?.basicData?.destination_predio_type}
              variant="outlined"
            >
              <option
                value=""
              />
              {statusOptions.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('destination_nit') !== -1 ? true : false}
              fullWidth
              label="Nit del Cliente"
              name="destination_nit"
              onChange={event =>
                handleFieldChange(event, 'destination_nit', event.target.value)
              }
              value={formKickoff?.destinationPoint?.basicData?.destination_nit}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              helperText="CODIGO DE SERVICIO/CIUDAD/SERVICIO/COMERCIO O SEDE DEL CLIENTE"
              label="Alias del Lugar"
              name="destination_alias"
              onChange={event =>
                handleFieldChange(event, 'destination_alias', event.target.value)
              }
              value={formKickoff?.destinationPoint?.basicData?.destination_alias}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              disabled
              fullWidth
              label="OTP"
              name="projectId"
              onChange={event =>
                handleFieldChange(event, 'projectId', event.target.value)
              }
              value={formKickoff?.destinationPoint?.basicData?.projectId}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="OTP Asociada"
              name="destination_otp_asociada"
              onChange={event =>
                handleFieldChange(event, 'destination_otp_asociada', event.target.value)
              }
              required
              value={formKickoff?.destinationPoint?.basicData?.destination_otp_asociada}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={6}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('destination_privateLine_type') !== -1 ? true : false}
              fullWidth
              label="Tipo de Private Line"
              name="destination_privateLine_type"
              onChange={event =>
                handleFieldChange(
                  event,
                  'destination_privateLine_type',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.destinationPoint?.basicData?.destination_privateLine_type}
              variant="outlined"
            >
              <option
                value=""
              />
              {privateLine_type.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={6}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('destination_instalation_type') !== -1 ? true : false}
              fullWidth
              label="Tipo de Instalación"
              name="destination_instalation_type"
              onChange={event =>
                handleFieldChange(
                  event,
                  'destination_instalation_type',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.destinationPoint?.basicData?.destination_instalation_type}
              variant="outlined"
            >
              <option
                value=""
              />
              {instalationType.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('destination_ancho_banda') !== -1 ? true : false}
              fullWidth
              label="Ancho de Banda"
              name="destination_ancho_banda"
              onChange={event =>
                handleFieldChange(event, 'destination_ancho_banda', event.target.value)
              }
              value={formKickoff?.destinationPoint?.basicData?.destination_ancho_banda}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              helperText="(Aplica para UM Existente)"
              label="ID del servicio actual"
              name="destination_id_current_service"
              onChange={event =>
                handleFieldChange(event, 'destination_id_current_service', event.target.value)
              }
              value={formKickoff?.destinationPoint?.basicData?.destination_id_current_service}
              variant="outlined"
            />
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

BasicData.propTypes = {
  className: PropTypes.string
};

export default BasicData;
