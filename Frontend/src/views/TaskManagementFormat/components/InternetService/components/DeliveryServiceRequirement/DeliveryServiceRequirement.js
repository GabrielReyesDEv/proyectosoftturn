import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  TextField,
  Grid
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { saveFormKickoff } from 'actions';

const useStyles = makeStyles(theme => ({
  root: {},
  field: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  }
}));

const DeliveryServiceRequirement = props => {
  const { className, ...rest } = props;

  const classes = useStyles();
  const dispatch = useDispatch();
  const { formKickoff, checkKickOff } = useSelector(state => state.formKickoff);

  const rfc = ['SI => Cliente Critico Punto Central', 'SI => Servicio Critico (Listado)', 'SI => Cliente Critico', 'SI => RFC Estándar Saturación', 'SI => Cliente Critico Punto Central - RFC Estándar Saturación', 'NO' ];
  const consumables = ['N/A', 'Bandeja', 'Cables de Poder', 'Clavijas de Conexión', 'Accesorios para rackear (Orejas)']
  const handleFieldChange = (event, field) => {
    event.persist && event.persist();
    document.getElementsByName(field)[0].parentNode.parentNode.classList.remove('selected_error');

    dispatch(saveFormKickoff({
      ...formKickoff,
      deliveryServiceRequirement: {
        ...formKickoff.deliveryServiceRequirement,
        [field]: event.target.value,
      }
    }));

  };

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader title="Requerimientos para entrega del Servicio" />
      <CardContent>
        <Grid
          container
          spacing={4}
        >
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('requires_rfc') !== -1 ? true : false}
              fullWidth
              label="Requiere RFC"
              name="requires_rfc"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires_rfc',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.deliveryServiceRequirement?.requires_rfc}
              variant="outlined"
            >
              <option
                value=""
              />
              {rfc.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="Conversor de Medio"
              name="media_converter"
              onChange={event =>
                handleFieldChange(event, 'media_converter', event.target.value)
              }
              value={formKickoff?.deliveryServiceRequirement?.media_converter}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('router_reference') !== -1 ? true : false}
              fullWidth
              label="Referencia Router"
              name="router_reference"
              onChange={event =>
                handleFieldChange(event, 'router_reference', event.target.value)
              }
              value={formKickoff?.deliveryServiceRequirement?.router_reference}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="Módulos o tarjetas"
              name="modules_or_cards"
              onChange={event =>
                handleFieldChange(event, 'modules_or_cards', event.target.value)
              }
              value={formKickoff?.deliveryServiceRequirement?.modules_or_cards}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="Licencias"
              name="licensing"
              onChange={event =>
                handleFieldChange(event, 'licensing', event.target.value)
              }
              required
              value={formKickoff?.deliveryServiceRequirement?.licensing}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('additional_equipment') !== -1 ? true : false}
              fullWidth
              label="Equipos adicionales"
              name="additional_equipment"
              onChange={event =>
                handleFieldChange(event, 'additional_equipment', event.target.value)
              }
              value={formKickoff?.deliveryServiceRequirement?.additional_equipment}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('consumables') !== -1 ? true : false}
              fullWidth
              label="Consumibles"
              name="consumables"
              onChange={event =>
                handleFieldChange(
                  event,
                  'consumables',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.deliveryServiceRequirement?.consumables}
              variant="outlined"
            >
              <option
                value=""
              />
              {consumables.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('import_registration_and_valued_letter') !== -1 ? true : false}
              fullWidth
              label="Registro Importanción y carta valorizada"
              name="import_registration_and_valued_letter"
              onChange={event =>
                handleFieldChange(
                  event,
                  'import_registration_and_valued_letter',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.deliveryServiceRequirement?.import_registration_and_valued_letter}
              variant="outlined"
            >
              <option
                value=""
              />
              {['Si', 'No'].map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

DeliveryServiceRequirement.propTypes = {
  className: PropTypes.string
};

export default DeliveryServiceRequirement;
