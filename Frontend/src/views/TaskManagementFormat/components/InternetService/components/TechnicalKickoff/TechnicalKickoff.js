import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardContent,
  TextField,
  Grid
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { saveFormKickoff } from 'actions';

const useStyles = makeStyles(theme => ({
  root: {},
  field: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  }
}));

const TechnicalKickoff = props => {
  const { className, ...rest } = props;

  const classes = useStyles();
  const dispatch = useDispatch();
  const { formKickoff, checkKickOff } = useSelector(state => state.formKickoff);

  const ip_address = ['Cantidad IPs: 2 - Mascara: /30', 'Cantidad IPs: 6 - Mascara: /29', 'Cantidad IPs: 14 - Mascara: /28 - Requiere Viabilidad Preventa', 'Cantidad IPs: 30 - Mascara: /27 - Requiere Viabilidad Preventa']
  const yes_no = ['No', 'Si'];
  const emails_quantity = [20, 40, 140, 160, 200];
  const hosting_storage = [20, 40, 140, 160, 200];
  const email_storage = [2, 4, 14, 16, 20];
  const hosting_platform = ['N/A', 'Solaris', 'Windows'];
  const handleFieldChange = (event, field) => {
    event.persist && event.persist();
    document.getElementsByName(field)[0].parentNode.parentNode.classList.remove('selected_error');

    dispatch(saveFormKickoff({
      ...formKickoff,
      technicalKickoff: {
        ...formKickoff.technicalKickoff,
        [field]: event.target.value,
      }
    }));

  };

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardContent>
        <Grid
          container
          spacing={4}
        >
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="Ancho de Banda Exclusivo NAP"
              name="NAP_bandwidth"
              onChange={event =>
                handleFieldChange(event, 'NAP_bandwidth', event.target.value)
              }
              value={formKickoff?.technicalKickoff?.NAP_bandwidth}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('internet_bandwidth') !== -1 ? true : false}
              fullWidth
              label="Ancho de Banda Internet"
              name="internet_bandwidth"
              onChange={event =>
                handleFieldChange(event, 'internet_bandwidth', event.target.value)
              }
              value={formKickoff?.technicalKickoff?.internet_bandwidth}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={6}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('ip_address') !== -1 ? true : false}
              fullWidth
              label="Direcciones IP"
              name="ip_address"
              onChange={event =>
                handleFieldChange(
                  event,
                  'ip_address',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.technicalKickoff?.ip_address}
              variant="outlined"
            >
              <option
                value=""
              />
              {ip_address.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="Activación Correo"
              name="email_activation"
              onChange={event =>
                handleFieldChange(
                  event,
                  'email_activation',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.technicalKickoff?.email_activation}
              variant="outlined"
            >
              <option
                value=""
              />
              {yes_no.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="Activación WEB Hosting"
              name="web_hosting_activation"
              onChange={event =>
                handleFieldChange(
                  event,
                  'web_hosting_activation',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.technicalKickoff?.web_hosting_activation}
              variant="outlined"
            >
              <option
                value=""
              />
              {yes_no.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="¿Dominio Existente?"
              name="existing_domain"
              onChange={event =>
                handleFieldChange(
                  event,
                  'existing_domain',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.technicalKickoff?.existing_domain}
              variant="outlined"
            >
              <option
                value=""
              />
              {yes_no.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="Dominio a Comprar"
              name="domain_to_buy"
              onChange={event =>
                handleFieldChange(event, 'domain_to_buy', event.target.value)
              }
              value={formKickoff?.technicalKickoff?.domain_to_buy}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="Cantidad de cuentas de correo"
              name="emails_quantity"
              onChange={event =>
                handleFieldChange(
                  event,
                  'emails_quantity',
                  event.target.value
                )
              }
              select
              SelectProps={{ native: true }}
              // eslint-disable-next-line react/jsx-sort-props
              type="number"
              value={formKickoff?.technicalKickoff?.emails_quantity}
              variant="outlined"
            >
              <option
                value=""
              />
              {emails_quantity.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="Espacio de correo (GB)"
              name="email_storage"
              onChange={event =>
                handleFieldChange(
                  event,
                  'email_storage',
                  event.target.value
                )
              }
              select
              SelectProps={{ native: true }}
              // eslint-disable-next-line react/jsx-sort-props
              type="number"
              value={formKickoff?.technicalKickoff?.email_storage}
              variant="outlined"
            >
              <option
                value=""
              />
              {email_storage.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="Plataforma de Web Hosting"
              name="web_hosting_platform"
              onChange={event =>
                handleFieldChange(
                  event,
                  'web_hosting_platform',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.technicalKickoff?.web_hosting_platform}
              variant="outlined"
            >
              <option
                value=""
              />
              {hosting_platform.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="Almacenamiento Web Hosting (MB)"
              name="web_hosting_storage"
              onChange={event =>
                handleFieldChange(
                  event,
                  'web_hosting_storage',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.technicalKickoff?.web_hosting_storage}
              variant="outlined"
            >
              <option
                value=""
              />
              {hosting_storage.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
          <Grid
            item
            xs={12}
          >
            <TextField
              fullWidth
              helperText="Por favor documentar nombre de la promoción"
              label="¿Aplica alguna promoción vigente?"
              name="current_promotion"
              onChange={event =>
                handleFieldChange(event, 'current_promotion', event.target.value)
              }
              value={formKickoff?.technicalKickoff?.current_promotion}
              variant="outlined"
            />
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

TechnicalKickoff.propTypes = {
  className: PropTypes.string
};

export default TechnicalKickoff;
