import React from 'react';
import PropTypes from 'prop-types';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { 
  IconButton,
  Drawer,
  Divider,
  List,
} from '@material-ui/core';

import { useSelector } from 'react-redux';

const drawerWidth = 390;

const useStyles = makeStyles((theme) => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-start',
  },
  labelParent: {
    fontFamily: '-apple-system,BlinkMacSystemFont, Segoe UI,Roboto,Oxygen,Ubuntu,Fira Sans,Droid Sans,Helvetica Neue,sans-serif',
    wordWrap: 'break-word',
    borderRadius: 3,
    padding: 7,
    width:'85%',
    color: 'rgb(23, 43, 77)',
    background: '#FFFF',
    '&:hover': {
      background: '#EBECF0',
    },
  },
  labelChildren: {
    fontFamily: '-apple-system,BlinkMacSystemFont, Segoe UI,Roboto,Oxygen,Ubuntu,Fira Sans,Droid Sans,Helvetica Neue,sans-serif',
    wordWrap: 'break-word',
    padding: 7,
    color: 'rgb(23, 43, 77)',
  },
  labelChildrenInfo: {
    fontFamily: '-apple-system,BlinkMacSystemFont, Segoe UI,Roboto,Oxygen,Ubuntu,Fira Sans,Droid Sans,Helvetica Neue,sans-serif',
    wordWrap: 'break-word',
    borderRadius: 3,
    padding: 7,
    width:'100%',
    color: 'rgb(107, 119, 140)',
    background: '#FFFF',
    '&:hover': {
      background: '#EBECF0',
    },
  },
}));


const OtpInfo = props => {
    
  // eslint-disable-next-line react/prop-types
  const { handleDrawerClose, openDrawer } = props;
  const classes = useStyles();
  const theme = useTheme();
  const {  otpInfo } = useSelector(state => state.formKickoff);
 
  return (
    <Drawer
      anchor="right"
      classes={{
        paper: classes.drawerPaper,
      }}
      className={classes.drawer}
      open={openDrawer}
      variant="persistent"
    >
      <div className={classes.drawerHeader}>
        <IconButton onClick={handleDrawerClose}>
          {theme.direction === 'rtl' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
        </IconButton>
        <div className={classes.labelParent}>
          <h2>OTP: {otpInfo?.projectId}</h2>
        </div>
      </div>
      <Divider />
      <List
        aria-label="mailbox folders"
        component="nav"
        style={{ padding: 10 }}
      >
        <div className={classes.labelChildren}>
          <h4>Ciudad </h4>
        </div>
        <div className={classes.labelChildrenInfo}>
          <h4>{otpInfo?.city}</h4>
        </div>
        <Divider />
        <div className={classes.labelChildren}>
          <h4>Ciudad Inidente </h4>
        </div>
        <div className={classes.labelChildrenInfo}>
          <h4>{otpInfo?.incidentCity}</h4>
        </div>
        <Divider />
        <div className={classes.labelChildren}>
          <h4>Departamento </h4>
        </div>
        <div className={classes.labelChildrenInfo}>
          <h4>{otpInfo?.department}</h4>
        </div>
        <Divider />
        <div className={classes.labelChildren}>
          <h4>Direccion de origen </h4>
        </div>
        <div className={classes.labelChildrenInfo}>
          <h4>{otpInfo?.sourceAddress}</h4>
        </div>
        <Divider />
        <div className={classes.labelChildren}>
          <h4>Direccion de destino </h4>
        </div>
        <div className={classes.labelChildrenInfo}>
          <h4>{otpInfo?.destinationAddress}</h4>
        </div>
        <Divider />
        <div className={classes.labelChildren}>
          <h4>Descripcion </h4>
        </div>
        <div className={classes.labelChildrenInfo}>
          <h4>{otpInfo?.description}</h4>
        </div>
        <Divider />
        <div className={classes.labelChildren}>
          <h3>Cliente: </h3>
        </div>
        <div className={classes.labelChildren}>
          <h4>Nombre </h4>
        </div>
        <div className={classes.labelChildrenInfo}>
          <h4>{otpInfo?.client}</h4>
        </div>
        <div className={classes.labelChildren}>
          <h4>Trabajo </h4>
        </div>
        <div className={classes.labelChildrenInfo}>
          <h4>{otpInfo?.orderType}</h4>
        </div>
        <div className={classes.labelChildren}>
          <h4>Servicio </h4>
        </div>
        <div className={classes.labelChildrenInfo}>
          <h4>{otpInfo?.service}</h4>
        </div>
        <div className={classes.labelChildren}>
          <h4>Codigo de servicio </h4>
        </div>
        <div className={classes.labelChildrenInfo}>
          <h4>{otpInfo?.serviceCode}</h4>
        </div>
        <Divider />
        <div className={classes.labelChildren}>
          <h4>Familia </h4>
        </div>
        <div className={classes.labelChildrenInfo}>
          <h4>{otpInfo?.family}</h4>
        </div>
        <Divider />
        <div className={classes.labelChildren}>
          <h4>Grupo </h4>
        </div>
        <div className={classes.labelChildrenInfo}>
          <h4>{otpInfo?.group}</h4>
        </div>
        <Divider />
        <div className={classes.labelChildren}>
          <h4>Segmento </h4>
        </div>
        <div className={classes.labelChildrenInfo}>
          <h4>{otpInfo?.segment}</h4>
        </div>
        <Divider />
        <div className={classes.labelChildren}>
          <h4>MRC </h4>
        </div>
        <div className={classes.labelChildrenInfo}>
          <h4>{otpInfo?.mrc}</h4>
        </div>
        <Divider />
        <div className={classes.labelChildren}>
          <h4>Prioridad </h4>
        </div>
        <div className={classes.labelChildrenInfo}>
          <h4>{otpInfo?.priority}</h4>
        </div>
        <Divider />
        <div className={classes.labelChildren}>
          <h4>Producto </h4>
        </div>
        <div className={classes.labelChildrenInfo}>
          <h4>{otpInfo?.product}</h4>
        </div>
        <Divider />
        <div className={classes.labelChildren}>
          <h3>Fechas: </h3>
        </div>
        <div className={classes.labelChildren}>
          <h4>Creacion </h4>
        </div>
        <div className={classes.labelChildrenInfo}>
          <h4>{otpInfo?.createdAt}</h4>
        </div>
        <div className={classes.labelChildren}>
          <h4>Tiempo de incidente </h4>
        </div>
        <div className={classes.labelChildrenInfo}>
          <h4>{otpInfo?.incidentDate}</h4>
        </div>
        <div className={classes.labelChildren}>
          <h4>Compromiso </h4>
        </div>
        <div className={classes.labelChildrenInfo}>
          <h4>{otpInfo?.commitmentDate}</h4>
        </div>
        <div className={classes.labelChildren}>
          <h4>Programacion </h4>
        </div>
        <div className={classes.labelChildrenInfo}>
          <h4>{otpInfo?.programmingDate}</h4>
        </div>
        <div className={classes.labelChildren}>
          <h4>Actualizacion </h4>
        </div>
        <div className={classes.labelChildrenInfo}>
          <h4>{otpInfo?.updatedAt}</h4>
        </div>
        <div className={classes.labelChildren}>
          <h4>Finalizacion </h4>
        </div>
        <div className={classes.labelChildrenInfo}>
          <h4>{otpInfo?.finishDate}</h4>
        </div>
      </List>
    </Drawer>
  );
};

OtpInfo.propTypes = {
  className: PropTypes.string
};

export default OtpInfo;
