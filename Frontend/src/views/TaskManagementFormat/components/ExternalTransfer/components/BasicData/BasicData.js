import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  TextField,
  Grid
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { saveFormKickoff } from 'actions';

const useStyles = makeStyles(theme => ({
  root: {},
  alert: {
    marginBottom: theme.spacing(3)
  },
  formGroup: {
    marginBottom: theme.spacing(3)
  },
  fieldGroup: {
    display: 'flex',
    alignItems: 'center'
  },
  addIcon: {
    margin: theme.spacing(1)
  },
  fieldHint: {
    margin: theme.spacing(1, 0)
  },
  addButton: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  tags: {
    marginTop: theme.spacing(1),
    '& > * + *': {
      marginLeft: theme.spacing(1)
    }
  },
  flexGrow: {
    flexGrow: 1
  },
  dateField: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  }
}));

const BasicData = props => {
  const { className, ...rest } = props;

  const classes = useStyles();
  const dispatch = useDispatch();
  const { formKickoff, checkKickOff, inputsOtp, otpInfo } = useSelector(state => state.formKickoff);

  React.useEffect(() => {
    document.getElementsByName('city')[0].parentNode.parentNode.classList.remove('selected_error');
    document.getElementsByName('sourceAddress')[0].parentNode.parentNode.classList.remove('selected_error');
    dispatch(saveFormKickoff({
      basicData: {
        ...formKickoff,
        city : otpInfo.city,
        sourceAddress: otpInfo.sourceAddress,
        projectId: otpInfo.projectId,
      }
    }));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const statusOptions = ['Edificio', 'Casa'];
  const external_transfer_type = ['Estándar - Se recogen equipos en Sede Antigua y se llevan a sede Nueva','Paralelo - Se habilitan Nuevos Recursos de UM, Equipos, Config'];
  const serviceType = ['Internet Dedicado con diferenciación de tráfico (Internet / NAP)','Internet Dedicado + Monitoreo CPE (Gestion Proactiva)','Internet Dedicado Administrado + Monitoreo CPE (Gestion Proactiva)','Internet Dedicado Empresarial','Internet  Banda ancha FO','MPLS Avanzado Intranet  + Monitoreo CPE (Gestión Proactiva)','MPLS Avanzado Extranet  + Monitoreo CPE (Gestión Proactiva)','MPLS Avanzado con Punta Backend','MPLS Avanzado con Punta en Rack de Appliance (Componente Datacenter)','MPLS Avanzado con Punta Claro Connect','MPLS Transaccional','Telefonia Pública - Líneas Análogas','Telefonia Pública - Líneas E1 - R2','Telefonia Pública - Líneas E1 - PRI','Telefonia Pública - Línea SIP (Troncal IP Ethernet con Audiocodec o GW Cisco)','Telefonia Pública - Línea SIP (Centralizado)','PBX Distribuida - Línea SIP  (Troncal IP Ethernet con Audiocodec o GW Cisco)','PBX Distribuida - Línea SIP  (Centralizado)','PBX Distribuida  Linea E1 -R2','PBX Distribuida  Linea E1 -PRI','Telefonia Corporativa','Local - P2P','Local - P2MP','Nacional - P2P','Nacional - P2MP','VPRN'];
  const activityType = ['Instalar UM con PE','Instalar UM con PE sobre OTP de Pymes','Instalar UM con CT','Instalar UM con HFC','Instalar UM con 3G']
  const requires_release_reources = ['SI - Generar Tarea de Desconexión Tercero al finalizar el Traslado','NO - Recursos de UM Propia en Sede Antigua']
 
  const handleFieldChange = (event, field) => {
    event.persist && event.persist();
    var input = inputsOtp.includes(field);
    if (input === true) document.getElementsByName(field)[0].parentNode.parentNode.classList.add('otp_change');
    document.getElementsByName(field)[0].parentNode.parentNode.classList.remove('selected_error');

    dispatch(saveFormKickoff({
      ...formKickoff,
      basicData: {
        ...formKickoff.basicData,
        [field]: event.target.value,
      }
    }));
  
  };

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader title="Datos Basicos" />
      <CardContent>
        <Grid
          container
          spacing={4}
        >
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('city') !== -1 ? true : false}
              fullWidth
              label="CIUDAD"
              name="city"
              onChange={event =>
                handleFieldChange(event, 'city', event.target.value)
              }
              value={formKickoff?.basicData?.city}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('sourceAddress') !== -1 ? true : false}
              fullWidth
              helperText="actual del servicio"
              label="Direccion ubicacion"
              name="sourceAddress"
              onChange={event =>
                handleFieldChange(event, 'sourceAddress', event.target.value)
              }
              value={formKickoff?.basicData?.sourceAddress}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={4}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('destinationAddress') !== -1 ? true : false}
              fullWidth
              helperText="donde se traslada el servicio"
              label="Direccion"
              name="destinationAddress"
              onChange={event =>
                handleFieldChange(event, 'destinationAddress', event.target.value)
              }
              value={formKickoff?.basicData?.destinationAddress}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('predio_type') !== -1 ? true : false}
              fullWidth
              label="TIPO PREDIO"
              name="predio_type"
              onChange={event =>
                handleFieldChange(
                  event,
                  'predio_type',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.basicData?.predio_type}
              variant="outlined"
            >
              <option
                value=""
              />
              {statusOptions.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('nit') !== -1 ? true : false}
              fullWidth
              label="Nit del Cliente"
              name="nit"
              onChange={event =>
                handleFieldChange(event, 'nit', event.target.value)
              }
              value={formKickoff?.basicData?.nit}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              helperText="CODIGO DE SERVICIO/CIUDAD/SERVICIO/COMERCIO O SEDE DEL CLIENTE"
              label="Alias del Lugar"
              name="alias"
              onChange={event =>
                handleFieldChange(event, 'alias', event.target.value)
              }
              value={formKickoff?.basicData?.alias}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              disabled
              fullWidth
              label="OTP"
              name="projectId"
              onChange={event =>
                handleFieldChange(event, 'projectId', event.target.value)
              }
              value={formKickoff?.basicData?.projectId}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="OTP Asociada"
              name="otp_asociada"
              onChange={event =>
                handleFieldChange(event, 'otp_asociada', event.target.value)
              }
              required
              value={formKickoff?.basicData?.otp_asociada}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('amount_service_transfer') !== -1 ? true : false}
              fullWidth
              label="Cantidad de servicio a trasladar"
              name="amount_service_transfer"
              onChange={event =>
                handleFieldChange(event, 'amount_service_transfer', event.target.value)
              }
              value={formKickoff?.basicData?.amount_service_transfer}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('transfer_service_code') !== -1 ? true : false}
              fullWidth
              label="Codigo de servicio a trasladar"
              name="transfer_service_code"
              onChange={event =>
                handleFieldChange(event, 'transfer_service_code', event.target.value)
              }
              value={formKickoff?.basicData?.transfer_service_code}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={6}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('external_transfer_type') !== -1 ? true : false}
              fullWidth
              label="Tipo de traslado externo"
              name="external_transfer_type"
              onChange={event =>
                handleFieldChange(
                  event,
                  'external_transfer_type',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.basicData?.external_transfer_type}
              variant="outlined"
            >
              <option
                value=""
              />
              {external_transfer_type.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('service_type') !== -1 ? true : false}
              fullWidth
              label="Tipo de Servicio "
              name="service_type"
              onChange={event =>
                handleFieldChange(
                  event,
                  'service_type',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.basicData?.service_type}
              variant="outlined"
            >
              <option
                value=""
              />
              {serviceType.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              fullWidth
              label="Ancho de Banda"
              name="ancho_banda"
              onChange={event =>
                handleFieldChange(event, 'ancho_banda', event.target.value)
              }
              required
              value={formKickoff?.basicData?.ancho_banda}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={6}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('activity_type') !== -1 ? true : false}
              fullWidth
              label="Tipo de Actividad "
              name="activity_type"
              onChange={event =>
                handleFieldChange(
                  event,
                  'activity_type',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.basicData?.activity_type}
              variant="outlined"
            >
              <option
                value=""
              />
              {activityType.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          
          </Grid>
          <Grid
            item
            md={3}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('id_current_service') !== -1 ? true : false}
              fullWidth
              helperText="(Aplica para UM Existente)"
              label="ID del servicio actual"
              name="id_current_service"
              onChange={event =>
                handleFieldChange(event, 'id_current_service', event.target.value)
              }
              value={formKickoff?.basicData?.id_current_service}
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={6}
            xs={12}
          >
            <TextField
              className={`${classes.dateField} selected_error`}
              error={checkKickOff.indexOf('requires_release_reources') !== -1 ? true : false}
              fullWidth
              helperText="Ultima milla antigua (Proveedor Tercero)"
              label="Requiere liberacion de recursos"
              name="requires_release_reources"
              onChange={event =>
                handleFieldChange(
                  event,
                  'requires_release_reources',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={formKickoff?.basicData?.requires_release_reources}
              variant="outlined"
            >
              <option
                value=""
              />
              {requires_release_reources.map(option => (
                <option
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))}
            </TextField>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

BasicData.propTypes = {
  className: PropTypes.string
};

export default BasicData;
