import React, { useEffect }from 'react';
import useRouter from 'utils/useRouter';
import { useSelector, useDispatch } from 'react-redux';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import PersonPinIcon from '@material-ui/icons/PersonPin';
import BookIcon from '@material-ui/icons/Book';
import { 
  AppBar,
  Box,
  Grid,
  Tab,
  Tabs,
  Typography,
  Backdrop,
  CircularProgress,
  Fab,
  Tooltip
} from '@material-ui/core';
import { Alert } from 'components';
import {
  Header,
  Baseline,
  InitialReport,
  InternetService,
  ServiceFacility,
  Landline,
  PrivateLine,
  Mpls,
  DataCenterPoint,
  ManagedLan,
  ManagedPBX,
  ExternalTransfer,
  InternalTransfer,
  Novelty,
  InputSave,
  OtpInfo
} from './components';
import { saveCheckKickOff, saveOtp, saveInputsOTP } from 'actions';
import getAuthorization from 'utils/getAuthorization';
import axios from 'utils/axios';

function TabPanel(props) {
  const { children, value, index, ...other } = props;
  return (
    <div
      aria-labelledby={`scrollable-force-tab-${index}`}
      hidden={value !== index}
      id={`scrollable-force-tabpanel-${index}`}
      role="tabpanel"
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-force-tab-${index}`,
    'aria-controls': `scrollable-force-tabpanel-${index}`,
  };
}

const drawerWidth = 390;

const useStyles = makeStyles((theme) => ({
  alert: {
    marginTop: theme.spacing(3)
  },
  aboutAuthor: {
    marginTop: theme.spacing(3)
  },
  aboutProject: {
    marginTop: theme.spacing(3)
  },
  actions: {
    marginTop: theme.spacing(3)
  },
  backdrop: {
    zIndex: theme.zIndex.modal + 1,
    color: '#fff',
  },
  bottomFloat: {
    position: 'absolute',
    bottom: '25px',
    right: '25px',
    zIndex: '2',
  },
  root: {
    display: 'flex',
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginRight: drawerWidth,
  },
  title: {
    flexGrow: 1,
  },
  hide: {
    display: 'none',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginRight: 0
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginRight: 350,
  },
}));

// eslint-disable-next-line react/no-multi-comp
const TaskManagementFormat = () => {
  const classes = useStyles();
  const router = useRouter();

  const taskId = router.match.params.taskId
  const projectId = router.match.params.projectId
  const activityId = router.match.params.activityId

  const dispatch = useDispatch();
  const [value, setValue] = React.useState(0);
  const [dataTask, setDataTask] = React.useState({});
  // const selectedData = useSelector(state => state.tasks.selectedTasks);
  const [open, setOpen] = React.useState(true);
  const { service } = useSelector(state => state.serviceReducer);
  const [openDrawer, setOpenDrawer] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpenDrawer(true);
  };

  const handleDrawerClose = () => {
    setOpenDrawer(false);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const formatBase = (service) => {
    switch (service) {
      case 'Internet Dedicado Empresarial':
      case 'Internet Dedicado':
      case 'PL ETHERNET':
        return(<InternetService oth={activityId} />)

      case 'Instalación Servicio Telefonia Fija PBX Distribuida Linea E1':
      case 'Instalación Servicio Telefonia Fija PBX Distribuida Linea SIP':
      case 'Instalación Servicio Telefonia Fija PBX Distribuida Linea SIP con Gateway de Voz':
      case 'Instalación Telefonía Publica Básica - Internet Dedicado':
        return(<Landline oth={activityId} />)

      case 'PRIVATE LINE':
        return(<PrivateLine oth={activityId} />)

      case 'MPLS Avanzado Intranet':
      case 'MPLS Avanzado Intranet - Varios Puntos':
      case 'MPLS Avanzado Intranet con Backup de Ultima Milla - NDS 2':
      case 'MPLS Avanzado Intranet con Backup de Ultima Milla y Router - NDS1':
      case 'MPLS Avanzado Extranet':
      case 'Backend MPLS':
      case 'MPLS Avanzado con Componente Datacenter Claro':
      case 'MPLS Transaccional 3G':
        return(<Mpls oth={activityId} />)

      case 'Punta DATACENTER':
        return(<DataCenterPoint oth={activityId} />)

      case 'LAN ADMINISTRADA':
        return(<ManagedLan oth={activityId} />)

      case 'SOLUCIONES ADMINISTRATIVAS - COMUNICACIONES UNIFICADAS PBX ADMINISTRADA':
        return(<ManagedPBX oth={activityId} />)

      case 'Traslado Externo Servicio':
        return(<ExternalTransfer oth={activityId} />)

      case 'Traslado Interno Servicio':
        return(<InternalTransfer oth={activityId} />)

      case 'Cambio de Equipos Servicio':
      case 'Cambio de Servicio Telefonia Fija Pública Linea Basica a Linea E1':
      case 'Cambio de Servicio Telefonia Fija Pública Linea SIP a PBX Distribuida Linea SIP':
      case 'Cambio de Última Milla':
      case 'Cambio de Equipo':
        return(<Novelty oth={activityId} />)

      default:
        return '';
    }

  }
  useEffect(() => {
    let mounted = true
    dispatch(saveCheckKickOff([]));
    
    const fetchProject = () => {
      axios.post('/projects/pagination', { 'eq': [{ 'field': 'projectId', 'values': [`${projectId}`]}] }, getAuthorization()).then(response => {
        if(response.data.data[0]) handleClose();
        if (mounted) {
          dispatch(saveOtp(response.data.data[0]));
          dispatch(saveInputsOTP([
            'city',
            'sourceAddress',
            'destinationAddress',
            'client',
          ]));
          
        }
      });
    };

    fetchProject();
    const fetchTask = () => {
      axios.post('tasks/pagination', { 'eq': [{ 'field': '_id', 'values': `${taskId}`}] }, getAuthorization()).then(response => {
        if (mounted) {
          setDataTask(response.data.data[0]);
        }
      });
    };

    fetchTask();

    return () => {
      mounted = false;
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleChange = (_event, newValue) => {
    setValue(newValue);
  };

  return (
    <div>
      <AppBar
        color="default"
        position="sticky"
      >
        <Tabs
          aria-label="scrollable force tabs example"
          indicatorColor="primary"
          onChange={handleChange}
          scrollButtons="on"
          textColor="primary"
          value={value}
          variant="scrollable"
        >
          <Tab
            icon={<PersonPinIcon />}
            label={`OTP: ${projectId}`}
            {...a11yProps(0)}
          />
        </Tabs>
      </AppBar>
      <TabPanel
        index={0}
        value={0}
      >
        <Header />
        <Grid
          container
          spacing={4}
        >
          <Grid
            item
            md={4}
            xs={12}
          >
            <ServiceFacility id={activityId} />
          </Grid>
        </Grid>
        {service ? (
          <>
            <main
              className={clsx(classes.content, {
                [classes.contentShift]: openDrawer,
              })}
            >
              <Alert
                className={classes.alert}
                message="Fechas"
              />
              <Baseline className={classes.alert} />
              { formatBase(service) }
              <Alert
                className={classes.alert}
                message="Reporte de Inicio"
              />
              <InitialReport className={classes.alert} />

            </main>

            <Tooltip title="Informacion OTP">
              <Fab
                className={classes.bottomFloat}
                color="primary"
                onClick={handleDrawerOpen}
              >
                <BookIcon />
              </Fab>
            </Tooltip>
            <InputSave
              activityId={activityId}
              dataTask={dataTask}
              projectId={projectId}
              taskId={taskId}
            />
          </>
        ) : ''}
      </TabPanel>
      <OtpInfo
        handleDrawerClose={() => handleDrawerClose()}
        openDrawer={openDrawer}
      />

      <Backdrop
        className={classes.backdrop}
        open={open}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
    </div>
  );
}

export default TaskManagementFormat;