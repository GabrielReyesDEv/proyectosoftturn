import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';
import { Button } from '@material-ui/core';
import PropTypes from 'prop-types';
import { Page } from 'components';
import {
  Header,
  DataProject,
  ProjectInfo,
  DataActivity,
} from './components';
import { useSnackbar } from 'notistack';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';
import Cookies from 'js-cookie';

const useStyles = makeStyles(theme => ({
  root: {
    width: theme.breakpoints.values.lg,
    maxWidth: '100%',
    margin: '0 auto',
    padding: theme.spacing(3, 3, 6, 3)
  },
  aboutAuthor: {
    marginTop: theme.spacing(3)
  },
  aboutProject: {
    marginTop: theme.spacing(3)
  },
  projectCover: {
    marginTop: theme.spacing(3)
  },
  projectDetails: {
    marginTop: theme.spacing(3)
  },
  preferences: {
    marginTop: theme.spacing(3)
  },
  actions: {
    marginTop: theme.spacing(3)
  }
}));

const ProjectCreate = () => {
  const classes = useStyles();
  const [projectSelected, setProjectSelected] = useState('');
  const [activityInfo, setActivityInfo] = useState({});
  const [crear, setCrear] = useState(false);
  const [usersSelect, setUsersSelect] = React.useState([]);
  // eslint-disable-next-line no-unused-vars
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  useEffect(() => {
    let mounted = true;
    const initialSelect = async () => {
      const urlU = '/users/pagination';
      const columnsNeeded = {
        'firstName': 1,
        'lastName': 1,
        'identification': 1,
        'roles.area': 1,
      }
      const bodyU = {
        limit: 0,
        page: 1,
        columnsNeeded: columnsNeeded,
      }
      if (mounted) {
        const petition = await fetchQuery(bodyU, urlU);
        usersByArea(petition.dataResponse[0].data, petition.dataResponse[0].data.length);
      }
    }
    initialSelect();
    return () => {
      mounted = false;
    };
  }, []);

  const usersByArea = (users, tam) => {
    let arrayAreas = {};

    for (let index = 0; index < tam; index++) {
      const user = users[index];
      const roles = user.roles;
      const tam2 = user.roles.length;
      for (let index2 = 0; index2 < tam2; index2++) {
        const rol = roles[index2];
        if (arrayAreas.[rol.area]) {
          const hola = arrayAreas.[rol.area].filter(userLocal => userLocal.identification === user.identification);
          if (hola.length === 0) {
            const auxArr = arrayAreas.[rol.area];
            auxArr.push(
              {
                firstName: user.firstName, 
                lastName: user.lastName, 
                identification: user.identification,
                author: `${user.firstName} ${user.lastName}#${user.identification}`,
              }
            )
            arrayAreas = {...arrayAreas, [rol.area]: auxArr};
          }
        } else {
          arrayAreas = {...arrayAreas, [rol.area]: [
            {
              firstName: user.firstName, 
              lastName: user.lastName, 
              identification: user.identification,
              author: `${user.firstName} ${user.lastName}#${user.identification}`,
            }
          ]};
        }
      }
    }
    setUsersSelect(arrayAreas);
  };

  const handleChangeActivity = (values) => {
    setActivityInfo(values);
  }

  const handleSelectProject = (project) => {
    setProjectSelected(project);
  };

  const handleUnSelect = () => {
    setProjectSelected('');
  };

  const validacionErrores = (camposReq, info) => {
    let errorVacios = 0;
    for (let index = 0; index < camposReq.length; index++) {
      const aux = Object.fromEntries(Object.entries(info).filter(([key]) => key === camposReq[index]));
      if (Object.values(aux)[0] === '' || Object.values(aux)[0] === undefined) {
        errorVacios = 1;
        index = camposReq.length
      }
    }
    return errorVacios;
  }

  const fetchQuery = async (data, url, message = '') => {
    let result = 0;
    let dataResponse = [];
    await axios
      .post(
        url,
        data,
        getAuthorization(),
      )
      .then((response) => {
        result = response.data.data.length;
        dataResponse = response.data.data;
        if (message !== '') {
          enqueueSnackbar(message.msg, {
            variant: message.variant,
          });
        }
      })
      .catch((err) => {
        enqueueSnackbar(`error: ${err}`, {
          variant: 'error',
        });
        result = -1;
      })
    return {result, dataResponse};
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    setCrear(true);

    const camposReqActivity = [
      'activityId',
      'status',
      'priority',
      'area',
      'author',
    ];

    let errorVacios = validacionErrores(camposReqActivity, activityInfo);
    
    if (errorVacios === 0) {
      let url = '/activities/pagination';
      let body = {
        'limit': 10,
        'page': 1,
        'eq': [{ 'field': 'activityId', 'values': [activityInfo.activityId] }],
      };
      let registros = await fetchQuery(body, url);
      if (registros.result === 0) {
        let activityFinal = {
          ...activityInfo, 
          projectId: projectSelected.projectId,
        }
        url = '/activities/';
        registros = await fetchQuery(
          activityFinal, 
          url, 
          {
            msg: `Actividad ${activityInfo.activityId} Creado con exito`,
            variant: 'success',
          });
        window.location.href = '/management/activities';
      }  else {
        enqueueSnackbar(`Ya existe la Actividad con id ${activityInfo.activityId}`, {
          variant: 'error',
        });
      }
    }  else {
      enqueueSnackbar('Algunos campos estan vacios', {
        variant: 'error',
      });
    }
  };

  return (
    <Page
      className={classes.root}
      title="Project Create"
    >
      <Header />
      {
        projectSelected !== '' ? (
          <>
            <ProjectInfo
              className={classes.aboutAuthor}
              onUnSelect={handleUnSelect}
              project={projectSelected}
            />
            <DataActivity 
              activityInfo={handleChangeActivity}
              className={classes.aboutAuthor}
              create={crear}
              usersSelect={usersSelect}
            />
            <div className={classes.actions}>
              <Button
                color="primary"
                onClick={handleSubmit}
                variant="contained"
              >
            Crear Actividad
              </Button>
            </div>
          </>
        ) :
          (
            <DataProject
              className={classes.aboutAuthor}
              onSelectProject={handleSelectProject}
            />
          )
      }
      {/* <AboutAuthor className={classes.aboutAuthor} />
      <AboutProject className={classes.aboutProject} />
      <ProjectCover className={classes.projectCover} />
      <ProjectDetails className={classes.projectDetails} />
      <Preferences className={classes.preferences} /> */}
    </Page>
  );
};

ProjectCreate.propTypes = {
  onSelectProject: PropTypes.func
};

export default ProjectCreate;
