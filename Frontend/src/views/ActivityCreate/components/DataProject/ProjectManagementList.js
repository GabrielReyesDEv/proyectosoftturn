/* eslint-disable */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Typography,
  Menu,
  MenuItem,
  Tooltip,
  IconButton,
  ListItemIcon,
  ListItemText
} from '@material-ui/core';
import { Page, Paginate, SearchBar, BottomBar } from 'components';
import { Header, ProjectCard } from './components';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import PersonIcon from '@material-ui/icons/Person';
import Cookies from 'js-cookie';
import { useDispatch, useSelector } from 'react-redux';
import { saveAssignment, setUpdateReport, setAssigned } from 'actions';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  options: {
    marginTop: theme.spacing(3),
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  results: {
    marginTop: theme.spacing(3)
  },
  paginate: {
    marginTop: theme.spacing(3),
    display: 'flex',
    justifyContent: 'center'
  }
}));

const ProjectManagementList = (props) => {
  const { onSelectProject, ...rest } = props;
  const classes = useStyles();
  const [flag] = useState(false);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [page, setPage] = useState(0);
  const [totalReg, setTotalReg] = useState(0);
  const [projects, setProjects] = useState([]);
  const [keyword, setKeyword] = useState('');
  const [filters, setFilters] = useState([]);
  const [selectsCkeckTitle, setSelectsCkeckTitle] = useState('');
  const dispatch = useDispatch();
  const assignment = useSelector(state => state.assignment);
  const updatereport = useSelector(state => state.updatereport);
  const url = '/projects';
  const columnsNeeded = {
    'client': 1,
    'author': 1,
    'projectId': 1,
    'service': 1,
    'type': 1,
    'createdAt': 1,
    'programmingDate': 1,
    'status': 1,
  }
  const dataStates = [
    { 'projectStatus': 'requested' },
    { 'projectStatus': 'denied' },
    { 'projectStatus': 'Created' },
    { 'projectStatus': 'In progress' },
    { 'projectStatus': 'Initiated' },
    { 'projectStatus': 'Terminated' },
    { 'projectStatus': 'Cancelled' },
  ];

  // let userOnyx = '';
  // if (Cookies.get('roles') !== undefined) {
  //   const findUser = JSON.parse(Cookies.get('roles')).find((x) => x.role === 'Ingeniero Gestion');
  //   if (findUser !== '') { userOnyx = findUser.userOnyx; };
  // }

  const fetchProjects = async (body) => {
    await axios
          .post(
            '/projects/pagination',
            body,
            getAuthorization(),
          )
          .then((response) => {
            setProjects(response.data.data)
            setTotalReg(response.data.total);
            setPage(response.data.page);
            setRowsPerPage(response.data.per_page);
          });
  };

  const bodyFunction = (limit, page, searchLocal = '',filtersLocal = []) => {
    const data = [];
    let body = {
      'limit': limit,
      'page': page,
      // 'columnsNeeded': columnsNeeded
    };
    if(searchLocal !== '') {
      for (var i in columnsNeeded) data.push({ 'field': [i], 'value': searchLocal});
      body = { ...body, 'or': data };
    }
    if(filtersLocal.length !== 0) {
      body = { ...body, 'and': filtersLocal};
    }
    return body;
  }

  useEffect(() => {
    fetchProjects({limit: 10, page: 1, columnsNeeded});
  }, flag);

  const handleFilter = (e) => {
    const filtersLocal = [];
    if (e.createAt[0] !== '' || e.createAt[1] !== '' || e.status.length !== 0 || e.updateAt[0] !== '' || e.updateAt[1] !== '') {
      if (e.createAt[0] !== '' || e.createAt[1] !== '') filtersLocal.push({ 'field': 'createAt', 'value': e.createAt[0] !== '' && e.createAt[1] !== '' ? e.createAt : (e.createAt[0] === '' ? [e.createAt[1]] : [e.createAt[0]]) });
      if (e.updateAt[0] !== '' || e.updateAt[1] !== '') filtersLocal.push({ 'field': 'updateAt', 'value': e.updateAt[0] !== '' && e.updateAt[1] !== '' ? e.updateAt : (e.updateAt[0] === '' ? [e.updateAt[1]] : [e.updateAt[0]]) });
      if (e.status.length !== 0) filtersLocal.push({ 'field': 'status', 'value': e.status });
    }
    setFilters(filtersLocal);
    const body = bodyFunction(rowsPerPage, 1, keyword, filtersLocal);
    fetchProjects(body);
  };

  const handleSearch = (e) => {
    setKeyword(e);
    const body = bodyFunction(rowsPerPage, 1, e, filters);
    fetchProjects(body);
  };

  const handleChangePage = (e) => {
    const body = bodyFunction(rowsPerPage, (e.selected + 1), keyword, filters);
    fetchProjects(body);
  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleAssignment = () => {
    setSelectsCkeckTitle('Asignar Actividades');
    dispatch(saveAssignment(true));
    handleClose();
  }

  const handleUpdateReport = () => {
    setSelectsCkeckTitle('Reporte de Actualización');
    dispatch(saveAssignment(true));
    handleClose();
  }

  const handleCloseBottomBar = () => {
    dispatch(saveAssignment(false));
    dispatch(setUpdateReport([]));
    dispatch(setAssigned([]));
  }
  

  return (
    <Page
      className={classes.root}
      title="Project Management List"
    >
      <Header />
      <SearchBar
        states={dataStates}
        value={keyword}
        onFilter={handleFilter}
        onSearch={handleSearch}
      />
      <div className={classes.results}>
        <div className={classes.options}>
          <Typography
            color="textSecondary"
            gutterBottom
            variant="body2"
          >
            {totalReg} Registros encontrados. Página {page} de{' '}
            {Math.ceil(totalReg / rowsPerPage)}
          </Typography>
        </div>
        {projects.map(project => (
          <ProjectCard
            key={project.id}
            onSelectProject={onSelectProject}
            project={project}
          />
        ))}
      </div>
      <div className={classes.paginate}>
        <Paginate pageCount={Math.ceil(totalReg / rowsPerPage)} onPageChange={handleChangePage}/>
      </div>
    </Page>
  );
};

ProjectManagementList.propTypes = {
  onSelectProject: PropTypes.func,
};

export default ProjectManagementList;
