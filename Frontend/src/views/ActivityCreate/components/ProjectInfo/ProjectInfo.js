import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import moment from 'moment';
import { makeStyles } from '@material-ui/styles';
import {
  Avatar,
  Card,
  CardHeader,
  CardContent,
  Link,
  colors,
  Button,
  Typography,
} from '@material-ui/core';
import getInitials from 'utils/getInitials';

const useStyles = makeStyles(theme => ({
  root: {},
  alert: {
    marginBottom: theme.spacing(3)
  },
  formGroup: {
    marginBottom: theme.spacing(3)
  },
  fieldGroup: {
    display: 'flex',
    alignItems: 'center'
  },
  fieldGroupSpaced: {
    display: 'flex',
    alignItems: 'center',
    '& > * + *': {
      marginLeft: theme.spacing(2)
    }
  },
  fieldHint: {
    margin: theme.spacing(1, 0)
  },
  tags: {
    marginTop: theme.spacing(1),
    '& > * + *': {
      marginLeft: theme.spacing(1)
    }
  },
  flexGrow: {
    flexGrow: 1
  },
  dateField: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  },
  content: {
    padding: theme.spacing(2),
    flexGrow: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
      flexWrap: 'wrap'
    },
    '&:last-child': {
      paddingBottom: theme.spacing(2)
    }
  },
  header: {
    maxWidth: '100%',
    width: 240,
    display: 'flex',
    [theme.breakpoints.down('sm')]: {
      marginBottom: theme.spacing(2),
      flexBasis: '100%'
    }
  },
  avatar: {
    marginRight: theme.spacing(2)
  },
  stats: {
    padding: theme.spacing(1),
    [theme.breakpoints.down('sm')]: {
      flexBasis: '50%'
    }
  },
  actions: {
    padding: theme.spacing(1),
    [theme.breakpoints.down('sm')]: {
      flexBasis: '50%'
    },
    display: 'flex',
    alignItems: 'center',
  }
}));

const DataActivity = props => {
  const { onUnSelect, project, className, ...rest } = props;
  const classes = useStyles();

  const statusColors = {
    '2- En ejecución': colors.orange[600],
    Canceled: colors.grey[600],
    Completed: colors.green[600]
  };

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader title="Proyecto Asociado" />
      <CardContent className={classes.content}>
        <div className={classes.header}>
          <Avatar
            alt="Author"
            className={classes.avatar}
            src={project.client}
          >
            {getInitials(project.author?.split('#')[0])}
          </Avatar>
          <div>
            <Typography variant="body2">
              <Link
                color="textPrimary"
                to="#"
                variant="h5"
              >
                {project.client}
              </Link>
            </Typography>
            <Typography variant="body2">
              por {' '}
              <Link //Leonel: retirar o agregar ruta de direccionamiento en link
                color="textPrimary"
                to="/management/users"
                variant="h6"
              >
                {project.author?.split('#')[0]}
              </Link>
            </Typography>
          </div>
        </div>
        <div className={classes.stats}>
          <Typography variant="h6">
            {''}
            {project.projectId}
          </Typography>
          <Typography variant="body2">Id Proyecto</Typography>
        </div>
        <div className={classes.stats}>
          <Typography variant="h6">{project.type}</Typography>
          <Typography variant="body2">Tipo de Trabajo</Typography>
        </div>
        <div className={classes.stats}>
          <Typography variant="h6">
            {moment(project.createdAt).format('DD MMMM YYYY')}
          </Typography>
          <Typography variant="body2">Inicio del Proyecto</Typography>
        </div>
        <div className={classes.stats}>
          <Typography variant="h6">
            {moment(project.programmingDate).format('DD MMMM YYYY')}
          </Typography>
          <Typography variant="body2">Compromiso del Proyecto</Typography>
        </div>
        <div className={classes.stats}>
          <Typography
            style={{ color: statusColors[project.status] }}
            variant="h6"
          >
            {project.status}
          </Typography>
          <Typography variant="body2">Estado del proyecto</Typography>
        </div>
      </CardContent>
      <div className={classes.actions}>
        <Button
          color="primary"
          onClick={onUnSelect}
          size="small"
          variant="outlined"
        >
              Seleccionar otro Proyecto
        </Button>
      </div>
    </Card>
  );
};

DataActivity.propTypes = {
  className: PropTypes.string,
  onUnSelect: PropTypes.func,
  project: PropTypes.object,
};

export default DataActivity;
