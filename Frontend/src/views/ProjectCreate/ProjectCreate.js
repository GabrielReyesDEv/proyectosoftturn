import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';
import { Button, Switch } from '@material-ui/core';

import { Page } from 'components';
import {
  Header,
  DataProject,
  DataActivity,
} from './components';
import { useSnackbar } from 'notistack';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';
import FormControlLabel from '@material-ui/core/FormControlLabel';

const useStyles = makeStyles(theme => ({
  root: {
    width: theme.breakpoints.values.lg,
    maxWidth: '100%',
    margin: '0 auto',
    padding: theme.spacing(3, 3, 6, 3)
  },
  aboutAuthor: {
    marginTop: theme.spacing(3)
  },
  dataProject: {
    marginTop: theme.spacing(3)
  },
  aboutProject: {
    marginTop: theme.spacing(3)
  },
  projectCover: {
    marginTop: theme.spacing(3)
  },
  projectDetails: {
    marginTop: theme.spacing(3)
  },
  preferences: {
    marginTop: theme.spacing(3)
  },
  actions: {
    marginTop: theme.spacing(3)
  }
}));

const ProjectCreate = () => {
  console.log('localStorage.getItem(items-color): ', localStorage.getItem('items-color'));
  const classes = useStyles();
  const [crear, setCrear] = useState(false);
  const [projectInfo, setProjectInfo] = useState({});
  const [activityInfo, setActivityInfo] = useState({});
  const [checked, setChecked] = React.useState(false);
  const [usersSelect, setUsersSelect] = React.useState([]);
  // eslint-disable-next-line no-unused-vars
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  useEffect(() => {
    let mounted = true;
    const initialSelect = async () => {
      const urlU = '/users/pagination';
      const columnsNeeded = {
        'firstName': 1,
        'lastName': 1,
        'identification': 1,
        'roles.area': 1,
      }
      const bodyU = {
        limit: 0,
        page: 1,
        columnsNeeded: columnsNeeded,
      }
      if (mounted) {
        const petition = await fetchQuery(bodyU, urlU);
        usersByArea(petition.dataResponse[0].data, petition.dataResponse[0].data.length);
      }
    }
    initialSelect();
    return () => {
      mounted = false;
    };
  }, []);

  const usersByArea = (users, tam) => {
    let arrayAreas = {};

    for (let index = 0; index < tam; index++) {
      const user = users[index];
      const roles = user.roles;
      const tam2 = user.roles.length;
      for (let index2 = 0; index2 < tam2; index2++) {
        const rol = roles[index2];
        if (arrayAreas.[rol.area]) {
          const hola = arrayAreas.[rol.area].filter(userLocal => userLocal.identification === user.identification);
          if (hola.length === 0) {
            const auxArr = arrayAreas.[rol.area];
            auxArr.push(
              {
                firstName: user.firstName, 
                lastName: user.lastName, 
                identification: user.identification,
                author: `${user.firstName} ${user.lastName}#${user.identification}`,
              }
            )
            arrayAreas = {...arrayAreas, [rol.area]: auxArr};
          }
        } else {
          arrayAreas = {...arrayAreas, [rol.area]: [
            {
              firstName: user.firstName, 
              lastName: user.lastName, 
              identification: user.identification,
              author: `${user.firstName} ${user.lastName}#${user.identification}`,
            }
          ]};
        }
      }
    }
    setUsersSelect(arrayAreas);
  };

  const toggleChecked = () => {
    setChecked((prev) => !prev);
  };

  const handleChangeProject = (values) => {
    setProjectInfo(values);
  }

  const handleChangeActivity = (values) => {
    setActivityInfo(values);
  }

  const validacionErrores = (camposReq, info) => {
    let errorVacios = 0;
    for (let index = 0; index < camposReq.length; index++) {
      const aux = Object.fromEntries(Object.entries(info).filter(([key]) => key === camposReq[index]));
      if (Object.values(aux)[0] === '' || Object.values(aux)[0] === undefined) {
        errorVacios = 1;
        index = camposReq.length
      }
    }
    return errorVacios;
  }

  const fetchQuery = async (data, url, message = '') => {
    let result = 0;
    let dataResponse = [];
    await axios
      .post(
        url,
        data,
        getAuthorization(),
      )
      .then((response) => {
        result = response.data.data.length;
        dataResponse = response.data.data;
        if (message !== '') {
          enqueueSnackbar(message.msg, {
            variant: message.variant,
          });
        }
      })
      .catch((err) => {
        enqueueSnackbar(`error: ${err}`, {
          variant: 'error',
        });
        result = -1;
      })
    return {result, dataResponse}; 
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    setCrear(true);
    const camposReqProject = [
      'projectId',
      'type',
      'status',
      'priority',
      'author',
    ];
    const camposReqActivity = [
      'activityId',
      'status',
      'priority',
      'area',
    ];

    let errorVacios = validacionErrores(camposReqProject, projectInfo);
    
    if (errorVacios === 0 && checked) {
      errorVacios = validacionErrores(camposReqActivity, activityInfo);
    }
    if (errorVacios === 0) {
      let url = '/projects/pagination';
      let body = {
        'limit': 10,
        'page': 1,
        'eq': [{ 'field': 'projectId', 'values': [projectInfo.projectId] }],
      };
      let registros = await fetchQuery(body, url);
      if (registros.result === 0) {
        if (checked) {
          url = '/activities/pagination';
          body = {
            'limit': 10,
            'page': 1,
            'eq': [{ 'field': 'activityId', 'values': [activityInfo.activityId] }],
          };
          registros = await fetchQuery(body, url);
        }
        if (registros.result === 0) {
          url = '/projects/';
          registros = await fetchQuery(
            projectInfo, 
            url, 
            {
              msg: `Proyecto ${projectInfo.projectId} Creado con exito`,
              variant: 'success',
            });
          if (registros.result !== -1 && checked) {
            let activityFinal = {
              ...activityInfo, 
              author: projectInfo.author,
              projectId: projectInfo.projectId,
              members: projectInfo.members,
              resolucion15: activityInfo.resolucion15 ? activityInfo.resolucion15 : 'SIN RESOLUCION15'
            }
            url = '/activities/';
            registros = await fetchQuery(
              activityFinal, 
              url, 
              {
                msg: `Actividad ${activityInfo.activityId} Creado con exito`,
                variant: 'success',
              });
          }
          if (registros.result !== -1) window.location.href = '/management/projects';
        }  else {
          enqueueSnackbar(`Ya existe la Actividad con id ${activityInfo.activityId}`, {
            variant: 'error',
          });
        }
      }  else {
        enqueueSnackbar(`Ya existe el Proyecto con id ${projectInfo.projectId}`, {
          variant: 'error',
        });
      }
    }  else {
      enqueueSnackbar('Algunos campos estan vacios', {
        variant: 'error',
      });
    }
  };

  return (
    <Page
      className={classes.root}
      title="Project Create"
    >

      <Header />
      <DataProject
        className={classes.dataProject}
        create={crear}
        projectInfo={handleChangeProject}
        usersSelect={usersSelect}
      />
      <FormControlLabel
        className={classes.dataProject}
        control={
          <Switch
            checked={checked}
            onChange={toggleChecked}
            size="small"
          />}
        label="Crear Actividad Asociada"
      />
      {checked && (
        <DataActivity 
          activityInfo={handleChangeActivity}
          className={classes.dataProject}
          create={crear}
          usersSelect={usersSelect}
        />
      )}
      {/* <AboutAuthor className={classes.aboutAuthor} />
      <AboutProject className={classes.aboutProject} />
      <ProjectCover className={classes.projectCover} />
      <ProjectDetails className={classes.projectDetails} />
      <Preferences className={classes.preferences} /> */}
      <div className={classes.actions}>
        <Button
          // color="primary"
          color={`${localStorage.getItem('items-color')}`}
          onClick={handleSubmit}
          variant="contained"
        >
          Crear Proyecto
        </Button>
      </div>
    </Page>
  );
};

export default ProjectCreate;
