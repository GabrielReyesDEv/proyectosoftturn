import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import moment from 'moment';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  TextField,
} from '@material-ui/core';
import { DatePicker } from '@material-ui/pickers';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { Alert } from 'components';
import Cookies from 'js-cookie';

const useStyles = makeStyles(theme => ({
  root: {},
  alert: {
    marginBottom: theme.spacing(3)
  },
  formGroup: {
    marginBottom: theme.spacing(3)
  },
  fieldGroup: {
    display: 'flex',
    alignItems: 'center'
  },
  fieldGroupSpaced: {
    display: 'flex',
    alignItems: 'center',
    '& > * + *': {
      marginLeft: theme.spacing(2)
    }
  },
  fieldHint: {
    margin: theme.spacing(1, 0)
  },
  tags: {
    marginTop: theme.spacing(1),
    '& > * + *': {
      marginLeft: theme.spacing(1)
    }
  },
  flexGrow: {
    flexGrow: 1
  },
  dateField: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  }
}));

const DataProject = props => {
  const { className, create, projectInfo, usersSelect, ...rest } = props;
  const identification = Cookies.get('identification');
  const firstName = Cookies.get('firstName');
  const lastName = Cookies.get('lastName');
  const email = Cookies.get('email');
  const tel = Cookies.get('phone');

  const classes = useStyles();

  const optionsResp = usersSelect.['Nivel 1'];

  const initialValues = {
    projectId: '',
    authorNotify: `${firstName} ${lastName}#${identification}`,
    author: '',
    createdAt: moment().format('YYYY-MM-DD HH:mm'),
    updatedAt: moment().format('YYYY-MM-DD HH:mm'),
    type: '',
    status: '',
    priority: '',
    programmingDate: moment().format('YYYY-MM-DD HH:mm'),
    finishDate: moment().format('YYYY-MM-DD HH:mm'),
    members: [
      {
        name: `${firstName} ${lastName}`,
        mail: email,
        phone: `${tel}`,
      },
      {
        name: 'Melani Viveros',
        mail: 'melani.viverosm.ext@claro.com.co',
        phone: '3102129290',
      },
      {
        name: 'Cristian Ramirez',
        mail: 'cristian.ramirezr.ext@claro.com.co',
        phone: '3102852816',
      }
    ]
  };

  const [values, setValues] = useState({ ...initialValues });
  const [calendarTrigger, setCalendarTrigger] = useState(null);

  projectInfo(values);
  const handleFieldChange = (event, field, value) => {
    event.persist && event.persist();
    if (field === 'author') {
      setValues(values => ({
        ...values,
        [field]: value !== null ? value.author : '',
      }));
    } else {
      setValues(values => ({
        ...values,
        [field]: value
      }));
    } 
  };

  // const handleCalendarOpen = trigger => {
  //   setCalendarTrigger(trigger);
  // };

  const handleCalendarChange = () => {};

  const handleCalendarAccept = date => {
    setValues(values => ({
      ...values,
      [calendarTrigger]: date
    }));
  };

  const handleCalendarClose = () => {
    setCalendarTrigger(false);
  };

  const calendarOpen = Boolean(calendarTrigger);
  const calendarMinDate =
  calendarTrigger === 'startDate'
    ? moment()
    : moment(values.startDate).add(1, 'day');
  const calendarValue = values[calendarTrigger];

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader title="Datos del Proyecto" />
      <CardContent>
        <form>
          <Alert
            className={classes.alert}
            message="Una vez creado el proyecto el Project Id solo podra ser cambiado contactandose con soporte."
          />
          <div className={classes.formGroup}>
            <div className={classes.fieldGroupSpaced}>
              <TextField
                error={create && values.projectId === ''}
                fullWidth
                helperText={create && values.projectId === '' ? '*Campo Requerido' : false}
                label="Project Id"
                name="projectId"
                onChange={event =>
                  handleFieldChange(event, 'projectId', event.target.value)
                }
                required
                value={values.projectId}
                variant="outlined"
              />
              <TextField
                fullWidth
                label="Nombre Cliente"
                name="client"
                onChange={event =>
                  handleFieldChange(event, 'client', event.target.value !== '' ? event.target.value : undefined)
                }
                value={values.client}
                variant="outlined"
              />
            </div>
          </div>
          <div className={classes.formGroup}>
            <div className={classes.fieldGroupSpaced}>
              <Autocomplete
                fullWidth
                getOptionLabel={(option) => option.title}
                id="type"
                // eslint-disable-next-line no-use-before-define
                name="type"
                onChange={event =>
                  handleFieldChange(event, 'type', event.target.innerText)
                }
                options={optionsType}
                renderInput={(params) => (
                  // eslint-disable-next-line react/jsx-props-no-spreading
                  <TextField
                    {...params}
                    error={create && values.type === ''}
                    fullWidth
                    helperText={create && values.type === '' ? '*Campo Requerido' : false}
                    label="Tipo de Proyecto"
                    required
                    variant="outlined"
                  />
                )}
              />
              <Autocomplete
                fullWidth
                getOptionLabel={(option) => option.title}
                id="status"
                // eslint-disable-next-line no-use-before-define
                name="status"
                onChange={event =>
                  handleFieldChange(event, 'status', event.target.innerText)
                }
                options={optionsStatus}
                renderInput={(params) => (
                  // eslint-disable-next-line react/jsx-props-no-spreading
                  <TextField
                    {...params}
                    error={create && values.status === ''}
                    fullWidth
                    helperText={create && values.status === '' ? '*Campo Requerido' : false}
                    label="Estado del Proyecto"
                    required
                    variant="outlined"
                  />
                )}
              />
              <Autocomplete
                fullWidth
                getOptionLabel={(option) => option.title}
                id="priority"
                // eslint-disable-next-line no-use-before-define
                name="priority"
                onChange={event =>
                  handleFieldChange(event, 'priority', parseInt(event.target.innerText))
                }
                options={optionsPriority}
                renderInput={(params) => (
                  // eslint-disable-next-line react/jsx-props-no-spreading
                  <TextField
                    {...params}
                    error={create && values.priority === ''}
                    fullWidth
                    helperText={create && values.priority === '' ? '*Campo Requerido' : false}
                    label="Prioridad del Proyecto"
                    required
                    variant="outlined"
                  />
                )}
                required
                type="number"
              />
            </div>
          </div>
          <div className={classes.formGroup}>
            <div className={classes.fieldGroup}>
              <TextField
                className={classes.dateField}
                fullWidth
                label="Ciudad"
                name="city"
                onChange={event =>
                  handleFieldChange(event, 'city', event.target.value !== '' ? event.target.value : undefined)
                }
                value={values.city}
                variant="outlined"
              />
              <TextField
                InputLabelProps={{
                  shrink: true,
                }}
                className={classes.dateField}
                defaultValue={moment(values.programmingDate, 'YYYY-MM-DD HH:mm').format('YYYY-MM-DD')}
                fullWidth
                id="programmingDate"
                label="Fecha de Programación"
                name="programmingDate"
                onChange={event =>
                  handleFieldChange(event, 'programmingDate', moment(event.target.value, 'YYYY-MM-DD').format('YYYY-MM-DD HH:mm'))
                }
                required
                type="date"
                // value={values && values.programmingDate && moment(values.programmingDate, 'YYYY-MM-DD HH:mm').format('MM/DD/YYYY')}
                variant="outlined"
              />
              <TextField
                InputLabelProps={{
                  shrink: true,
                }}
                className={classes.dateField}
                defaultValue={moment(values.finishDate, 'YYYY-MM-DD HH:mm').format('YYYY-MM-DD')}
                fullWidth
                label="Fecha de Compromiso"
                name="finishDate"
                onChange={event =>
                  handleFieldChange(event, 'finishDate', moment(event.target.value, 'YYYY-MM-DD').format('YYYY-MM-DD HH:mm'))
                }
                required
                type="date"
                variant="outlined"
              />
            </div>
          </div>
          <div className={classes.formGroup}>
            <div className={classes.fieldGroup}>
              <TextField
                className={classes.dateField}
                fullWidth
                label="Resolucion 1"
                name="resolucion1"
                onChange={event =>
                  handleFieldChange(event, 'resolucion1', event.target.value !== '' ? event.target.value : undefined)
                }
                value={values.resolucion1}
                variant="outlined"
              />
              <TextField
                className={classes.dateField}
                fullWidth
                label="Resolucion 2"
                name="resolucion2"
                onChange={event =>
                  handleFieldChange(event, 'resolucion2', event.target.value !== '' ? event.target.value : undefined)
                }
                value={values.resolucion2}
                variant="outlined"
              />
            </div>
          </div>
          <div className={classes.formGroup}>
            <div className={classes.fieldGroup}>
              <TextField
                className={classes.dateField}
                fullWidth
                label="Resolucion 3"
                name="resolucion3"
                onChange={event =>
                  handleFieldChange(event, 'resolucion3', event.target.value !== '' ? event.target.value : undefined)
                }
                value={values.resolucion3}
                variant="outlined"
              />
              <TextField
                className={classes.dateField}
                fullWidth
                label="Resolucion 4"
                name="resolucion4"
                onChange={event =>
                  handleFieldChange(event, 'resolucion4', event.target.value !== '' ? event.target.value : undefined)
                }
                value={values.resolucion4}
                variant="outlined"
              />
            </div>
          </div>
          <div className={classes.formGroup}>
            <div className={classes.fieldGroupSpaced}>
              <Autocomplete
                fullWidth
                getOptionLabel={(option) => (option.author.split('#')[0])}
                id="author"
                name="author"
                // eslint-disable-next-line no-use-before-define
                onChange={(event, val) =>
                  handleFieldChange(event, 'author', val)
                }
                options={optionsResp}
                renderInput={(params) => (
                  // eslint-disable-next-line react/jsx-props-no-spreading
                  <TextField
                    {...params}
                    error={create && values.author === ''}
                    fullWidth
                    helperText={create && values.author === '' ? '*Campo Requerido' : false}
                    label="Responsable"
                    required
                    variant="outlined"
                  />
                )}
                renderOption={(option) => (
                  <React.Fragment>
                    {option.author.replace('#', ' ')}
                  </React.Fragment>
                )}
              />
            </div>
          </div>
        </form>
      </CardContent>
      <DatePicker
        minDate={calendarMinDate}
        onAccept={handleCalendarAccept}
        onChange={handleCalendarChange}
        onClose={handleCalendarClose}
        open={calendarOpen}
        style={{ display: 'none' }} // Temporal fix to hide the input element
        value={calendarValue}
        variant="dialog"
      />
    </Card>
  );
};

const optionsType = [
  { title: 'Adicion / Retiro de numeros', value: 'Adicion / Retiro de numeros' },
  { title: 'Adicion de cuentas de correo', value: 'Adicion de cuentas de correo' },
  { title: 'Adición de Equipos', value: 'Adición de Equipos' },
  { title: 'Cambio de Equipos', value: 'Cambio de Equipos' },
  { title: 'Cambio de Servicio', value: 'Cambio de Servicio' },
  { title: 'Cambio de Velocidad(Ampliación - Reducción)', value: 'Cambio de Velocidad(Ampliación - Reducción)' },
  { title: 'Cambio Plan Pymes', value: 'Cambio Plan Pymes' },
  { title: 'Cambio Tipo Acceso, Servicio y Ampliación', value: 'Cambio Tipo Acceso, Servicio y Ampliación' },
  { title: 'Desconexión Temporal', value: 'Desconexión Temporal' },
  { title: 'Desinstalación Cliente Corporativo', value: 'Desinstalación Cliente Corporativo' },
  { title: 'Instalación Grandes Proyectos', value: 'Instalación Grandes Proyectos' },
  { title: 'Instalación Proyectos Especiales', value: 'Instalación Proyectos Especiales' },
  { title: 'Instalación Pymes', value: 'Instalación Pymes' },
  { title: 'Instalación Servicios Datacenter', value: 'Instalación Servicios Datacenter' },
  { title: 'Instalación Servicios Estándar', value: 'Instalación Servicios Estándar' },
  { title: 'Instalación Servicios Transmisión', value: 'Instalación Servicios Transmisión' },
  { title: 'Novedades soluciones administradas', value: 'Novedades soluciones administradas' },
  { title: 'Nueva Instalación', value: 'Nueva Instalación' },
  { title: 'Reconexión Cliente', value: 'Reconexión Cliente' },
  { title: 'Retiro de Equipos', value: 'Retiro de Equipos' },
  { title: 'Suspensión por Cartera', value: 'Suspensión por Cartera' },
  { title: 'Suspensión por Cartera Pymes', value: 'Suspensión por Cartera Pymes' },
  { title: 'Traslado Externo', value: 'Traslado Externo' },
  { title: 'Traslado Interno', value: 'Traslado Interno' },
  { title: 'Cambio de Medio', value: 'Cambio de Medio' },
  { title: 'CambioTipo de Acceso', value: 'CambioTipo de Acceso' },
  { title: 'Caso de Seguimiento', value: 'Caso de Seguimiento' },
  { title: 'Factibilidad', value: 'Factibilidad' },
  { title: 'Instalación Demo Implementación', value: 'Instalación Demo Implementación' },
  { title: 'Plan Accion', value: 'Plan Accion' },
  { title: 'Punto Cental (Nueva)', value: 'Punto Cental (Nueva)' },
  { title: 'Punto Central', value: 'Punto Central' },
  { title: 'Punto Central Pymes', value: 'Punto Central Pymes' },
  { title: 'RED - Crecimiento de Red', value: 'RED - Crecimiento de Red' },
  { title: 'RED - Expansion de Red', value: 'RED - Expansion de Red' },
];

const optionsStatus = [
  { title: '1- Generada', value: '1- Generada' },
  { title: '2- En ejecución', value: '2- En ejecución' },
  { title: '3- Terminada', value: '3- Terminada' },
  { title: 'Alistamiento', value: 'Alistamiento' },
  { title: 'Caja OB', value: 'Caja OB' },
  { title: 'Detalle Solución', value: 'Detalle Solución' },
  { title: 'Diseño', value: 'Diseño' },
  { title: 'Ejecución ruta', value: 'Ejecución ruta' },
  { title: 'Entrega', value: 'Entrega' },
  { title: 'Factibilidad', value: 'Factibilidad' },
  { title: 'Instalación', value: 'Instalación' },
  { title: 'Liberación Recursos', value: 'Liberación Recursos' },
  { title: 'Pendiente Cliente', value: 'Pendiente Cliente' },
  { title: 'Pendiente Telmex', value: 'Pendiente Telmex' },
];

const optionsPriority = [
  {title: '0', value: 0},
  {title: '1', value: 1},
  {title: '2', value: 2},
  {title: '3', value: 3},
  {title: '4', value: 4},
  {title: '5', value: 5},
  {title: '6', value: 6},
  {title: '7', value: 7},
  {title: '8', value: 8},
  {title: '9', value: 9},  
]

DataProject.propTypes = {
  className: PropTypes.string,
  create: PropTypes.bool,
  projectInfo: PropTypes.func,
  usersSelect: PropTypes.array,
};

export default DataProject;
