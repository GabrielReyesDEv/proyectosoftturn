import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import moment from 'moment';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  TextField,
  Grid,
} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Cookies from 'js-cookie';
import { Alert } from 'components';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';

const useStyles = makeStyles(theme => ({
  root: {},
  alert: {
    marginBottom: theme.spacing(3)
  },
  formGroup: {
    marginBottom: theme.spacing(3)
  },
  fieldGroup: {
    display: 'flex',
    alignItems: 'center'
  },
  fieldGroupSpaced: {
    display: 'flex',
    alignItems: 'center',
    '& > * + *': {
      marginLeft: theme.spacing(2)
    }
  },
  fieldHint: {
    margin: theme.spacing(1, 0)
  },
  tags: {
    marginTop: theme.spacing(1),
    '& > * + *': {
      marginLeft: theme.spacing(1)
    }
  },
  flexGrow: {
    flexGrow: 1
  },
  dateField: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  }
}));

const DataActivity = props => {
  const { activityInfo, className, create, usersSelect, ...rest } = props;
  const identification = Cookies.get('identification');
  const firstName = Cookies.get('firstName');
  const lastName = Cookies.get('lastName');
  const email = Cookies.get('email');
  const tel = Cookies.get('phone');

  const [arrAreas, setArrAreas] = useState([]);
  const [arrResponsables, setArrResponsables] = useState([]);

  const classes = useStyles();

  useEffect(() => {
    let mounted = true;
    
    const fetchStatus = () => {
      const group = 'BO'
      axios.get(`/areas?limit=100&page=1&group=${group}`, getAuthorization()).then(response => {
        if (mounted) {
          for (let index = 0; index < response.data.data.length; index++) {
            arrAreas.push({ title: response.data.data[index].name, value: response.data.data[index].name },);
          }
          setArrAreas(arrAreas)
        }
      });
    };
    
    fetchStatus();
    

    return () => {
      mounted = false;
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const initialValues = {
    activityId: '',
    authorNotify: `${firstName} ${lastName}#${identification}`,
    author: '',
    priority: '',
    status: '',
    area: '',
    createdAt: moment().format('YYYY-MM-DD HH:mm'),
    updatedAt: moment().format('YYYY-MM-DD HH:mm'),
    members: [
      {
        name: `${firstName} ${lastName}`,
        mail: email,
        phone: `${tel}`,
      },
      {
        name: 'Melani Viveros',
        mail: 'melani.viverosm.ext@claro.com.co',
        phone: '3102129290',
      },
      {
        name: 'Cristian Ramirez',
        mail: 'cristian.ramirezr.ext@claro.com.co',
        phone: '3102852816',
      }
    ]
  };

  const [values, setValues] = useState({ ...initialValues });

  activityInfo(values);
  const handleFieldChange = (event, field, value) => {
    event.persist && event.persist();
    if (field === 'area') {
      setArrResponsables(usersSelect.[value])
    }
    if (field === 'author') {
      setValues(values => ({
        ...values,
        [field]: value !== null ? value.author : '',
      }));
    } else {
      setValues(values => ({
        ...values,
        [field]: value
      }));
    }
  };

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader title="Actividad Asociada" />
      <CardContent>
        <form>
          <Alert
            className={classes.alert}
            message="Una vez creada la actividad el Activity Id solo podra ser cambiado contactandose con soporte."
          />
          <div className={classes.formGroup}>
            <div className={classes.fieldGroupSpaced}>
              <TextField
                error={create && values.activityId === ''}
                fullWidth
                helperText={create && values.activityId === '' ? '*Campo Requerido' : false}
                label="Activity Id"
                name="activityId"
                onChange={event =>
                  handleFieldChange(event, 'activityId', event.target.value)
                }
                required
                value={values.activityId}
                variant="outlined"
              />
              <Autocomplete
                fullWidth
                getOptionLabel={(option) => option.title}
                id="priority"
                // eslint-disable-next-line no-use-before-define
                name="priority"
                onChange={event =>
                  handleFieldChange(event, 'priority', parseInt(event.target.innerText))
                }
                options={optionsPriority}
                renderInput={(params) => (
                  // eslint-disable-next-line react/jsx-props-no-spreading
                  <TextField
                    {...params}
                    error={create && values.priority === ''}
                    fullWidth
                    helperText={create && values.priority === '' ? '*Campo Requerido' : false}
                    label="Prioridad Actividad"
                    required
                    variant="outlined"
                  />
                )}
              />
              <Autocomplete
                fullWidth
                getOptionLabel={(option) => option.title}
                id="status"
                // eslint-disable-next-line no-use-before-define
                name="status"
                onChange={event =>
                  handleFieldChange(event, 'status', event.target.innerText)
                }
                options={options}
                renderInput={(params) => (
                  // eslint-disable-next-line react/jsx-props-no-spreading
                  <TextField
                    {...params}
                    error={create && values.status === ''}
                    fullWidth
                    helperText={create && values.status === '' ? '*Campo Requerido' : false}
                    label="Estado Actividad"
                    required
                    variant="outlined"
                  />
                )}
              />
            </div>
          </div>
          <div className={classes.formGroup}>
            <div className={classes.fieldGroupSpaced}>
              <TextField
                fullWidth
                label="Resolucion 15"
                name="resolucion15"
                onChange={event =>
                  handleFieldChange(event, 'resolucion15', event.target.value !== '' ? event.target.value : undefined)
                }
                value={values.resolucion15}
                variant="outlined"
              />
              <TextField
                className={classes.dateField}
                fullWidth
                label="Resolucion 26"
                name="resolucion26"
                onChange={event =>
                  handleFieldChange(event, 'resolucion26', event.target.value !== '' ? event.target.value : undefined)
                }
                value={values.resolucion26}
                variant="outlined"
              />
            </div>
          </div>
          <div className={classes.formGroup}>
            <div className={classes.fieldGroupSpaced}>
              <TextField
                fullWidth
                label="Resolucion 37"
                name="resolucion37"
                onChange={event =>
                  handleFieldChange(event, 'resolucion37', event.target.value !== '' ? event.target.value : undefined)
                }
                value={values.resolucion37}
                variant="outlined"
              />
              <TextField
                className={classes.dateField}
                fullWidth
                label="Resolucion 48"
                name="resolucion48"
                onChange={event =>
                  handleFieldChange(event, 'resolucion48', event.target.value !== '' ? event.target.value : undefined)
                }
                value={values.resolucion48}
                variant="outlined"
              />
            </div>
          </div>
          <div className={classes.formGroup}>
            <div className={classes.fieldGroupSpaced}>
              <Autocomplete
                fullWidth
                getOptionLabel={(option) => option.title}
                id="area"
                // eslint-disable-next-line no-use-before-define
                name="area"
                onChange={event =>
                  handleFieldChange(event, 'area', event.target.innerText)
                }
                options={arrAreas}
                renderInput={(params) => (
                  // eslint-disable-next-line react/jsx-props-no-spreading
                  <TextField
                    {...params}
                    error={create && values.area === ''}
                    fullWidth
                    helperText={create && values.area === '' ? '*Campo Requerido' : false}
                    label="Area"
                    required
                    variant="outlined"
                  />
                )}
              />
              {
                values.area !== '' && values.area !== undefined ? (
                  <Autocomplete
                    fullWidth
                    getOptionLabel={(option) => (option.author.split('#')[0])}
                    id="author"
                    name="author"
                    // eslint-disable-next-line no-use-before-define
                    onChange={(event, val) =>
                      handleFieldChange(event, 'author', val)
                    }
                    options={arrResponsables}
                    renderInput={(params) => (
                      // eslint-disable-next-line react/jsx-props-no-spreading
                      <TextField
                        {...params}
                        error={create && values.author === ''}
                        fullWidth
                        helperText={create && values.author === '' ? '*Campo Requerido' : false}
                        label="Responsable"
                        required
                        variant="outlined"
                      />
                    )}
                    renderOption={(option) => (
                      <React.Fragment>
                        {option.author.replace('#', ' ')}
                      </React.Fragment>
                    )}
                  />
                ) : (
                  <Grid
                    item
                    md={'50%'}
                    sm={12}
                    xs={12}
                  />
                )
              }
            </div>
          </div>
        </form>
      </CardContent>
    </Card>
  );
};

const options = [
  { title: '1- Generada', value: '1- Generada' },
  { title: '2- En ejecución', value: '2- En ejecución' },
  { title: '3- Terminada', value: '3- Terminada' },
  { title: 'Alistamiento', value: 'Alistamiento' },
  { title: 'Caja OB', value: 'Caja OB' },
  { title: 'Detalle Solución', value: 'Detalle Solución' },
  { title: 'Diseño', value: 'Diseño' },
  { title: 'Ejecución ruta', value: 'Ejecución ruta' },
  { title: 'Entrega', value: 'Entrega' },
  { title: 'Factibilidad', value: 'Factibilidad' },
  { title: 'Instalación', value: 'Instalación' },
  { title: 'Liberación Recursos', value: 'Liberación Recursos' },
  { title: 'Pendiente Cliente', value: 'Pendiente Cliente' },
  { title: 'Pendiente Telmex', value: 'Pendiente Telmex' },
  { title: 'CONF. Configuracion de servicio', value: 'CONF. Configuracion de servicio' },
];

const optionsPriority = [
  {title: '0', value: 0},
  {title: '1', value: 1},
  {title: '2', value: 2},
  {title: '3', value: 3},
  {title: '4', value: 4},
  {title: '5', value: 5},
  {title: '6', value: 6},
  {title: '7', value: 7},
  {title: '8', value: 8},
  {title: '9', value: 9},
];

DataActivity.propTypes = {
  activityInfo: PropTypes.func,
  className: PropTypes.string,
  create: PropTypes.bool,
  usersSelect: PropTypes.array,
};

export default DataActivity;
