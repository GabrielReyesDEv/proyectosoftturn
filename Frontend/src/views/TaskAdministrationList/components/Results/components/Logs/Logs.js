// Se debe agregar una validacion para el estado previo, cuando se le de guardar y el estado previo es igual al estado seleccionado se debe agregar una nueva fecha como nombre alertDate

import React, { Fragment, useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import PerfectScrollbar from 'react-perfect-scrollbar';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import {
  Button,
  Dialog,
  Typography,
  Grid,
  colors
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import formatDate from 'utils/formatDate';

const useStyles = makeStyles(theme => ({
  root: {
    width: 960
  },
  header: {
    padding: theme.spacing(3),
    maxWidth: 720,
    margin: '0 auto'
  },
  content: {
    padding: theme.spacing(0, 2),
    maxWidth: 720,
    margin: '5% auto'
  },
  helperText: {
    textAlign: 'right',
    marginRight: 0
  },
  list: {
    maxHeight: 200
  },
  author: {
    margin: theme.spacing(4, 0),
    display: 'flex'
  },
  avatar: {
    marginRight: theme.spacing(2)
  },
  actions: {
    backgroundColor: colors.grey[100],
    padding: theme.spacing(2),
    display: 'flex',
    justifyContent: 'center'
  },
  loader: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  textField: {
    marginTop: theme.spacing(2),
  },
  title: {
    marginTop: theme.spacing(2),
  },
  applyButton: {
    color: theme.palette.white,
    backgroundColor: colors.green[600],
    '&:hover': {
      backgroundColor: colors.green[900]
    }
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  }
}));

const Logs = props => {
  const { logs, currentStatus, type, open, onClose, onApply, className, task, ...rest } = props;
  const classes = useStyles();

  const [expanded, setExpanded] = useState(false);

  const handleChangeMore = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  return (
    <Dialog
      maxWidth="lg"
      onClose={onClose}
      open={open}
      disableBackdropClick
      disableEscapeKeyDown
    >
      <Grid
          alignItems="center"
          container
          spacing={2}
        >
          
        
        <div
        {...rest}
        className={clsx(classes.root, className, 'form')}
      >
        <div className={classes.header}>
          <Typography
            align="center"
            className={classes.title}
            gutterBottom
            variant="h3"
          >
            Registros {type}
          </Typography>
          <Typography
            align="center"
            className={classes.subtitle}
            variant="subtitle2"
          >
            {task.projectId && `OTP: ${task.projectId}`}
            {task.activityId && ` OTH: ${task.activityId}`}
          </Typography>
          <Typography
            align="center"
            className={classes.subtitle}
            variant="subtitle2"
          >
            Esto es lo que ha pasado con la actividad
          </Typography>
        </div>
        <div className={classes.content}>
          <div>
          {logs.length > 0 && (
            <Fragment>
              <Typography
                align="start"
                className={classes.title}
                gutterBottom
                variant="h6"
              >
                Detalles
              </Typography>
              <PerfectScrollbar className={classes.list} options={{ suppressScrollX: true }}>
                {logs.map((file, i) => (
                  <Accordion expanded={expanded === `panel${i}`} onChange={handleChangeMore(`panel${i}`)}>
                    <AccordionSummary
                      expandIcon={<ExpandMoreIcon />}
                      aria-controls="panel1bh-content"
                      id="panel1bh-header"
                    >
                      <Typography className={classes.heading}>{`${formatDate(file.createdAt).date} ${formatDate(file.createdAt).time}`}</Typography>
                      <Typography className={classes.secondaryHeading}>{`${file.author.split('#')[0]} / ${file.status}`}</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                      <Typography>
                        {file.description}
                      </Typography>
                    </AccordionDetails>
                  </Accordion>
                ))}
              </PerfectScrollbar>
            </Fragment>
          )}
          </div>
        </div>
        <div className={classes.actions}>
          <Button
            onClick={onClose}
            variant="contained"
          >
            Cerrar
          </Button>
        </div>
      </div>
      </Grid>
    </Dialog>
  );
};

Logs.propTypes = {
  author: PropTypes.object.isRequired,
  className: PropTypes.string,
  onApply: PropTypes.func,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired
};

export default Logs;
