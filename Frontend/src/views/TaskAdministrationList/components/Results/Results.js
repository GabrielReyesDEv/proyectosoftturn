import React, { useState, useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { connect, useDispatch } from 'react-redux';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { makeStyles } from '@material-ui/styles';
import {
  Avatar,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Checkbox,
  Divider,
  Button,
  IconButton,
  Link,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Tooltip,
  Typography,
  LinearProgress,
  colors
} from '@material-ui/core';
import moment from 'moment';
import ScheduleIcon from '@material-ui/icons/Schedule';
import { Label, ReviewStars, GenericMoreButton, TableAssignBar } from 'components';
import { Application, StatusChange, Logs } from './components';
import getTimeStatus from 'utils/getTimeStatus';
import formatDate from 'utils/formatDate';
import duration from 'utils/duration';
import { useTranslation } from 'react-i18next';
import { selectedTasks, saveAssignment, tasksStatus } from 'actions';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';
import Cookies from 'js-cookie';
import { useSnackbar } from 'notistack';
import ListAltIcon from '@material-ui/icons/ListAlt';


const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  inner: {
    minWidth: 700
  },
  nameCell: {
    display: 'flex',
    alignItems: 'center'
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  actions: {
    padding: theme.spacing(1),
    justifyContent: 'flex-end'
  },
  icon: {
    fontSize: 18,
    height: 18,
    width: 18,
    paddingRight: '3px'
  },
  checkbox: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
  },
  selected: {
    backgroundColor: '#F4F6F8',
  }
}));

const Results = props => {
  const { className, onClickChangePage, onClickRowsPerPage, data, assignment, page, rowsPerPage, roles, totalReg, taskStatus, ...rest } = props;

  const classes = useStyles();
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const nombre = Cookies.get('firstName');
  const apellido = Cookies.get('lastName');
  const identificacion = Cookies.get('identification');
  // eslint-disable-next-line no-unused-vars
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const [task, setTask] = useState(data);
  const [selectedOrders, setSelectedOrders] = useState([]);
  const [selectedData, setSelectedData] = useState([]);
  const [openApplication, setOpenApplication] = useState(false);
  const [openStatusChange, setOpenStatusChange] = useState(false);
  const [openLogs, setOpenLogs] = useState(false);

  const showButton = (type, task) => {
    switch (type) {
      case 'PC':
        return (
          <Button
            color="primary"
            component={RouterLink}
            size="small"
            to={`/format/${task._id}/${task.projectId}/${task.activityId}`}

            variant="outlined"
          >
            Cerrar
          </Button>
        )
      case 'AS':
        return (
          <Button
            color="primary"
            size="small"
            onClick={() => handleApplicationOpen(task)}
            variant="outlined"
          >
            Asignar
          </Button>
        )
      default: 
        return (
          <Button
            color="primary"
            size="small"
            onClick={() => handleStatusChangeOpen(task)}
            variant="outlined"
          >
            Actualizar
          </Button>
        )
    }
  }

  const handleApplicationOpen = (tsk) => {
    setTask(tsk)
    setOpenApplication(true);
  };

  const handleAsignClick = async (usuario, task) => {
    const idObject = task._id;
    const newlog = {
      status: 'Asignación#AS',
      createdAt: moment().format('YYYY-MM-DD HH:mm'),
      description: `Tarea asignada a Ejecutor ${usuario.split('#')[0]} othId: ${task.activityId}`,
      author: `${nombre} ${apellido}#${identificacion}`,
    }
    const logAux = task.logs ? task.logs : [];
    logAux.push(newlog);
    if (usuario === '') {
      enqueueSnackbar(`Debe seleccionar un Usuario.`, {
        variant: 'error',
      });
    } else {
      delete task._id;
      await axios.put(`/tasks/${idObject}`,
                {
                  field: '_id',
                  updateNotify: `${nombre} ${apellido}#${identificacion}`,
                  author: usuario,
                  message: `Tarea asignada a ${usuario.split('#')[0]}`,
                  logs: logAux,
                  role: 'Executor',
                },
                getAuthorization())
                .then(() => {
                    enqueueSnackbar(`Tarea asignada a ${usuario.split('#')[0]}`, {
                      variant: 'success',
                    });
                    window.location.href = '/management/tasks';
                })
                .catch((err) => {
                  enqueueSnackbar(`error: ${err}`, {
                    variant: 'error',
                  });
                })
                setOpenApplication(false);
    }
  };

  const handleApplicationClose = () => {
    setOpenApplication(false);
  };

  const handleStatusChangeOpen = (tsk) => {
    console.log({status: tsk.status, current: tsk.zolidStatus, logs: tsk.logs, task: tsk})
    dispatch(tasksStatus({status: tsk.status, current: tsk.zolidStatus, logs: tsk.logs, task: tsk}));
    setOpenStatusChange(true);
  };

  const handleStatusChangeClose = () => {
    setOpenStatusChange(false);
  };

  const selectedRowsConstruction = (event, newSelectedData, selectedDataInfo, order) => {
    const role = order.role;
    const group = order.group;
    const area = order.area;
    const areaObj = selectedDataInfo[group] ? selectedDataInfo[group][area] : {};

    if (group !== 'undefined' && selectedDataInfo[group]) {
      if (area !== 'undefined' && selectedDataInfo[group][area]) {
        if (role !== 'undefined' && selectedDataInfo[group][area][role]) {
          const dataIndex = selectedDataInfo[group][area][role].findIndex(i => i._id ===  order._id);
          if (dataIndex === -1) {
            newSelectedData = newSelectedData.concat(selectedDataInfo[group][area][role], {...order});
          } else if (dataIndex === 0) {
            newSelectedData = newSelectedData.concat(selectedDataInfo[group][area][role].slice(1));
          } else if (dataIndex === selectedDataInfo.length - 1) {
            newSelectedData = newSelectedData.concat(selectedDataInfo[group][area][role].slice(0, -1));
          } else if (dataIndex > 0) {
            newSelectedData = newSelectedData.concat(
              selectedDataInfo[group][area][role].slice(0, dataIndex),
              selectedDataInfo[group][area][role].slice(dataIndex + 1)
            );
          }

        } else {
          newSelectedData = [
            {...order}
          ];
        }
      } else {
        newSelectedData = [
          {...order}
        ];  
      }
    } else {
      newSelectedData = [
        {...order}
      ];
    }

    if (newSelectedData.length === 0 ) {
      delete selectedDataInfo[group][area][role];

      if (Object.keys(selectedDataInfo[group][area]).length === 0)
        delete selectedDataInfo[group][area];

      if (Object.keys(selectedDataInfo[group]).length === 0)
        delete selectedDataInfo[group];
      
      return selectedDataInfo;

    }


    return {
      ...selectedDataInfo,
      [group]: {
        ...selectedDataInfo[group],
        [area]: {
          ...areaObj,
          [role]: [
            ...new Set(newSelectedData)
          ]
        }
      }
    };
  }

  const handleSelectOne = (event, order) => {
    let id = order._id
    const selectedIndex = selectedOrders.indexOf(id);
    let newSelectedOrders = [];
    let newSelectedData = [];

    if (selectedIndex === -1) {
      newSelectedOrders = newSelectedOrders.concat(selectedOrders, id);
    } else if (selectedIndex === 0) {
      newSelectedOrders = newSelectedOrders.concat(selectedOrders.slice(1));
    } else if (selectedIndex === selectedOrders.length - 1) {
      newSelectedOrders = newSelectedOrders.concat(selectedOrders.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelectedOrders = newSelectedOrders.concat(
        selectedOrders.slice(0, selectedIndex),
        selectedOrders.slice(selectedIndex + 1)
      );
    }

    const unique = selectedRowsConstruction(event, newSelectedData, selectedData, order)

    setSelectedOrders(newSelectedOrders);
    setSelectedData(unique);
    dispatch(selectedTasks(unique));
  };


  const cleanAsigned = () => {
    setSelectedOrders([]);
    dispatch(saveAssignment(false));
    dispatch(selectedTasks([]));
  }


  const taskStatusColors = {
    canceled: colors.grey[600],
    warning: colors.orange[600],
    onTime: colors.green[600],
    outOfTime: colors.red[600]
  };

  const statusColors = {
    Assigned: colors.orange[600],
    Canceled: colors.red[600],
    Terminated: colors.green[600],
    Created: colors.grey[600]
  };

  const handleLogsOpen = (tsk) => {
    dispatch(tasksStatus({status: tsk.status, current: tsk.zolidStatus, logs: tsk.logs, task: tsk}));
    setOpenLogs(true);
  };

  const handleLogsClose = () => {
    setOpenLogs(false);
  };

  const fetchUser = async () => {
    try {
      const groups = Object.keys(roles) || [];
      const queryList =  [];

      for (let i = 0; i < groups.length; i++) {
        for (let j = 0; j < Object.keys(roles[groups[i]]).length; j++) {
          const area = Object.keys(roles[groups[i]])[j];
          for (let l = 0; l < Object.keys(roles[groups[i]][area]).length; l++) {
            const role = Object.keys(roles[groups[i]][area])[l];
            queryList.push({ 'group': groups[i], 'area': area, 'role' : role });
          }
        }        
      }

      const petition = await axios.post('/users/pagination', {
        "roles": {
          "$elemMatch": {
            "$or": [
              ...new
              Set(queryList)
            ]
          }
        }
      }, getAuthorization());
      if (petition.data) return petition.data.data[0].data;
      return [];

    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    dispatch(selectedTasks(null));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div
      {...rest}
      className={clsx(classes.root, className)}
    >
      
      <Typography
        color="textSecondary"
        gutterBottom
        variant="body2"
      >
        {totalReg} Registros encontrados. Página {page + 1} de {Math.ceil(totalReg / rowsPerPage)}
      </Typography>
      <Card>
        <CardHeader
          action={<GenericMoreButton />}
          title="Todas las tareas"
        />
        <Divider />
        <CardContent className={classes.content}>
          <PerfectScrollbar>
            <div className={classes.inner}>
            {props.loading ? (
                <LinearProgress />
                ): ''}
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell align="center"></TableCell>
                    <TableCell align="center">Información</TableCell>
                    <TableCell align="center">Fecha de Entrega</TableCell>
                    <TableCell align="center">Fecha de Creación</TableCell>
                    <TableCell align="center">Responsable</TableCell>
                    <TableCell align="center">Estado</TableCell>
                    <TableCell align="center">Terminado</TableCell>
                    <TableCell align="center">Actions</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>     
                  {data.data?.map(task => (
                    <TableRow
                      hover
                      key={task.id}
                      className={clsx(selectedOrders.indexOf(task._id) !== -1 && classes.selected)}
                    >
                      <TableCell align="center">
                        <div className={classes.checkbox}>
                          {assignment.show && (
                            <Checkbox
                              checked={selectedOrders.indexOf(task._id) !== -1}
                              color="primary"
                              onChange={event => handleSelectOne(event, task)}
                              value={selectedOrders.indexOf(task._id) !== -1}
                            />
                          )}
                          <ReviewStars value={task.priority} calendar={task.type} />
                        </div>
                      </TableCell>
                      <TableCell align="left">
                        <div className={classes.nameCell}>
                          <Tooltip title={task.status?.split("#")[0]} placement="left">
                            <Avatar
                              className={classes.avatar}
                              src={task.avatar}
                            >
                              {task.status?.split("#")[1]}
                            </Avatar>
                          </Tooltip>
                          <div>
                            <Typography variant="body2">
                              {task.activityId && `OTH: ${task.activityId}`}
                              {task.projectId && ` OTP: ${task.projectId}`}
                            </Typography>
                            <Tooltip title={'Registros'} placement="left">
                              <IconButton onClick={() => handleLogsOpen(task)} color={'primary'}>
                                <ListAltIcon />
                              </IconButton>
                            </Tooltip>
                            <Link
                              color="inherit"
                              component={RouterLink}
                              onClick={handleApplicationOpen}
                              variant="h6"
                            >
                              {task.orderType}
                            </Link>
                            <Typography variant="body2">
                              <Label
                                color={taskStatusColors[getTimeStatus(task.deadlineDate, task.finishDate && task.finishDate)]}
                                variant="outlined"
                              >
                                <ScheduleIcon className={classes.icon}/>{t(getTimeStatus(task.deadlineDate, task.finishDate && task.finishDate))}
                              </Label>
                            </Typography>
                          </div>
                        </div>
                      </TableCell>
                      <TableCell align="center">
                        {formatDate(task.deadlineDate).date}
                        <Typography variant="h6">
                        {formatDate(task.deadlineDate).time}
                        </Typography>
                      </TableCell>
                      <TableCell align="center">
                        {formatDate(task.createdAt).date}
                        <Typography variant="h6">
                        {formatDate(task.createdAt).time}
                        </Typography>
                      </TableCell>
                      <TableCell align="center">
                        <Typography
                          variant="h6"
                        >
                          {task.author?.split('#')[0]}
                        </Typography>
                      </TableCell>
                      <TableCell align="center">
                        <Typography
                          style={{ color: statusColors[task.zolidStatus] }}
                          variant="h6"
                        >
                          {t(task.zolidStatus)}
                          <Typography>
                          {task.finishDate && `${formatDate(task.finishDate).date} ${formatDate(task.finishDate).time}`}
                          </Typography>
                        </Typography>
                      </TableCell>
                      <TableCell align="center">
                        {duration(task.createdAt, task.finishDate)}
                      </TableCell>
                      <TableCell align="center">
                      {task.zolidStatus !== 'Terminated' && (
                        showButton(task?.status?.split("#")[1], task)
                      )}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
              { openApplication && (
                <Application 
                  onApply={handleAsignClick}
                  onClose={handleApplicationClose}
                  open={openApplication}
                  task={task}
                />
              )}
              { (openStatusChange && taskStatus) && (
                <StatusChange 
                  onApply={handleStatusChangeClose}
                  onClose={handleStatusChangeClose}
                  open={openStatusChange}
                  currentStatus={taskStatus.current}
                  type={taskStatus.status}
                  logs={taskStatus.logs}
                  task={taskStatus.task}
                />
              )}
              { (openLogs) && (
                <Logs 
                  onApply={handleLogsClose}
                  onClose={handleLogsClose}
                  open={openLogs}
                  currentStatus={taskStatus.current}
                  type={taskStatus.status}
                  logs={taskStatus.logs}
                  task={taskStatus.task}
                />
              )}
            </div>
          </PerfectScrollbar>
        </CardContent>
        <CardActions className={classes.actions}>
          <TablePagination
            component="div"
            count={totalReg}
            onChangePage={onClickChangePage}
            onChangeRowsPerPage={onClickRowsPerPage}
            page={page}
            rowsPerPage={rowsPerPage}
            rowsPerPageOptions={[10, 25, 50]}
            labelDisplayedRows={
              ({ from, to, count }) => {
                return '' + from + '-' + to + ' de ' + count
              }
            }
            labelRowsPerPage={'Registros por página:'}
          />
        </CardActions>
      </Card>
      <TableAssignBar
        onCancel={cleanAsigned}
        selected={selectedOrders}
        data={roles}
        fetchUsers={fetchUser}
      />
    </div>
  );
};

Results.propTypes = {
  className: PropTypes.string,
  tasks: PropTypes.array.isRequired
};

Results.defaultProps = {
  tasks: []
};

const mapStateToProps = (state) => {
  return {
    assignment: state.assignment,
    roles: state.tasks.selectedTasks,
    taskStatus: state.tasks.taskStatus
  };
};

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(Results);
