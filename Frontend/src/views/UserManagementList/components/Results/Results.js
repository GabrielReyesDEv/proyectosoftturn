import React, { useState, useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { makeStyles } from '@material-ui/styles';
import {
  Avatar,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  colors,
  Divider,
  LinearProgress,
  Link,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography
} from '@material-ui/core';

import getInitials from 'utils/getInitials';
import { GenericMoreButton, Label } from 'components';
import gravatar from '../../../../utils/gravatar';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  inner: {
    minWidth: 700
  },
  nameCell: {
    display: 'flex',
    alignItems: 'center'
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  actions: {
    padding: theme.spacing(1),
    justifyContent: 'flex-end'
  }
}));

const Results = props => {
  // eslint-disable-next-line react/prop-types
  const { className, data, page, rowsPerPage, onClickChangePage, onClickRowsPerPage, ...rest } = props;

  const classes = useStyles();

  const { t } = useTranslation();

  const [users, setUsers] = useState(data);

  const statusColors = {
    pending: colors.orange[600],
    active: colors.green[600],
    suspended: colors.red[600]
  };

  useEffect(() => {
    setUsers(data);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  return (
    <div
      {...rest}
      className={clsx(classes.root, className)}
    >
      <Typography
        color="textSecondary"
        gutterBottom
        variant="body2"
      >
        {users.total} Registros encontrados. Pagina {page + 1} de {users.total_pages}
      </Typography>
      <Card>
        <CardHeader
          action={<GenericMoreButton />}
          title={t('allUsers')}
        />
        <Divider />
        <CardContent className={classes.content}>
          <PerfectScrollbar>
            <div className={classes.inner}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>{t('name') + ' / Correo'}</TableCell>
                    <TableCell>{t('identification')}</TableCell>
                    <TableCell>Roles</TableCell>
                    <TableCell>Estado</TableCell>
                    <TableCell align="right">Actions</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {users.data && users.data[0].data.length > 0 && users.data[0].data.map(customer => (
                    <TableRow
                      hover
                      key={customer.id}
                    >
                      <TableCell>
                        <div className={classes.nameCell}>
                          <Avatar
                            className={classes.avatar}
                            src={gravatar(customer.email)}
                          >
                            {getInitials(customer.firstName)}
                          </Avatar>
                          <div>
                            <Link
                              color="inherit"
                              component={RouterLink}
                              to="/management/users/1"
                              variant="h6"
                            >
                              {customer.name}
                            </Link>
                            <Typography variant="h6">{`${customer.firstName} ${customer.lastName}`}</Typography>
                            <Typography variant="body1">{customer.email}</Typography>
                          </div>
                        </div>
                      </TableCell>
                      <TableCell>{customer.identification}</TableCell>
                      <TableCell>{customer.roles && customer.roles.map((roles) => 
                        <Label
                          color={'#008fd5'}
                          variant="outlined"
                        >
                          {`${roles.role} del area ${roles.area}\n`}
                        </Label>
                      )}</TableCell>
                      <TableCell>
                        <Label
                          color={statusColors[customer.status.toLowerCase()]}
                          variant="outlined"
                        >
                          {customer.status}
                        </Label>
                      </TableCell>
                      <TableCell align="right">
                        <Button
                          color="primary"
                          component={RouterLink}
                          size="small"
                          to={`/management/users/${customer._id}`}
                          variant="outlined"
                        >
                          Ver
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
              {props.loading ? (
                <LinearProgress />
              ): ''}
            </div>
          </PerfectScrollbar>
        </CardContent>
        <CardActions className={classes.actions}>
          <TablePagination
            component="div"
            count={users.total}
            labelDisplayedRows={
              ({ from, to, count }) => {
                return '' + from + '-' + to + ' de ' + count
              }
            }
            labelRowsPerPage={'Registros por página:'}
            onChangePage={onClickChangePage}
            onChangeRowsPerPage={onClickRowsPerPage}
            page={page}
            rowsPerPage={rowsPerPage}
            rowsPerPageOptions={[5, 10, 25]}
          />
        </CardActions>
      </Card>
    </div>
  );
};

Results.propTypes = {
  className: PropTypes.string,
  customers: PropTypes.array.isRequired
};

Results.defaultProps = {
  customers: []
};

export default Results;
