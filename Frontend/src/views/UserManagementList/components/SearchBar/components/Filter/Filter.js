import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Button,
  Collapse,
  Divider,
  Drawer,
  TextField,
  Typography,
  InputLabel,
  Select,
  Checkbox,
  MenuItem,
  ListItemText,
} from '@material-ui/core';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import CloseIcon from '@material-ui/icons/Close';
import DeleteIcon from '@material-ui/icons/DeleteOutlined';
import { isEmpty, keysIn, groupBy } from 'lodash';

const useStyles = makeStyles(theme => ({
  root: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  drawer: {
    width: 420,
    maxWidth: '100%'
  },
  header: {
    padding: theme.spacing(2, 1),
    display: 'flex',
    justifyContent: 'space-between'
  },
  buttonIcon: {
    marginRight: theme.spacing(1)
  },
  content: {
    padding: theme.spacing(0, 3),
    flexGrow: 1
  },
  contentSection: {
    padding: theme.spacing(2, 0)
  },
  contentSectionHeader: {
    display: 'flex',
    justifyContent: 'space-between',
    cursor: 'pointer'
  },
  contentSectionContent: {},
  formGroup: {
    padding: theme.spacing(2, 0)
  },
  fieldGroup: {
    display: 'flex',
    alignItems: 'center'
  },
  field: {
    marginTop: 0,
    marginBottom: 0
  },
  flexGrow: {
    flexGrow: 1
  },
  addButton: {
    marginLeft: theme.spacing(1)
  },
  tags: {
    marginTop: theme.spacing(1)
  },
  minAmount: {
    marginRight: theme.spacing(3)
  },
  maxAmount: {
    marginLeft: theme.spacing(3)
  },
  radioGroup: {},
  actions: {
    padding: theme.spacing(3),
    '& > * + *': {
      marginTop: theme.spacing(2)
    }
  },
  statusField: {
    marginRight: '18px',
  }
}));

const Filter = props => {
  const { states, open, onClose, onFilter, className, ...rest } = props;

  const classes = useStyles();

  const initialValues = {
    firstName: '',
    lastName: '',
    identification: '',
    email: '',
    roles: [],
    areas: [],
    status: [],
  };

  const [expandProject, setExpandProject] = useState(true);
  const [values, setValues] = useState({ ...initialValues });

  const handleClear = () => {
    setValues({ ...initialValues });
    onFilter && onFilter(initialValues);
  };

  const handleFieldChange = (event, field, value, index = '') => {
    event.persist && event.persist();
    if(index === '') {
      setValues(values => ({
        ...values,
        [field]: value
      }));
    } else {
      const auxDate = values.[field];
      auxDate[index] = value;
      setValues(values => ({
        ...values,
        [field]: auxDate
      }));
    }
  };

  const handleToggleProject = () => {
    setExpandProject(expandProject => !expandProject);
  };

  const handleSubmit = event => {
    event.preventDefault();
    onFilter && onFilter(values);
  };

  const dataStates = states;
  const optionRoles = keysIn(groupBy(dataStates, 'userRole'));
  const optionAreas = keysIn(groupBy(dataStates, 'userArea'));
  const optionStatus = keysIn(groupBy(dataStates, 'userStatus'));

  return (
    <Drawer
      anchor="right"
      classes={{ paper: classes.drawer }}
      onClose={onClose}
      open={open}
      variant="temporary"
    >
      <form
        {...rest}
        className={clsx(classes.root, className)}
        onSubmit={handleSubmit}
      >
        <div className={classes.header}>
          <Button
            onClick={onClose}
            size="small"
          >
            <CloseIcon className={classes.buttonIcon} />
            Cerrar
          </Button>
        </div>
        <div className={classes.content}>
          <div className={classes.contentSection}>
            <div
              className={classes.contentSectionHeader}
              onClick={handleToggleProject}
            >
              <Typography variant="h5">Usuario</Typography>
              {expandProject ? <ExpandLessIcon /> : <ExpandMoreIcon />}
            </div>
            <Divider />
            <Collapse in={expandProject}>
              <div className={classes.contentSectionContent}>
                <div className={classes.formGroup}>
                  <TextField
                    fullWidth
                    label="Nombres"
                    name="firstName"
                    onChange={event =>
                      handleFieldChange(event, 'firstName', event.target.value !== '' ? event.target.value : undefined)
                    }
                    value={values.firstName}
                    variant="outlined"
                  />
                </div>
                <div className={classes.formGroup}>
                  <TextField
                    fullWidth
                    label="Apellidos"
                    name="lastName"
                    onChange={event =>
                      handleFieldChange(event, 'lastName', event.target.value !== '' ? event.target.value : undefined)
                    }
                    value={values.lastName}
                    variant="outlined"
                  />
                </div>
                <div className={classes.formGroup}>
                  <TextField
                    fullWidth
                    label="Identificación"
                    name="identification"
                    onChange={event =>
                      handleFieldChange(event, 'identification', event.target.value !== '' ? event.target.value : undefined)
                    }
                    type="number"
                    value={values.identification}
                    variant="outlined"
                  />
                </div>
                <div className={classes.formGroup}>
                  <TextField
                    fullWidth
                    label="Correo"
                    name="email"
                    onChange={event =>
                      handleFieldChange(event, 'email', event.target.value !== '' ? event.target.value : undefined)
                    }
                    value={values.email}
                    variant="outlined"
                  />
                </div>
                <div className={classes.formGroup}>
                  <div className={classes.fieldGroup}>
                    <InputLabel
                      className={classes.statusField}
                      htmlFor="select-multiple-chip"
                    >
                     Rol del Usuario
                    </InputLabel>
                    <Select
                      className={clsx(classes.field, classes.flexGrow)}
                      multiple
                      onChange={(event) => {
                        handleFieldChange(event, 'roles', event.target.value)
                      }}
                      renderValue={(selected) => {
                        const renderItems = selected.map((item) => {
                          return isEmpty(item) ? '(Blank)' : item;
                        });
                        return renderItems.join(', ');
                      }}
                      value={values.roles}
                    >
                      {optionRoles.map((item) => (
                        <MenuItem
                          key={item}
                          value={item}
                        >
                          <Checkbox
                            checked={values.roles.indexOf(item) > -1}
                            color="primary"
                          />
                          <ListItemText
                            primary={isEmpty(item) ? '(Blank)' : item}
                          />
                        </MenuItem>
                      ))}
                    </Select>
                  </div>
                </div>
                <div className={classes.formGroup}>
                  <div className={classes.fieldGroup}>
                    <InputLabel
                      className={classes.statusField}
                      htmlFor="select-multiple-chip"
                    >
                     Area
                    </InputLabel>
                    <Select
                      className={clsx(classes.field, classes.flexGrow)}
                      multiple
                      onChange={(event) => {
                        handleFieldChange(event, 'areas', event.target.value)
                      }}
                      renderValue={(selected) => {
                        const renderItems = selected.map((item) => {
                          return isEmpty(item) ? '(Blank)' : item;
                        });
                        return renderItems.join(', ');
                      }}
                      value={values.areas}
                    >
                      {optionAreas.map((item) => (
                        <MenuItem
                          key={item}
                          value={item}
                        >
                          <Checkbox
                            checked={values.areas.indexOf(item) > -1}
                            color="primary"
                          />
                          <ListItemText
                            primary={isEmpty(item) ? '(Blank)' : item}
                          />
                        </MenuItem>
                      ))}
                    </Select>
                  </div>
                </div>
                <div className={classes.formGroup}>
                  <div className={classes.fieldGroup}>
                    <InputLabel
                      className={classes.statusField}
                      htmlFor="select-multiple-chip"
                    >
                     Estado del usuario
                    </InputLabel>
                    <Select
                      className={clsx(classes.field, classes.flexGrow)}
                      multiple
                      onChange={(event) => {
                        handleFieldChange(event, 'status', event.target.value)
                      }}
                      renderValue={(selected) => {
                        const renderItems = selected.map((item) => {
                          return isEmpty(item) ? '(Blank)' : item;
                        });
                        return renderItems.join(', ');
                      }}
                      value={values.status}
                    >
                      {optionStatus.map((item) => (
                        <MenuItem
                          key={item}
                          value={item}
                        >
                          <Checkbox
                            checked={values.status.indexOf(item) > -1}
                            color="primary"
                          />
                          <ListItemText
                            primary={isEmpty(item) ? '(Blank)' : item}
                          />
                        </MenuItem>
                      ))}
                    </Select>
                  </div>
                </div>
              </div>
            </Collapse>
          </div>
        </div>
        <div className={classes.actions}>
          <Button
            fullWidth
            onClick={handleClear}
            variant="contained"
          >
            <DeleteIcon className={classes.buttonIcon} />
            Limpiar
          </Button>
          <Button
            color="primary"
            fullWidth
            type="submit"
            variant="contained"
          >
            Aplicar filtros
          </Button>
        </div>
      </form>
    </Drawer>
  );
};

Filter.propTypes = {
  className: PropTypes.string,
  onClose: PropTypes.func,
  onFilter: PropTypes.func,
  open: PropTypes.bool.isRequired,
  states: PropTypes.object,
};

export default Filter;
