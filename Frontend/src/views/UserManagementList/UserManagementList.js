/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';
import { Page } from 'components';
import { Header, Results, SearchBar } from './components';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  results: {
    marginTop: theme.spacing(3)
  }
}));

const UserManagementList = () => {
  const classes = useStyles();

  const [loading, setLoading] = useState(1);
  const [customers, setCustomers] = useState([]);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [totalReg, setTotalReg] = useState(0);
  const [page, setPage] = useState(0);
  const [keyword, setKeyword] = useState('');
  const [filters, setFilters] = useState([]);

  const columnsNeeded = {
    'firstName': 1,
    'lastName': 1,
    'identification': 1,
    'email': 1,
    'roles.role': 1,
    'roles.area': 1,
    'status': 1,
  }
  const dataStates = [
    { 'userRole': 'Coordinator' },
    { 'userRole': 'Dispatcher' },
    { 'userRole': 'Ejecutor' },
    { 'userRole': 'Executor' },
    { 'userRole': 'Ingeniero Gestion' },
    { 'userStatus': 'Active' },
    { 'userStatus': 'InActive' },
  ];

  const fetchCustomers = async (body) => {
    try {

      setLoading(true);
      const petition = await axios.post(
        '/users/pagination',
        body,
        getAuthorization(),
      );
      
      if(petition.status === 200) {
        setCustomers(petition.data)
        setLoading(0);
        setTotalReg(petition.data.total);
        setPage(petition.data.page-1);
        setRowsPerPage(petition.data.per_page);
      }

    } catch (error) {
      console.error(error);
    }

  };

  useEffect(() => {
    fetchCustomers({
      limit: rowsPerPage,
      page: 1,
    });
  }, []);

  const bodyFunction = (limit, page, searchLocal = '',filtersLocal = []) => {
    const data = [];
    let body = {
      'limit': limit,
      'page': page,
    };
    if(searchLocal !== '') {
      for (var i in columnsNeeded) data.push({ 'field': [i], 'value': searchLocal});
      body = { ...body, 'or': data };
    }
    if(filtersLocal.length !== 0) {
      body = { ...body, 'and': filtersLocal};
    }
    return body;
  }

  const handleFilter = (e) => {
    const filtersLocal = [];
    if (e.firstName !== '' && e.firstName !== undefined) filtersLocal.push({ 'field': 'firstName', 'value': e.firstName });
    if (e.lastName !== '' && e.lastName !== undefined) filtersLocal.push({ 'field': 'lastName', 'value': e.lastName });
    if (e.identification !== '' && e.identification !== undefined) filtersLocal.push({ 'field': 'identification', 'value': e.identification });
    if (e.email !== '' && e.email !== undefined) filtersLocal.push({ 'field': 'email', 'value': e.email });
    if (e.status.length !== 0) filtersLocal.push({ 'field': 'status', 'value': e.status });
    if (e.roles.length !== 0) filtersLocal.push({ 'field': 'roles.role', 'value': e.roles });
    if (e.areas.length !== 0) filtersLocal.push({ 'field': 'roles.area', 'value': e.areas });
    setFilters(filtersLocal);
    const body = bodyFunction(rowsPerPage, 1, keyword, filtersLocal);
    fetchCustomers(body);
  };

  const handleSearch = (e) => {
    setKeyword(e);
    const body = bodyFunction(rowsPerPage, 1, e, filters);
    fetchCustomers(body);
  };

  const handleChangePage = async (event, newPage) => {
    const body = bodyFunction(rowsPerPage, (newPage + 1), keyword, filters);
    fetchCustomers(body);
    // fetchTasks({'page': newPage+1, 'limit': rowsPerPage});
  };

  const handleChangeLimit = event => {
    const body = bodyFunction(event.target.value, (page + 1), keyword, filters);
    fetchCustomers(body);
  };

  return (
    <Page
      className={classes.root}
      title="Customer Management List"
    >
      <Header />
      <SearchBar
        onFilter={handleFilter}
        onSearch={handleSearch}
        states={dataStates}
        value={keyword}
      />
      {customers && (
        <Results
          className={classes.results}
          data={customers}
          loading={loading}
          onClickChangePage={handleChangePage}
          onClickRowsPerPage={handleChangeLimit}
          page={page}
          rowsPerPage={rowsPerPage}
          setLoading={setLoading}
          setTotalReg={setTotalReg}
          totalReg={totalReg}
        />
      )}
    </Page>
  );
};

export default UserManagementList;
