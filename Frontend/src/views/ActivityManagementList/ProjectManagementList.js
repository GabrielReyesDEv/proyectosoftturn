/* eslint-disable */
import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';
import {
  CircularProgress,
  IconButton,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
  Tooltip,
  Typography
} from '@material-ui/core';
import { Page, Paginate, BottomBar } from 'components';
import { Header, ProjectCard, SearchBar } from './components';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Cookies from 'js-cookie';
import axios from 'utils/axios';
import { saveAssignment, setAssigned, setUpdateReport, setResponsable } from 'actions';
import { useDispatch, useSelector } from 'react-redux';
import getAuthorization from 'utils/getAuthorization';
import PersonIcon from '@material-ui/icons/Person';
import { useHistory } from 'react-router';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  options: {
    marginTop: theme.spacing(3),
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  loadContainer: {
    display: 'flex',
    justifyContent: 'center'
  },
  results: {
    marginTop: theme.spacing(3)
  },
  paginate: {
    marginTop: theme.spacing(3),
    display: 'flex',
    justifyContent: 'center'
  }
}));

const ProjectManagementList = () => {
  const history = useHistory();
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [page, setPage] = useState(0);
  const [totalReg, setTotalReg] = useState(0);
  const [projects, setProjects] = useState([]);
  const [keyword, setKeyword] = useState('');
  const [filters, setFilters] = useState([]);
  const [selectsCkeckTitle, setSelectsCkeckTitle] = useState('');
  const dispatch = useDispatch();
  const assignment = useSelector(state => state.assignment);
  const updatereport = useSelector(state => state.updatereport);
  const url = '/oths/pagination';
  const columnsNeeded = {
    'area': 1,
    'author': 1,
    'projectId': 1,
    'activityId': 1,
    'updatedAt': 1,
    'createdAt': 1,
    'priority': 1,
    'type': 1,
    'status': 1,
    'resolucion15': 1,
    'resolucion26': 1,
    'resolucion37': 1,
    'resolucion48': 1,
    'undefined': 1,
  }
  const dataStates = [
    { 'activityStatus': '1- Generada' },
    { 'activityStatus': '2- En ejecución' },
    { 'activityStatus': '3- Terminada' },
    { 'activityStatus': 'Alistamiento' },
    { 'activityStatus': 'Caja OB' },
    { 'activityStatus': 'Detalle Solución' },
    { 'activityStatus': 'Diseño' },
    { 'activityStatus': 'Ejecución ruta' },
    { 'activityStatus': 'Entrega' },
    { 'activityStatus': 'Factibilidad' },
    { 'activityStatus': 'Instalación' },
    { 'activityStatus': 'Liberación Recursos' },
    { 'activityStatus': 'Pendiente Cliente' },
    { 'activityStatus': 'Pendiente Telmex' },
    { 'activityStatus': 'CONF. Configuracion de servicio' },
    { 'activityPriority': 0 },
    { 'activityPriority': 1 },
    { 'activityPriority': 2 },
    { 'activityPriority': 3 },
    { 'activityPriority': 4 },
    { 'activityPriority': 5 },
    { 'activityPriority': 6 },
    { 'activityPriority': 7 },
    { 'activityPriority': 8 },
    { 'activityPriority': 9 },
  ];

  // let userOnyx = '';
  // if (Cookies.get('roles') !== undefined) {
  //   const findUser = JSON.parse(Cookies.get('roles')).find((x) => x.role === 'Ingeniero Gestion');
  //   if (findUser !== '') { userOnyx = findUser.userOnyx; }
  // }

  const fetchProjects = async (body) => {
    try {

      setLoading(true);
      const petition = await axios.post(
        '/activities/pagination',
        body,
        getAuthorization(),
      );
      
      if(petition.status === 200) {
        setProjects(petition.data.data)
        setTotalReg(petition.data.total);
        setPage(petition.data.page);
        setRowsPerPage(petition.data.per_page);
        setLoading(false);
      }

    } catch (error) {
      console.error(error);
      setLoading(false);
    }
  };

  const bodyFunction = (limit, page, searchLocal = '',filtersLocal = []) => {
    const data = [];
    let body = {
      'limit': limit,
      'page': page,
      'columnsNeeded': columnsNeeded,
      // 'userOnyx': userOnyx,
    };
    if(searchLocal !== '') {
      for (var i in columnsNeeded) data.push({ 'field': [i], 'value': searchLocal});
      body = { ...body, 'or': data };
    }
    if(filtersLocal.length !== 0) {
      body = { ...body, 'and': filtersLocal};
    }
    return body;
  }

  useEffect(() => {
    fetchProjects({limit: 10, page: 1, columnsNeeded});
  },[]);

  const handleFilter = (e) => {
    const filtersLocal = [];
    // if (e.createAt[0] !== '' || e.createAt[1] !== '' || e.status.length !== 0 || e.updateAt[0] !== '' || e.updateAt[1] !== '') {
      if (e.createAt[0] !== '' || e.createAt[1] !== '') filtersLocal.push({ 'field': 'createAt', 'value': e.createAt[0] !== '' && e.createAt[1] !== '' ? e.createAt : (e.createAt[0] === '' ? [e.createAt[1]] : [e.createAt[0]]) });
      if (e.updateAt[0] !== '' || e.updateAt[1] !== '') filtersLocal.push({ 'field': 'updateAt', 'value': e.updateAt[0] !== '' && e.updateAt[1] !== '' ? e.updateAt : (e.updateAt[0] === '' ? [e.updateAt[1]] : [e.updateAt[0]]) });
      if (e.author !== '' && e.author !== undefined) filtersLocal.push({ 'field': 'author', 'value': e.author });
      if (e.projectId !== '' && e.projectId !== undefined) filtersLocal.push({ 'field': 'projectId', 'value': e.projectId });
      if (e.activityId !== '' && e.activityId !== undefined) filtersLocal.push({ 'field': 'activityId', 'value': e.activityId });
      if (e.resolucion15 !== '' && e.resolucion15 !== undefined) filtersLocal.push({ 'field': 'resolucion15', 'value': e.resolucion15 });
      if (e.resolucion26 !== '' && e.resolucion26 !== undefined) filtersLocal.push({ 'field': 'resolucion26', 'value': e.resolucion26 });
      if (e.resolucion37 !== '' && e.resolucion37 !== undefined) filtersLocal.push({ 'field': 'resolucion37', 'value': e.resolucion37 });
      if (e.resolucion48 !== '' && e.resolucion48 !== undefined) filtersLocal.push({ 'field': 'resolucion48', 'value': e.resolucion48 });
      if (e.status.length !== 0) filtersLocal.push({ 'field': 'status', 'value': e.status });
      if (e.priority.length !== 0) filtersLocal.push({ 'field': 'priority', 'value': e.priority });
      if (e.area.length !== 0) filtersLocal.push({ 'field': 'area', 'value': e.area });
    // }
    setFilters(filtersLocal);
    const body = bodyFunction(rowsPerPage, 1, keyword, filtersLocal);
    fetchProjects(body);
  };

  const handleSearch = (e) => {
    setKeyword(e);
    const body = bodyFunction(rowsPerPage, 1, e, filters);
    fetchProjects(body);
  };

  const handleChangePage = (e) => {
    const body = bodyFunction(rowsPerPage, (e.selected + 1), keyword, filters);
    fetchProjects(body);
  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleAssignment = () => {
    setSelectsCkeckTitle('Asignar Actividades');
    dispatch(saveAssignment(true));
    handleClose();
  }

  const handleCloseBottomBar = () => {
    dispatch(saveAssignment(false));
    dispatch(setUpdateReport([]));
    dispatch(setAssigned([]));
  }

  const cleanAsigned = () => {
    dispatch(saveAssignment(false));
    dispatch(setAssigned([]));
  }


  return (
    <Page
      className={classes.root}
      title="Project Management List"
    >
      <Header />
      <SearchBar
        states={dataStates}
        value={keyword}
        onFilter={handleFilter}
        onSearch={handleSearch}
      />
      <div className={classes.results}>
        <div className={classes.options}>
          <Typography
            color="textSecondary"
            gutterBottom
            variant="body2"
          >
            {totalReg} Registros encontrados. Página {page} de{' '}
            {Math.ceil(totalReg / rowsPerPage)}
          </Typography>
          <div>
            <Tooltip title="Mas opciones">
              <IconButton aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
                <MoreVertIcon />
              </IconButton>
            </Tooltip>
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}
            >
              <MenuItem onClick={handleAssignment}>
                <ListItemIcon>
                  <PersonIcon />
                </ListItemIcon>
                <ListItemText primary="Asignar" />
              </MenuItem>
            </Menu>
          </div>
        </div>
        {projects.map(project => (
          <ProjectCard
            key={project.id}
            project={project}
          />
        ))}
      </div>
      <div className={classes.paginate}>
        <Paginate
          onPageChange={handleChangePage}
          pageCount={Math.ceil(totalReg / rowsPerPage)}
        />
      </div>
      <BottomBar
        route='/activities/'
        role={{ "role": "Ingeniero Gestion", "area":"Configuracion", "group": "BO" }}
        selected={assignment?.Assigned || []}
        selectedProjects={assignment?.Assigned || []}
        title={selectsCkeckTitle}
        onClose={handleCloseBottomBar}
      />
    </Page>
  );
};

export default ProjectManagementList;
