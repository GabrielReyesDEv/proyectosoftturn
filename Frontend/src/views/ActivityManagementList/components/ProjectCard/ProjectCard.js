import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import moment from 'moment';
import { makeStyles } from '@material-ui/styles';
import { ReviewStars } from 'components';
import { useDispatch, useSelector } from 'react-redux';
import {
  Avatar,
  Button,
  Card,
  CardContent,
  Checkbox,
  Link,
  Typography,
  colors,
  Tooltip
} from '@material-ui/core';
import { setAssigned, setUpdateReport } from 'actions';
import getInitials from 'utils/getInitials';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    flexWrap: 'wrap',
    marginBottom: theme.spacing(2)
  },
  content: {
    padding: theme.spacing(2),
    flexGrow: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
      flexWrap: 'wrap'
    },
    '&:last-child': {
      paddingBottom: theme.spacing(2)
    }
  },
  header: {
    maxWidth: '100%',
    width: 240,
    display: 'flex',
    [theme.breakpoints.down('sm')]: {
      marginBottom: theme.spacing(2),
      flexBasis: '100%'
    }
  },
  avatar: {
    marginRight: theme.spacing(2)
  },
  stats: {
    padding: theme.spacing(1),
    [theme.breakpoints.down('sm')]: {
      flexBasis: '50%'
    }
  },
  actions: {
    padding: theme.spacing(1),
    [theme.breakpoints.down('sm')]: {
      flexBasis: '50%'
    }
  }
}));

const ProjectCard = props => {
  const { project, className, ...rest } = props;
  const dispatch = useDispatch();
  const classes = useStyles();

  const statusColors = {
    'In progress': colors.orange[600],
    Canceled: colors.grey[600],
    Completed: colors.green[600]
  };

  const assignment = useSelector(state => state.assignment);

  const handleSelectOne = (event, activitie) => {
    const selectedCustomers = assignment?.Assigned || [];
    const selectedIndex = selectedCustomers.indexOf(activitie);
    
    let newSelectedCustomers = [];

    if (selectedIndex === -1) {
      newSelectedCustomers = newSelectedCustomers.concat(selectedCustomers, activitie);

    } else if (selectedIndex === 0) {
      newSelectedCustomers = newSelectedCustomers.concat(
        selectedCustomers.slice(1)
      );
    } else if (selectedIndex === selectedCustomers.length - 1) {
      newSelectedCustomers = newSelectedCustomers.concat(
        selectedCustomers.slice(0, -1)
      );
    } else if (selectedIndex > 0) {
      newSelectedCustomers = newSelectedCustomers.concat(
        selectedCustomers.slice(0, selectedIndex),
        selectedCustomers.slice(selectedIndex + 1)
      );
    }

    dispatch(setAssigned(newSelectedCustomers));
  };

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardContent className={classes.content}>
      {
          assignment?.show && (
          // assignment?.show === true && assignment?.Assigned?.length !==0 && (

            <div className={classes.actions}>
              <Checkbox
                checked={
                  assignment?.Assigned?.includes(project) === true
                }
                color="primary"
                // indeterminate={
                // selectedOrders.length > 0 &&
                // selectedOrders.length < tasks.length
                // }
                onChange={event =>
                  handleSelectOne(event, project)
                }
              />
            </div>
          )
        }
        <span>
          <ReviewStars value={project.priority} />
        </span>
        <div className={classes.header}>
          <Avatar
            alt="Author"
            className={classes.avatar}
            src={project.area}
          >
            {getInitials(project.area)}
          </Avatar>
          
          <div>
            
            <Typography variant="body2">
              <Link
                color="textPrimary"
                component={RouterLink}
                to="#"
                variant="h5"
              >
                {project.type}
              </Link>
            </Typography>
            <Typography variant="body2">
              responsable {' '}
              <Link //Leonel: retirar o agregar ruta de direccionamiento en link
                color="textPrimary"
                component={RouterLink}
                to="/management/users"
                variant="h6"
              >
                {project.author?.split('#')[0]}
              </Link>
            </Typography>
          </div>
        </div>
        <div className={classes.stats}>
          <Typography variant="h6">
            {''}
            {project.projectId}
          </Typography>
          <Typography variant="body2">Id Proyecto</Typography>
          <Typography variant="h6">
            {''}
            {project.activityId}
          </Typography>
          <Typography variant="body2">Id Actividad</Typography>
        </div>
        <div className={classes.stats}>
          <Typography variant="h6">{project.area ? project.area : 'Sin Area'}</Typography>
          <Typography variant="body2">Area de Trabajo</Typography>
        </div>
        <div className={classes.stats}>
          <Typography variant="h6">
            {moment(project.createdAt).format('DD MMMM YYYY')}
          </Typography>
          <Typography variant="body2">Inicio del Proyecto</Typography>
        </div>
        <div className={classes.stats}>
          <Typography variant="h6">
            {moment(project.updatedAt).format('DD MMMM YYYY')}
          </Typography>
          <Typography variant="body2">Actualizacion del Proyecto</Typography>
        </div>
        <div className={classes.stats}>
          <Typography
            style={{ color: statusColors[project.status] }}
            variant="h6"
          >
            {project.status}
          </Typography>
          <Typography variant="body2">Estado del proyecto</Typography>
        </div>
        <div className={classes.actions}>
          <a href={`/activities/${project.activityId}/overview`}>
            <Button
              color="primary"
              size="small"
              variant="outlined"
            >
              Ver
            </Button>
          </a>
          
        </div>
      </CardContent>
    </Card>
  );
};

ProjectCard.propTypes = {
  className: PropTypes.string,
  project: PropTypes.object.isRequired
};

export default ProjectCard;
