import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import moment from 'moment';
import { makeStyles } from '@material-ui/styles';
import { useDispatch, useSelector } from 'react-redux';
import { ReviewStars } from 'components';
import {
  Avatar,
  Button,
  Card,
  CardContent,
  Checkbox,
  colors,
  Link,
  Typography,
  Tooltip,
} from '@material-ui/core';
import { setAssigned, setUpdateReport } from 'actions';
import getInitials from 'utils/getInitials';
import DynamicFeedIcon from '@material-ui/icons/DynamicFeed';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    flexWrap: 'wrap',
    marginBottom: theme.spacing(2)
  },
  content: {
    padding: theme.spacing(2),
    flexGrow: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
      flexWrap: 'wrap'
    },
    '&:last-child': {
      paddingBottom: theme.spacing(2)
    }
  },
  header: {
    maxWidth: '100%',
    width: 240,
    display: 'flex',
    alignItems: 'center',
    [theme.breakpoints.down('sm')]: {
      marginBottom: theme.spacing(2),
      flexBasis: '100%'
    }
  },
  avatar: {
    marginRight: theme.spacing(2)
  },
  stats: {
    padding: theme.spacing(1),
    [theme.breakpoints.down('sm')]: {
      flexBasis: '50%'
    }
  },
  selected: {
    backgroundColor: '#eee'
  },
  actions: {
    padding: theme.spacing(1),
    [theme.breakpoints.down('sm')]: {
      flexBasis: '50%'
    }
  }
}));

const ProjectCard = props => {
  const { project, className, ...rest } = props;
  const dispatch = useDispatch();
  const classes = useStyles();

  const statusColors = {
    '2- En ejecución': colors.orange[600],
    Canceled: colors.grey[600],
    Completed: colors.green[600]
  };

  const assignment = useSelector(state => state.assignment);
  const updatereport = useSelector(state => state.updatereport);

  const handleSelectOne = (event, id) => {
    const selectedProjects = updatereport?.UpdatedReport || [];
    const selectedCustomers = assignment?.Assigned || [];
    const selectedIndex = selectedCustomers.indexOf(id);
    
    let newSelectedCustomers = [];
    let newSelectedProjects = [];

    if (selectedIndex === -1) {
      newSelectedCustomers = newSelectedCustomers.concat(selectedCustomers, id);
      newSelectedProjects = newSelectedProjects.concat(selectedProjects, [project]);

    } else if (selectedIndex === 0) {
      newSelectedCustomers = newSelectedCustomers.concat(
        selectedCustomers.slice(1)
      );
      newSelectedProjects = newSelectedProjects.concat(
        selectedProjects.slice(1)
      );
    } else if (selectedIndex === selectedCustomers.length - 1) {
      newSelectedCustomers = newSelectedCustomers.concat(
        selectedCustomers.slice(0, -1)
      );
      newSelectedProjects = newSelectedProjects.concat(
        selectedProjects.slice(0, -1)
      );
    } else if (selectedIndex > 0) {
      newSelectedCustomers = newSelectedCustomers.concat(
        selectedCustomers.slice(0, selectedIndex),
        selectedCustomers.slice(selectedIndex + 1)
      );
      newSelectedProjects = newSelectedProjects.concat(
        selectedProjects.slice(0, selectedIndex),
        selectedProjects.slice(selectedIndex + 1)
      );
    }

    dispatch(setUpdateReport(newSelectedProjects));
    dispatch(setAssigned(newSelectedCustomers));
  };


  return (
    <Card
      {...rest}
      className={clsx(classes.root, className, assignment?.Assigned?.includes(project._id) === true && classes.selected)}
    >
      <CardContent className={classes.content}>
        {
          assignment?.show && (
          // assignment?.show === true && assignment?.Assigned?.length !==0 && (

            <div className={classes.actions}>
              <Checkbox
                checked={
                  assignment?.Assigned?.includes(project._id) === true
                }
                color="primary"
                // indeterminate={
                // selectedOrders.length > 0 &&
                // selectedOrders.length < tasks.length
                // }
                onChange={event =>
                  handleSelectOne(event, project._id)
                }
              />
            </div>
          )
        }
        <span>
          <ReviewStars value={project.priority} />
        </span>
        <div className={classes.header}>
          <Avatar
            alt="Author"
            className={classes.avatar}
            src={project.client}
          >
            {getInitials(project.author?.split('#')[0])}
          </Avatar>
          <div>
            <Typography variant="body2">
              <Link
                color="textPrimary"
                component={RouterLink}
                to="#"
                variant="h5"
              >
                {project.client}
              </Link>
            </Typography>
            <Typography variant="body2">
              por {' '}
              <Link //Leonel: retirar o agregar ruta de direccionamiento en link
                color="textPrimary"
                component={RouterLink}
                to="/management/users"
                variant="h6"
              >
                {project.author?.split('#')[0]}
              </Link>
            </Typography>
          </div>
        </div>
        <div className={classes.stats}>
          <Typography variant="h6">
            {''}
            {project.projectId}
          </Typography>
          <Typography variant="body2">Id Proyecto</Typography>
        </div>
        <div className={classes.stats}>
          <Typography variant="h6">{project.type}</Typography>
          <Typography variant="body2">Tipo de Trabajo</Typography>
        </div>
        <div className={classes.stats}>
          <Typography variant="h6">
            {moment(project.createdAt).format('DD MMMM YYYY')}
          </Typography>
          <Typography variant="body2">Inicio del Proyecto</Typography>
        </div>
        <div className={classes.stats}>
          <Typography variant="h6">
            {moment(project.programmingDate).format('DD MMMM YYYY')}
          </Typography>
          <Typography variant="body2">Compromiso del Proyecto</Typography>
        </div>
        <div className={classes.stats}>
          <Typography
            style={{ color: statusColors[project.status] }}
            variant="h6"
          >
            {project.status}
          </Typography>
          <Typography variant="body2">Estado del proyecto</Typography>
        </div>
        <div className={classes.actions}>
          <a href={`/projects/${project.projectId}/overview`}>
            <Button
              color="primary"
              size="small"
              variant="outlined"
            >
              Ver
            </Button>
          </a>
        </div>
        <div>
          <Tooltip
            aria-label="Hitos"
            title="Hitos"
          >
            <Link
              color="textPrimary"
              component={RouterLink}
              to={`/projects/${project.projectId}/hitos`}
              variant="h5"
            >
               <Button
                color="primary"
                size="small"
                variant="outlined"
              >
                <DynamicFeedIcon />
              </Button>
            </Link>
          </Tooltip>
        </div>
      </CardContent>
    </Card>
  );
};

ProjectCard.propTypes = {
  className: PropTypes.string,
  project: PropTypes.object.isRequired
};

export default ProjectCard;
