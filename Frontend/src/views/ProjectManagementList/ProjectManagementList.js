import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';
import {
  CircularProgress,
  IconButton,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
  Tooltip,
  Typography
} from '@material-ui/core';
import { Page, Paginate, BottomBar } from 'components';
import { Header, ProjectCard, SearchBar } from './components';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import PersonIcon from '@material-ui/icons/Person';
import Cookies from 'js-cookie';
import { useDispatch, useSelector } from 'react-redux';
import { saveAssignment, setAssigned, setUpdateReport, setResponsable } from 'actions';
import { useHistory } from 'react-router';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  options: {
    marginTop: theme.spacing(3),
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  loadContainer: {
    display: 'flex',
    justifyContent: 'center'
  },
  results: {
    marginTop: theme.spacing(3)
  },
  paginate: {
    marginTop: theme.spacing(3),
    display: 'flex',
    justifyContent: 'center'
  }
}));

const ProjectManagementList = (props) => {
  const history = useHistory();
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [page, setPage] = useState(0);
  const [totalReg, setTotalReg] = useState(0);
  const [projects, setProjects] = useState([]);
  const [keyword, setKeyword] = useState('');
  const [filters, setFilters] = useState([]);
  const [selectsCkeckTitle, setSelectsCkeckTitle] = useState('');
  const dispatch = useDispatch();
  const assignment = useSelector(state => state.assignment);
  const updatereport = useSelector(state => state.updatereport);
  const url = '/projects';
  const columnsNeeded = {
    'client': 1,
    'author': 1,
    'projectId': 1,
    'service': 1,
    'type': 1,
    'createdAt': 1,
    'programmingDate': 1,
    'status': 1,
  }
  const dataStates = [
    { 'projectStatus': '1- Generada' },
    { 'projectStatus': '2- En ejecución' },
    { 'projectStatus': '3- Terminada' },
    { 'projectStatus': 'Alistamiento' },
    { 'projectStatus': 'Caja OB' },
    { 'projectStatus': 'Detalle Solución' },
    { 'projectStatus': 'Diseño' },
    { 'projectStatus': 'Ejecución ruta' },
    { 'projectStatus': 'Entrega' },
    { 'projectStatus': 'Factibilidad' },
    { 'projectStatus': 'Instalación' },
    { 'projectStatus': 'Liberación Recursos' },
    { 'projectStatus': 'Pendiente Cliente' },
    { 'projectStatus': 'Pendiente Telmex' },
    { 'projectPriority': 0 },
    { 'projectPriority': 1 },
    { 'projectPriority': 2 },
    { 'projectPriority': 3 },
    { 'projectPriority': 4 },
    { 'projectPriority': 5 },
    { 'projectPriority': 6 },
    { 'projectPriority': 7 },
    { 'projectPriority': 8 },
    { 'projectPriority': 9 },
    { 'projectType': 'Adicion / Retiro de numeros' },
    { 'projectType': 'Adicion de cuentas de correo' },
    { 'projectType': 'Adición de Equipos' },
    { 'projectType': 'Cambio de Equipos' },
    { 'projectType': 'Cambio de Servicio' },
    { 'projectType': 'Cambio de Velocidad(Ampliación - Reducción)' },
    { 'projectType': 'Cambio Plan Pymes' },
    { 'projectType': 'Cambio Tipo Acceso, Servicio y Ampliación' },
    { 'projectType': 'Desconexión Temporal' },
    { 'projectType': 'Desinstalación Cliente Corporativo' },
    { 'projectType': 'Instalación Grandes Proyectos' },
    { 'projectType': 'Instalación Proyectos Especiales' },
    { 'projectType': 'Instalación Pymes' },
    { 'projectType': 'Instalación Servicios Datacenter' },
    { 'projectType': 'Instalación Servicios Estándar' },
    { 'projectType': 'Instalación Servicios Transmisión' },
    { 'projectType': 'Novedades soluciones administradas' },
    { 'projectType': 'Nueva Instalación' },
    { 'projectType': 'Reconexión Cliente' },
    { 'projectType': 'Retiro de Equipos' },
    { 'projectType': 'Suspensión por Cartera' },
    { 'projectType': 'Suspensión por Cartera Pymes' },
    { 'projectType': 'Traslado Externo' },
    { 'projectType': 'Traslado Interno' },
    { 'projectType': 'Cambio de Medio' },
    { 'projectType': 'CambioTipo de Acceso' },
    { 'projectType': 'Caso de Seguimiento' },
    { 'projectType': 'Factibilidad' },
    { 'projectType': 'Instalación Demo Implementación' },
    { 'projectType': 'Plan Accion' },
    { 'projectType': 'Punto Cental (Nueva)' },
    { 'projectType': 'Punto Central' },
    { 'projectType': 'Punto Central Pymes' },
    { 'projectType': 'RED - Crecimiento de Red' },
    { 'projectType': 'RED - Expansion de Red' },
  ];

  // let userOnyx = '';
  // if (Cookies.get('roles') !== undefined) {
  //   const findUser = JSON.parse(Cookies.get('roles')).find((x) => x.role === 'Ingeniero Gestion');
  //   if (findUser !== '') { userOnyx = findUser.userOnyx; };
  // }

  const fetchProjects = async (body) => {
    try {

      setLoading(true);
      const petition = await axios.post(
        '/projects/pagination',
        body,
        getAuthorization(),
      );
      
      if(petition.status === 200) {
        setProjects(petition.data.data)
        setTotalReg(petition.data.total);
        setPage(petition.data.page);
        setRowsPerPage(petition.data.per_page);
        setLoading(false);
      }

    } catch (error) {
      console.error(error);
      setLoading(false);
    }

  };

  const bodyFunction = (limit, page, searchLocal = '',filtersLocal = []) => {
    const data = [];
    let body = {
      'limit': limit,
      'page': page,
      // 'columnsNeeded': columnsNeeded
    };
    if(searchLocal !== '') {
      for (var i in columnsNeeded) data.push({ 'field': [i], 'value': searchLocal});
      body = { ...body, 'or': data };
    }
    if(filtersLocal.length !== 0) {
      body = { ...body, 'and': filtersLocal};
    }
    return body;
  }
  useEffect(() => {
    cleanAsigned();
    dispatch(setUpdateReport([]));
    dispatch(setAssigned([]));
    dispatch(saveAssignment(false));
    fetchProjects({limit: 10, page: 1});
  },[]);


  const handleFilter = (e) => {
    const filtersLocal = [];
    if (e.createAt[0] !== '' || e.createAt[1] !== '') filtersLocal.push({ 'field': 'createAt', 'value': e.createAt[0] !== '' && e.createAt[1] !== '' ? e.createAt : (e.createAt[0] === '' ? [e.createAt[1]] : [e.createAt[0]]) });
    if (e.updateAt[0] !== '' || e.updateAt[1] !== '') filtersLocal.push({ 'field': 'updateAt', 'value': e.updateAt[0] !== '' && e.updateAt[1] !== '' ? e.updateAt : (e.updateAt[0] === '' ? [e.updateAt[1]] : [e.updateAt[0]]) });
    if (e.status.length !== 0) filtersLocal.push({ 'field': 'status', 'value': e.status });
    if (e.type.length !== 0) filtersLocal.push({ 'field': 'type', 'value': e.type });
    if (e.priority.length !== 0) filtersLocal.push({ 'field': 'priority', 'value': e.priority });
    if (e.projectId !== '' && e.projectId !== undefined) filtersLocal.push({ 'field': 'projectId', 'value': e.projectId });
    if (e.client !== '' && e.client !== undefined) filtersLocal.push({ 'field': 'client', 'value': e.client });
    if (e.author !== '' && e.author !== undefined) filtersLocal.push({ 'field': 'author', 'value': e.author });
    setFilters(filtersLocal);
    const body = bodyFunction(rowsPerPage, 1, keyword, filtersLocal);
    fetchProjects(body);
  };

  const handleSearch = (e) => {
    setKeyword(e);
    const body = bodyFunction(rowsPerPage, 1, e, filters);
    fetchProjects(body);
  };

  const handleChangePage = (e) => {
    const body = bodyFunction(rowsPerPage, (e.selected + 1), keyword, filters);
    fetchProjects(body);
  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleAssignment = () => {
    setSelectsCkeckTitle('Asignar Actividades');
    dispatch(saveAssignment(true));
    handleClose();
  }

  const handleUpdateReport = () => {
    setSelectsCkeckTitle('Reporte de Actualización');
    dispatch(saveAssignment(true));
    handleClose();
  }

  const handleCloseBottomBar = () => {
    dispatch(saveAssignment(false));
    dispatch(setUpdateReport([]));
    dispatch(setAssigned([]));
  }
  
  const cleanAsigned = () => {
    dispatch(saveAssignment(false));
    dispatch(setAssigned([]));
  }

  return (
    <Page
      className={classes.root}
      title="Project Management List"
    >
      <Header />
      <SearchBar
        onFilter={handleFilter}
        onSearch={handleSearch}
        states={dataStates}
        value={keyword}
      />
      <div className={classes.results}>
        <div className={classes.options}>
          <Typography
            color="textSecondary"
            gutterBottom
            variant="body2"
          >
            {totalReg} Registros encontrados. Página {page} de{' '}
            {Math.ceil(totalReg / rowsPerPage)}
          </Typography>
          <div>
            <Tooltip title="Mas opciones">
              <IconButton
                aria-controls="simple-menu"
                aria-haspopup="true"
                onClick={handleClick}
              >
                <MoreVertIcon />
              </IconButton>
            </Tooltip>
            <Menu
              anchorEl={anchorEl}
              id="simple-menu"
              keepMounted
              onClose={handleClose}
              open={Boolean(anchorEl)}
            >
              <MenuItem onClick={handleAssignment}>
                <ListItemIcon>
                  <PersonIcon />
                </ListItemIcon>
                <ListItemText primary="Asignar" />
              </MenuItem>
              <MenuItem onClick={handleUpdateReport}>
                <ListItemIcon>
                  <PersonIcon />
                </ListItemIcon>
                <ListItemText primary="Reporte de Actualización"/>
              </MenuItem>
            </Menu>
          </div>
        </div>
        {loading ? (
          <div className={classes.loadContainer}>
            <CircularProgress />
          </div>
        ) : (
          <>
            {projects.length > 0 ? projects.map((project, key) => (
              <ProjectCard
                key={project._id}
                project={project}
              />
            )) : (
              <Typography
                align="center"
                variant="h6"
              >
                Sin Resultados.
              </Typography>
            )}
          </>
        )}
      </div>
      <div className={classes.paginate}>
        <Paginate
          onPageChange={handleChangePage}
          pageCount={Math.ceil(totalReg / rowsPerPage)}
        />
      </div>
      <BottomBar
        onClose={handleCloseBottomBar}
        role={{ 'role': 'Ingeniero Gestion', 'area':'Configuracion', 'group': 'BO' }}
        route="/projects/"
        selected={assignment?.Assigned || []}
        selectedProjects={updatereport?.UpdatedReport || []}
        title={selectsCkeckTitle}
      />
    </Page>
  );
};

export default ProjectManagementList;
