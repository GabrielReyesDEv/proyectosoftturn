import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Avatar,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  Link,
  Typography,
  colors
} from '@material-ui/core';
import duration from 'utils/duration';
import getInitials from 'utils/getInitials';
import { Label } from 'components';

const useStyles = makeStyles(theme => ({
  root: {},
  header: {
    paddingBottom: 0
  },
  content: {
    padding: 0,
    '&:last-child': {
      paddingBottom: 0
    }
  },
  description: {
    padding: theme.spacing(2, 3, 1, 3)
  },
  tags: {
    padding: theme.spacing(0, 3, 1, 3),
    '& > * + *': {
      marginLeft: theme.spacing(1)
    }
  },
  learnMoreButton: {
    marginLeft: theme.spacing(2)
  },
  likedButton: {
    color: colors.red[600]
  },
  shareButton: {
    marginLeft: theme.spacing(1)
  },
  details: {
    padding: theme.spacing(1, 3)
  }
}));

const ProjectCard = props => {
  const { activity, task, className, ...rest } = props;

  const statusColors = {
    'Primary': colors.red[600],
    'Secondary': colors.orange[600],
    'Calendar': colors.green[600],
  };

  const classes = useStyles();

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader
        avatar={
          <Avatar
            alt="Author"
            src={activity.activityType}
          >
            {getInitials(task.author?.split('#')[0])}
          </Avatar>
        }
        className={classes.header}
        disableTypography
        subheader={
          <Typography variant="body2">
            {task.activityId}{' '} por{' '}
            <Link
              color="textPrimary"
              component={RouterLink}
              to="/profile/1/timeline"
              variant="h6"
            >
              {task.author?.split('#')[0]}
            </Link>{' '}
              Actualizado: {duration(task.updatedAt, task.updatedAt)}
          </Typography>
        }
        title={
          <Link
            color="textPrimary"
            component={RouterLink}
            to="/administration/tasks"
            variant="h5"
          >
            {activity.title}
          </Link>
        }
      />
      <CardContent className={classes.content}>
        <div className={classes.description}>
          <Typography
            colo="textSecondary"
            variant="subtitle2"
          >
            {task.orderType}
          </Typography>
          <Typography
            colo="textSecondary"
            variant="subtitle2"
          >
            {activity.projectId ? `\nProyecto ${activity.projectId}` : ''}
          </Typography>
        </div>
        <div className={classes.tags}>
          <Label
            color={statusColors[task.type]}
            key={task.type}
          >
            {task.type}
          </Label>
        </div>
        <Divider />
        <div className={classes.details}>
          <Grid
            alignItems="center"
            container
            justify="space-between"
            spacing={3}
          >
            <Grid item>
              <Typography variant="h5">{task.deadlineDate}</Typography>
              <Typography variant="body2">Fecha Entrega</Typography>
            </Grid>
            <Grid item>
              <Typography variant="h5">{task.group}</Typography>
              <Typography variant="body2">Grupo</Typography>
            </Grid>
            <Grid item>
              <Typography variant="h5">{task.role}</Typography>
              <Typography variant="body2">Rol</Typography>
            </Grid>
            <Grid item>
              <Typography variant="h5">{activity.status?.split('#')[0]}</Typography>
              <Typography variant="body2">Estado</Typography>
            </Grid>
            <Grid item>
              <Button
                className={classes.learnMoreButton}
                component={RouterLink}
                size="small"
                to={`/activities/${activity.activityId}/overview`}
              >
                Mostrar más
              </Button>
            </Grid>
          </Grid>
        </div>
      </CardContent>
    </Card>
  );
};

ProjectCard.propTypes = {
  activity: PropTypes.object.isRequired,
  className: PropTypes.string,
  task: PropTypes.object.isRequired
};

export default ProjectCard;
