import React, {useState} from 'react';
import 'assets/scss/InputFile.scss';
import { makeStyles } from '@material-ui/styles';
import Button from '@material-ui/core/Button';
import clsx from 'clsx';
import { green, red } from '@material-ui/core/colors';
import {
  CardMedia,
  CircularProgress,
  Typography,
} from '@material-ui/core';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import CancelOutlinedIcon from '@material-ui/icons/CancelOutlined';

const useStyles = makeStyles(theme => ({
  media: {
    borderTopRightRadius: 4,
    borderBottomRightRadius: 4,
    padding: theme.spacing(3),
    color: '333',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },
  buttonProgress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
  buttonSuccess: {
    backgroundColor: green[500],
    '&:hover': {
      backgroundColor: green[700],
    },
  },
  buttonHerror: {
    backgroundColor: red[500],
    '&:hover': {
      backgroundColor: red[700],
    },
  },
  divider: {
    margin: theme.spacing(2, 0)
  },
  wrapper: {
    margin: theme.spacing(1),
    position: 'relative',
  },
  fabProgress: {
    color: green[500],
    position: 'absolute',
    top: -6,
    left: -6,
    zIndex: 1,
  }
}));


const InputFile = (props) => {
  const classes = useStyles();

  const [icon, setIcon] = useState(0);

    const buttonClassname = clsx({
      [classes.buttonSuccess]: props.success,
      [classes.buttonHerror]: props.error,
    });

    const showFiles = (files) => {
      const fileLabelText = document.getElementById('fileLabelText');
      console.log('length: ', files?.length)
      if (files?.length > 1) {
        setIcon(1);
        fileLabelText.innerText = `${files} selected`;
      } else {
        fileLabelText.innerText = 'Click Aquí para Seleccionar un Archivo';
        setIcon(0);
      }
    };

    const handleChange = (e) => {
      const droppedfiles = e.target.files;
      showFiles(droppedfiles[0]?.name);
    };

    return (
      <div className='inputFileContainer'>
        <form id='file_form'>
          <CardMedia
            className={classes.media}
            title="Cover"
          >
            <label htmlFor='file_imput' id='file_label'>
              <div className={classes.wrapper}>
                {!props.success && !props.error ? (
                  icon ? (
                    <InsertDriveFileIcon style={{ color : '#008fd5', width: '157px', height: '157px'}}/>    
                  ) : (
                    <InsertDriveFileIcon style={{ color : '#aaaaaa', width: '157px', height: '157px'}}/>
                  )
                ) : (
                  <>
                    {props.success ? (
                      <CheckCircleOutlineIcon style={{ color : green[500], width: '157px', height: '157px'}}/>
                    ) : ''}
                    {props.error ? (
                      <CancelOutlinedIcon style={{ color : red[500], width: '157px', height: '157px'}}/>
                    ) : ''}
                  </>
                )}
                {props.loading && <CircularProgress size={24} style={{color: 'white'}} className={classes.buttonProgress} />}
              </div>
              <div className={classes.divider}></div>
              <Typography id='fileLabelText' variant="subtitle2">
                Click Aquí para Seleccionar un Archivo
              </Typography>
            </label>
          </CardMedia>
          <input type='file' name='file' id='file_imput' onChange={handleChange} />
          <div className={classes.wrapper}>
            <Button
              variant='contained'
              className={buttonClassname}
              disabled={props.loading}
              onClick={props.accion}
              color='primary'
            >
              Cargar Informacíon
            </Button>
            {props.loading && <CircularProgress size={24} style={{color: '#3h51b5'}} className={classes.buttonProgress} />}
          </div>
        </form>
      </div>
    );
}
export default InputFile;
