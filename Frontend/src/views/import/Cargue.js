// eslint-disable-next-line no-unused-vars
import React, { useState } from 'react';
//import { DataTypeProvider } from '@devexpress/dx-react-grid';
import axios from 'utils/axios';
import { makeStyles } from '@material-ui/styles';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import { InputFile } from './components';
import {
  Typography,
  Button,
  LinearProgress,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing(6, 2)
  },
  title: {
    minHeight: '68vh',
    display: 'flex',
    flexFlow: 'column nowrap',
    justifyContent: 'center',
    alignItems: 'center',
  }
}));


const Cargue = (props) => {
  const classes = useStyles();

  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState(false);
  const [error, setError] = useState(false);
  const [state, setState] = useState({
    loading: false,
    modalIsOpen: false,
    modalMessage: '',
    modalText: '',
    success: false,
  });
  const timer = React.useRef();

  const handleCloseModal = () => {
    setState({ modalIsOpen: false });
  };
  const viewError = (segundos) => {
    setLoading(false);
    setSuccess(false);
    setError(true);
    if (segundos) {
      return timer.current = window.setTimeout(() => {
        setError(false)
      }, segundos);
    }
    return null;
  };

  const viewSuccess = (segundos) => {
    setLoading(false);
    setLoading(false);
    setSuccess(true);
    if (segundos) {
      return timer.current = window.setTimeout(() => {
        setSuccess(false)
      }, segundos);
    }
    return null;
  };

  const handleInputChange = async () => {
    setSuccess(false);
    setError(false);
    setLoading(true);
    console.info('validando...');
    const target = document.getElementById('file_imput');
    if (!target.files[0]) {
      setState({
        modalMessage: 'Inserte un Archivo..',
        modalIsOpen: true,
      });
      viewError(3000);
      return false;
    }
    const nombreArchivo = target.files[0].name;
    if (!nombreArchivo.match('.xlsx')) {
      setState({
        modalMessage: 'El Formato del Archivo es equivocado',
        modalIsOpen: true,
      });
      viewError(3000);
      return false;
    }
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const { name } = target;

    setState({
      [name]: value,
    });
    if (name === 'file') {

      const data = new FormData();
      data.append('file', target.files[0]);
      try {
        const importe = await axios.post('/s3/import', data, { headers: { 'Content-Type': 'multipart/form-data' } })
      if (importe.status === 200) {
        viewSuccess();
        const postImport = await axios.post('/loadData/functionality',{});
        if (postImport.status === 201) {
          setState({
            modalMessage: 'Cargue en proceso, cuando termine se le enviara un correo notificandole',
            modalIsOpen: true,
          });    
        }  
      }
        
      } catch (error) {
        console.error(error)
        viewError(5000);
      }
    }
  };

  return (
    <>
      {loading && <LinearProgress />}
      <div className={classes.title}>
        <Typography
        style={{margin: '20px 0'}}
          gutterBottom
          variant="h2"
        >
          Cargue de Informacion
        </Typography>
        <InputFile
          loading={loading}
          success={success}
          error={error}
          accion={handleInputChange}
          // accion={handleButtonClick}
        />
        <Dialog
          open={state.modalIsOpen}
          onClose={handleCloseModal}        
        >
          {state.loading === true ? (
            <div className='loaderminiCont'>
              <div id='loaderMini' />
              <h1>{state.modalMessage}</h1>
              <h4>{state.modalText} </h4>  {/*eslint-disable-line*/}
            </div>
          ) : (
            <div>
              <DialogTitle>
                <Typography
                  gutterBottom
                  variant="h2"
                >
                  {state.modalMessage}
                </Typography>
              </DialogTitle>
              <DialogContent>
                <DialogContentText>
                  <Typography variant="subtitle2">
                    {state.modalText}
                  </Typography>
                </DialogContentText>
              </DialogContent>
              {state.success === true ? (
                <div className='loaderminiCont'>
                  <CheckCircleOutlineIcon style={{ fontSize: 90, color: '#14b914' }} />
                </div>
              ) : ''}
              <DialogActions>
                <Button onClick={handleCloseModal} color="secondary">
                  Cerrar
                </Button>
              </DialogActions>
            </div>
          )}
        </Dialog>
      </div>
    </>
  );
};

export default Cargue;
