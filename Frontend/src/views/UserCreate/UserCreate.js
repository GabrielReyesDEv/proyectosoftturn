import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { useSelector } from 'react-redux';
import { Button, Grid } from '@material-ui/core';
import useRouter from 'utils/useRouter';
import { Page } from 'components';
import {
  Header,
  AreaSelection,
  UserInfo
} from './components';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';
import moment from 'moment';

const useStyles = makeStyles(theme => ({
  root: {
    width: theme.breakpoints.values.lg,
    maxWidth: '100%',
    margin: '0 auto',
    padding: theme.spacing(3, 3, 6, 3)
  },
  aboutAuthor: {
    marginTop: theme.spacing(3)
  },
  aboutProject: {
    marginTop: theme.spacing(3)
  },
  projectCover: {
    marginTop: theme.spacing(3)
  },
  projectDetails: {
    marginTop: theme.spacing(3)
  },
  preferences: {
    marginTop: theme.spacing(3)
  },
  actions: {
    marginTop: theme.spacing(3),
    display: 'flex',
    justifyContent: 'flex-end'
  }
}));

const UserCreate = () => {
  const classes = useStyles();
  const user = useSelector(state => state.newUser);
  const router = useRouter();

  const handleCreate = async () => {
    try {
      const consult = await axios.post('/auth/sign-up', {
          ...user.values,
          createdAt: moment().format('DD/MM/YYYY'),
          password: '$2b$10$EOe35sHKi.Q264Nq7ip5Iuyk8G06SAMbdBmYHdjMwlKZBBS7edGlm',
          roles: user.roles,
          status: 'Active',
          group: 'BO',
          verified: true
        } , getAuthorization());
      if (consult.status === 201) router.history.push('/management/users');
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <Page
      className={classes.root}
      title="Crear usuario"
    >
      <Header />
      <Grid
        container
        spacing={3}
      >
        <Grid
          item
          md={8}
          xl={3}
          xs={12}
        >
          <UserInfo className={classes.aboutProject} />
        </Grid>
        <Grid
          item
          md={4}
          xl={3}
          xs={12}
        >
          <AreaSelection className={classes.aboutAuthor} />
          <div className={classes.actions}>
            <Button
              color="primary"
              onClick={handleCreate}
              variant="contained"
            >
          Crear Usuario
            </Button>
          </div>
        </Grid>
      </Grid>
    </Page>
  );
};

export default UserCreate;
