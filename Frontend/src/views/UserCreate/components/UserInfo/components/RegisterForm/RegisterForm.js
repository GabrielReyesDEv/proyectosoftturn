import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { editUser } from 'actions';
import validate from 'validate.js';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  TextField
} from '@material-ui/core';

const schema = {
  firstName: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 32
    }
  },
  lastName: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 32
    }
  },
  identification: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 32
    }
  },
  phone: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 32
    }
  },
  workPosition: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 128
    }
  },
  email: {
    presence: { allowEmpty: false, message: 'is required' },
    email: true,
    length: {
      maximum: 64
    }
  }
};

const useStyles = makeStyles(theme => ({
  root: {},
  fields: {
    margin: theme.spacing(-1),
    display: 'flex',
    flexWrap: 'wrap',
    '& > *': {
      flexGrow: 1,
      margin: theme.spacing(1)
    }
  }
}));

const RegisterForm = props => {
  const { className, ...rest } = props;

  const classes = useStyles();
  const dispatch = useDispatch();
  const user = useSelector(state => state.newUser);

  useEffect(() => {
    const errors = validate(user.values, schema);

    dispatch(editUser({
      isValid: errors ? false : true,
      errors: errors || {}
    }));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleChange = event => {
    event.persist();
    dispatch(editUser({
      ...user,
      values: {
        ...user.values,
        [event.target.name]: event.target.value
      },
      touched: {
        ...user.touched,
        [event.target.name]: true
      }
    }));
  };

  const handleSubmit = async event => {
    event.preventDefault();
  };

  const hasError = field =>
    user.touched[field] && user.errors[field] ? true : false;

  return (
    <form
      {...rest}
      className={clsx(classes.root, className)}
      onSubmit={handleSubmit}
    >
      <div className={classes.fields}>
        <TextField
          error={hasError('firstName')}
          helperText={
            hasError('firstName') ? user.errors.firstName[0] : null
          }
          label="First name"
          name="firstName"
          onChange={handleChange}
          value={user.values.firstName || ''}
          variant="outlined"
        />
        <TextField
          error={hasError('lastName')}
          helperText={
            hasError('lastName') ? user.errors.lastName[0] : null
          }
          label="Last name"
          name="lastName"
          onChange={handleChange}
          value={user.values.lastName || ''}
          variant="outlined"
        />
        <TextField
          error={hasError('email')}
          fullWidth
          helperText={hasError('email') ? user.errors.email[0] : null}
          label="Email address"
          name="email"
          onChange={handleChange}
          value={user.values.email || ''}
          variant="outlined"
        />
        <TextField
          error={hasError('identification')}
          helperText={
            hasError('identification') ? user.errors.identification[0] : null
          }
          label="Cedula"
          name="identification"
          type="number"
          onChange={handleChange}
          value={user.values.identification || ''}
          variant="outlined"
        />
        <TextField
          error={hasError('phone')}
          helperText={
            hasError('phone') ? user.errors.phone[0] : null
          }
          label="Telefono"
          name="phone"
          type="number"
          onChange={handleChange}
          value={user.values.phone || ''}
          variant="outlined"
        />
        
        <TextField
          error={hasError('workPosition')}
          fullWidth
          helperText={
            hasError('workPosition') ? user.errors.workPosition[0] : null
          }
          label="Cargo"
          name="workPosition"
          onChange={handleChange}
          value={user.values.workPosition || ''}
          variant="outlined"
        />
      </div>
    </form>
  );
};

RegisterForm.propTypes = {
  className: PropTypes.string
};

export default RegisterForm;
