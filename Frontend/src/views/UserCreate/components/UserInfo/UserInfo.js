import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardContent,
  CardHeader
} from '@material-ui/core';
import { RegisterForm } from './components';

const useStyles = makeStyles(theme => ({
  root: {
  },
}));

const UserInfo = (props) => {
  const { className, ...rest } = props;
  const classes = useStyles();

  return (
    <>
      <Card 
        {...rest}
        className={clsx(classes.root, className)}
      >
        <CardHeader title="Información" />
        <CardContent>
          <RegisterForm container spacing={1} className={classes.registerForm} />
        </CardContent>
      </Card>
    </>
  );

};

UserInfo.propTypes = {
  className: PropTypes.string
};

export default UserInfo;
