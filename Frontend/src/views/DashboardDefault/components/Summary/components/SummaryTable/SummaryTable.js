import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  colors
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {},
  critical: {
    '& $indicator': {
      borderColor: colors.red[600]
    }
  },
  indicator: {
    height: 12,
    width: 12,
    borderWidth: 4,
    borderStyle: 'solid',
    borderColor: colors.grey[100],
    borderRadius: '50%'
  },
  viewButton: {
    marginLeft: theme.spacing(2)
  }
}));

const OtpTable = props => {

  const classes = useStyles();

  return (
    <div className={classes.inner}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell sortDirection="desc">
              Semana
            </TableCell>
            <TableCell>MRC</TableCell>
            <TableCell>Ots</TableCell>
            <TableCell>Entregado</TableCell>
            <TableCell>Programado</TableCell>
            <TableCell>Falta</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow
              hover
            >
              <TableCell>Entregado Noviembre</TableCell>
              <TableCell>$ 370.000</TableCell>
              <TableCell></TableCell>
              <TableCell>$ 370.000</TableCell>
              <TableCell></TableCell>
              <TableCell></TableCell>
          </TableRow>
          <TableRow
              hover
            >
              <TableCell>Semana 1</TableCell>
              <TableCell>$ 3.802.473</TableCell>
              <TableCell>6</TableCell>
              <TableCell>$ 3.392.473</TableCell>
              <TableCell>$ -</TableCell>
              <TableCell>$ 410.000</TableCell>
          </TableRow>
          <TableRow
              hover
            >
              <TableCell>Semana 2</TableCell>
              <TableCell>$ 18.838.262</TableCell>
              <TableCell>26</TableCell>
              <TableCell>$ 10.003.058</TableCell>
              <TableCell>$ -</TableCell>
              <TableCell>$ 8.835.204</TableCell>
          </TableRow>
          <TableRow
              hover
            >
              <TableCell>Semana 2</TableCell>
              <TableCell>$ 18.838.262</TableCell>
              <TableCell>26</TableCell>
              <TableCell>$ 10.003.058</TableCell>
              <TableCell>$ -</TableCell>
              <TableCell>$ 8.835.204</TableCell>
          </TableRow>
          <TableRow
              hover
            >
              <TableCell>Semana 3</TableCell>
              <TableCell>$ 19.514.862</TableCell>
              <TableCell>45</TableCell>
              <TableCell>$ 1.476.065</TableCell>
              <TableCell>$ 10.289.033</TableCell>
              <TableCell>$ 7.749.764</TableCell>
          </TableRow>
          <TableRow
              hover
            >
              <TableCell>Semana 4</TableCell>
              <TableCell>$ -</TableCell>
              <TableCell></TableCell>
              <TableCell>$ -</TableCell>
              <TableCell>$ 10.084.700</TableCell>
              <TableCell>$ 10.084.700</TableCell>
          </TableRow>
          <TableRow
              hover
            >
              <TableCell>Semana 5</TableCell>
              <TableCell>$ -</TableCell>
              <TableCell></TableCell>
              <TableCell>$ -</TableCell>
              <TableCell>$  3.934.242</TableCell>
              <TableCell>$  3.934.242</TableCell>
          </TableRow>
          {/* {projects.map(project => (
            <TableRow
              hover
              key={project.id}
            >
              <TableCell>{project.title}</TableCell>
              <TableCell>
                <div className={classes.author}>
                  <Avatar
                    alt="Author"
                    className={classes.avatar}
                    src={project.author.avatar}
                  >
                    {project.author.name}
                  </Avatar>
                  {project.author.name}
                </div>
              </TableCell>
              <TableCell>
                {project.currency}
                {project.price}
              </TableCell>
              <TableCell>
                <div className={classes.tags}>
                  {project.tags.map(tag => (
                    <Label
                      color={tag.color}
                      key={tag.text}
                    >
                      {tag.text}
                    </Label>
                  ))}
                </div>
              </TableCell>
              <TableCell align="right">
                <Button
                  color="primary"
                  component={RouterLink}
                  size="small"
                  to="/projects/1/overview"
                  variant="outlined"
                >
                  View
                </Button>
              </TableCell>
            </TableRow>
          ))} */}
        </TableBody>
      </Table>
    </div>
  );
};

OtpTable.propTypes = {
  className: PropTypes.string,
  task: PropTypes.object.isRequired
};

export default OtpTable;
