import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import PerfectScrollbar from 'react-perfect-scrollbar';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardContent,
  Box,
  Typography,
  AppBar,
  Tabs,
  Tab
} from '@material-ui/core';
import { SummaryTable } from './components';

import axios from 'utils/axios';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    'aria-controls': `scrollable-auto-tabpanel-${index}`,
  };
}

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  inner: {
    padding: 0
  },
  author: {
    display: 'flex',
    alignItems: 'center'
  },
  avatar: {
    marginRight: theme.spacing(1)
  },
  tags: {
    '& > * + *': {
      marginLeft: theme.spacing(1)
    }
  },
  actions: {
    justifyContent: 'flex-end'
  },
  arrowForwardIcon: {
    marginLeft: theme.spacing(1)
  }
}));

const LatestProjects = props => {
  const { className, ...rest } = props;

  const classes = useStyles();
  const [projects, setProjects] = useState([]);

  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  useEffect(() => {
    let mounted = true;

    const fetchProjects = () => {
      axios.get('/api/dashboard/latest-projects').then(response => {
        if (mounted) {
          setProjects(response.data.projects);
        }
      });
    };

    fetchProjects();

    return () => {
      mounted = false;
    };
  }, []);

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardContent className={classes.content}>
        <PerfectScrollbar options={{ suppressScrollY: true }}>
          <AppBar position="static" color="default">
            <Tabs
              value={value}
              onChange={handleChange}
              indicatorColor="primary"
              textColor="primary"
              variant="scrollable"
              scrollButtons="auto"
              aria-label="scrollable auto tabs example"
            >
              <Tab label="Outlook" {...a11yProps(0)} />
              <Tab label="Backlog" {...a11yProps(1)} />
              <Tab label="Novedades" {...a11yProps(2)} />
              <Tab label="All" {...a11yProps(2)} />
            </Tabs>
          </AppBar>
          <div className={classes.inner}>
            <TabPanel value={value} index={0}>
              <SummaryTable projects={projects}/>
            </TabPanel>
            <TabPanel value={value} index={1}>
              <SummaryTable projects={projects}/>
            </TabPanel>
            <TabPanel value={value} index={2}>
              <SummaryTable projects={projects}/>
            </TabPanel>
            <TabPanel value={value} index={2}>
              <SummaryTable projects={projects}/>
            </TabPanel>
          </div>
        </PerfectScrollbar>
      </CardContent>
    </Card>
  );
};

LatestProjects.propTypes = {
  className: PropTypes.string
};

export default LatestProjects;
