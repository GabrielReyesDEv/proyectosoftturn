import React from 'react';
import PropTypes from 'prop-types';
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow
} from '@material-ui/core';

const OtpTable = props => {

  return (
    <div>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell >OT</TableCell>
            <TableCell>NOMBRE_CLIENTE</TableCell>
            <TableCell>TIEMPO</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow
              hover
            >
              <TableCell>15693766
              </TableCell>
              <TableCell>CODESA</TableCell>
              <TableCell>Fuera de tiempos</TableCell>
            </TableRow>
            <TableRow
              hover
            >
              <TableCell>15693788
              </TableCell>
              <TableCell>CODESA</TableCell>
              <TableCell>Fuera de tiempos</TableCell>
            </TableRow>
          {/* {projects.map(project => (
            <TableRow
              hover
              key={project.id}
            >
              <TableCell>
                {project.currency}
                {project.price}
              </TableCell>
              <TableCell>{project.title}</TableCell>
              <TableCell>En tiempos</TableCell>
            </TableRow>
          ))} */}
        </TableBody>
      </Table>
    </div>
  );
};

OtpTable.propTypes = {
  className: PropTypes.string,
  task: PropTypes.object.isRequired
};

export default OtpTable;
