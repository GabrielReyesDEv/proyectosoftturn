import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  colors
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {},
  critical: {
    '& $indicator': {
      borderColor: colors.red[600]
    }
  },
  indicator: {
    height: 12,
    width: 12,
    borderWidth: 4,
    borderStyle: 'solid',
    borderColor: colors.grey[100],
    borderRadius: '50%'
  },
  viewButton: {
    marginLeft: theme.spacing(2)
  }
}));

const OtpTable = props => {

  const classes = useStyles();

  return (
    <div className={classes.inner}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell >OT</TableCell>
            <TableCell>NOMBRE_CLIENTE</TableCell>
            <TableCell>TIEMPO</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow
              hover
            >
              <TableCell>15694648</TableCell>
              <TableCell>FINANCREDITOS LTDA.</TableCell>
              <TableCell>5</TableCell>
            </TableRow>
          {/* {projects.map(project => (
            <TableRow
              hover
              key={project.id}
            >
              <TableCell>
                {project.currency}
                {project.price}
              </TableCell>
              <TableCell>{project.title}</TableCell>
              <TableCell>En tiempos</TableCell>
            </TableRow>
          ))} */}
        </TableBody>
      </Table>
      
    </div>
  );
};

OtpTable.propTypes = {
  className: PropTypes.string,
  task: PropTypes.object.isRequired
};

export default OtpTable;
