import React from 'react';
import PropTypes from 'prop-types';
import {
  Grid
} from '@material-ui/core';
import { TasksReview, OtsWithoutManagement } from './components';

const LatestProjects = props => {

  return (
    <>
    <Grid
        item
        xs={12}
      >
      <TasksReview />
    </Grid>
    <Grid
        item
        xs={12}
      >
      <OtsWithoutManagement />
    </Grid>
    </>
  );
};

LatestProjects.propTypes = {
  className: PropTypes.string
};

export default LatestProjects;
