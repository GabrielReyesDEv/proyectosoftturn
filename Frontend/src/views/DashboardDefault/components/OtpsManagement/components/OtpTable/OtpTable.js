import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TableSortLabel,
  Tooltip,
  colors
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {},
  critical: {
    '& $indicator': {
      borderColor: colors.red[600]
    }
  },
  indicator: {
    height: 12,
    width: 12,
    borderWidth: 4,
    borderStyle: 'solid',
    borderColor: colors.grey[100],
    borderRadius: '50%'
  },
  viewButton: {
    marginLeft: theme.spacing(2)
  }
}));

const OtpTable = props => {

  const classes = useStyles();

  return (
    <div className={classes.inner}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell sortDirection="desc">
              <Tooltip
                enterDelay={300}
                title="Sort"
              >
                <TableSortLabel
                  active
                  direction="desc"
                >
                  OTP
                </TableSortLabel>
              </Tooltip>
            </TableCell>
            <TableCell>OTP_NOMBRE_CLIENTE</TableCell>
            <TableCell>MRC</TableCell>
            <TableCell>Fecha Programación</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow
            hover
          >
            <TableCell>15310728</TableCell>
            <TableCell>LAUDATOS S A S</TableCell>
            <TableCell>$500.000</TableCell>
            <TableCell>10/12/2020</TableCell>
          </TableRow>
          <TableRow
            hover
          >
            <TableCell>15721360</TableCell>
            <TableCell>MINISTERIO DE DEFENSA NACIONAL</TableCell>
            <TableCell>$500.000</TableCell>
            <TableCell>10/12/2020</TableCell>
          </TableRow>
          <TableRow
            hover
          >
            <TableCell>15714879</TableCell>
            <TableCell>UFINET COLOMBIA S.A.</TableCell>
            <TableCell>$500.000</TableCell>
            <TableCell>10/12/2020</TableCell>
          </TableRow>
          {/* {projects.map(project => (
            <TableRow
              hover
              key={project.id}
            >
              <TableCell>{project.title}</TableCell>
              <TableCell>
                <div className={classes.author}>
                  <Avatar
                    alt="Author"
                    className={classes.avatar}
                    src={project.author.avatar}
                  >
                    {project.author.name}
                  </Avatar>
                  {project.author.name}
                </div>
              </TableCell>
              <TableCell>
                {project.currency}
                {project.price}
              </TableCell>
              <TableCell>
                <div className={classes.tags}>
                  {project.tags.map(tag => (
                    <Label
                      color={tag.color}
                      key={tag.text}
                    >
                      {tag.text}
                    </Label>
                  ))}
                </div>
              </TableCell>
              <TableCell align="right">
                <Button
                  color="primary"
                  component={RouterLink}
                  size="small"
                  to="/projects/1/overview"
                  variant="outlined"
                >
                  View
                </Button>
              </TableCell>
            </TableRow>
          ))} */}
        </TableBody>
      </Table>
    </div>
  );
};

OtpTable.propTypes = {
  className: PropTypes.string,
  task: PropTypes.object.isRequired
};

export default OtpTable;
