import React, { useEffect } from 'react';
import clsx from 'clsx';
import PerfectScrollbar from 'react-perfect-scrollbar';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardContent,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  AppBar,
  Tabs,
  Tab,
  Box,
  Typography,
} from '@material-ui/core';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    'aria-controls': `scrollable-auto-tabpanel-${index}`,
  };
}

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  inner: {
  },
  author: {
    display: 'flex',
    alignItems: 'center'
  },
  avatar: {
    marginRight: theme.spacing(1)
  },
  tags: {
    '& > * + *': {
      marginLeft: theme.spacing(1)
    }
  },
  actions: {
    justifyContent: 'flex-end'
  },
  arrowForwardIcon: {
    marginLeft: theme.spacing(1)
  }
}));

const LatestProjects = props => {
  const { className, ...rest } = props;

  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  useEffect(() => {
    // let mounted = true;

    // const fetchProjects = () => {
    //   axios.get('/api/dashboard/latest-projects').then(response => {
    //     if (mounted) {
    //       setProjects(response.data.projects);
    //     }
    //   });
    // };

    // fetchProjects();

    return () => {
      // mounted = false;
    };
  }, []);

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardContent className={classes.content}>
        <PerfectScrollbar options={{ suppressScrollY: true }}>
        <AppBar position="static" color="default">
            <Tabs
              value={value}
              onChange={handleChange}
              indicatorColor="primary"
              textColor="primary"
              variant="scrollable"
              scrollButtons="auto"
              aria-label="scrollable auto tabs example"
            >
              <Tab label="Empresas" {...a11yProps(0)} />
              <Tab label="Negocios" {...a11yProps(1)} />
            </Tabs>
          </AppBar>
          <div className={classes.inner}>
          <TabPanel value={value} index={0}>
          <Table>
              <TableHead>
                <TableRow>
                  <TableCell>
                    BackLog Instalaciones Empresas
                  </TableCell>
                  <TableCell>Compromiso MRC</TableCell>
                  <TableCell>Total</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {/* {projects.map(project => ( */}
                  <TableRow
                    hover
                  >
                    <TableCell>COMPROMISO</TableCell>
                    <TableCell>$ 205.152.767</TableCell>
                    <TableCell></TableCell>
                  </TableRow>
                  <TableRow
                    hover
                  >
                    <TableCell>ENTREGADO EN SIAO</TableCell>
                    <TableCell>$ 4.257.616</TableCell>
                    <TableCell></TableCell>
                  </TableRow>
                  <TableRow
                    hover
                  >
                    <TableCell>ES PROGRAMADO</TableCell>
                    <TableCell>$ 15.276.646</TableCell>
                    <TableCell></TableCell>
                  </TableRow>
                  <TableRow
                    hover
                  >
                    <TableCell>PENDIENTE POR PROGRAMAR</TableCell>
                    <TableCell>$ 179.077.800</TableCell>
                    <TableCell></TableCell>
                  </TableRow>
                  <TableRow
                    hover
                  >
                    <TableCell>PENDIENTE CLIENTE/CANCELADA</TableCell>
                    <TableCell>$ 2.843.111</TableCell>
                    <TableCell></TableCell>
                  </TableRow>
                {/* ))} */}
              </TableBody>
            </Table>
          
            </TabPanel>
            <TabPanel value={value} index={1}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>
                    Backlog Instalaciones Negocios
                  </TableCell>
                  <TableCell>Compromiso MRC</TableCell>
                  <TableCell>Total</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
              <TableRow
                    hover
                  >
                    <TableCell>COMPROMISO</TableCell>
                    <TableCell>$ 29.480.216</TableCell>
                    <TableCell></TableCell>
                  </TableRow>
                  <TableRow
                    hover
                  >
                    <TableCell>ENTREGADO EN SIAO</TableCell>
                    <TableCell>$ 4.925.350</TableCell>
                    <TableCell></TableCell>
                  </TableRow>
                  <TableRow
                    hover
                  >
                    <TableCell>ES PROGRAMADO</TableCell>
                    <TableCell>$ -</TableCell>
                    <TableCell></TableCell>
                  </TableRow>
                  <TableRow
                    hover
                  >
                    <TableCell>PENDIENTE POR PROGRAMAR</TableCell>
                    <TableCell>$ 88.671.496</TableCell>
                    <TableCell></TableCell>
                  </TableRow>
                  <TableRow
                    hover
                  >
                    <TableCell>PENDIENTE CLIENTE/CANCELADA</TableCell>
                    <TableCell>$ -</TableCell>
                    <TableCell></TableCell>
                  </TableRow>

              </TableBody>
            </Table>
            </TabPanel>
          </div>
        </PerfectScrollbar>
      </CardContent>
    </Card>
  );
};

LatestProjects.propTypes = {
  className: PropTypes.string
};

export default LatestProjects;
