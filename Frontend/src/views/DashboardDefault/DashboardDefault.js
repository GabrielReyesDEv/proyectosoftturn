import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';

import { Page } from 'components';
import {
  Header,
  OtpsManagement,
  Delivered,
  Summary,
  CompletionPercentage,
  OutlookReview,
  BacklogReview,
  TeamManagement,
  Activities,
  Tasks,
  Commitment
} from './components';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  container: {
    padding: theme.spacing(2)
  }
}));

const DashboardDefault = () => {
  const classes = useStyles();
  const [data, setData] = useState({});

  useEffect(() => {
    let mounted = true;

    const fetchConversations = async () => {
      // const response = await api.getDashboardClientCounters();
      const response = []
      if (mounted) {
        setData(response?.data?.data[0]);
      }
    };

    fetchConversations();

    return () => {
      mounted = false;
    };
  }, []);

  return (
    <Page
      className={classes.root}
      title="Tablero Cliente"
    >
      <Header />
      <Grid
        className={classes.container}
        container
        spacing={3}
      >
        <Grid
          item
          lg={3}
          sm={6}
          xs={12}
        >
          <Commitment data={data?.total_programado?.length > 0 ? data.total_programado[0] : { count: 0, value: 0 }} />
        </Grid>
        <Grid
          item
          lg={3}
          sm={6}
          xs={12}
        >
          <Delivered data={data?.total_entregado_en_siao?.length > 0 ? data.total_entregado_en_siao[0] : { count: 0, value: 0 }} />
        </Grid>
        <Grid
          item
          lg={3}
          sm={6}
          xs={12}
        >
          <Activities data={data}/>
        </Grid>
        <Grid
          item
          lg={3}
          sm={6}
          xs={12}
        >
          <CompletionPercentage data={data} />
        </Grid>
        <Grid
            item
            lg={6}
            xs={12}
          >
          <Summary />
        </Grid>
        <Grid
            item
            lg={3}
            xs={12}
          >
          <OutlookReview />
        </Grid>
        <Grid
            item
            lg={3}
            xs={12}
          >
          <BacklogReview />
        </Grid>
        <Grid
          container
          xs={12}
        >
          <Grid
          item
          lg={5}
          xs={12}
          className={classes.container}
          >
            <OtpsManagement />
          </Grid>
          <Grid
          lg={4}
          xs={12}
          className={classes.container}
          >
            <Tasks />
          </Grid>
          <Grid
          item
          lg={3}
          xs={12}
          className={classes.container}
          >
            <TeamManagement />
          </Grid>
        </Grid>
        
      </Grid>
    </Page>
  );
};

export default DashboardDefault;
