import React, { useState, useEffect } from 'react';
import Cookies from 'js-cookie';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Divider } from '@material-ui/core';
import queryString from 'query-string';
import io from "socket.io-client";

import {
  ConversationToolbar,
  ConversationMessages,
  ConversationForm
} from './components';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: theme.palette.white
  }
}));

let socket;

const ConversationDetails = props => {
  const { conversation, className, ...rest } = props;

  const classes = useStyles();

  const [name, setName] = useState('');
  // const [room, setRoom] = useState('');
  // const [users, setUsers] = useState('');
  const [message, setMessage] = useState('');
  const [messages, setMessages] = useState([]);
  const ENDPOINT = 'http://localhost:6000/';
  const userId = Cookies.get('identification');

  useEffect(() => {
    const {  room } = queryString.parse(window.location.search);
    const name = userId
    socket = io(ENDPOINT);

    // setRoom(room);
    setName(userId)

    socket.emit('join', { name, room }, (error) => {
      if(error) {
        alert(error);
      }
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  
  useEffect(() => {
    socket.on('message', message => {
      setMessages(msgs => [ ...msgs, message ]);
    });
    
    socket.on("roomData", ({ users }) => {
      // setUsers(users);
    });
  }, []);

  const sendMessage = (event) => {
    event.preventDefault();

    if(message) {
      socket.emit('sendMessage', message, () => setMessage(''));
    }
  }

  return (
    <div
      {...rest}
      className={clsx(classes.root, className)}
    >
      <ConversationToolbar conversation={conversation} />
      <Divider />
      <ConversationMessages messages={messages} name={name}/>
      <Divider />
      <ConversationForm message={message} setMessage={setMessage} sendMessage={sendMessage} />
    </div>
  );
};

ConversationDetails.propTypes = {
  className: PropTypes.string,
  conversation: PropTypes.object.isRequired
};

export default ConversationDetails;
