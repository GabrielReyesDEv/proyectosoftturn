import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Button,
  Grid,
  Menu,
  MenuItem,
  ListItemText,
  Typography
} from '@material-ui/core';
import { ToggleButtonGroup, ToggleButton } from '@material-ui/lab';
import ViewModuleIcon from '@material-ui/icons/ViewModule';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import { Paginate } from 'components';
import { ProjectCard } from './components';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%'
  },
  header: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    marginBottom: theme.spacing(2)
  },
  title: {
    position: 'relative',
    '&:after': {
      position: 'absolute',
      bottom: -8,
      left: 0,
      content: '" "',
      height: 3,
      width: 48,
      backgroundColor: theme.palette.primary.main
    }
  },
  actions: {
    display: 'flex',
    alignItems: 'center'
  },
  sortButton: {
    textTransform: 'none',
    letterSpacing: 0,
    marginRight: theme.spacing(2)
  },
  paginate: {
    marginTop: theme.spacing(3),
    display: 'flex',
    justifyContent: 'center'
  }
}));

const Projects = props => {
  const { idProject, project, className, ...rest } = props;
  const classes = useStyles();
  const sortRef = useRef(null);
  const [openSort, setOpenSort] = useState(false);
  const [selectedSort, setSelectedSort] = useState('Most popular');
  const [mode, setMode] = useState('grid');
  const [activities, setActivities] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage] = useState(9);
  const [totalReg, setTotalReg] = useState(0);
  const columnsNeeded = {
    'cliente': 1,
    'author': 1,
    'ProjectId': 1,
    'activityId': 1,
    'producto': 1,
    'fechaCreacionOtHija': 1,
    'fechaCompromiso': 1,
    'status': 1,
    'resolucion15': 1,
    'createAt': 1,
    'tipo': 1,
    'updatedAt': 1,
    'cargoMensual': 1,
    'resolucion37': 1,
    'familia': 1,
  }

  // let userOnyx = '';
  // if (Cookies.get('roles') !== undefined) {
  //   const findUser = JSON.parse(Cookies.get('roles')).find((x) => x.role === 'Ingeniero Gestion');
  //   if (findUser !== '') { userOnyx = findUser.userOnyx; }
  // }

  const fetchProjects = async (body) => {
    await axios
      .post(
        '/activities/pagination',
        body,
        getAuthorization(),
      )
      .then((response) => {
        setActivities(response.data.data)
        setTotalReg(response.data.total);
        setPage(response.data.page);
      });
  };

  useEffect(() => {
    fetchProjects({
      limit: rowsPerPage,
      page: 1,
      columnsNeeded,
      'eq': [{ 'field': 'projectId', 'values': [idProject] }],
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleChangePage = (e) => {
    let body = {
      'limit': rowsPerPage,
      'page': (e.selected + 1),
      columnsNeeded,
      'eq': [{ 'field': 'projectId', 'values': [idProject] }],
    };
    fetchProjects(body);
  };

  const handleSortOpen = () => {
    setOpenSort(true);
  };

  const handleSortClose = () => {
    setOpenSort(false);
  };

  const handleSortSelect = value => {
    setSelectedSort(value);
    setOpenSort(false);
  };

  const handleModeChange = (event, value) => {
    setMode(value);
  };

  return (
    <div
      {...rest}
      className={clsx(classes.root, className)}
    >
      <div className={classes.header}>
        <Typography
          className={classes.title}
          variant="h5"
        >
          Mostrando {totalReg} Actividades encontradas. Página {page} de{' '}
          {Math.ceil(totalReg / rowsPerPage)}
        </Typography>
        <div className={classes.actions}>
          <Button
            className={classes.sortButton}
            onClick={handleSortOpen}
            ref={sortRef}
          >
            {selectedSort}
            <ArrowDropDownIcon />
          </Button>
          <ToggleButtonGroup
            exclusive
            onChange={handleModeChange}
            size="small"
            value={mode}
          >
            <ToggleButton value="grid">
              <ViewModuleIcon />
            </ToggleButton>
          </ToggleButtonGroup>
        </div>
      </div>
      <Grid
        container
        spacing={3}
      >
        {activities.map(activity => (
          <Grid
            item
            key={activity.id}
            md={mode === 'grid' ? 4 : 12}
            sm={mode === 'grid' ? 6 : 12}
            xs={12}
          >
            <ProjectCard
              activity={activity}
              project={project}
            />
          </Grid>
        ))}
      </Grid>
      <div className={classes.paginate}>
        <Paginate
          onPageChange={handleChangePage}
          pageCount={Math.ceil(totalReg / rowsPerPage)}
        />
      </div>
      <Menu
        anchorEl={sortRef.current}
        className={classes.menu}
        onClose={handleSortClose}
        open={openSort}
      >
        {['Most recent', 'Popular', 'Price high', 'Price low', 'On sale'].map(
          option => (
            <MenuItem
              className={classes.menuItem}
              key={option}
              onClick={() => handleSortSelect(option)}
            >
              <ListItemText primary={option} />
            </MenuItem>
          )
        )}
      </Menu>
    </div>
  );
};

Projects.propTypes = {
  className: PropTypes.string,
  idProject: PropTypes.string,
  project: PropTypes.object,
};

export default Projects;
