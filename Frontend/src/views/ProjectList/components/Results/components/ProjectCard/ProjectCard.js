import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Avatar,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  Link,
  Typography,
  colors
} from '@material-ui/core';
import duration from 'utils/duration';
import getInitials from 'utils/getInitials';
import { Label } from 'components';

const useStyles = makeStyles(theme => ({
  root: {},
  header: {
    paddingBottom: 0
  },
  content: {
    padding: 0,
    '&:last-child': {
      paddingBottom: 0
    }
  },
  description: {
    padding: theme.spacing(2, 3, 1, 3)
  },
  tags: {
    padding: theme.spacing(0, 3, 1, 3),
    '& > * + *': {
      marginLeft: theme.spacing(1)
    }
  },
  learnMoreButton: {
    marginLeft: theme.spacing(2)
  },
  likedButton: {
    color: colors.red[600]
  },
  shareButton: {
    marginLeft: theme.spacing(1)
  },
  details: {
    padding: theme.spacing(1, 3)
  }
}));

const ProjectCard = props => {
  const { activity, project, className, ...rest } = props;
  const classes = useStyles();

  const statusColors = {
    '1- Generada': colors.orange[600],
    '2- En ejecución': colors.red[600],
    '3- Terminada': colors.green[600],
    'Cancelada': colors.grey[600]
  };

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader
        avatar={
          <Avatar
            alt="Author"
            src={activity.familia}
          >
            {getInitials(activity.author)}
          </Avatar>
        }
        className={classes.header}
        disableTypography
        subheader={
          <Typography variant="body2">
            {activity.activityId}{' '} por{' '}
            <Link
              color="textPrimary"
              component={RouterLink}
              to={`/activities/${activity.activityId}/overview`}
              variant="h6"
            >
              {activity.author?.split('#')[0]}
            </Link>{' '}
            Actualizado: {duration(activity.updatedAt, activity.updatedAt)}
          </Typography>
        }
        title={
          <Link
            color="textPrimary"
            component={RouterLink}
            to={`/activities/${activity.activityId}/overview`}
            variant="h5"
          >
            {activity.tipo}
          </Link>
        }
      />
      <CardContent className={classes.content}>
        <div className={classes.description}>
          <Typography
            colo="textSecondary"
            variant="subtitle2"
          >
            {activity.resolucion37}
          </Typography>
        </div>
        <div className={classes.tags}>
          <Label
            color={statusColors[activity.status]}
            key={activity.status}
          >
            {activity.status}
          </Label>
        </div>
        <Divider />
        <div className={classes.details}>
          <Grid
            alignItems="center"
            container
            justify="space-between"
            spacing={3}
          >
            <Grid item>
              <Typography variant="h5">{project.city}</Typography>
              <Typography variant="body2">Ubicación</Typography>
            </Grid>
            <Grid item>
              <Button
                className={classes.learnMoreButton}
                component={RouterLink}
                size="small"
                to={`/activities/${activity.activityId}/overview`}
              >
                Mostrar más
              </Button>
            </Grid>
          </Grid>
        </div>
      </CardContent>
    </Card>
  );
};

ProjectCard.propTypes = {
  activity: PropTypes.object.isRequired,
  className: PropTypes.string,
  project: PropTypes.object.isRequired
};

export default ProjectCard;
