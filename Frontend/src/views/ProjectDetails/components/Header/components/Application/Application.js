import React, { useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Avatar,
  Button,
  Dialog,
  TextField,
  Typography,
  colors,
  Checkbox,
  FormControlLabel,
  FormLabel,
  FormControl,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Grid,
} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import getInitials from 'utils/getInitials';
import moment from 'moment';

const useStyles = makeStyles(theme => ({
  root: {
    width: 960
  },
  header: {
    padding: theme.spacing(3),
    maxWidth: 720,
    margin: '0 auto'
  },
  content: {
    padding: theme.spacing(0, 2),
    maxWidth: 720,
    margin: '0 auto'
  },
  helperText: {
    textAlign: 'right',
    marginRight: 0
  },
  author: {
    margin: theme.spacing(4, 0),
    display: 'flex'
  },
  avatar: {
    marginRight: theme.spacing(2)
  },
  actions: {
    backgroundColor: colors.grey[100],
    padding: theme.spacing(2),
    display: 'flex',
    justifyContent: 'center'
  },
  applyButton: {
    color: theme.palette.white,
    backgroundColor: colors.green[600],
    '&:hover': {
      backgroundColor: colors.green[900]
    }
  },
  formGroup: {
    marginBottom: theme.spacing(3)
  },
  fieldGroup: {
    display: 'flex',
    alignItems: 'center'
  },
  fieldGroupSpaced: {
    display: 'flex',
    alignItems: 'center',
    '& > * + *': {
      marginLeft: theme.spacing(2)
    }
  },
  dateField: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  },
}));

const Application = props => {
  const { create, valuesCheck, areas, handleChange, project, projectInfo, open, onClose, onApply, className, ...rest } = props;
  const [values, setValues] = useState({ ...project });
  const [defaultSel, setDefault] = useState({
    type:{
      default: { title: values.type, value: values.type }
    },
    status:{
      default: { title: values.status, value: values.status }
    },
    priority:{
      default: { title: `${values.priority}`, value: parseInt(values.priority) }
    }
  });

  const classes = useStyles();

  projectInfo(values);
  const handleFieldChange = (event, field, value, type = '') => {
    event.persist && event.persist();
    setValues(values => ({
      ...values,
      [field]: value
    }));
    if (type === 'autoComplete') {
      setDefault(defaultSel => ({
        ...defaultSel,
        [field]: { title: `${value}`, value: value }
      }));
    }
  };

  return (
    <Dialog
      maxWidth="lg"
      onClose={onClose}
      open={open}
    >
      <div
        {...rest}
        className={clsx(classes.root, className)}
      >
        <div className={classes.header}>
          <Typography
            align="center"
            className={classes.title}
            gutterBottom
            variant="h3"
          >
            {`Actualizar Proyecto ${values.projectId}`}
          </Typography>
          <Typography
            align="center"
            className={classes.subtitle}
            variant="subtitle2"
          >
            Los siguientes campos pueden ser actualizados 
          </Typography>
        </div>
        <div className={classes.content}>
          <div className={classes.formGroup}>
            <div className={classes.fieldGroupSpaced}>
              <TextField
                fullWidth
                label="Nombre Cliente"
                name="client"
                onChange={event =>
                  handleFieldChange(event, 'client', event.target.value !== '' ? event.target.value : undefined)
                }
                value={values.client}
                variant="outlined"
              />
              <TextField
                className={classes.dateField}
                fullWidth
                label="Producto"
                name="product"
                onChange={event =>
                  handleFieldChange(event, 'product', event.target.value !== '' ? event.target.value : undefined)
                }
                value={values.product}
                variant="outlined"
              />
            </div>
          </div>
        </div>
        <div className={classes.content}>
          <div className={classes.formGroup}>
            <div className={classes.fieldGroupSpaced}>
              <Autocomplete
                fullWidth
                getOptionLabel={(option) => (option.title ? option.title : values.type)}
                id="type"
                // eslint-disable-next-line no-use-before-define
                name="type"
                onChange={event =>(
                  handleFieldChange(event, 'type', event.target.innerText, 'autoComplete')
                )
                }
                options={optionsType}
                renderInput={(params) => (
                  // eslint-disable-next-line react/jsx-props-no-spreading
                  <TextField
                    {...params}
                    error={create && (values.type === '' || values.type === undefined)}
                    fullWidth
                    helperText={create && (values.type === '' || values.type === undefined) ? '*Campo Requerido' : false}
                    label="Tipo de Proyecto"
                    required
                    variant="outlined"
                  />
                )}
                value={defaultSel.type}
              />
              <Autocomplete
                fullWidth
                getOptionLabel={(option) => (option.title ? option.title : values.status)}
                id="status"
                // eslint-disable-next-line no-use-before-define
                name="status"
                onChange={event => (
                  handleFieldChange(event, 'status', event.target.innerText, 'autoComplete')
                )
                }
                options={optionsStatus}
                renderInput={(params) => (
                  // eslint-disable-next-line react/jsx-props-no-spreading
                  <TextField
                    {...params}
                    error={create && (values.status === '' || values.status === undefined)}
                    fullWidth
                    helperText={create && (values.status === ''  || values.status === undefined) ? '*Campo Requerido' : false}
                    label="Estado del Proyecto"
                    required
                    variant="outlined"
                  />
                )}
                value={defaultSel.status}
              />
              <Autocomplete
                fullWidth
                getOptionLabel={(option) => (option.title ? option.title : `${values.priority}`)}
                id="priority"
                // eslint-disable-next-line no-use-before-define
                name="priority"
                onChange={event => (
                  handleFieldChange(event, 'priority', (isNaN(parseInt(event.target.innerText)) ? '' : parseInt(event.target.innerText)), 'autoComplete')
                )
                }
                options={optionsPriority}
                renderInput={(params) => (
                  // eslint-disable-next-line react/jsx-props-no-spreading
                  <TextField
                    {...params}
                    error={create && (values.priority === '' || values.priority === undefined)}
                    fullWidth
                    helperText={create && (values.priority === '' || values.priority === undefined) ? '*Campo Requerido' : false}
                    label="Prioridad del Proyecto"
                    required
                    variant="outlined"
                  />
                )}
                required
                type="number"
                value={defaultSel.priority}
              />
            </div>
          </div>
        </div>
        <div className={classes.content}>
          <div className={classes.formGroup}>
            <div className={classes.fieldGroup}>
              <TextField
                className={classes.dateField}
                defaultValue={moment(values.programmingDate, 'YYYY-MM-DD HH:mm').format('YYYY-MM-DD')}
                fullWidth
                id="programmingDate"
                InputLabelProps={{
                  shrink: true,
                }}
                label="Fecha de Programación"
                name="programmingDate"
                onChange={event =>
                  handleFieldChange(event, 'programmingDate', moment(event.target.value, 'YYYY-MM-DD').format('YYYY-MM-DD HH:mm'))
                }
                required
                type="date"
                // value={values && values.programmingDate && moment(values.programmingDate, 'YYYY-MM-DD HH:mm').format('MM/DD/YYYY')}
                variant="outlined"
              />
              <TextField
                className={classes.dateField}
                defaultValue={moment(values.finishDate, 'YYYY-MM-DD HH:mm').format('YYYY-MM-DD')}
                fullWidth
                InputLabelProps={{
                  shrink: true,
                }}
                label="Fecha de Compromiso"
                name="finishDate"
                onChange={event =>
                  handleFieldChange(event, 'finishDate', moment(event.target.value, 'YYYY-MM-DD').format('YYYY-MM-DD HH:mm'))
                }
                required
                type="date"
                variant="outlined"
              />
            </div>
          </div>
        </div>
        <div className={classes.content}>
          <div className={classes.formGroup}>
            <div className={classes.fieldGroup}>
              <TextField
                className={classes.dateField}
                fullWidth
                label="Ciudad"
                name="city"
                onChange={event =>
                  handleFieldChange(event, 'city', event.target.value !== '' ? event.target.value : undefined)
                }
                value={values.city}
                variant="outlined"
              />
              <TextField
                className={classes.dateField}
                fullWidth
                label="Departmento"
                name="department"
                onChange={event =>
                  handleFieldChange(event, 'department', event.target.value !== '' ? event.target.value : undefined)
                }
                value={values.department}
                variant="outlined"
              />
            </div>
          </div>
        </div>
        <div className={classes.content}>
          <div className={classes.formGroup}>
            <div className={classes.fieldGroup}>
              <TextField
                className={classes.dateField}
                fullWidth
                label="Direccion de origen"
                name="sourceAddress"
                onChange={event =>
                  handleFieldChange(event, 'sourceAddress', event.target.value !== '' ? event.target.value : undefined)
                }
                value={values.sourceAddress}
                variant="outlined"
              />
              <TextField
                className={classes.dateField}
                fullWidth
                label="Direccion de destino"
                name="destinationAddress"
                onChange={event =>
                  handleFieldChange(event, 'destinationAddress', event.target.value !== '' ? event.target.value : undefined)
                }
                value={values.destinationAddress}
                variant="outlined"
              />
            </div>
          </div>
        </div>
        <div className={classes.content}>
          <div className={classes.formGroup}>
            <div className={classes.fieldGroupSpaced}>
              <TextField
                fullWidth
                label="Resolucion 1"
                name="resolucion1"
                onChange={event =>
                  handleFieldChange(event, 'resolucion1', event.target.value !== '' ? event.target.value : undefined)
                }
                value={values.resolucion1}
                variant="outlined"
              />
              <TextField
                className={classes.dateField}
                fullWidth
                label="Resolucion 2"
                name="resolucion2"
                onChange={event =>
                  handleFieldChange(event, 'resolucion2', event.target.value !== '' ? event.target.value : undefined)
                }
                value={values.resolucion2}
                variant="outlined"
              />
            </div>
          </div>
        </div>
        <div className={classes.content}>
          <div className={classes.formGroup}>
            <div className={classes.fieldGroupSpaced}>
              <TextField
                fullWidth
                label="Resolucion 3"
                name="resolucion3"
                onChange={event =>
                  handleFieldChange(event, 'resolucion3', event.target.value !== '' ? event.target.value : undefined)
                }
                value={values.resolucion3}
                variant="outlined"
              />
              <TextField
                className={classes.dateField}
                fullWidth
                label="Resolucion 4"
                name="resolucion4"
                onChange={event =>
                  handleFieldChange(event, 'resolucion4', event.target.value !== '' ? event.target.value : undefined)
                }
                value={values.resolucion4}
                variant="outlined"
              />
            </div>
          </div>
        </div>
        <div className={classes.content}>
          <TextField
            autoFocus
            className={classes.textField}
            // eslint-disable-next-line react/jsx-sort-props
            FormHelperTextProps={{ classes: { root: classes.helperText } }}
            fullWidth
            helperText={`${200 - (values.description ? values.description.length : 0)} characters left`}
            label="Descripción"
            multiline
            onChange={event =>
              handleFieldChange(event, 'description', event.target.value)
            }
            placeholder="Agregue aqui una pequeña descripcion"
            rows={5}
            value={values.description}
            variant="outlined"
          />
          <div className={classes.author}>
            <Typography variant="h6">Disparar actividades:</Typography>
          </div>
          {/* <div className={classes.content}> */}
          <FormControl fullWidth>
            <FormLabel >Area:</FormLabel>
            {areas.length > 0 && areas.map((data) => {
              const type = data.name;
              return (
                <div>
                  <FormControlLabel
                    control={
                      <Checkbox 
                        checked={valuesCheck && valuesCheck.type}
                        color="primary"
                        name={type}
                        onChange={handleChange}
                      />}
                    fullWidth
                    label={type}
                  />
                  {valuesCheck[type] === true ? (
                    <Accordion fullWidth>
                      <AccordionSummary
                        expandIcon={<ExpandMoreIcon />}
                        fullWidth
                      >
                        <Typography
                          className={classes.heading}
                          fullWidth
                        >
                                Requerimientos:
                        </Typography>
                      </AccordionSummary>
                      <AccordionDetails fullWidth>
                        <Grid
                          container
                          fullWidth
                          id={`cont_${type}`}
                          spacing={4}
                        >
                          <Grid
                            item
                            md={6}
                            xs={12}
                          >
                            <TextField
                              fullWidth
                              label="Tipo de actividad"
                              name="OT_HIJA"
                              select
                              SelectProps={{ native: true }}
                              variant="outlined"
                            >
                              <option
                                value=""
                              />
                              {data.activities.map(option => (
                                <option
                                  key={option}
                                  value={option}
                                >
                                  {option}
                                </option>
                              ))}
                            </TextField>
                          </Grid>
                          <Grid
                            item
                            md={6}
                            xs={12}
                          >
                            <TextField
                              fullWidth
                              label="ID de la actividad"
                              name="ID_ORDEN_TRABAJO_HIJA"
                              required
                              type="number"
                              variant="outlined"
                            />
                          </Grid>
                        </Grid>
                      </AccordionDetails>
                    </Accordion>
                  ): ''} 
                </div>
              )
            })}

          </FormControl>
          {/* </div> */}
          <div className={classes.author}>
            <Typography variant="h6">{'Creado por: '}</Typography>
          </div>
          <div className={classes.author}>
            <Avatar
              alt="Author"
              className={classes.avatar}
              src={''}
            >
              {getInitials(values.author?.split('#')[0])}
            </Avatar>
            <div>
              <Typography variant="h3">{values.author?.split('#')[0]}</Typography>
              <Typography variant="subtitle2">{values.author?.split('#')[1]}</Typography>
            </div>
          </div>
        </div>
        <div className={classes.actions}>
          <Button
            className={classes.applyButton}
            onClick={() => onApply(values, areas)}
            variant="contained"
          >
            Editar Proyecto
          </Button>
        </div>
      </div>
    </Dialog>
  );
};

const optionsType = [
  { title: 'Adicion / Retiro de numeros', value: 'Adicion / Retiro de numeros' },
  { title: 'Adicion de cuentas de correo', value: 'Adicion de cuentas de correo' },
  { title: 'Adición de Equipos', value: '$Adición de Equipos' },
  { title: 'Cambio de Equipos', value: 'Cambio de Equipos' },
  { title: 'Cambio de Servicio', value: 'Cambio de Servicio' },
  { title: 'Cambio de Velocidad(Ampliación - Reducción)', value: 'Cambio de Velocidad(Ampliación - Reducción)' },
  { title: 'Cambio Plan Pymes', value: 'Cambio Plan Pymes' },
  { title: 'Cambio Tipo Acceso, Servicio y Ampliación', value: 'Cambio Tipo Acceso, Servicio y Ampliación' },
  { title: 'Desconexión Temporal', value: 'Desconexión Temporal' },
  { title: 'Desinstalación Cliente Corporativo', value: 'Desinstalación Cliente Corporativo' },
  { title: 'Instalación Grandes Proyectos', value: 'Instalación Grandes Proyectos' },
  { title: 'Instalación Proyectos Especiales', value: 'Instalación Proyectos Especiales' },
  { title: 'Instalación Pymes', value: 'Instalación Pymes' },
  { title: 'Instalación Servicios Datacenter', value: 'Instalación Servicios Datacenter' },
  { title: 'Instalación Servicios Estándar', value: 'Instalación Servicios Estándar' },
  { title: 'Instalación Servicios Transmisión', value: 'Instalación Servicios Transmisión' },
  { title: 'Novedades soluciones administradas', value: 'Novedades soluciones administradas' },
  { title: 'Nueva Instalación', value: 'Nueva Instalación' },
  { title: 'Reconexión Cliente', value: 'Reconexión Cliente' },
  { title: 'Retiro de Equipos', value: 'Retiro de Equipos' },
  { title: 'Suspensión por Cartera', value: 'Suspensión por Cartera' },
  { title: 'Suspensión por Cartera Pymes', value: 'Suspensión por Cartera Pymes' },
  { title: 'Traslado Externo', value: 'Traslado Externo' },
  { title: 'Traslado Interno', value: 'Traslado Interno' },
  { title: 'Cambio de Medio', value: 'Cambio de Medio' },
  { title: 'CambioTipo de Acceso', value: 'CambioTipo de Acceso' },
  { title: 'Caso de Seguimiento', value: 'Caso de Seguimiento' },
  { title: 'Factibilidad', value: 'Factibilidad' },
  { title: 'Instalación Demo Implementación', value: 'Instalación Demo Implementación' },
  { title: 'Plan Accion', value: 'Plan Accion' },
  { title: 'Punto Cental (Nueva)', value: 'Punto Cental (Nueva)' },
  { title: 'Punto Central', value: 'Punto Central' },
  { title: 'Punto Central Pymes', value: 'Punto Central Pymes' },
  { title: 'RED - Crecimiento de Red', value: 'RED - Crecimiento de Red' },
  { title: 'RED - Expansion de Red', value: 'RED - Expansion de Red' },
];

const optionsStatus = [
  { title: '1- Generada', value: '1- Generada' },
  { title: '2- En ejecución', value: '2- En ejecución' },
  { title: '3- Terminada', value: '3- Terminada' },
  { title: 'Alistamiento', value: 'Alistamiento' },
  { title: 'Caja OB', value: 'Caja OB' },
  { title: 'Detalle Solución', value: 'Detalle Solución' },
  { title: 'Diseño', value: 'Diseño' },
  { title: 'Ejecución ruta', value: 'Ejecución ruta' },
  { title: 'Entrega', value: 'Entrega' },
  { title: 'Factibilidad', value: 'Factibilidad' },
  { title: 'Instalación', value: 'Instalación' },
  { title: 'Liberación Recursos', value: 'Liberación Recursos' },
  { title: 'Pendiente Cliente', value: 'Pendiente Cliente' },
  { title: 'Pendiente Telmex', value: 'Pendiente Telmex' },
];

const optionsPriority = [
  {title: '0', value: 0},
  {title: '1', value: 1},
  {title: '2', value: 2},
  {title: '3', value: 3},
  {title: '4', value: 4},
  {title: '5', value: 5},
  {title: '6', value: 6},
  {title: '7', value: 7},
  {title: '8', value: 8},
  {title: '9', value: 9},  
]

Application.propTypes = {
  areas: PropTypes.array,
  author: PropTypes.object.isRequired,
  className: PropTypes.string,
  create: PropTypes.bool,
  handleChange: PropTypes.func,
  onApply: PropTypes.func,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired,
  project: PropTypes.object,
  projectInfo: PropTypes.object,
  valuesCheck: PropTypes.object,
};

export default Application;
