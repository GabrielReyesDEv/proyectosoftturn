import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { Typography, Grid, Button, colors } from '@material-ui/core';
import moment from 'moment';
import { Label } from 'components';
import { Application } from './components';
import { useSnackbar } from 'notistack';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';
import Cookies from 'js-cookie';

const useStyles = makeStyles(theme => ({
  root: {},
  label: {
    marginTop: theme.spacing(1)
  },
  shareButton: {
    marginRight: theme.spacing(2)
  },
  shareIcon: {
    marginRight: theme.spacing(1)
  },
  applyButton: {
    color: theme.palette.white,
    backgroundColor: colors.green[600],
    '&:hover': {
      backgroundColor: colors.green[900]
    }
  }
}));

const Header = props => {
  const [crear, setCrear] = useState(false);
  const [projectInfo, setProjectInfo] = useState({});
  const userOnyx = Cookies.get('userOnyx');
  const firstName = Cookies.get('firstName');
  const lastName = Cookies.get('lastName');
  const { project, className, ...rest } = props;
  const classes = useStyles();

  const [openApplication, setOpenApplication] = useState(false);
  // eslint-disable-next-line no-unused-vars
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const handleChangeProject = (values) => {
    setProjectInfo(values);
  }

  const handleApplicationOpen = () => {
    setOpenApplication(true);
  };

  const handleApplicationClose = () => {
    setOpenApplication(false);
  };

  const validacionErrores = (camposReq, info) => {
    let errorVacios = 0;
    for (let index = 0; index < camposReq.length; index++) {
      const aux = Object.fromEntries(Object.entries(info).filter(([key]) => key === camposReq[index]));
      if (Object.values(aux)[0] === '' || Object.values(aux)[0] === undefined) {
        errorVacios = 1;
        index = camposReq.length
      }
    }
    return errorVacios;
  }

  const fetchQuery = async (data, url, message = '') => {
    let result = 0;
    await axios
      .put(
        url,
        data,
        getAuthorization(),
      )
      .then((response) => {
        result = response.data.data.length;
        if (message !== '') {
          enqueueSnackbar(message.msg, {
            variant: message.variant,
          });
        }
      })
      .catch((err) => {
        enqueueSnackbar(`error: ${err}`, {
          variant: 'error',
        });
        result = -1;
      })
    return result; 
  };

  React.useEffect(() => {

    axios.post('/areas/pagination', { 'eq': [{ 'field': 'group', 'values': ['BO']}] }, getAuthorization()).then(response => {    
      setAreas(response.data.data);
    });
  }, []);

  const [valuesCheck, setValuesCheck] = React.useState({});
  const [areas, setAreas] = React.useState([]);

  const handleChange = (e) => {
    setValuesCheck({
      ...valuesCheck,
      [e.target.name]: e.target.checked
    });
  };

  const handleEditButton = async() => {
    // parseInt(idValue)
    const array = []
    for (let key in valuesCheck) {
      if ( valuesCheck[key] === true ) {
        const data = areas.find((val) => val.name === key)
        if (data) {
          const user = areas.filter((data) => data.name === key);
          const divElement = await document.getElementById(`cont_${key}`).querySelectorAll('input, select')
          const typeValue = divElement[0].value;
          console.log('typeValue'. typeValue);
          const idValue = divElement[1].value;
          let info = {
            OT_HIJA: typeValue,
            ID_ORDEN_TRABAJO_HIJA: idValue,
            FECHA_CREACION_OT_HIJA: moment().format('YYYY-MM-DD HH:mm'),
            ESTADO_ORDEN_TRABAJO_HIJA: '1- Generada',
            USUARIO_ASIGNADO4: user[0]?.userOnyx ? user[0].userOnyx : `${firstName} ${lastName}`,
            RESOLUCION_15: 'SIN RESOLUCION1',
            RESOLUCION_26: 'SIN RESOLUCION2',
            RESOLUCION_37: 'SIN RESOLUCION3',
            RESOLUCION_48: 'SIN RESOLUCION4',
            FEC_ACTUALIZACION_ONYX_HIJA: moment().format('YYYY-MM-DD HH:mm'),
          }
          const projectObject = { projectId: 'NRO_OT_ONYX', 
            author: 'USUARIO_ASIGNADO', 
            segment: 'SEGMENTO', 
            group: 'GRUPO', 
            family: 'FAMILIA', 
            product: 'PRODUCTO', 
            description: 'DESCRIPCION', 
            resolucion1: 'RESOLUCION_1', 
            resolucion2: 'RESOLUCION_2', 
            resolucion3: 'RESOLUCION_3', 
            resolucion4: 'RESOLUCION_4', 
            incidentCity: 'CIUDAD_INCIDENTE', 
            incidentCity3: 'CIUDAD_INCIDENTE3', 
            client: 'NOMBRE_CLIENTE', 
            orderType: 'ORDEN_TRABAJO', 
            service: 'SERVICIO', 
            serviceCode: 'ID_ENLACE', 
            status: 'ESTADO_ORDEN_TRABAJO', 
            createdAt: 'FECHA_CREACION', 
            incidentDate: 'TIEMPO_INCIDENTE', 
            commitmentDate: 'FECHA_COMPROMISO', 
            programmingDate: 'FECHA_PROGRAMACION', 
            city: 'CIUDAD', 
            department: 'DEPARTAMENTO', 
            destinationAddress: 'DIRECCION_DESTINO', 
            sourceAddress: 'DIRECCION_ORIGEN', 
            finishDate: 'FECHA_REALIZACION', 
            mrc: 'MONTO_MONEDA_LOCAL_CARGO_MENSUAL'
          }
          for (let key in projectInfo) {
            info[projectObject[key]] = projectInfo[key]
          }

          info.USUARIO_ASIGNADO = userOnyx;
          // info.NRO_OT_ONYX = parseInt(info.NRO_OT_ONYX);
          delete info.undefined

          array.push(info)
        }
      }
    }

    console.log('array final', array);


    setCrear(true);
    const idObject = projectInfo._id;
    const camposReqProject = [
      'projectId',
      'status',
      'priority',
      'type',
    ];
    let errorVacios = validacionErrores(camposReqProject, projectInfo);
    if (errorVacios === 0) {
      let url = `/projects/${idObject}`;
      delete projectInfo._id;
      const finalProject = {
        ...projectInfo, 
        field: '_id',
        updatedAt: moment().format('YYYY-MM-DD HH:mm'),
      }
      await fetchQuery(
        finalProject, 
        url, 
        {
          msg: `Proyecto ${projectInfo.projectId} Actualizada con exito`,
          variant: 'success',
        });

      try {
        await axios({
          url: 'activities/importUpdt',
          method: 'post',
          data: array,
        });
      
        enqueueSnackbar('Se crearon las actividades correctamente', {
          variant: 'success',
        });

        // window.location.href = `/projects/${projectInfo.projectId}/overview`;

    
      } catch (e) {
        enqueueSnackbar('Error de conexion', {
          variant: 'Error',
        });
        console.error('Failure!');
        console.error('Import error: ', e);
        console.error(e && e.response && e.response.status);
      }
    } else {
      enqueueSnackbar('Algunos campos estan vacios', {
        variant: 'error',
      });
    }
  };

  return (
    <div
      {...rest}
      className={clsx(classes.root, className)}
    >
      <Grid
        alignItems="flex-end"
        container
        justify="space-between"
        spacing={3}
      >
        <Grid item>
          <Typography
            component="h2"
            gutterBottom
            variant="overline"
          >
            Explorar Proyectos
          </Typography>
          <Typography
            component="h1"
            gutterBottom
            variant="h3"
          >
            {project.title}
          </Typography>
          <Label
            className={classes.label}
            color={colors.green[600]}
            variant="outlined"
          >
            Activo
          </Label>
        </Grid>
        <Grid item>
          <Button
            className={classes.applyButton}
            onClick={handleApplicationOpen}
            variant="contained"
          >
            Editar
          </Button>
        </Grid>
      </Grid>
      {areas !== [] ? 
        <Application
          areas={areas}
          create={crear}
          handleChange={handleChange}
          onApply={handleEditButton}
          onClose={handleApplicationClose}
          open={openApplication}
          project={project}
          projectInfo={handleChangeProject}
          valuesCheck={valuesCheck}
        />
        : ''
      }

    </div>
  );
};

Header.propTypes = {
  className: PropTypes.string,
  project: PropTypes.object.isRequired
};

Header.defaultProps = {};

export default Header;
