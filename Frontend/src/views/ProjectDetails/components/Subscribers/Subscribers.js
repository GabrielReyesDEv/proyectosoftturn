import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';

import { Results } from '../../../ProjectList/components';

const useStyles = makeStyles(theme => ({
  root: {
  }
}));

const Subscribers = props => {
  const { idProject, project, subscribers, className, ...rest } = props;

  const classes = useStyles();

  return (
    <Grid
      {...rest}
      className={clsx(classes.root, className)}
      container
    >

      <Results
        className={classes.results}
        idProject={idProject}
        project={project}
      />
    </Grid>
  );
};

Subscribers.propTypes = {
  className: PropTypes.string,
  idProject: PropTypes.string,
  project: PropTypes.object,
  subscribers: PropTypes.array.isRequired
};

export default Subscribers;
