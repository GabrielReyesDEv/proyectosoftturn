import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Avatar,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
  Tooltip,
  colors,
} from '@material-ui/core';
import moment from 'moment';
import getInitials from 'utils/getInitials';
import EmailIcon from '@material-ui/icons/Email';
import PhoneIcon from '@material-ui/icons/Phone';
import { Application } from './components';
import { useSnackbar } from 'notistack';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';

const useStyles = makeStyles(() => ({
  root: {},
  header: {
    paddingBottom: 0
  },
  content: {
    paddingTop: 0
  },
  actions: {
    backgroundColor: colors.grey[50]
  },
  manageButton: {
    width: '100%'
  }
}));

const Members = props => {
  const { members, project, className, ...rest } = props;
  const classes = useStyles();
  const [openApplication, setOpenApplication] = useState(false);
  const [membersInfo, setMembersInfo] = useState({});
  const [crear, setCrear] = useState(false);
  // eslint-disable-next-line no-unused-vars
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const handleChangeMembers = (values) => {
    setMembersInfo(values);
  }

  const handleApplicationOpen = () => {
    setOpenApplication(true);
  };

  const handleApplicationClose = () => {
    setOpenApplication(false);
  };

  const validacionErrores = (camposReq, info) => {
    let errorVacios = 0;
    for (let index = 0; index < camposReq.length; index++) {
      for (let j = 0; j < info.length; j++) {
        const auxCamposReq = camposReq[index];
        const aux = Object.fromEntries(Object.entries(info[j]).filter(([key]) => key === auxCamposReq));
        if (Object.values(aux)[0] === '' || Object.values(aux)[0] === undefined) {
          errorVacios = 1;
          index = camposReq.length;
          j = info.length;
        }
      }
    }
    return errorVacios;
  }

  const fetchQuery = async (data, url, message = '') => {
    let result = 0;
    await axios
      .put(
        url,
        data,
        getAuthorization(),
      )
      .then((response) => {
        result = response.data.data.length;
        if (message !== '') {
          enqueueSnackbar(message.msg, {
            variant: message.variant,
          });
        }
      })
      .catch((err) => {
        enqueueSnackbar(`error: ${err}`, {
          variant: 'error',
        });
        result = -1;
      })
    return result; 
  };

  const handleEditButton = async () => {
    setCrear(true);
    const idObject = project._id;
    const camposReqProject = [
      'name',
      'mail',
      'phone',
    ];
    let errorVacios = validacionErrores(camposReqProject, membersInfo.members);
    if (errorVacios === 0) {
      let url = `/projects/${idObject}`;
      const finalProject = {
        ...membersInfo, 
        field: '_id',
        updatedAt: moment().format('YYYY-MM-DD HH:mm'),
      }
      await fetchQuery(
        finalProject, 
        url, 
        {
          msg: `Proyecto ${project.projectId} Actualizado con exito`,
          variant: 'success',
        });
      window.location.href = `/projects/${project.projectId}/overview`;
    } else {
      enqueueSnackbar('Algunos campos estan vacios', {
        variant: 'error',
      });
    }
  }

  return (
    <div>
      <Card
        {...rest}
        className={clsx(classes.root, className)}
      >
        <CardHeader
          className={classes.header}
          title="Contactos del Proyecto"
          titleTypographyProps={{
            variant: 'overline'
          }}
        />
        <CardContent className={classes.content}>
          <List>
            {members.map(member => (
              <ListItem
                disableGutters
                key={member.id}
              >
                <ListItemAvatar>
                  <Avatar
                    alt="Author"
                    className={classes.avatar}
                    src={member.avatar}
                  >
                    {getInitials(member.name)}
                  </Avatar>
                </ListItemAvatar>
                <ListItemText
                  primary={member.name}
                  primaryTypographyProps={{ variant: 'h6' }}
                  secondary={member.bio}
                />
                <ListItemSecondaryAction>
                  <a href={`mailto:${member.mail}`}>
                    <Tooltip
                      aria-label="add"
                      title={member.mail}
                    >
                      <IconButton
                        aria-label="delete"
                        edge="end"
                      >
                        <EmailIcon />
                      </IconButton>
                    </Tooltip>
                  </a>
                  <a href={`tel:${member.phone}`}>
                    <Tooltip
                      aria-label="add"
                      title={member.phone}
                    >
                      <IconButton
                        aria-label="delete"
                        edge="end"
                      >
                        <PhoneIcon />
                      </IconButton>
                    </Tooltip>
                  </a>
                </ListItemSecondaryAction>
              </ListItem>
            ))}
          </List>
        </CardContent>
        <CardActions className={classes.actions}>
          <Button
            className={classes.manageButton}
            onClick={handleApplicationOpen}
          >
          Editar Contactos
          </Button>
        </CardActions>
      </Card>
      <Application
        create={crear}
        member={members}
        membersInfo={handleChangeMembers}
        onApply={handleEditButton}
        onClose={handleApplicationClose}
        open={openApplication}
      />
    </div>
  );
};

Members.propTypes = {
  className: PropTypes.string,
  members: PropTypes.array.isRequired,
  project: PropTypes.object,
};

export default Members;
