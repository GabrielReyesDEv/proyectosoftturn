import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import moment from 'moment';
import { makeStyles } from '@material-ui/styles';
import {
  Avatar,
  Card,
  CardContent,
  CardHeader,
  List,
  ListItem,
  Typography
} from '@material-ui/core';

import getInitials from 'utils/getInitials';
import { Label } from 'components';
import { colors } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {},
  header: {
    paddingBottom: 0
  },
  content: {
    paddingTop: 0
  },
  listItem: {
    padding: theme.spacing(2, 0),
    justifyContent: 'space-between'
  },
  labelChildren: {
    width:'100%',
    display: 'flex',
    wordWrap: 'break-word',
    flexFlow: 'column',
    padding: 7,
  },
}));

const Holder = props => {
  const { project, className, ...rest } = props;

  const classes = useStyles();

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader
        avatar={
          <Avatar
            alt="Author"
            className={classes.avatar}
            component={RouterLink}
            src={''}
            to="/profile/1/timeline"
          >
            {getInitials(project.author?.split('#')[0])}
          </Avatar>
        }
        className={classes.header}
        disableTypography
        subheader={
          <Typography
            component={RouterLink}
            to="/profile/1/timeline"
            variant="h5"
          >
            {project.author?.split('#')[0]}
          </Typography>
        }
        title={
          <Typography
            display="block"
            variant="overline"
          >
            Ingeniero Responsable
          </Typography>
        }
      />
      <CardContent className={classes.content}>
        <List>
          <ListItem
            className={classes.listItem}
            disableGutters
            divider
          >
            <Typography variant="subtitle2">Entrega</Typography>
            <Typography variant="h6">
              {moment(project.programmingDate).format('DD MMM YYYY')}
            </Typography>
          </ListItem>
          <ListItem
            className={classes.listItem}
            disableGutters
            divider
          >
            <Typography variant="subtitle2">Project ID</Typography>
            <Typography variant="h6">
              {project.projectId}
            </Typography>
          </ListItem>
          <ListItem
            className={classes.listItem}
            disableGutters
            divider
          >
            <div className={classes.labelChildren}>
              <div className={classes.labelChildren}>
                <Typography variant="subtitle2">Tipo de Servicio</Typography>
              </div>
              <div className={classes.labelChildren}>
                <Label color={colors.indigo[600]}>{project.type}</Label>
              </div>
            </div>
          </ListItem>
          <ListItem
            className={classes.listItem}
            disableGutters
            divider
          >
            <Typography variant="subtitle2">Última Actualización</Typography>
            <Typography variant="h6">
              {moment(project.updateAt).format('DD MMM YYYY')}
            </Typography>
          </ListItem>
        </List>
      </CardContent>
    </Card>
  );
};

Holder.propTypes = {
  className: PropTypes.string,
  project: PropTypes.object.isRequired
};

export default Holder;
