import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { Card, CardContent, Typography } from '@material-ui/core';

import { Markdown } from 'components';

const useStyles = makeStyles(() => ({
  root: {},
  detailProject: {
    marginTop: '18px'
  }
}));

const Details = props => {
  const { project, className, ...rest } = props;
  const classes = useStyles();

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardContent>
        <Markdown source={project.familia} />
        {project.grupo && (
          <>
            <Typography variant="h4">Grupo:</Typography>
            <Typography variant="body1">
              {project.grupo}
            </Typography>
          </>
        )}
        {project.mrc && (
          <>
            <Typography
              className={classes.detailProject}
              variant="h4"
            >Costo:</Typography>
            <Typography variant="body1">
              {`${project.mrc} $`}
            </Typography>
          </>
        )}
        {project.departament && (
          <>
            <Typography
              className={classes.detailProject}
              variant="h4"
            >Departamento:</Typography>
            <Typography variant="body1">
              {project.department}
            </Typography>
          </>
        )}
        {project.city && (
          <>
            <Typography
              className={classes.detailProject}
              variant="h4"
            >Ciudad:</Typography>
            <Typography variant="body1">
              {project.city}
            </Typography>
          </>
        )}
        {project.incidentCity && project.incidentCity!== '' && (
          <>
            <Typography
              className={classes.detailProject}
              variant="h4"
            >Ciudad Incidente:</Typography>
            <Typography variant="body1">
              {project.incidentCity}
            </Typography>
          </>
        )}
        {project.destinationAddress && project.destinationAddress!== '' && (
          <>
            <Typography
              className={classes.detailProject}
              variant="h4"
            >Ciudad de Destino:</Typography>
            <Typography variant="body1">
              {project.destinationAddress}
            </Typography>
          </>
        )}
        {project.client && (
          <>
            <Typography
              className={classes.detailProject}
              variant="h4"
            >Cliente:</Typography>
            <Typography variant="body1">
              {project.client}
            </Typography>
          </>
        )}
        {project.product && (
          <>
            <Typography
              className={classes.detailProject}
              variant="h4"
            >Producto:</Typography>
            <Typography variant="body1">
              {project.product}
            </Typography>
          </>
        )}
        {project.status && (
          <>
            <Typography
              className={classes.detailProject}
              variant="h4"
            >Estado:</Typography>
            <Typography variant="body1">
              {project.status}
            </Typography>
          </>
        )}
        {project.description && (
          <>
            <Typography
              className={classes.detailProject}
              variant="h4"
            >Descripción:</Typography>
            <Typography variant="body1">
              {project.description}
            </Typography>
          </>
        )}
      </CardContent>
    </Card>
  );
};

Details.propTypes = {
  project: PropTypes.object.isRequired,
  className: PropTypes.string
};

export default Details;
