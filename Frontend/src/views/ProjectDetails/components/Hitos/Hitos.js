import React, {useEffect  } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { 
  Button,
  Grid, 
  Checkbox, 
  InputAdornment, 
  FormControl, 
  FormControlLabel, 
  InputLabel, 
  Select, 
  MenuItem, 
  TextField, 
  Typography,
} from '@material-ui/core';
import AssessmentIcon from '@material-ui/icons/Assessment';
import RateReviewIcon from '@material-ui/icons/RateReview';
import DateRangeIcon from '@material-ui/icons/DateRange';
import moment from 'moment';
import Cookies from 'js-cookie';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';
import { useHistory } from 'react-router';

const drawerWidth = 540;

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(3),
    overflowX: 'auto',
    boxShadow: 'unset',
  },
  header: {
    display: 'flex',
    flexFlow: 'row nowrap',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: '25px',
  },
  header__select: {
    margin: 0,
    width: '30%'
  },
  cont: {
    width: '100%',
    background: '#ffffff',
    boxShadow: '0 1px 4px 0 rgba(0, 0, 0, 0.14)',
  },
  tittle: {
    width: '100%',
    display: 'flex',
    flexFlow: 'row nowrap',
    justifyContent: 'flex-start',
  },
  drawer: {
    flexShrink: 0,
  },
  drawer2: {
    width: 'auto',
  },
  drawerPaper: {
    width: drawerWidth,
    top: '64px',
  },
  reportDrawer: {
    // width: drawerWidth,
    // top: '64px',
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
    justifyContent: 'flex-start',
    minWidth: 700,
  },
  infoHeader: {
    color: '#FFF',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: '25px',
    backgroundColor: '#008fd5',
  },
  infoHeaderH4: {
    marginTop: '0px',
    minHeight: 'auto',
    fontFamily: '\'Roboto\', \'Helvetica\', \'Arial\', sans-serif',
    fontWeight: '300',
    marginBottom: '3px',
    textDecoration: 'none',
    fontSize: '18px',
  },
  margin: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  marginReporte: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: 'auto',
  },
  tittleSideBar: {
    marginLeft: '20px',
  },
  contSideBar: {
    height: '75%',
  },
  headeSideBar: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: '25px',
  },
  appBar: {
    position: 'relative',
  },
  ReoprtTitle: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
  formControl: {
    minWidth: '200px',
    // marginLeft: '10%',
    // marginBottom: '10%',
  },
  FirstContainerBadges: {
    width: '97%',
    display: 'flex',
    flexFlow: 'row wrap',
    justifyContent: 'space-around',
    marginLeft: '20px',
    marginTop: '18px',
  },
  badgeContainer: {
    // width: '28%',
    minWidth: '220px',
    height: 'unset',
    // height: '104%',
    display: 'flex',
    padding: '17px',
    flexFlow: 'column',
    boxShadow: '0 4px 15px 0 rgba(0, 0, 0, 0.14)',
    marginTop: '15px',
    alignItems: 'center',
    borderRadius: '20px',
    backgroundColor: '#FFFFFF',
    margin: '0 15px',
  },
  inputUpdate: {

  },
  badgeTitle: {
    color: 'white',
    width: '67%',
    padding: '5%',
    textAlign: 'center',
    marginTop: '-45px',
    background: 'linear-gradient(to bottom right, #2c8fd5, #58ace7)',
    borderRadius: '7px',
    fontSize: '100%',
  },
  inputSelectUpdate: {
    width: '100%',
  },
  observationsHitos: {
    marginTop: '4%',
    marginLeft: '2%',
    width: '95%',
  },
  conOth: {
    maxHeight: '85%',
    overflowY: 'scroll',
  },
}));

const Hitos = props => {
  const { idProject, project, className, ...rest } = props;
  // console.log('project: ', project);
  const classes = useStyles();
  const history = useHistory();

  const [checked, setChecked] = React.useState({
    fechaModificacion: moment().format('YYYY-MM-DD'),
    usuarioSesion: Cookies.get('name'),
    actividadActual: '',
    actividades: [
      {
        tittle: "Visita obra civil",
        estado: '',
        observaciones: '',
        fechaCompromiso: '',
        noAplica: false,
      },
      {
        tittle: "Ejecución obra civil",
        estado: '',
        observaciones: '',
        fechaCompromiso: '',
        noAplica: false,
      },
      {
        tittle: "Empalmes",
        estado: '',
        observaciones: '',
        fechaCompromiso: '',
        noAplica: false,
      },
      {
        tittle: "Entrega Servicio",
        estado: '',
        observaciones: '',
        fechaCompromiso: '',
        noAplica: false,
      },
    ],
    observacionesGenrales: '',
  });

  const options = [ 
      {tittle: 'Ejecutada', value: 'Ejecutada'},
      {tittle: 'Programada', value: 'Programada'},
      {tittle: 'Aprobado', value: 'Aprobado'},
      {tittle: 'Configurado', value: 'Configurado'},
      {tittle: 'Fecha tentativa', value: 'Fecha tentativa'},
      {tittle: 'Cerrada', value: 'Cerrada'},
      {tittle: 'No Aplica', value: 'No Aplica'}
  ];
  const handleChange = (e) => {
    const { target } = e;
    setChecked({
      ...checked,
      [target.name]: target.value,
    });
    // console.log('cambio');
  };

  const handleChangeWithGroup = (i, e) => {
    const { target } = e;
    const arr = checked.actividades;
    arr[i] = {
      ...arr[i],
      [target.name]: target.checked ? target.checked : target.value,
    };
    setChecked({
      ...checked,
      actividades: arr,
    });
    // console.log(target.checked);
  };

  const handleSave = async () => {
    try {

        const petition = await axios.put(`projects/${project._id}`,
        {
          field: '_id',
          hitos: checked,
        } 
        ,getAuthorization());

        // console.log('petition: ', petition);
        history.go(0);
    } catch (error) {
      console.error(error);
    }      

  };

  useEffect(() => {
    project?.hitos && setChecked({
      ...checked,
      actividadActual: project?.hitos?.actividadActual,
      actividades: project?.hitos?.actividades,
      observacionesGenrales:  project?.hitos?.observacionesGenrales,
    });
  },[]);

  return (
    <Grid
      {...rest}
      className={clsx(classes.root, className)}
      container
    >
      <div
        className={classes.appBar}
      >
        <div className={classes.header}>
          <div>
            <FormControl className={classes.formControl}>
              <InputLabel id="actividadActual">
                Actividad Actual:
              </InputLabel>
              <Select
                id="actividadActual"
                labelId="actividadActual"
                name="actividadActual"
                onChange={handleChange}
                value={checked.actividadActual || ''}
              >
                <MenuItem value="">
                  <em>Seleccione...</em>
                </MenuItem>
                <MenuItem value="Vista Obra Civil (VOC)">Vista Obra Civil (VOC)</MenuItem>
                <MenuItem value="Visita EjecucionObra civil (EOC)">Visita EjecucionObra civil (EOC)</MenuItem>
                <MenuItem value="Empalmes (EM)">Empalmes (EM)</MenuItem>
                <MenuItem value="Entrega Servicio">Entrega Servicio</MenuItem>
                <MenuItem value="Pendiente Cliente">Pendiente Cliente</MenuItem>
              </Select>
            </FormControl>
          </div>
          <div>
            <Button
              color="primary"
              size="small"
              variant="outlined"
              onClick={handleSave}
            >
              Actualizar
            </Button>
          </div>
        </div>
        <div className={classes.FirstContainerBadges}>
          
          {checked.actividades.map((obj, i) => (
            <div className={classes.badgeContainer}>
              <div className={classes.badgeTitle}>
                <Typography
                  style={{color:'white'}}
                  variant="body1"
                >
                  {`${i+1} - ${obj.tittle}`}
                </Typography>
              </div>
              <FormControl className={classes.inputSelectUpdate}>
                <InputLabel htmlFor={`estado${i}`}>Estado:</InputLabel>
                <Select
                  id={`estado${i}`}
                  name="estado"
                  onChange={(e) => handleChangeWithGroup(i, e)}
                  startAdornment={(
                    <InputAdornment position="start">
                      <AssessmentIcon />
                    </InputAdornment>
                  )}
                  value={checked.actividades[i].estado || ''}
                >
                  <MenuItem value="">
                    <em>Seleccione...</em>
                  </MenuItem>
                  {options.map(item => (
                    <MenuItem value={item.value}>
                      {item.tittle}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
              <FormControl>
                <TextField
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <RateReviewIcon />
                      </InputAdornment>
                    ),
                  }}
                  label="Observaciones"
                  multiline
                  name="observaciones"
                  onChange={(e) => handleChangeWithGroup(i, e)}
                  rows="3"
                  value={checked.actividades[i].observaciones || ''}
                />
              </FormControl>
              <FormControl>
                <TextField
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <DateRangeIcon />
                      </InputAdornment>
                    ),
                  }}
                  id="fechaCompromiso"
                  label="Fecha"
                  name="fechaCompromiso"
                  onChange={(e) => handleChangeWithGroup(i, e)}
                  type="date"
                  value={checked.actividades[i].fechaCompromiso || ''}
                />
              </FormControl>
              <FormControl className={classes.inputUpdate}>
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={checked.actividades[i].noAplica || false}
                      color="primary"
                      // id="noAplica"
                      name="noAplica"
                      onChange={(e) => handleChangeWithGroup(i, e)}
                    />
                  }
                  label="NO APLICA"
                />
              </FormControl>
            </div>
          ))}
        </div>
        <div>
          <Grid
            item
            sm={12}
            xs={12}
          >
            <TextField
              className={classes.observationsHitos}
              id="observacionesGenrales"
              label="OBSERVACIONES PENDIENTES DEL CLIENTE"
              multiline
              name="observacionesGenrales"
              onChange={handleChange}
              rows="3"
              value={checked.observacionesGenrales || ''}
              variant="outlined"
            />
          </Grid>
        </div>
      </div>
    </Grid>
  );
};

Hitos.propTypes = {
  className: PropTypes.string,
  idProject: PropTypes.string,
  project: PropTypes.object,
  subscribers: PropTypes.array.isRequired
};

export default Hitos;
