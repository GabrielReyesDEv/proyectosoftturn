import React, { useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Grid,
  Divider,
  Switch,
  TextField,
  Typography,
  colors
} from '@material-ui/core';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';
import SuccessSnackbar from '../SuccessSnackbar';
import moment from 'moment';
import useRouter from 'utils/useRouter';

const useStyles = makeStyles(theme => ({
  root: {},
  saveButton: {
    color: theme.palette.white,
    backgroundColor: colors.green[600],
    '&:hover': {
      backgroundColor: colors.green[900]
    }
  }
}));

const GeneralSettings = props => {
  const { profile, className, ...rest } = props;
  const router = useRouter();
  const classes = useStyles();
  const [openSnackbar, setOpenSnackbar] = useState(false);
  const [values, setValues] = useState({
    firstName: profile.firstName,
    lastName: profile.lastName,
    email: profile.email,
    phone: profile.phone,
    workPosition: profile.workPosition,
    identification: profile.identification,
    status: profile.status
  });

  const handleChange = event => {
    event.persist();
    console.log(event.target.checked, values);
    setValues({
      ...values,
      [event.target.name]:
        event.target.type === 'checkbox'
          ? (event.target.checked ? 'Active' : 'Inactive')
          : event.target.value
    });
  };

  const handleSubmit = async event => {
    try {
      event.preventDefault();
      console.log('user: ', profile, 'cambios: ', values);
      const petition = await axios.put(`/users/${profile._id}`, {
        field: '_id',
        lastUpdated: moment().format('YYYY-MM-DD HH:mm'),
        ...values,
      }, getAuthorization());
      if(petition.status === 200) {
        setOpenSnackbar(true);
        router.history.go(0);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleSnackbarClose = () => {
    setOpenSnackbar(false);
  };

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <form onSubmit={handleSubmit}>
        <CardHeader title="Información" />
        <Divider />
        <CardContent>
          <Grid
            container
            spacing={4}
          >
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                helperText="Please specify the first name"
                label="Nombre"
                name="firstName"
                onChange={handleChange}
                required
                value={values.firstName}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Apellido"
                name="lastName"
                onChange={handleChange}
                required
                value={values.lastName}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Dirección de Correo"
                name="email"
                onChange={handleChange}
                required
                value={values.email}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Cédula"
                name="identification"
                onChange={handleChange}
                type="text"
                value={values.identification}
                variant="outlined"
              />
            </Grid>
            
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Teléfono"
                name="phone"
                onChange={handleChange}
                type="text"
                value={values.phone}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Cargo"
                name="workPosition"
                onChange={handleChange}
                required
                value={values.workPosition}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <Typography variant="h6">Disponible para trabajar</Typography>
              <Typography variant="body2">
                Significa que se le puede asignar actividades en Zolid
              </Typography>
              <Switch
                checked={values.status === 'Active' ? true : false}
                color="secondary"
                edge="start"
                name="status"
                onChange={handleChange}
              />
            </Grid>
        </Grid>
        </CardContent>
        <Divider />
        <CardActions>
          <Button
            className={classes.saveButton}
            type="submit"
            variant="contained"
          >
            Guardar Cambios
          </Button>
        </CardActions>
      </form>
      <SuccessSnackbar
        onClose={handleSnackbarClose}
        open={openSnackbar}
        message='Usuario actualizado'
      />
    </Card>
  );
};

GeneralSettings.propTypes = {
  className: PropTypes.string,
  profile: PropTypes.object.isRequired
};

export default GeneralSettings;
