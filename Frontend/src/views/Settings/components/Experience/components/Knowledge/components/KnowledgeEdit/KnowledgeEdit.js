import React, { useState } from 'react';
import clsx from 'clsx';
import moment from 'moment';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Modal,
  Card,
  CardContent,
  CardActions,
  Grid,
  Typography,
  TextField,
  Button,
  colors
} from '@material-ui/core';
import { DatePicker } from '@material-ui/pickers';
import useRouter from 'utils/useRouter';
import axios from 'utils/axios'
import getAuthorization from 'utils/getAuthorization';

let running = true;

const useStyles = makeStyles(theme => ({
  root: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    outline: 'none',
    boxShadow: theme.shadows[20],
    width: 700,
    maxHeight: '100%',
    overflowY: 'auto',
    maxWidth: '100%'
  },
  container: {
    marginTop: theme.spacing(1)
  },
  actions: {
    justifyContent: 'flex-end'
  },
  saveButton: {
    color: theme.palette.white,
    backgroundColor: colors.green[600],
    '&:hover': {
      backgroundColor: colors.green[900]
    }
  },
  dateField: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  }
}));

const KnowledgeEdit = props => {
  const { id, open, onClose, knowledge, className, ...rest } = props;

  const classes = useStyles();
  const router = useRouter();
 
  const [formState, setFormState] = useState({});
  const [calendarTrigger, setCalendarTrigger] = useState(null);
  const [calendarFocus, setCalendarFocus] = useState('');

  if (!open) {
    !running && (running = true)
    return null;
  } else {
    running && setFormState({ ...knowledge })
    running = false;
  }

  const handleFieldChange = (event, key) => {
    event.persist();
    setFormState(formState => ({
      ...formState,
      [key]: {
        ...formState[key],
        [event.target.name]:
          event.target.type === 'checkbox'
            ? event.target.checked
            : event.target.value
      } 
    }));
  };

  const handleCalendarOpen = (trigger, key) => {
    setCalendarFocus(key);
    setCalendarTrigger(trigger);
  };

  const handleCalendarChange = () => {};

  const handleCalendarAccept = date => {
    setFormState(formState => ({
      ...formState,
      [calendarFocus]: {
        ...formState[calendarFocus],
        [calendarTrigger]: moment(date).format('DD/MM/YYYY')
      }
    }));
  };

  const handleCalendarClose = () => {
    setCalendarFocus('');
    setCalendarTrigger(false);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const consult = await axios.put(`/users/${id}`, { field: '_id', knowledge: formState }, getAuthorization())
      if (consult.status === 200) router.history.go(0);
    } catch (error) {
      console.error(error);
    }
  };

  const calendarOpen = Boolean(calendarTrigger);
  const calendarMinDate =
    calendarTrigger === 'startDate'
      ? moment()
      : moment(formState.startDate).add(1, 'day');
  const calendarValue = formState[calendarTrigger];
  const levelOptions = ['Alto', 'Medio', 'Bajo'];
  const statusOptions = ['Active', 'Pending', 'En curso solicitud', 'Bloqueado'];

 

  return (
    <Modal
      onClose={onClose}
      open={open}
    >
      <Card
        {...rest}
        className={clsx(classes.root, className)}
      >
        <form>
          <CardContent>
            <Typography
              align="center"
              gutterBottom
              variant="h3"
            >
              Editar Conocimiento
            </Typography>
            <div className={classes.container}>
              <Typography
                align="left"
                gutterBottom
                variant="h6"
              >
                IP
              </Typography>

              <Grid
                container
                spacing={3}
              >
                <Grid
                  item
                  md={4}
                  xs={12}
                >
                  <TextField
                    fullWidth
                    label="Nivel"
                    name="level"
                    onChange={event =>
                      handleFieldChange(
                        event,
                        'ip'
                      )
                    }
                    select
                    // eslint-disable-next-line react/jsx-sort-props
                    SelectProps={{ native: true }}
                    value={formState?.ip?.level || ''}
                    variant="outlined"
                  >
                    <option
                      disabled
                      value=""
                    />
                    {levelOptions.map(option => (
                      <option
                        key={option}
                        value={option}
                      >
                        {option}
                      </option>
                    ))}
                  </TextField>
                </Grid>
                <Grid
                  item
                  md={4}
                  xs={12}
                >
                  <TextField
                    fullWidth
                    label="Certificado Claro"
                    name="status"
                    onChange={event =>
                      handleFieldChange(
                        event,
                        'ip'
                      )
                    }
                    select
                    // eslint-disable-next-line react/jsx-sort-props
                    SelectProps={{ native: true }}
                    value={formState?.ip?.status || ''}
                    variant="outlined"
                  >
                    <option
                      disabled
                      value=""
                    />
                    {statusOptions.map(option => (
                      <option
                        key={option}
                        value={option}
                      >
                        {option}
                      </option>
                    ))}
                  </TextField>
                </Grid>
              
                <Grid
                  item
                  md={4}
                  xs={12}
                >
                
                  <TextField
                    className={classes.dateField}
                    fullWidth
                    label="Examen"
                    name="date"
                    onClick={() => handleCalendarOpen('date', 'ip')}
                    value={formState?.ip?.date || ''}
                    variant="outlined"
                  />
                </Grid>
              </Grid>
            </div>
            <div className={classes.container}>
              <Typography
                align="left"
                gutterBottom
                variant="h6"
              >
                Telefonía
              </Typography>

              <Grid
                className={classes.container}
                container
                spacing={3}
              >
                <Grid
                  item
                  md={4}
                  xs={12}
                >
                  <TextField
                    fullWidth
                    label="Nivel"
                    name="level"
                    onChange={event =>
                      handleFieldChange(
                        event,
                        'telefonia'
                      )
                    }
                    select
                    // eslint-disable-next-line react/jsx-sort-props
                    SelectProps={{ native: true }}
                    value={formState?.telefonia?.level || ''}
                    variant="outlined"
                  >
                    <option
                      disabled
                      value=""
                    />
                    {levelOptions.map(option => (
                      <option
                        key={option}
                        value={option}
                      >
                        {option}
                      </option>
                    ))}
                  </TextField>
                </Grid>
                <Grid
                  item
                  md={4}
                  xs={12}
                >
                  <TextField
                    fullWidth
                    label="Certificado Claro"
                    name="status"
                    onChange={event =>
                      handleFieldChange(
                        event,
                        'telefonia'
                      )
                    }
                    select
                    // eslint-disable-next-line react/jsx-sort-props
                    SelectProps={{ native: true }}
                    value={formState?.telefonia?.status || ''}
                    variant="outlined"
                  >
                    <option
                      disabled
                      value=""
                    />
                    {statusOptions.map(option => (
                      <option
                        key={option}
                        value={option}
                      >
                        {option}
                      </option>
                    ))}
                  </TextField>
                </Grid>
              
                <Grid
                  item
                  md={4}
                  xs={12}
                >
                
                  <TextField
                    className={classes.dateField}
                    fullWidth
                    label="Examen"
                    name="date"
                    onClick={() => handleCalendarOpen('date', 'telefonia')}
                    value={formState?.telefonia?.date || ''}
                    variant="outlined"
                  />
                </Grid>
              </Grid>
            </div>
            <div className={classes.container}>
              <Typography
                align="left"
                gutterBottom
                variant="h6"
              >
                Tx
              </Typography>

              <Grid
                className={classes.container}
                container
                spacing={3}
              >
                <Grid
                  item
                  md={4}
                  xs={12}
                >
                  <TextField
                    fullWidth
                    label="Nivel"
                    name="level"
                    onChange={event =>
                      handleFieldChange(
                        event,
                        'tx'
                      )
                    }
                    select
                    // eslint-disable-next-line react/jsx-sort-props
                    SelectProps={{ native: true }}
                    value={formState?.tx?.level || ''}
                    variant="outlined"
                  >
                    <option
                      disabled
                      value=""
                    />
                    {levelOptions.map(option => (
                      <option
                        key={option}
                        value={option}
                      >
                        {option}
                      </option>
                    ))}
                  </TextField>
                </Grid>
                <Grid
                  item
                  md={4}
                  xs={12}
                >
                  <TextField
                    fullWidth
                    label="Certificado Claro"
                    name="status"
                    onChange={event =>
                      handleFieldChange(
                        event,
                        'tx'
                      )
                    }
                    select
                    // eslint-disable-next-line react/jsx-sort-props
                    SelectProps={{ native: true }}
                    value={formState?.tx?.status || ''}
                    variant="outlined"
                  >
                    <option
                      disabled
                      value=""
                    />
                    {statusOptions.map(option => (
                      <option
                        key={option}
                        value={option}
                      >
                        {option}
                      </option>
                    ))}
                  </TextField>
                </Grid>
              
                <Grid
                  item
                  md={4}
                  xs={12}
                >
                
                  <TextField
                    className={classes.dateField}
                    fullWidth
                    label="Examen"
                    name="date"
                    onClick={() => handleCalendarOpen('date', 'tx')}
                    value={formState?.tx?.date || ''}
                    variant="outlined"
                  />
                </Grid>
              </Grid>
            </div>
            <div className={classes.container}>
              <Typography
                align="left"
                gutterBottom
                variant="h6"
              >
                SDWAN
              </Typography>

              <Grid
                className={classes.container}
                container
                spacing={3}
              >
                <Grid
                  item
                  md={4}
                  xs={12}
                >
                  <TextField
                    fullWidth
                    label="Nivel"
                    name="level"
                    onChange={event =>
                      handleFieldChange(
                        event,
                        'sdwan'
                      )
                    }
                    select
                    // eslint-disable-next-line react/jsx-sort-props
                    SelectProps={{ native: true }}
                    value={formState?.sdwan?.level || ''}
                    variant="outlined"
                  >
                    <option
                      disabled
                      value=""
                    />
                    {levelOptions.map(option => (
                      <option
                        key={option}
                        value={option}
                      >
                        {option}
                      </option>
                    ))}
                  </TextField>
                </Grid>
                <Grid
                  item
                  md={4}
                  xs={12}
                >
                  <TextField
                    fullWidth
                    label="Certificado Claro"
                    name="status"
                    onChange={event =>
                      handleFieldChange(
                        event,
                        'sdwan'
                      )
                    }
                    select
                    // eslint-disable-next-line react/jsx-sort-props
                    SelectProps={{ native: true }}
                    value={formState?.sdwan?.status || ''}
                    variant="outlined"
                  >
                    <option
                      disabled
                      value=""
                    />
                    {statusOptions.map(option => (
                      <option
                        key={option}
                        value={option}
                      >
                        {option}
                      </option>
                    ))}
                  </TextField>
                </Grid>
              
                <Grid
                  item
                  md={4}
                  xs={12}
                >
                
                  <TextField
                    className={classes.dateField}
                    fullWidth
                    label="Examen"
                    name="date"
                    onClick={() => handleCalendarOpen('date', 'sdwan')}
                    value={formState?.sdwan?.date || ''}
                    variant="outlined"
                  />
                </Grid>
              </Grid>
            </div>
            <div className={classes.container}>
              <Typography
                align="left"
                gutterBottom
                variant="h6"
              >
                SR/RX
              </Typography>

              <Grid
                className={classes.container}
                container
                spacing={3}
              >
                <Grid
                  item
                  md={4}
                  xs={12}
                >
                  <TextField
                    fullWidth
                    label="Nivel"
                    name="level"
                    onChange={event =>
                      handleFieldChange(
                        event,
                        'srrx'
                      )
                    }
                    select
                    // eslint-disable-next-line react/jsx-sort-props
                    SelectProps={{ native: true }}
                    value={formState?.srrx?.level || ''}
                    variant="outlined"
                  >
                    <option
                      disabled
                      value=""
                    />
                    {levelOptions.map(option => (
                      <option
                        key={option}
                        value={option}
                      >
                        {option}
                      </option>
                    ))}
                  </TextField>
                </Grid>
                <Grid
                  item
                  md={4}
                  xs={12}
                >
                  <TextField
                    fullWidth
                    label="Certificado Claro"
                    name="status"
                    onChange={event =>
                      handleFieldChange(
                        event,
                        'srrx'
                      )
                    }
                    select
                    // eslint-disable-next-line react/jsx-sort-props
                    SelectProps={{ native: true }}
                    value={formState?.srrx?.status || ''}
                    variant="outlined"
                  >
                    <option
                      disabled
                      value=""
                    />
                    {statusOptions.map(option => (
                      <option
                        key={option}
                        value={option}
                      >
                        {option}
                      </option>
                    ))}
                  </TextField>
                </Grid>
              
                <Grid
                  item
                  md={4}
                  xs={12}
                >
                
                  <TextField
                    className={classes.dateField}
                    fullWidth
                    label="Examen"
                    name="date"
                    onClick={() => handleCalendarOpen('date', 'srrx')}
                    value={formState?.srrx?.date || ''}
                    variant="outlined"
                  />
                </Grid>
              </Grid>
            </div>
            <div className={classes.container}>
              <Typography
                align="left"
                gutterBottom
                variant="h6"
              >
                Procesos
              </Typography>

              <Grid
                className={classes.container}
                container
                spacing={3}
              >
                <Grid
                  item
                  md={4}
                  xs={12}
                >
                  <TextField
                    fullWidth
                    label="Nivel"
                    name="level"
                    onChange={event =>
                      handleFieldChange(
                        event,
                        'procesos'
                      )
                    }
                    select
                    // eslint-disable-next-line react/jsx-sort-props
                    SelectProps={{ native: true }}
                    value={formState?.procesos?.level || ''}
                    variant="outlined"
                  >
                    <option
                      disabled
                      value=""
                    />
                    {levelOptions.map(option => (
                      <option
                        key={option}
                        value={option}
                      >
                        {option}
                      </option>
                    ))}
                  </TextField>
                </Grid>
                <Grid
                  item
                  md={4}
                  xs={12}
                >
                  <TextField
                    fullWidth
                    label="Certificado Claro"
                    name="status"
                    onChange={event =>
                      handleFieldChange(
                        event,
                        'procesos'
                      )
                    }
                    select
                    // eslint-disable-next-line react/jsx-sort-props
                    SelectProps={{ native: true }}
                    value={formState?.procesos?.status || ''}
                    variant="outlined"
                  >
                    <option
                      disabled
                      value=""
                    />
                    {statusOptions.map(option => (
                      <option
                        key={option}
                        value={option}
                      >
                        {option}
                      </option>
                    ))}
                  </TextField>
                </Grid>
              
                <Grid
                  item
                  md={4}
                  xs={12}
                >
                
                  <TextField
                    className={classes.dateField}
                    fullWidth
                    label="Examen"
                    name="date"
                    onClick={() => handleCalendarOpen('date', 'procesos')}
                    value={formState?.procesos?.date || ''}
                    variant="outlined"
                  />
                </Grid>
              </Grid>
            </div>
          
          </CardContent>
          <CardActions className={classes.actions}>
            <Button
              onClick={onClose}
              variant="contained"
            >
              Cerrar
            </Button>
            <Button
              className={classes.saveButton}
              onClick={handleSubmit}
              type="submit"
              variant="contained"
            >
              Save
            </Button>
          </CardActions>
          <DatePicker
            minDate={calendarMinDate}
            onAccept={handleCalendarAccept}
            onChange={handleCalendarChange}
            onClose={handleCalendarClose}
            open={calendarOpen}
            style={{ display: 'none' }} // Temporal fix to hide the input element
            value={calendarValue}
            variant="dialog"
          />
        </form>
      </Card>
    </Modal>
  );
};

KnowledgeEdit.displayName = 'KnowledgeEdit';

KnowledgeEdit.propTypes = {
  className: PropTypes.string,
  knowledge: PropTypes.any,
  onClose: PropTypes.func,
  open: PropTypes.bool
};

KnowledgeEdit.defaultProps = {
  open: false,
  onClose: () => {}
};

export default KnowledgeEdit;
