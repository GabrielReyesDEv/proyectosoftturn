import React, { useState } from 'react';
import clsx from 'clsx';
import moment from 'moment';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Modal,
  Card,
  CardContent,
  CardActions,
  Grid,
  Typography,
  TextField,
  Button,
  colors
} from '@material-ui/core';
import { DatePicker } from '@material-ui/pickers';
import useRouter from 'utils/useRouter';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';

let running = true;

const useStyles = makeStyles(theme => ({
  root: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    outline: 'none',
    boxShadow: theme.shadows[20],
    width: 700,
    maxHeight: '100%',
    overflowY: 'auto',
    maxWidth: '100%'
  },
  container: {
    marginTop: theme.spacing(3)
  },
  actions: {
    justifyContent: 'flex-end'
  },
  saveButton: {
    color: theme.palette.white,
    backgroundColor: colors.green[600],
    '&:hover': {
      backgroundColor: colors.green[900]
    }
  },
  dateField: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  }
}));

const PlatformEdit = props => {
  const { id, index, open, onClose, study, className, ...rest } = props;

  const classes = useStyles();
  const router = useRouter();
 
  const [formState, setFormState] = useState({ ...study, duration: 0 });

  const [calendarTrigger, setCalendarTrigger] = useState(null);

  const handleFieldChange = event => {
    event.persist();
    setFormState(formState => ({
      ...formState,
      [event.target.name]: event.target.value
    }));
  };

  const handleCalendarOpen = trigger => {
    setCalendarTrigger(trigger);
  };

  const handleCalendarChange = (date) => {
    date= moment(date,'DD/MM/YYYY');
    let duration = 0
    if (calendarTrigger === 'initialDate') {
      duration = Math.floor(moment.duration( moment(formState.finalDate,'DD/MM/YYYY').diff(date)).asYears())
    } else {
      duration = Math.floor(moment.duration(date.diff(moment(formState.initialDate,'DD/MM/YYYY'))).asYears())
    }
    setFormState(formState => ({
      ...formState,
      [calendarTrigger]: moment(date).format('DD/MM/YYYY'),
      duration: (duration < 0 ? 0 : duration)
    }));
  };

  const handleCalendarAccept = date => {
    
  };

  const handleCalendarClose = () => {
    setCalendarTrigger(false);
  };

  if (!open) {
    !running && (running = true)
    return null;
  } else {
    running && setFormState({ ...study })
    running = false;
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const consult = await axios.put(`/users/${id}`, { field: '_id', [`experience.${index}`]: formState }, getAuthorization())

      if (consult.status === 200) router.history.go(0);
    } catch (error) {
      console.error(error);
    }
  };

  const calendarOpen = Boolean(calendarTrigger);
  const calendarMaxDate = moment();
  const calendarValue = formState[calendarTrigger];

  return (
    <Modal
      onClose={onClose}
      open={open}
    >
      <Card
        {...rest}
        className={clsx(classes.root, className)}
      >
        <form>
          <CardContent>
            <Typography
              align="center"
              gutterBottom
              variant="h3"
            >
              Editar Experiencia {formState.name}
            </Typography>
            <Grid
              className={classes.container}
              container
              spacing={3}
            >
              <Grid
                item
                md={12}
                xs={12}
              >
                <TextField
                  fullWidth
                  label={'Compañía'}
                  name="company"
                  onChange={handleFieldChange}
                  required
                  value={formState.company}
                  variant="outlined"
                />
              </Grid>
              <Grid
                item
                md={8}
                xs={12}
              >
                <TextField
                  fullWidth
                  label={'Cargo'}
                  name="workPosition"
                  onChange={handleFieldChange}
                  required
                  value={formState.workPosition}
                  variant="outlined"
                />
              </Grid>
              <Grid
                item
                md={4}
                xs={12}
              >
                <TextField
                  disabled
                  fullWidth
                  label={'Duración (Años)'}
                  name="duration"
                  onChange={handleFieldChange}
                  required
                  type="number"
                  value={formState.duration ? formState.duration : 0}
                  variant="outlined"
                />
              </Grid>
              <Grid
                item
                md={6}
                xs={12}
              >
                <TextField
                  className={classes.dateField}
                  fullWidth
                  label={'Inicio'}
                  name="initialDate"
                  onClick={() => handleCalendarOpen('initialDate')}
                  required
                  value={formState.initialDate}
                  variant="outlined"
                />
              </Grid>
              <Grid
                item
                md={6}
                xs={12}
              >
                <TextField
                  className={classes.dateField}
                  fullWidth
                  label={'Fin'}
                  name="finalDate"
                  onClick={() => handleCalendarOpen('finalDate')}
                  required
                  value={formState.finalDate}
                  variant="outlined"
                />
              </Grid>
            </Grid>
          </CardContent>
          <CardActions className={classes.actions}>
            <Button
              onClick={onClose}
              variant="contained"
            >
              Cerrar
            </Button>
            <Button
              className={classes.saveButton}
              onClick={handleSubmit}
              type="submit"
              variant="contained"
            >
              Save
            </Button>
          </CardActions>
          <DatePicker
            maxDate={calendarMaxDate}
            onAccept={handleCalendarAccept}
            onChange={handleCalendarChange}
            onClose={handleCalendarClose}
            open={calendarOpen}
            style={{ display: 'none' }} // Temporal fix to hide the input element
            value={calendarValue}
            variant="dialog"
          />
        </form>
      </Card>
    </Modal>
  );
};

PlatformEdit.displayName = 'PlatformEdit';

PlatformEdit.propTypes = {
  className: PropTypes.string,
  study: PropTypes.any,
  onClose: PropTypes.func,
  open: PropTypes.bool
};

PlatformEdit.defaultProps = {
  open: false,
  onClose: () => {}
};

export default PlatformEdit;
