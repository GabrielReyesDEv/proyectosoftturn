import React, { useState } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import clsx from 'clsx';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { makeStyles } from '@material-ui/styles';
import {
  Button,
  Card,
  CardContent,
  CardHeader,
  colors,
  Divider,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableRow,
  TableHead
} from '@material-ui/core';

import { Label } from 'components';
import AddIcon from '@material-ui/icons/Add';
import { TelecommunicationsExperienceEdit, TelecommunicationsExperienceAdd } from './components';

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  actions: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    '& > * + *': {
      marginLeft: 0
    }
  },
  buttonIcon: {
    marginRight: theme.spacing(1)
  },
  action: {
    margin: 0
  }
}));

const TelecommunicationsExperience = props => {
  const { id, studies, className, ...rest } = props;

  const classes = useStyles();
  const [openEdit, setOpenEdit] = useState(false);
  const [openAdd, setOpenAdd] = useState(false);
  const [study, setStudy] = useState({});
  const [index, setIndex] = useState(0);

  const handleEditOpen = (study, i) => {
    setStudy({ ...study });
    setIndex(i)
    setOpenEdit(true);
  };

  const handleEditClose = () => {
    setOpenEdit(false);
  };

  const handleAddOpen = () => {
    setOpenAdd(true);
  };

  const handleAddClose = () => {
    setOpenAdd(false);
  };

  const statusColors = {
    Suspended: colors.orange[600],
    Active: colors.green[600],
    rejected: colors.red[600]
  };

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader
        action={
          <Button onClick={() => handleAddOpen()}>
            <IconButton aria-label="Add">
              <AddIcon />
            </IconButton>
          </Button>
        }
        classes={{ action: classes.action }}
        className={classes.action}
        title="Trabajo en Telecomunicaciones"
      />
      <TelecommunicationsExperienceAdd
        id={id}
        onClose={handleAddClose}
        open={openAdd}
        work={{initialDate: moment().format('DD/MM/YYYY'), finalDate: moment().format('DD/MM/YYYY')}}
      />
      <Divider />
      <CardContent className={classes.content}>
        <PerfectScrollbar>
          <div className={classes.inner}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Empresa</TableCell>
                  <TableCell>Cargo</TableCell>
                  <TableCell>Inicio</TableCell>
                  <TableCell>Fin</TableCell>
                  <TableCell>Duración (Años)</TableCell>
                  <TableCell>Acciones</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {studies.map((study, i) => (
                  <TableRow key={studies.id}>
                    <TableCell>{study.company}</TableCell>
                    <TableCell>
                      <Label
                        color={statusColors[study.status]}
                        variant="outlined"
                      >
                        {study.workPosition}
                      </Label>
                    </TableCell>
                    <TableCell>{study.initialDate}</TableCell>
                    <TableCell>{study.finalDate}</TableCell>
                    <TableCell>{study.duration}</TableCell>
                    <TableCell>
                      <Button
                        color="primary"
                        onClick={() => handleEditOpen(study, i)}
                        size="small"
                        variant="outlined"
                      >
                      Editar
                      </Button>
                    </TableCell>
                  </TableRow>
                ))}
                <TelecommunicationsExperienceEdit
                  id={id}
                  index={index}
                  onClose={handleEditClose}
                  open={openEdit}
                  study={study}
                />
              </TableBody>
            </Table>
          </div>
        </PerfectScrollbar>
      </CardContent>
    </Card>

  );
};

TelecommunicationsExperience.propTypes = {
  className: PropTypes.string,
  studies: PropTypes.object.isRequired
};

export default TelecommunicationsExperience;
