import React, { useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { makeStyles } from '@material-ui/styles';
import {
  Button,
  Card,
  CardHeader,
  CardContent,
  Divider,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  colors
} from '@material-ui/core';

import { Label } from 'components';
import { PlatformEdit } from './components';

const useStyles = makeStyles(() => ({
  root: {},
  content: {
    padding: 0
  },
  inner: {
    minWidth: 40
  }
}));

const PlatformList = props => {
  const { id, className, data, platforms, ...rest } = props;

  const classes = useStyles();
  const [openEdit, setOpenEdit] = useState(false);
  const [platform, setPlatform] = useState({});
  const [namePlatform, setNamePlatform] = useState('');

  const handleEditOpen = (platform) => {
    setNamePlatform(platform);
    const obj = data[platform] || {};
    setPlatform({ ...obj });
    setOpenEdit(true);
  };

  const handleEditClose = () => {
    setOpenEdit(false);
  };

  const statusColors = {
    Pending: colors.orange[600],
    Active: colors.green[600],
    Rejected: colors.red[600]
  };
  React.useEffect(()=> {
    console.log(platforms)
    console.log(data)
  },[])
  return (
    <div
      {...rest}
      className={clsx(classes.root, className)}
    >
      <Card>
        <CardHeader
          title="Listado"
        />
        <Divider />
        <CardContent className={classes.content}>
          <PerfectScrollbar>
            <div className={classes.inner}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Nombre</TableCell>
                    <TableCell>Usuario</TableCell>
                    <TableCell>Contraseña</TableCell>
                    <TableCell>Ultima Actualización</TableCell>
                    <TableCell>Estado</TableCell>
                    <TableCell align="right">Acciones</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {platforms.map((platform, i) => (
                    <TableRow key={i}>
                      <TableCell>{platform}</TableCell>
                      <TableCell>{data ? data[platform]?.user : 'Sin Usuario'}</TableCell>
                      <TableCell>{data ? data[platform]?.password : 'Sin Contraseña'}</TableCell>
                      <TableCell>
                        {data ? data[platform]?.date : ''}
                      </TableCell>
                      <TableCell>
                        <Label
                          color={statusColors[data[platform]?.status || 'Pending']}
                          variant="outlined"
                        >
                          {data[platform]?.status || 'Pending'}
                        </Label>
                      </TableCell>
                      <TableCell align="right">
                        <Button
                          color="primary"
                          onClick={() => handleEditOpen(platform)}
                          size="small"
                          variant="outlined"
                        >
                          Editar
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))}
                  <PlatformEdit
                    id={id}
                    platform={platform}
                    name={namePlatform}
                    onClose={handleEditClose}
                    open={openEdit}
                  />
                </TableBody>
              </Table>
            </div>
          </PerfectScrollbar>
        </CardContent>
      </Card>
    </div>
  );
};

PlatformList.propTypes = {
  className: PropTypes.string
};

export default PlatformList;
