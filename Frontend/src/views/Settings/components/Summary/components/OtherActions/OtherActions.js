import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  Button,
  Divider,
  Typography
} from '@material-ui/core';
import NotInterestedIcon from '@material-ui/icons/NotInterested';
import DeleteIcon from '@material-ui/icons/DeleteOutline';
import { RemoveUser } from './components';
const useStyles = makeStyles(theme => ({
  root: {},
  mainActions: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start'
  },
  notice: {
    marginTop: theme.spacing(1)
  },
  deleteButton: {
    marginTop: theme.spacing(1),
    color: theme.palette.white,
    backgroundColor: theme.palette.error.main,
    '&:hover': {
      backgroundColor: theme.palette.error.dark
    }
  },
  buttonIcon: {
    marginRight: theme.spacing(1)
  }
}));
const OtherActions = props => {
  const { className, id, ...rest } = props;

  const [openDeleteUser, setOpenDeleteUser] = useState(false);

  const classes = useStyles();

  const handleUserOpen = () => {
    setOpenDeleteUser(true);
  };

  const handleUserClose = () => {
    setOpenDeleteUser(false);
  };

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader title="Otras acciones" />
      <Divider />
      <CardContent>
        <div className={classes.mainActions}>
          <Button>
            <NotInterestedIcon className={classes.buttonIcon} />
            Inhabilitar cuenta
          </Button>
        </div>
        <Typography
          className={classes.notice}
          variant="body2"
        >
          Precaución! Eliminar los datos de este usuario no podrá ser recuperados en el futuro.
        </Typography>
        <Button
          className={classes.deleteButton}
          onClick={handleUserOpen}
        >
          <DeleteIcon className={classes.buttonIcon} />
          Eliminar cuenta de usuario
        </Button>
      </CardContent>
      <RemoveUser 
        id={id}
        onClose={handleUserClose}
        open={openDeleteUser}
        
      />
    </Card>
  );
};

OtherActions.propTypes = {
  className: PropTypes.string
};

export default OtherActions;
