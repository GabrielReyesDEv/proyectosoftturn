import React from 'react';
import PropTypes from 'prop-types';
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Button,
} from '@material-ui/core';
import useRouter from 'utils/useRouter';
import { useTranslation } from 'react-i18next';
import axios from 'utils/axios'
import getAuthorization from 'utils/getAuthorization';

const RemoveUser = props => {
  const { id, onClose, open } = props;
  const { t } = useTranslation();

  const router = useRouter();

  const handleConfirm = async (event) => {
    event.preventDefault();
    try {
      const consult = await axios.delete(`/users/${id}`, getAuthorization())
      if (consult.status === 200) router.history.go(0);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <Dialog
      onClose={onClose}
      open={open}
    >
      <DialogTitle id="alert-dialog-title">{t('caution')}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {`${t('are you sure to delete the user')}`}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button
          color="secondary"
          onClick={onClose}
        >
          {t('Disagree')}
        </Button>
        <Button
          autoFocus
          color="primary"
          onClick={handleConfirm}
        >
          {t('Agree')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

RemoveUser.displayName = 'RemoveUser';

RemoveUser.propTypes = {
  className: PropTypes.string,
  id: PropTypes.any,
  onClose: PropTypes.func,
  open: PropTypes.bool
};

RemoveUser.defaultProps = {
  open: false,
};

export default RemoveUser;
