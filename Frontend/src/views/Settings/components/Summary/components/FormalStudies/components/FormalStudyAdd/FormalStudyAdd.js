import React, { useState } from 'react';
import clsx from 'clsx';
import moment from 'moment';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Modal,
  Card,
  CardContent,
  CardActions,
  Grid,
  Typography,
  TextField,
  Button,
  colors
} from '@material-ui/core';
import { DatePicker } from '@material-ui/pickers';
import useRouter from 'utils/useRouter';
import { useTranslation } from 'react-i18next';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';


let running = true;

const useStyles = makeStyles(theme => ({
  root: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    outline: 'none',
    boxShadow: theme.shadows[20],
    width: 700,
    maxHeight: '100%',
    overflowY: 'auto',
    maxWidth: '100%'
  },
  container: {
    marginTop: theme.spacing(3)
  },
  actions: {
    justifyContent: 'flex-end'
  },
  saveButton: {
    color: theme.palette.white,
    backgroundColor: colors.green[600],
    '&:hover': {
      backgroundColor: colors.green[900]
    }
  },
  dateField: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  }
}));

const FormalStudyAdd = props => {
  const { id, className, onClose,  open, platform, ...rest } = props;
  const { t } = useTranslation();

  const classes = useStyles();
  const router = useRouter();
 
  const [formState, setFormState] = useState({});
  const [calendarTrigger, setCalendarTrigger] = useState(null);

  if (!open) {
    !running && (running = true)
    return null;
  } else {
    running && setFormState({ ...platform, year: moment().format('YYYY') })
    running = false;
  }

  const handleFieldChange = event => {
    event.persist();
    setFormState(formState => ({
      ...formState,
      [event.target.name]:
        event.target.type === 'checkbox'
          ? event.target.checked
          : event.target.value
    }));
  };

  const handleCalendarOpen = trigger => {
    setCalendarTrigger(trigger);
  };

  const handleCalendarChange = () => {};

  const handleCalendarAccept = date => {
    setFormState(formState => ({
      ...formState,
      [calendarTrigger]: moment(date).format('YYYY')
    }));
  };

  const handleCalendarClose = () => {
    setCalendarTrigger(false);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const consult = await axios.put(`/users/${id}`,{ field: '_id', '$push': { studies: formState}}, getAuthorization() )

      if (consult.status === 200) router.history.go(0);
    } catch (error) {
      console.error(error);
    }
    
  };

  const calendarOpen = Boolean(calendarTrigger);
  const calendarValue = formState[calendarTrigger];
  const statusOptions = ['finished', 'inProgress', 'suspended'];
 

  return (
    <Modal
      onClose={onClose}
      open={open}
    >
      <Card
        {...rest}
        className={clsx(classes.root, className)}
      >
        <form>
          <CardContent>
            <Typography
              align="center"
              gutterBottom
              variant="h3"
            >
              Agregar Estudio Formal
            </Typography>
            <Grid
              className={classes.container}
              container
              spacing={3}
            >
              <Grid
                item
                md={12}
                xs={12}
              >
                <TextField
                  fullWidth
                  label={t('institution')}
                  name="institution"
                  onChange={handleFieldChange}
                  required
                  value={formState.institution}
                  variant="outlined"
                />
              </Grid>
              <Grid
                item
                md={6}
                xs={12}
              >
                <TextField
                  fullWidth
                  label={t('status')}
                  name="status"
                  onChange={event => handleFieldChange(event, 'status', event.target.value)}
                  required
                  select
                  // eslint-disable-next-line react/jsx-sort-props
                  SelectProps={{ native: true }}
                  value={formState.status||''}
                  variant="outlined"
                >
                  <option
                    disabled
                    value=""
                  />
                  {statusOptions.map(option => (
                    <option
                      key={option}
                      value={option}
                    >
                      {t(option)}
                    </option>
                  ))}
                </TextField>
              </Grid>
              <Grid
                item
                md={6}
                xs={12}
              >
                <TextField
                  fullWidth
                  className={classes.dateField}
                  label={t('year')}
                  name="year"
                  onClick={() => handleCalendarOpen('year')}
                  required
                  value={moment(formState.year).format('YYYY')}
                  variant="outlined"
                />
              </Grid>
              <Grid
                item
                md={12}
                xs={12}
              >
                <TextField
                  fullWidth
                  label={t('course')}
                  name="courseName"
                  onChange={handleFieldChange}
                  required
                  value={formState.courseName}
                  variant="outlined"
                />
              </Grid>
              <Grid
                item
                md={12}
                xs={12}
              >
                <TextField
                  fullWidth
                  label={t('titleAwarded')}
                  name="titleAwarded"
                  onChange={handleFieldChange}
                  required
                  value={formState.titleAwarded}
                  variant="outlined"
                />
              </Grid>
            </Grid>
            
            <TextField
              type="hidden"
              name="name"
              value={formState.name}
            />
          </CardContent>
          <CardActions className={classes.actions}>
            <Button
              onClick={onClose}
              variant="contained"
            >
              {t('close')}
            </Button>
            <Button
              className={classes.saveButton}
              onClick={handleSubmit}
              type='submit'
              variant="contained"
            >
              {t('save')}
            </Button>
          </CardActions>
          <DatePicker
            views={["year"]}
            maxDate={moment()}
            onAccept={handleCalendarAccept}
            onChange={handleCalendarChange}
            onClose={handleCalendarClose}
            open={calendarOpen}
            style={{ display: 'none' }} // Temporal fix to hide the input element
            value={calendarValue}
            variant="dialog"
          />
        </form>
      </Card>
    </Modal>
  );
};

FormalStudyAdd.displayName = 'FormalStudyAdd';

FormalStudyAdd.propTypes = {
  className: PropTypes.string,
  platform: PropTypes.any,
  onClose: PropTypes.func,
  open: PropTypes.bool
};

FormalStudyAdd.defaultProps = {
  open: false,
  onClose: () => {}
};

export default FormalStudyAdd;
