import React, { useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Modal,
  Card,
  CardContent,
  CardActions,
  Grid,
  Typography,
  TextField,
  MenuItem,
  Button,
  colors
} from '@material-ui/core';
import useRouter from 'utils/useRouter';
import getAuthorization from 'utils/getAuthorization';
import axios from 'utils/axios';

let running = true;

const useStyles = makeStyles(theme => ({
  root: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    outline: 'none',
    boxShadow: theme.shadows[20],
    width: 700,
    maxHeight: '100%',
    overflowY: 'auto',
    maxWidth: '100%'
  },
  container: {
    marginTop: theme.spacing(3)
  },
  actions: {
    justifyContent: 'flex-end'
  },
  saveButton: {
    color: theme.palette.white,
    backgroundColor: colors.green[600],
    '&:hover': {
      backgroundColor: colors.green[900]
    }
  },
  dateField: {
    '& + &': {
      marginLeft: theme.spacing(2)
    }
  }
}));

const PlatformEdit = props => {
  const { id, open, onClose, platform, className, ...rest } = props;

  const classes = useStyles();
  const router = useRouter();
 
  const [formState, setFormState] = useState({});

  if (!open) {
    !running && (running = true)
    return null;
  } else {
    running && setFormState({ ...platform })
    running = false;
  }

  const handleFieldChange = event => {
    event.persist();
    let reset = {};
    if (event.target.name === 'area') reset = { role: '' };
    setFormState(formState => ({
      ...formState,
      ...reset,
      [event.target.name]:
        event.target.type === 'checkbox'
          ? event.target.checked
          : event.target.value
    }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const consult = await axios.put(`/users/${id}`,{ field: '_id', '$push': { roles: formState}}, getAuthorization() )
      if (consult.status === 200) router.history.go(0);
    } catch (error) {
      console.error(error);
    }
  }

  const areas = [
    {
      value: 'Pymes',
      label: 'Pymes',
    },
    {
      value: 'Configuracion',
      label: 'Configuracion',
    }
  ];  

  const roles = {
    Pymes: [
      {
        value: 'Ejecutor',
        label: 'Ejecutor',
      },
      {
        value: 'Dispatcher',
        label: 'Dispatcher',
      }
    ],
    Configuracion: [
      {
        value: 'Ejecutor',
        label: 'Ejecutor',
      },
      {
        value: 'Dispatcher',
        label: 'Dispatcher',
      },
      {
        value: 'Coordinator',
        label: 'Coordinator',
      }
    ],
    empty: [{
      value: '',
      label: '',
    }]
  }; 
  return (
    <Modal
      onClose={onClose}
      open={open}
    >
      <Card
        {...rest}
        className={clsx(classes.root, className)}
      >
        <form>
          <CardContent>
            <Typography
              align="center"
              gutterBottom
              variant="h3"
            >
              Agregar Role
            </Typography>
            <Grid
              className={classes.container}
              container
              spacing={3}
            >
              <Grid
                item
                md={6}
                xs={12}
              >
                <TextField
                  fullWidth
                  helperText="Please select your currency"
                  id="filled-select-currency"
                  label="Select"
                  name="area"
                  onChange={handleFieldChange}
                  required
                  select
                  value={formState.area || ''}
                  variant="outlined"
                >
                  <MenuItem
                    disabled
                    key=""
                    value=""
                  />
                  {areas.map((option) => (
                    <MenuItem
                      key={option.value}
                      value={option.value}
                    >
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>
              <Grid
                item
                md={6}
                xs={12}
              >
                <TextField
                  fullWidth
                  helperText="Please select your currency"
                  id="filled-select-currency"
                  label="Select"
                  name="role"
                  onChange={handleFieldChange}
                  required
                  select
                  value={formState.role || ''}
                  variant="outlined"
                >
                  <MenuItem
                    disabled
                    key=""
                    value=""
                  />
                  {roles[formState?.area || 'empty'].map((option) => (
                    <MenuItem
                      key={option.value}
                      value={option.value}
                    >
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>
            </Grid>
            <TextField
              name="name"
              type="hidden"
              value={formState.name}
            />
          </CardContent>
          <CardActions className={classes.actions}>
            <Button
              onClick={onClose}
              variant="contained"
            >
              Cerrar
            </Button>
            <Button
              className={classes.saveButton}
              onClick={handleSubmit}
              type="submit"
              variant="contained"
            >
              Guardar
            </Button>
          </CardActions>
        </form>
      </Card>
    </Modal>
  );
};

PlatformEdit.displayName = 'PlatformEdit';

PlatformEdit.propTypes = {
  className: PropTypes.string,
  platform: PropTypes.any,
  onClose: PropTypes.func,
  open: PropTypes.bool
};

PlatformEdit.defaultProps = {
  open: false,
  onClose: () => {}
};

export default PlatformEdit;
