import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Grid,
  Button,
  Divider,
  TextField,
  colors
} from '@material-ui/core';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';
import useRouter from 'utils/useRouter';

const useStyles = makeStyles(theme => ({
  root: {},
  saveButton: {
    color: theme.palette.white,
    backgroundColor: colors.green[600],
    '&:hover': {
      backgroundColor: colors.green[900]
    }
  }
}));

const Security = props => {
  const { id, className, ...rest } = props;

  const classes = useStyles();
  const { history } = useRouter();

  const [values, setValues] = useState({
    password: '',
    confirm: ''
  });

  const handleChange = event => {
    setValues({
      ...values,
      [event.target.name]: event.target.value
    });
  };

  const savePassword = async () => {
    console.log('values: ', values);
    const { password } = values;
    try {
      const consult = await axios.put(`/users/${id}`,  { field: '_id', password} , getAuthorization())

      if (consult.status === 200) history.push('/auth/login');
    } catch (error) {
      console.error(error);
    }
  };

  const valid = values.password && values.password === values.confirm;

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader title="Cambiar Contraseña" />
      <Divider />
      <CardContent>
        <form>
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              md={4}
              sm={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Contraseña"
                name="password"
                onChange={handleChange}
                type="password"
                value={values.password}
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={4}
              sm={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Confirmar Contraseña"
                name="confirm"
                onChange={handleChange}
                type="password"
                value={values.confirm}
                variant="outlined"
              />
            </Grid>
          </Grid>
        </form>
      </CardContent>
      <Divider />
      <CardActions>
        <Button
          className={classes.saveButton}
          disabled={!valid}
          onClick={savePassword}
          variant="contained"
        >
          Guardar Cambios
        </Button>
      </CardActions>
    </Card>
  );
};

Security.propTypes = {
  className: PropTypes.string,
  id: PropTypes.string.isRequired
};

export default Security;
