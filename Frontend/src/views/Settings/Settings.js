import React, { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import Cookies from 'js-cookie';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { colors, Divider, LinearProgress, Tab, Tabs, } from '@material-ui/core';
import { useTranslation } from 'react-i18next';

import { Page } from 'components';
import { Header, General, Experience, Platforms, Security } from './components';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  tabs: {
    marginTop: theme.spacing(3)
  },
  divider: {
    backgroundColor: colors.grey[300]
  },
  content: {
    marginTop: theme.spacing(3)
  }
}));

const UserManagementDetails = props => {
  const { match, history } = props;
  const { t } = useTranslation();
  const classes = useStyles();
  const { id, tab } = match.params;

  const [loading, setLoading] = useState(1);
  const [data, setData] = useState(null);

  useEffect(() => {
    let mounted = true;
    const fetchData = async () => {
      try {
        const petition = await axios.get(`/users?limit=1&page=1&identification=${Cookies.get('identification')}`, getAuthorization());
        if(petition.status === 200 && mounted) {
          setData(petition.data.data[0]);
          setLoading(0);
        }
      } catch (error) {
        console.error(error);
      }
    };

    fetchData();
  },[id]);

  const handleTabsChange = (event, value) => {
    history.push(value);
  };

  const tabs = [
    { value: 'general', label: t('general') },
    { value: 'experience', label: t('experience') },
    { value: 'platforms', label: t('platforms') },
    { value: 'security', label: t('security') }
  ];

  if (!tab) {
    return <Redirect to={'/settings/general'} />;
  }

  if (!tabs.find(t => t.value === tab)) {
    return <Redirect to="/errors/error-404" />;
  }

  return (
    <Page
      className={classes.root}
      title={t('userManagementDetails')}
    >
      <Header user={data} />
      {loading ? (
        <LinearProgress />
      ): ''}
      <Tabs
        className={classes.tabs}
        onChange={handleTabsChange}
        scrollButtons="auto"
        value={tab}
        variant="scrollable"
      >
        {tabs.map(tab => (
          <Tab
            key={tab.value}
            label={tab.label}
            value={tab.value}
          />
        ))}
      </Tabs>
      <Divider className={classes.divider} />
      {<div className={classes.content}>
        {tab === 'general' && <General info={data}/>}
        {tab === 'experience' && <Experience
          experience={data}
          id={data && data._id}
        />}
        {tab === 'platforms' && <Platforms platforms={data}/>}
        {tab === 'security' && <Security id={data?._id}/>}
      </div>}
    </Page>
  );
};

UserManagementDetails.propTypes = {
  history: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired
};

export default UserManagementDetails;
