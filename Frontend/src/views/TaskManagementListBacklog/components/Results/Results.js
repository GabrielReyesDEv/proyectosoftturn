import React, { useState, useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { connect, useDispatch } from 'react-redux';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import CloseIcon from '@material-ui/icons/Close';
import { useSnackbar } from 'notistack';
import { makeStyles } from '@material-ui/styles';
import {
  Avatar,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Checkbox,
  Divider,
  Button,
  IconButton,
  Link,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Tooltip,
  Typography,
  LinearProgress,
  colors
} from '@material-ui/core';

import ScheduleIcon from '@material-ui/icons/Schedule';
import { Label, ReviewStars, GenericMoreButton, TableAssignBar } from 'components';
import { Application, StatusChange, Logs } from './components';
import getTimeStatus from 'utils/getTimeStatus';
import formatDate from 'utils/formatDate';
import moment from 'moment';
import duration from 'utils/duration';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';
import Cookies from 'js-cookie';
import ListAltIcon from '@material-ui/icons/ListAlt';
import { useTranslation } from 'react-i18next';
import { tasksStatus } from 'actions';


const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  inner: {
    minWidth: 700
  },
  nameCell: {
    display: 'flex',
    alignItems: 'center'
  },
  avatar: {
    height: 42,
    width: 42,
    marginRight: theme.spacing(1)
  },
  actions: {
    padding: theme.spacing(1),
    justifyContent: 'flex-end'
  },
  icon: {
    fontSize: 18,
    height: 18,
    width: 18,
    paddingRight: '3px'
  },
}));

const Results = props => {
  const { className, data, assignment, taskStatus, res, ...rest } = props;

  const classes = useStyles();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();  

  const [tasks, setTasks] = useState(data);
  const [selectedTasks, setSelectedTasks] = useState([]);
  const [openApplication, setOpenApplication] = useState(false);
  const [openStatusChange, setOpenStatusChange] = useState(false);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [oneTask, setOneTask] = useState({});
  const [openLogs, setOpenLogs] = useState(false);

  const handleSelectAll = event => {
    const selectedTasks = event.target.checked
      ? tasks.map(task => task.id)
      : [];

    setSelectedTasks(selectedTasks);
  };

  const fetchTasks = async (page, limit) => {
    axios.post('tasks/pagination', {
      "page": page + 1, "limit": limit,
      "filterFechas": [{"field": "deadlineDate", "values": ['null', `${res.today}`]}],
      "eq": [{"field": "area", "values": res.areas}, {"field": "group", "values": res.groups},{"field": "author", "values": res.authors}],
      "neq": [{"field": "hide", "values": [true]}, {"field": "zolidStatus", "values": ["Terminated"]}]
    }, getAuthorization()).then(response => {
      setTasks(response.data);
      setPage(response.data.page - 1);
      setRowsPerPage(response.data.per_page);
    });
  };

  const handleSelectOne = (event, id) => {
    const selectedIndex = selectedTasks.indexOf(id);
    let newSelectedTasks = [];

    if (selectedIndex === -1) {
      newSelectedTasks = newSelectedTasks.concat(selectedTasks, id);
    } else if (selectedIndex === 0) {
      newSelectedTasks = newSelectedTasks.concat(
        selectedTasks.slice(1)
      );
    } else if (selectedIndex === selectedTasks.length - 1) {
      newSelectedTasks = newSelectedTasks.concat(
        selectedTasks.slice(0, -1)
      );
    } else if (selectedIndex > 0) {
      newSelectedTasks = newSelectedTasks.concat(
        selectedTasks.slice(0, selectedIndex),
        selectedTasks.slice(selectedIndex + 1)
      );
    }

    setSelectedTasks(newSelectedTasks);
  };

  const handleChangePage = (event, page) => {
    setPage(page);
    fetchTasks(page, rowsPerPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(event.target.value);
    fetchTasks(page, event.target.value);
  };

  const handleApplicationOpen = (tsk) => {
    setOneTask(tsk)
    setOpenApplication(true);
  };
  const handleApplicationClose = () => {
    setOpenApplication(false);
  };

  const handleStatusChangeOpen = (task) => {
    dispatch(tasksStatus({status: task.status, current: task.zolidStatus, logs: task.logs, task}));
    setOpenStatusChange(true);
  };

  const handleStatusChangeClose = () => {
    setOpenStatusChange(false);
  };

  const handleLogsOpen = (task) => {
    dispatch(tasksStatus({status: task.status, current: task.zolidStatus, logs: task.logs, task}));
    setOpenLogs(true);
  };

  const handleLogsClose = () => {
    setOpenLogs(false);
  };

  const taskStatusColors = {
    canceled: colors.grey[600],
    warning: colors.orange[600],
    onTime: colors.green[600],
    outOfTime: colors.red[600]
  };

  const statusColors = {
    Assigned: colors.orange[600],
    Canceled: colors.red[600],
    Terminated: colors.green[600],
    Created: colors.grey[600]
  };

  useEffect(() => {
    setTasks(data);
    setPage(data.page - 1);
    setRowsPerPage(data.per_page);

    const handleDismiss = () => {
      closeSnackbar();
    };

    const action = (
      <IconButton className={classes.expand} onClick={handleDismiss}>
        <CloseIcon />
      </IconButton>
    );
  
    if (data.total > 0) {
      enqueueSnackbar('Finaliza tus tareas antes de actualizar una nueva', {
        variant: 'warning',
        action
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  const handleAsignClick = async (usuario, task) => {
    const nombre = Cookies.get('firstName');
    const apellido = Cookies.get('lastName');
    const identificacion = Cookies.get('identification');
    const idObject = task._id;
    const newlog = {
      status: 'Asignación#AS',
      createdAt: moment().format('YYYY-MM-DD HH:mm'),
      description: `Tarea asignada a Ejecutor ${usuario.split('#')[0]} othId: ${task.activityId}`,
      author: `${nombre} ${apellido}#${identificacion}`,
    }
    const logAux = task.logs ? task.logs : [];
    logAux.push(newlog);
    if (usuario === '') {
      enqueueSnackbar(`Debe seleccionar un Usuario.`, {
        variant: 'error',
      });
    } else {
      delete task._id;
      await axios.put(`/tasks/${idObject}`,
                {
                  field: '_id',
                  author: usuario,
                  message: `Tarea asignada a ${usuario.split('#')[0]}`,
                  logs: logAux,
                  role: 'Executor',
                },
                getAuthorization())
                .then(() => {
                    enqueueSnackbar(`Tarea asignada a ${usuario.split('#')[0]}`, {
                      variant: 'success',
                    });
                    window.location.href = '/management/tasks';
                })
                .catch((err) => {
                  enqueueSnackbar(`error: ${err}`, {
                    variant: 'error',
                  });
                })
                setOpenApplication(false);
    }
  };

  return (
    <div
      {...rest}
      className={clsx(classes.root, className)}
    >
      
      <Typography
        color="textSecondary"
        gutterBottom
        variant="body2"
      >
        {tasks.total} Registros encontrados. Página {page + 1} de {tasks.total_pages}
      </Typography>
      <Card>
        <CardHeader
          action={<GenericMoreButton />}
          title="Todas las Actividades"
        />
        <Divider />
        <CardContent className={classes.content}>
          <PerfectScrollbar>
            <div className={classes.inner}>
              <Table>
                <TableHead>
                  <TableRow>
                    {
                      assignment?.show && (<TableCell padding="checkbox">
                      <Checkbox
                        checked={selectedTasks.length === tasks.length}
                        color="primary"
                        indeterminate={
                          selectedTasks.length > 0 &&
                          selectedTasks.length < tasks.length
                        }
                        onChange={handleSelectAll}
                      />
                    </TableCell>)
                    }
                    
                    <TableCell></TableCell>
                    <TableCell align="center">Información</TableCell>
                    <TableCell align="center">Fecha de Entrega</TableCell>
                    <TableCell align="center">Fecha de Creación</TableCell>
                    <TableCell align="center">Responsable</TableCell>
                    <TableCell align="center">Estado</TableCell>
                    <TableCell align="center">Terminado</TableCell>
                    <TableCell align="center">Actions</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {tasks.data?.map(task => (
                    <TableRow
                      hover
                      key={task.id}
                    >
                      {
                      assignment?.show && (
                        <TableCell padding="checkbox">
                          { task.status?.split("#")[1] !== 'AS' && (
                            <Checkbox
                              checked={
                                selectedTasks.indexOf(task.id) !== -1
                              }
                              color="primary"
                              onChange={event =>
                                handleSelectOne(event, task.id)
                              }
                              value={selectedTasks.indexOf(task.id) !== -1}
                            />
                          )}
                        </TableCell>
                      )
                      }
                      <TableCell align="center">
                        <ReviewStars value={task.priority} calendar={task.type} />
                      </TableCell>
                      <TableCell align="left">
                        <div className={classes.nameCell}>
                          <Tooltip title={task.status?.split("#")[0]} placement="left">
                            <Avatar
                              className={classes.avatar}
                              src={task.avatar}
                            >
                              {task.status?.split("#")[1]}
                            </Avatar>
                          </Tooltip>
                          <div>
                            <Typography variant="body2">
                               {task.activityId && `OTH: ${task.activityId}`}
                               {task.projectId && ` OTP: ${task.projectId}`}
                            </Typography>
                            <Tooltip title={'Registros'} placement="left">
                              <IconButton onClick={() => handleLogsOpen(task)} color={'primary'}>
                                <ListAltIcon />
                              </IconButton>
                            </Tooltip>
                            <Link
                              color="inherit"
                              component={RouterLink}
                              onClick={handleApplicationOpen}
                              variant="h6"
                            >
                              {task.orderType}
                            </Link>
                            <Typography variant="body2">
                              <Label
                                color={taskStatusColors[getTimeStatus(task.deadlineDate, task.finishDate && task.finishDate)]}
                                variant="outlined"
                              >
                                <ScheduleIcon className={classes.icon}/>{t(getTimeStatus(task.deadlineDate, task.finishDate && task.finishDate))}
                              </Label>
                            </Typography>
                          </div>
                        </div>
                      </TableCell>
                      <TableCell align="center">
                        {formatDate(task.deadlineDate).date}
                        <Typography variant="h6">
                        {formatDate(task.deadlineDate).time}
                        </Typography>
                      </TableCell>
                      <TableCell align="center">
                        {formatDate(task.createdAt).date}
                        <Typography variant="h6">
                        {formatDate(task.createdAt).time}
                        </Typography>
                      </TableCell>
                      <TableCell align="center">
                        <Typography
                          variant="h6"
                        >
                          {task.author?.split('#')[0]}
                        </Typography>
                      </TableCell>
                      <TableCell align="center">
                        <Typography
                          style={{ color: statusColors[task.zolidStatus] }}
                          variant="h6"
                        >
                          {t(task.zolidStatus)}
                          <Typography>
                          {task.finishDate && `${formatDate(task.finishDate).date} ${formatDate(task.finishDate).time}`}
                          </Typography>
                        </Typography>
                      </TableCell>
                      <TableCell align="center">
                        {duration(task.createdAt, task.finishDate)}
                      </TableCell>
                      <TableCell align="center">
                      {task.zolidStatus !== 'Terminated' && (
                          task.status.split("#")[1] === 'AS' ? (
                            <Button
                              color="primary"
                              size="small"
                              onClick={handleApplicationOpen}
                              variant="outlined"
                            >
                              Asignar
                            </Button>
                          ) : (
                            <Button
                              color="primary"
                              size="small"
                              onClick={() => handleStatusChangeOpen(task)}
                              variant="outlined"
                            >
                              Iniciar
                            </Button>
                          )
                        )}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
              {props.loading ? (
                <LinearProgress />
              ): ''}
              { openApplication && (
                <Application 
                  onApply={handleAsignClick}
                  onClose={handleApplicationClose}
                  open={openApplication}
                  task={oneTask}
                />
              )}
              { (openStatusChange && taskStatus) && (
                <StatusChange 
                  onApply={handleStatusChangeClose}
                  onClose={handleStatusChangeClose}
                  open={openStatusChange}
                  currentStatus={taskStatus.current}
                  type={taskStatus.status}
                  logs={taskStatus.logs}
                  task={taskStatus.task}
                />
              )}
              { (openLogs) && (
                <Logs 
                  onApply={handleLogsClose}
                  onClose={handleLogsClose}
                  open={openLogs}
                  currentStatus={taskStatus.current}
                  type={taskStatus.status}
                  logs={taskStatus.logs}
                  task={taskStatus.task}
                />
              )}
            </div>
          </PerfectScrollbar>
        </CardContent>
        <CardActions className={classes.actions}>
          <TablePagination
            component="div"
            count={tasks.total}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
            page={page}
            rowsPerPage={rowsPerPage}
            rowsPerPageOptions={[10, 25, 50]}
            labelDisplayedRows={
              ({ from, to, count }) => {
                return '' + from + '-' + to + ' de ' + count
              }
            }
            labelRowsPerPage={'Registros por página:'}
          />
        </CardActions>
      </Card>
      <TableAssignBar selected={selectedTasks} />
    </div>
  );
};

Results.propTypes = {
  className: PropTypes.string,
  tasks: PropTypes.array.isRequired
};

Results.defaultProps = {
  tasks: []
};

const mapStateToProps = (state) => {
  return {
    assignment: state.assignment,
    taskStatus: state.tasks.taskStatus
  };
};

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(Results);
