import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Button,
  CircularProgress,
  Dialog,
  TextField,
  Typography,
  colors
} from '@material-ui/core';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';

const useStyles = makeStyles(theme => ({
  root: {
    width: 960
  },
  header: {
    padding: theme.spacing(3),
    maxWidth: 720,
    margin: '0 auto'
  },
  content: {
    padding: theme.spacing(0, 2),
    maxWidth: 720,
    margin: '5% auto'
  },
  helperText: {
    textAlign: 'right',
    marginRight: 0
  },
  author: {
    margin: theme.spacing(4, 0),
    display: 'flex'
  },
  avatar: {
    marginRight: theme.spacing(2)
  },
  actions: {
    backgroundColor: colors.grey[100],
    padding: theme.spacing(2),
    display: 'flex',
    justifyContent: 'center'
  },
  loader: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  applyButton: {
    color: theme.palette.white,
    backgroundColor: colors.green[600],
    '&:hover': {
      backgroundColor: colors.green[900]
    }
  }
}));

const Application = props => {
  const { open, onClose, onApply, className, task, ...rest } = props;

  const [value, setValue] = useState('');
  const [executors, setExecutors] = useState(null);

  const classes = useStyles();

  useEffect(() => {
    let mounted = true;
    
    const fetchStatus = () => {
      axios.post('/users/pagination', {
        'status': 'Active',
        'roles': {
          '$elemMatch': { 'group': task.group, 'area': task.area, 'role' : 'Ejecutor' }
        }
      }, getAuthorization()).then(response => {

        if (mounted) {
          setValue(response.data?.data && response.data?.data[0].data.length > 0 && response.data.data[0].data ? 
            `${response.data.data[0].data[0].firstName} ${response.data.data[0].data[0].lastName}#${response.data.data[0].data[0].identification}`:
            '');
          setExecutors(response.data?.data && response.data?.data[0].data.length > 0 && response.data.data[0].data ? response.data.data[0].data : [])
        }
      });
    };

    fetchStatus();
    

    return () => {
      mounted = false;
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [open]);

  const handleChange = event => {
    event.persist();
    setValue(event.target.value);
  };

  return (
    <Dialog
      maxWidth="lg"
      onClose={onClose}
      open={open}
    >
      <div
        {...rest}
        className={clsx(classes.root, className)}
      >
        <div className={classes.header}>
          <Typography
            align="center"
            className={classes.title}
            gutterBottom
            variant="h3"
          >
            Asignar Tarea
          </Typography>
          <Typography
            align="center"
            className={classes.subtitle}
            variant="subtitle2"
          >
            Selecciona un ejecutor de la lista.
          </Typography>
        </div>
        <div className={classes.content}>
          { executors ? (
            <TextField
              fullWidth
              label="Ejecutores"
              name="executors"
              onChange={event =>
                handleChange(
                  event,
                  'executors',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={value.name}
              variant="outlined"
            >
              <option
                disabled
                value=""
              />
              {executors.map(option => (
                <option
                  key={option.identification}
                  value={`${option.firstName} ${option.lastName}#${option.identification}`}
                >
                  {`${option.firstName} ${option.lastName}`}
                </option>
              ))}
            </TextField>) : (
            <div className={classes.loader}>
              <CircularProgress color="secondary" />
              <Typography>Buscando Ejecutores Activos...</Typography>
            </div>
          )
          }
        </div>
        <div className={classes.actions}>
          <Button
            className={classes.applyButton}
            onClick={() => onApply(value, task)}
            variant="contained"
          >
            Asignar
          </Button>
        </div>
      </div>
    </Dialog>
  );
};

Application.propTypes = {
  author: PropTypes.object.isRequired,
  className: PropTypes.string,
  onApply: PropTypes.func,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired,
  task: PropTypes.object,
};

export default Application;
