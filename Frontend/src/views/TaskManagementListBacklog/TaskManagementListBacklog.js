import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';

import axios from 'utils/axios';
import { Page, SearchBar } from 'components';
import { Header, Results } from './components';
import getAuthorization from 'utils/getAuthorization';
import moment from 'moment';
import Cookies from 'js-cookie';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  results: {
    marginTop: theme.spacing(3)
  }
}));

const ThirdPartiesManagementList = () => {
  const classes = useStyles();

  const [tasks, setTasks] = useState([]);
  const [loading, setLoading] = useState(1);
  const [resp, setResp] = useState({});
  useEffect(() => {
    let mounted = true;
    const fetchTasks = (authors = ['zolid#null', `${Cookies.get('firstName')} ${Cookies.get('lastName')}#${Cookies.get('identification')}`]) => {
      const today = moment().format('YYYY-MM-DD HH:mm');
      axios.get(`/users/info?identification=${Cookies.get('identification')}`, getAuthorization()).then((response) => {
        return response.data
      }).then((res) => {
        setResp({
          authors: authors,
          groups: res.data.groups,
          areas: res.data.areas,
          today,
        });
        axios.post('tasks/pagination', {
          "page": 1, "limit": 10,
          "filterFechas": [{"field": "deadlineDate", "values": ['null', `${today}`]}],
          "eq": [{"field": "area", "values": res.data.areas}, {"field": "group", "values": res.data.groups},{"field": "author", "values": authors}],
          "neq": [{"field": "hide", "values": [true]}, {"field": "zolidStatus", "values": ["Terminated"]}]
        }, getAuthorization()).then(response => {
          if (mounted) {
            setTasks(response.data)
            setLoading(0);
          }
        });
      })
      
    };
    fetchTasks();

    return () => {
      mounted = false;
    };
  }, []);

  const handleFilter = () => {};
  const handleSearch = () => {};

  return (
    <Page
      className={classes.root}
      title="Tareas Vencidas"
    >
      <Header />
      <SearchBar
        onFilter={handleFilter}
        onSearch={handleSearch}
      />
      {tasks && (
        <Results
          className={classes.results}
          data={tasks}
          res={resp}
          loading={loading}
          setLoading={setLoading}
        />
      )}
    </Page>
  );
};

export default ThirdPartiesManagementList;
