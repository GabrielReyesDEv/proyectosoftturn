import React, { Fragment, useState, useEffect } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import PerfectScrollbar from 'react-perfect-scrollbar';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import {
  Button,
  CircularProgress,
  Dialog,
  TextField,
  Typography,
  colors
} from '@material-ui/core';
import axios from 'utils/axios';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import formatDate from 'utils/formatDate';

let running = true;

// import getInitials from 'utils/getInitials';

const useStyles = makeStyles(theme => ({
  root: {
    width: 960
  },
  header: {
    padding: theme.spacing(3),
    maxWidth: 720,
    margin: '0 auto'
  },
  content: {
    padding: theme.spacing(0, 2),
    maxWidth: 720,
    margin: '5% auto'
  },
  helperText: {
    textAlign: 'right',
    marginRight: 0
  },
  list: {
    maxHeight: 320
  },
  author: {
    margin: theme.spacing(4, 0),
    display: 'flex'
  },
  avatar: {
    marginRight: theme.spacing(2)
  },
  actions: {
    backgroundColor: colors.grey[100],
    padding: theme.spacing(2),
    display: 'flex',
    justifyContent: 'center'
  },
  loader: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  textField: {
    marginTop: theme.spacing(2),
  },
  applyButton: {
    color: theme.palette.white,
    backgroundColor: colors.green[600],
    '&:hover': {
      backgroundColor: colors.green[900]
    }
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  }
}));

const Application = props => {
  const { logs, currentStatus, type, open, onClose, onApply, className, ...rest } = props;

  const classes = useStyles();

  const [value, setValue] = useState({name: currentStatus});
  const [status, setStatus] = useState(null);
  const [previousStatus, setPreviousStatus] = useState([]);
  const [expanded, setExpanded] = useState(false);

  const handleChange = event => {
    event.persist();
    setValue(event.target.value);
  };

  const handleChangeSelection = event => {
    event.persist();
    const prevStatus = logs.filter((log) => log.status === event.target.value)
    setPreviousStatus(prevStatus)
    setValue({ name: event.target.value })
  }

  const handleChangeMore = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  useEffect(() => {
    let mounted = true;
    
    const fetchStatus = () => {
      axios.get('/api/terceros/preloadedTasks').then(response => {
        if (mounted) {
          const values = response.data.preloadedTasks.filter((task) => task.type === type)
          setStatus(values[0]);
        }
      });
    };
    
    fetchStatus();

    return () => {
      mounted = false;
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const updateVariables = () => {
    const prevStatus = logs.filter((log) => log.status === currentStatus)
    const orderStatus = logs.find((log) => log.status === currentStatus)
    setPreviousStatus(prevStatus)
    setValue({ name: currentStatus, order: orderStatus })
  }

  if (!open) {
    !running && (running = true)
    return null;
  } else {
    running && updateVariables()
    running = false;
  }
  return (
    <Dialog
      maxWidth="lg"
      onClose={onClose}
      open={open}
    >
      <div
        {...rest}
        className={clsx(classes.root, className)}
      >
        <div className={classes.header}>
          <Typography
            align="center"
            className={classes.title}
            gutterBottom
            variant="h3"
          >
            Cambie el Estado {type}
          </Typography>
          <Typography
            align="center"
            className={classes.subtitle}
            variant="subtitle2"
          >
            Selecciona un estado de la lista.
          </Typography>
        </div>
        <div className={classes.content}>
          { status?.status ? (
          <div>
          <TextField
              fullWidth
              label={`${type}`}
              name="status"
              onChange={event =>
                handleChangeSelection(
                  event,
                  'status',
                  event.target.value
                )
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={value.name}
              variant="outlined"
            >
              <option
                value=""
                disabled
              />
              {status.status.map(option => (
                <option
                  value={`${option.value}`}
                >
                  {`${option.name}`}
                </option>
              ))}
            </TextField>
            <TextField
            autoFocus
            className={classes.textField}
            // eslint-disable-next-line react/jsx-sort-props
            FormHelperTextProps={{ classes: { root: classes.helperText } }}
            fullWidth
            helperText={`${200 - value.length} caracteres restantes`}
            label="Nota corta"
            multiline
            onChange={handleChange}
            placeholder="Ingrese una razón"
            rows={5}
            variant="outlined"
          />
          {previousStatus.length > 0 && (
            <Fragment>
              <Typography
                align="start"
                className={classes.title}
                gutterBottom
                variant="h6"
              >
                Estados Anteriores
              </Typography>
              <PerfectScrollbar options={{ suppressScrollX: true }}>
                {previousStatus.map((file, i) => (
                  <Accordion expanded={expanded === `panel${i}`} onChange={handleChangeMore(`panel${i}`)}>
                    <AccordionSummary
                      expandIcon={<ExpandMoreIcon />}
                      aria-controls="panel1bh-content"
                      id="panel1bh-header"
                    >
                      <Typography className={classes.heading}>{`${formatDate(file.createdAt).date} ${formatDate(file.createdAt).time}`}</Typography>
                      <Typography className={classes.secondaryHeading}>{`${file.author.split('#')[0]} / ${file.status}`}</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                      <Typography>
                        {file.note}
                      </Typography>
                    </AccordionDetails>
                  </Accordion>
                ))}
              </PerfectScrollbar>
            </Fragment>
          )}
          </div> 
          ) : (
              <div className={classes.loader}>
                <CircularProgress color="secondary" />
                <Typography>Buscando estados...</Typography>
              </div>
            )
          }
        </div>
        <div className={classes.actions}>
          <Button
            onClick={onClose}
            variant="contained"
          >
            Cerrar
          </Button>
          <Button
            className={classes.applyButton}
            onClick={onApply}
            variant="contained"
          >
            Guardar
          </Button>
        </div>
      </div>
    </Dialog>
  );
};

Application.propTypes = {
  author: PropTypes.object.isRequired,
  className: PropTypes.string,
  onApply: PropTypes.func,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired
};

export default Application;
