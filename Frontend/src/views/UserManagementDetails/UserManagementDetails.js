import React, { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { colors, Divider, LinearProgress, Tab, Tabs, } from '@material-ui/core';
import { useTranslation } from 'react-i18next';

import { Page } from 'components';
import { Header, Summary, Experience, Platforms, Logs } from './components';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  tabs: {
    marginTop: theme.spacing(3)
  },
  divider: {
    backgroundColor: colors.grey[300]
  },
  content: {
    marginTop: theme.spacing(3)
  }
}));

const UserManagementDetails = props => {
  const { match, history } = props;
  const { t } = useTranslation();
  const classes = useStyles();
  const { id, tab } = match.params;

  const [loading, setLoading] = useState(1);
  const [data, setData] = useState(null);

  useEffect(() => {
    let mounted = true;
    const fetchData = () => {
      axios.get(`/users/${id}`, getAuthorization()).then(response => {
        
        if (mounted) {
          setData(response.data.data);
          setLoading(0);
        }
      });
    };

    fetchData();
  },[id]);

  const handleTabsChange = (event, value) => {
    history.push(value);
  };

  const tabs = [
    { value: 'summary', label: t('summary') },
    { value: 'experience', label: t('experience') },
    { value: 'platforms', label: t('platforms') },
    { value: 'logs', label: t('logs') }
  ];

  if (!tab) {
    return <Redirect to={`/management/users/${id}/summary`} />;
  }

  if (!tabs.find(t => t.value === tab)) {
    return <Redirect to="/errors/error-404" />;
  }

  return (
    <Page
      className={classes.root}
      title={t('userManagementDetails')}
    >
      <Header user={data} />
      {loading ? (
        <LinearProgress />
      ): ''}
      <Tabs
        className={classes.tabs}
        onChange={handleTabsChange}
        scrollButtons="auto"
        value={tab}
        variant="scrollable"
      >
        {tabs.map(tab => (
          <Tab
            key={tab.value}
            label={tab.label}
            value={tab.value}
          />
        ))}
      </Tabs>
      <Divider className={classes.divider} />
      {<div className={classes.content}>
        {tab === 'summary' && <Summary user={data}/>}
        {tab === 'experience' && <Experience
          experience={data}
          id={id}
        />}
        {tab === 'platforms' && <Platforms platforms={data}/>}
        {tab === 'logs' && <Logs />}
      </div>}
    </Page>
  );
};

UserManagementDetails.propTypes = {
  history: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired
};

export default UserManagementDetails;
