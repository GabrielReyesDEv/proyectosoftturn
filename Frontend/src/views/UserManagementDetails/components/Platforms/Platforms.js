import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Grid
} from '@material-ui/core';

import { PlatformList } from './components';


const useStyles = makeStyles(() => ({
  root: {},
  content: {
    padding: 0
  },
  inner: {
    minWidth: 1150
  }
}));

const Platforms = props => {
  const { className, platforms, ...rest } = props;

  const classes = useStyles();

  const plataformas = ['pdsr', 'crm', 'eManagement', 'lyncCommunicator', 'netcraker', 'maximo', 'gponHw', 'gponZte', 'alcatel', 'softTelefonia', 'tacacs', 'denotherHendryx', 'ims', 'hst', 'remotoColaboration', 'remotoMoviles', 'msoftHuawei', 'cucdmHcs', 'solarwinds', 'acsTacacs', 'sharepointClaro'];

  if (!platforms) {
    return null;
  }

  return (
    <Grid
      {...rest}
      className={clsx(classes.root, className)}
      container
      spacing={3}
    >
      <Grid
        item
        lg={12}
        md={12}
        xl={12}
        xs={12}
      >
        <PlatformList
          id={platforms._id}
          data={platforms.platforms || ''}
          platforms={plataformas}
        />
      </Grid>
    </Grid>
  );
};

Platforms.propTypes = {
  className: PropTypes.string,
  platforms: PropTypes.object.isRequired
};

export default Platforms;
