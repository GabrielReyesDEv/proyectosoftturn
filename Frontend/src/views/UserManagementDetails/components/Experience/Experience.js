import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';
import clsx from 'clsx';

import { Page } from 'components';
import { Knowledge, TelecommunicationsExperience } from './components';

const useStyles = makeStyles(theme => ({
  
}));

const Experience = props => {
  const classes = useStyles();
  const { experience, id, className, ...rest } = props;

  if (!experience) {
    return null;
  }

  return (
    <Page
      {...rest}
      className={clsx(classes.root, className)}
      title="User Experience"
    >
      <Grid
        className={classes.container}
        container
        spacing={3}
      >
        <Grid
          item
          md={4}
          xl={3}
          xs={12}
        >
          <Knowledge
            id={id}
            knowledge={experience.knowledge}
          />
        </Grid>
        <Grid
          item
          md={8}
          xl={9}
          xs={12}
        >
          <TelecommunicationsExperience
            id={id}
            studies={experience.experience || []}
          />
        </Grid>
      </Grid>
    </Page>
  );
};

Experience.propTypes = {
  className: PropTypes.string
};

export default Experience;
