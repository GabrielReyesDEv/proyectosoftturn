import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { makeStyles } from '@material-ui/styles';
import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableRow,
  TableHead,
  colors
} from '@material-ui/core';
import {
  FormalStudyEdit,
  FormalStudyAdd
} from './components';
import AddIcon from '@material-ui/icons/Add';
import { useTranslation } from 'react-i18next';
import { Label } from 'components';

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  actions: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    '& > * + *': {
      marginLeft: 0
    }
  },
  action: {
    margin: 0
  },
  buttonIcon: {
    marginRight: theme.spacing(1)
  }
}));

const FormalStudies = props => {
  const { id, studies, className, ...rest } = props;
  const { t } = useTranslation();

  const classes = useStyles();
  const [openEdit, setOpenEdit] = useState(false);
  const [openAdd, setOpenAdd] = useState(false);
  const [study, setStudy] = useState({});
  const [position, setPosition] = useState(null);
  const handleAddOpen = () => {
    setOpenAdd(true);
  };

  const handleAddClose = () => {
    setOpenAdd(false);
  };

  const handleEditOpen = (study, position) => {
    setStudy({ ...study });
    setOpenEdit(true);
    setPosition(position);
  };

  const handleEditClose = () => {
    setOpenEdit(false);
  };

  const statusColors = {
    inProgress: colors.orange[600],
    finished: colors.green[600],
    suspended: colors.red[600]
  };

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader
        title={t('formalStudies')}
        action={
          <Button onClick={() => handleAddOpen()}>
            <IconButton aria-label="Add">
              <AddIcon />
            </IconButton>
          </Button>
        } 
        classes={{ action: classes.action }}
        className={classes.action}
      />
      <FormalStudyAdd
        id={id}
        onClose={handleAddClose}
        open={openAdd}
      />
      <FormalStudyEdit
        id={id}
        onClose={handleEditClose}
        open={openEdit}
        position = {position}
        study={study}
      />
      <Divider />
      <CardContent className={classes.content}>
        <PerfectScrollbar>
          <div className={classes.inner}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>{t('institution')}</TableCell>
                  <TableCell>{t('status')}</TableCell>
                  <TableCell>{t('year')}</TableCell>
                  <TableCell>{t('course')}</TableCell>
                  <TableCell>{t('titleAwarded')}</TableCell>
                  <TableCell>{t('actions')}</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {studies && studies.map((study, index) => (
                  <TableRow key={studies.id}>
                    <TableCell>{study.institution}</TableCell>
                    <TableCell>
                      <Label
                        color={statusColors[study.status]}
                        variant="outlined"
                      >
                        {t(study.status)}
                      </Label>
                    </TableCell>
                    <TableCell>{study.year}</TableCell>
                    <TableCell>{study.courseName}</TableCell>
                    <TableCell>{study.titleAwarded}</TableCell>
                    <TableCell>
                      <Button
                        color="primary"
                        onClick={() => handleEditOpen(study, index)}
                        size="small"
                        variant="outlined"
                      >
                        {t('edit')}
                      </Button>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </div>
        </PerfectScrollbar>
      </CardContent>
    </Card>

  );
};

FormalStudies.propTypes = {
  className: PropTypes.string,
  studies: PropTypes.object.isRequired
};

export default FormalStudies;
