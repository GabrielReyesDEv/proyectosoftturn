import React, { useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Modal,
  Card,
  CardContent,
  CardActions,
  Grid,
  Typography,
  TextField,
  Switch,
  Button,
  colors
} from '@material-ui/core';
import useRouter from 'utils/useRouter';
import { useTranslation } from 'react-i18next';
import axios from 'utils/axios'
import getAuthorization from 'utils/getAuthorization';

const useStyles = makeStyles(theme => ({
  root: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    outline: 'none',
    boxShadow: theme.shadows[20],
    width: 700,
    maxHeight: '100%',
    overflowY: 'auto',
    maxWidth: '100%'
  },
  container: {
    marginTop: theme.spacing(3)
  },
  actions: {
    justifyContent: 'flex-end'
  },
  saveButton: {
    color: theme.palette.white,
    backgroundColor: colors.green[600],
    '&:hover': {
      backgroundColor: colors.green[900]
    }
  }
}));

const UserEdit = props => {
  const { className, id, info, onClose, open, ...rest } = props;
  const { t } = useTranslation();

  const classes = useStyles();
  const router = useRouter();

  const [formState, setFormState] = useState({
    ...info
  });

  if (!open) {
    return null;
  }

  const handleFieldChange = event => {
    event.persist();
    setFormState(formState => ({
      ...formState,
      [event.target.name]:
        event.target.type === 'checkbox'
          ? event.target.checked
          : event.target.value
    }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const consult = await axios.put(`/users/${id}`, { field: '_id', ...formState }, getAuthorization())
      if (consult.status === 200) router.history.push(`/management/users/${id}`);
    } catch (error) {
      console.error(error);
    }
  };

  const statusOptions = ['Active', 'Pending', 'Suspended'];

  return (
    <Modal
      onClose={onClose}
      open={open}
    >
      <Card
        {...rest}
        className={clsx(classes.root, className)}
      >
        <form>
          <CardContent>
            <Typography
              align="center"
              gutterBottom
              variant="h3"
            >
              {`${t('edit')} ${t('info')}`}
            </Typography>
            <Grid
              className={classes.container}
              container
              spacing={3}
            >
              <Grid
                item
                md={6}
                xs={12}
              >
                <TextField
                  error={formState.email ? false : true}
                  fullWidth
                  label="Email address"
                  name="email"
                  onChange={handleFieldChange}
                  required
                  value={formState.email}
                  variant="outlined"
                />
              </Grid>
              <Grid
                item
                md={6}
                xs={12}
              >
                <TextField
                  error={formState.firstName ? false : true}
                  fullWidth
                  label={t('first name')}
                  name="firstName"
                  onChange={handleFieldChange}
                  required
                  value={formState.firstName}
                  variant="outlined"
                />
              </Grid>
              <Grid
                item
                md={6}
                xs={12}
              >
                <TextField
                  error={formState.lastName ? false : true}
                  fullWidth
                  label={t('last name')}
                  name="lastName"
                  onChange={handleFieldChange}
                  required
                  value={formState.lastName}
                  variant="outlined"
                />
              </Grid>
              <Grid
                item
                md={6}
                xs={12}
              >
                <TextField
                  error={formState.workPosition ? false : true}
                  fullWidth
                  label={t('workPosition')}
                  name="workPosition"
                  onChange={handleFieldChange}
                  required
                  value={formState.workPosition}
                  variant="outlined"
                />
              </Grid>
              <Grid
                item
                md={6}
                xs={12}
              >
                <TextField
                  error={formState.identification ? false : true}
                  fullWidth
                  label={t('identification')}
                  name="identification"
                  onChange={handleFieldChange}
                  required
                  value={formState.identification}
                  variant="outlined"
                />
              </Grid>
              <Grid
                item
                md={6}
                xs={12}
              >
                <TextField
                  fullWidth
                  label={t('phone')}
                  name="phone"
                  onChange={handleFieldChange}
                  value={formState.phone}
                  variant="outlined"
                />
              </Grid>
              <Grid
                item
                md={6}
                xs={12}
              >
                <TextField
                  fullWidth
                  label={t('status')}
                  name="status"
                  onChange={event => handleFieldChange(event, 'status', event.target.value)}
                  required
                  select
                  // eslint-disable-next-line react/jsx-sort-props
                  SelectProps={{ native: true }}
                  value={formState.status}
                  variant="outlined"
                >
                  <option
                    disabled
                    value=""
                  />
                  {statusOptions.map(option => (
                    <option
                      key={option}
                      value={option}
                    >
                      {t(option)}
                    </option>
                  ))}
                </TextField>
              </Grid>
              <Grid item />
              <Grid
                item
                md={6}
                xs={12}
              >
                <Typography variant="h5">Email Verified</Typography>
                <Typography variant="body2">
                  Disabling this will automatically send the user a verification
                  email
                </Typography>
                <Switch
                  checked={formState.verified||false}
                  color="secondary"
                  edge="start"
                  name="verified"
                  onChange={handleFieldChange}
                  value={formState.verified}
                />
              </Grid>
            </Grid>
          </CardContent>
          <CardActions className={classes.actions}>
            <Button
              onClick={onClose}
              variant="contained"
            >
              Close
            </Button>
            <Button
              className={classes.saveButton}
              onClick={handleSubmit}
              type="submit"
              variant="contained"
            >
              Save
            </Button>
          </CardActions>
        </form>
      </Card>
    </Modal>
  );
};

UserEdit.displayName = 'UserEdit';

UserEdit.propTypes = {
  className: PropTypes.string,
  info: PropTypes.any,
  onClose: PropTypes.func,
  open: PropTypes.bool
};

UserEdit.defaultProps = {
  open: false,
  onClose: () => {}
};

export default UserEdit;
