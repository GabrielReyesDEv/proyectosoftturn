import React from 'react';
import PropTypes from 'prop-types';
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Button,
} from '@material-ui/core';
import useRouter from 'utils/useRouter';
import { useTranslation } from 'react-i18next';
import axios from 'utils/axios'
import getAuthorization from 'utils/getAuthorization';

const SetPassword = props => {
  const { id, onClose, open } = props;
  const { t } = useTranslation();

  const router = useRouter();

  const handleConfirm = async (event) => {
    event.preventDefault();
    try {
      const consult = await axios.put(`/users/${id}`,  {field: '_id', password: '$2b$10$EOe35sHKi.Q264Nq7ip5Iuyk8G06SAMbdBmYHdjMwlKZBBS7edGlm'}, getAuthorization())     
      if (consult.status === 200) router.history.push(`/management/users/${id}`);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <Dialog
      onClose={onClose}
      open={open}
    >
      <DialogTitle id="alert-dialog-title">{t('caution')}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {`${t('are you sure to reset the password')}`}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button
          color="secondary"
          onClick={onClose}
        >
            Disagree
        </Button>
        <Button
          autoFocus
          color="primary"
          onClick={handleConfirm}
        >
            Agree
        </Button>
      </DialogActions>
    </Dialog>
  );
};

SetPassword.displayName = 'SetPassword';

SetPassword.propTypes = {
  className: PropTypes.string,
  id: PropTypes.any,
  onClose: PropTypes.func,
  open: PropTypes.bool
};

SetPassword.defaultProps = {
  open: false,
};

export default SetPassword;
