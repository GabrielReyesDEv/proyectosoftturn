/* eslint-disable react/prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';
import { UserInfo, FormalStudies, Roles, OtherActions } from './components';

const useStyles = makeStyles(() => ({
  root: {}
}));

const Summary = props => {
  const { className, user, ...rest } = props;

  const id = user?._id;
  const userInfo = {
    email: user?.email,
    firstName: user?.firstName,
    group: user?.group,
    identification: user?.identification,
    lastName: user?.lastName,
    phone: user?.phone,
    status: user?.status,
    verified: user?.verified,
    workPosition: user?.workPosition
  };

  const classes = useStyles();

  if (!user) {
    return null;
  }

  return (
    <Grid
      {...rest}
      className={clsx(classes.root, className)}
      container
      spacing={3}
    >
      <Grid
        item
        lg={5}
        md={6}
        xl={4}
        xs={12}
      >
        <UserInfo
          id={id}
          info={userInfo}
        />
      </Grid>
      <Grid
        item
        lg={7}
        md={6}
        xl={8}
        xs={12}
      >
        <FormalStudies
          id={id}
          studies={user.studies}
        />
      </Grid>
      <Grid
        item
        lg={5}
        md={6}
        xl={4}
        xs={12}
      >
        <Roles
          id={id}
          roles={user.roles}
        />
      </Grid>
      <Grid
        item
        lg={5}
        md={6}
        xl={4}
        xs={12}
      >
        <OtherActions id={id}/>
      </Grid>
    </Grid>
  );
};

Summary.propTypes = {
  className: PropTypes.string
};

export default Summary;
