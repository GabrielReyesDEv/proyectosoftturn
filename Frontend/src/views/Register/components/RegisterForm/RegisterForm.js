import React, { useState, useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import validate from 'validate.js';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Button,
  Checkbox,
  FormHelperText,
  TextField,
  Typography,
  Link
} from '@material-ui/core';
import useRouter from 'utils/useRouter';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';
import moment from 'moment';
import { useSnackbar } from 'notistack';

const schema = {
  firstName: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 32
    }
  },
  lastName: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 32
    }
  },
  email: {
    presence: { allowEmpty: false, message: 'is required' },
    email: true,
    length: {
      maximum: 64
    }
  },
  password: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 128
    }
  },
  policy: {
    presence: { allowEmpty: false, message: 'is required' },
    checked: true
  }
};

const useStyles = makeStyles(theme => ({
  root: {},
  fields: {
    margin: theme.spacing(-1),
    display: 'flex',
    flexWrap: 'wrap',
    '& > *': {
      flexGrow: 1,
      margin: theme.spacing(1)
    }
  },
  policy: {
    display: 'flex',
    alignItems: 'center'
  },
  policyCheckbox: {
    marginLeft: '-14px'
  },
  submitButton: {
    marginTop: theme.spacing(2),
    width: '100%'
  }
}));

const RegisterForm = props => {
  const { className, ...rest } = props;

  const classes = useStyles();
  const { history } = useRouter();

  const [errorMsg, setErrorMsg] = useState('');
  const [create, setCreate] = useState(false);
  // eslint-disable-next-line no-unused-vars
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const [formState, setFormState] = useState({
    isValid: false,
    values: {
      workPosition: 'No Especificado',
      status: 'Inactive',
      group: 'No Especificado',
      roles: [],
      createdAt: moment().format('DD/MM/YYYY')
    },
    touched: {},
    errors: {}
  });

  useEffect(() => {
    const errors = validate(formState.values, schema);

    setFormState(formState => ({
      ...formState,
      isValid: errors ? false : true,
      errors: errors || {}
    }));
  }, [formState.values]);

  const handleChange = event => {
    event.persist();

    setFormState(formState => ({
      ...formState,
      values: {
        ...formState.values,
        [event.target.name]:
          event.target.type === 'checkbox'
            ? event.target.checked
            : event.target.value
      },
      touched: {
        ...formState.touched,
        [event.target.name]: true
      }
    }));
  };

  const handleSubmit = async event => {
    event.preventDefault();
    setCreate(true);
    if (formState.values.policy) {
      delete formState.values.policy;
      const newUser = {...formState.values, email: formState.values.email.toLowerCase()};
      
      const consult = await axios.post('/auth/sign-up', newUser, getAuthorization());
      if (consult.status === 201 && consult.data.message === 'El Usuario ha sido creado con exito.') {
        enqueueSnackbar(consult.data.message, {
          variant: 'success',
        });
        history.push('/');
      } else {
        setErrorMsg(consult.data.message);
      }
    }
  };

  const hasError = field =>
    formState.touched[field] && formState.errors[field] ? true : false;

  return (
    <form
      {...rest}
      className={clsx(classes.root, className)}
      onSubmit={handleSubmit}
    >
      <div className={classes.fields}>
        <TextField
          error={hasError('firstName') || ((formState.values.firstName === '' || formState.values.firstName === undefined) && create)}
          helperText={
            hasError('firstName') ? formState.errors.firstName[0] : (
              ((formState.values.firstName === '' || formState.values.firstName === undefined) && create) ?
                'Campo obligatorio*' : null
            )
          }
          label="Nombres"
          name="firstName"
          onChange={handleChange}
          required
          value={formState.values.firstName || ''}
          variant="outlined"
        />
        <TextField
          error={hasError('lastName')}
          helperText={
            hasError('lastName') ? formState.errors.lastName[0] : null
          }
          label="Apellidos"
          name="lastName"
          onChange={handleChange}
          required
          value={formState.values.lastName || ''}
          variant="outlined"
        />
        <TextField
          error={hasError('email')}
          fullWidth
          helperText={hasError('email') ? formState.errors.email[0] : null}
          label="Correo"
          name="email"
          onChange={handleChange}
          required
          value={formState.values.email || ''}
          variant="outlined"
        />
        <TextField
          error={hasError('identification')}
          helperText={
            hasError('identification') ? formState.errors.identification[0] : null
          }
          label="Identificación"
          name="identification"
          onChange={handleChange}
          required
          value={formState.values.identification || ''}
          variant="outlined"
        />
        <TextField
          error={hasError('phone')}
          helperText={
            hasError('phone') ? formState.errors.phone[0] : null
          }
          label="Telefono"
          name="phone"
          onChange={handleChange}
          required
          value={formState.values.phone || ''}
          variant="outlined"
        />
        <TextField
          error={hasError('password')}
          fullWidth
          helperText={
            hasError('password') ? formState.errors.password[0] : null
          }
          label="Contraseña"
          name="password"
          onChange={handleChange}
          required
          type="password"
          value={formState.values.password || ''}
          variant="outlined"
        />
        <div>
          <div className={classes.policy}>
            <Checkbox
              checked={formState.values.policy || false}
              className={classes.policyCheckbox}
              color="primary"
              error={hasError('policy')}
              helperText={
                hasError('policy') ? formState.errors.policy[0] : null
              }
              name="policy"
              onChange={handleChange}
              required
            />
            <Typography
              color="textSecondary"
              variant="body1"
            >
              He leido los{' '}
              <Link
                color="secondary"
                component={RouterLink}
                to="#"
                underline="always"
                variant="h6"
              >
                Terminos y Condiciones
              </Link>
            </Typography>
          </div>
          {hasError('policy') && (
            <FormHelperText error>{formState.errors.policy[0]}</FormHelperText>
          )}
        </div>
      </div>
      <Button
        className={classes.submitButton}
        color="secondary"
        // disabled={!formState.isValid}
        size="large"
        type="submit"
        variant="contained"
      >
        Create account
      </Button>
      {create && errorMsg !== '' && (
        <FormHelperText error>{errorMsg}</FormHelperText>
      )}
    </form>
  );
};

RegisterForm.propTypes = {
  className: PropTypes.string
};

export default RegisterForm;
