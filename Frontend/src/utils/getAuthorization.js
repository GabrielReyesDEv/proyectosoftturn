export default () => ({
  headers: { authorization: `Bearer ${localStorage.getItem("jwt-token")}` }
});