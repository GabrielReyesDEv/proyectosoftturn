import moment from 'moment';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';

export default async (authors, areas, groups) => {
  console.log('entro', authors, areas, groups)
  const today = moment().format('YYYY-MM-DD');
  const res = await axios.post('tasks/pagination', {
    "datesFieldSearch": [{ "field": "deadlineDate", "gte": `${today} 00:00`, "lt": `${today} 23:59`}],
    "eq": [{"field": "area", "values": areas}, {"field": "group", "values": groups},{"field": "author", "values": authors}]
  }, getAuthorization()).then((res) => {
    console.log(res.data.data.length)
    if (res.data.data.length > 0) {
      return true
    } else {
      return false
    }
  })
  return res
}
