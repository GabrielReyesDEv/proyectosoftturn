import moment from 'moment';
import 'moment/locale/es';
moment.locale('es')
export default (start, end) => {
  moment.locale('es')
  if (end) {
    start = moment(end  , 'YYYY-MM-DD HH:mm')
    end = moment().format('YYYY-MM-DD HH:mm')

    const duration = start.from(end)
  
    return duration
  }
  return 'Sin registro'
}
 ;