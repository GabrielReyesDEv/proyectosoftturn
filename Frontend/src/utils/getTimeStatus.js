import moment from 'moment';

export default (deadline, finish) => {
  deadline = moment(deadline, 'YYYY-MM-DD HH:mm')
  finish ? finish = moment(finish, 'YYYY-MM-DD HH:mm') : finish = moment(moment().format('YYYY-MM-DD HH:mm'), 'YYYY-MM-DD HH:mm')

  const calculated_days = finish.diff(deadline, 'days', true)
  if (calculated_days <= 0) {
    return 'onTime'
  } else if ( calculated_days > 0 ) {
    return 'outOfTime'
  }
}
  
