import { removeToken } from 'tokenUtils';
import Cookies from 'js-cookie';


const closeSession = async () => {
  try {
    // document.cookie = 'email=';
    // document.cookie = 'firstName=';
    // document.cookie = 'lastName=';
    // document.cookie = 'id=';
    // document.cookie = 'roles=';
    // document.cookie = 'identification=';
    // document.cookie = 'phone=';
    // document.cookie = 'userOnyx=';
    // document.cookie = 'workPosition=';
    // Cookies.remove('email');
    // Cookies.remove('firstName');
    // Cookies.remove('lastName');
    // Cookies.remove('id');
    // Cookies.remove('roles');
    // Cookies.remove('identification');
    // Cookies.remove('phone');

    Cookies.set('email', undefined);
    Cookies.set('firstName', undefined);
    Cookies.set('lastName', undefined);
    Cookies.set('id', undefined);
    Cookies.set('roles', undefined);
    Cookies.set('identification', undefined);
    Cookies.set('phone', undefined);
    Cookies.set('workPosition', undefined);
    Cookies.set('userOnyx', undefined);
    removeToken();
  } catch (error) {
    console.error(error);
  }
}


const checkState = async (e, callBack) => {
  const { response } = e;
  if (response.status === 401 && response.data ==='Unauthorized')
  {
    closeSession();
    return true;
  }

  console.log(e);
  return false;
}

export { closeSession, checkState };