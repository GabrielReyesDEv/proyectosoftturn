import moment from 'moment';
export default (date) => {
  const moment_date = moment(date  , 'YYYY-MM-DD HH:mm')
  return { date: moment_date.format('DD/MM/YYYY'), time: moment_date.format('HH:mm')}
}
 ;