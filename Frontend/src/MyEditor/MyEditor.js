import React from 'react';
import NoSSR from 'react-no-ssr';
import { TextEditor } from './components';

const Loading = () => (<div>Loading...</div>);
const MyEditor = () => (
  <div>
    <NoSSR onSSR={<Loading />}>
      <TextEditor />
    </NoSSR>
  </div>
);

export default MyEditor;
