import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';
import * as serviceWorker from './serviceWorker';
import { SnackbarProvider } from 'notistack';
import './i18n';

ReactDOM.render(
  <SnackbarProvider
    SnackbarProvider
    maxSnack={3}
  >
    <App />
  </SnackbarProvider>
  , document.getElementById('root'));

serviceWorker.unregister();
