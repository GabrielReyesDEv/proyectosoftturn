export const getColor = () => localStorage.getItem("items-color");

export const saveColor = colorItems => localStorage.setItem("items-color", colorItems);

export const removeColor = () => localStorage.removeItem("items-color");