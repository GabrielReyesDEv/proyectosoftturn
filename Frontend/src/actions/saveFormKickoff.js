export const SAVE_FORM_KICKOFF = 'SAVE_FORM_KICKOFF';
export const SAVE_CHECK_KICKOFF = 'SAVE_CHECK_KICKOFF';
export const SAVE_INPUTS_OTPS = 'SAVE_INPUTS_OTPS';
export const SAVE_OTP = 'SAVE_OTP';

export const saveFormKickoff = (payload) => dispatch =>
  dispatch({
    type: SAVE_FORM_KICKOFF,
    payload
  });

export const saveCheckKickOff = (payload) => dispatch =>
  dispatch({
    type: SAVE_CHECK_KICKOFF,
    payload
  });

export const saveInputsOTP = (payload) => dispatch =>
  dispatch({
    type: SAVE_INPUTS_OTPS,
    payload
  });

export const saveOtp = (payload) => dispatch =>
  dispatch({
    type: SAVE_OTP,
    payload
  });