export const SAVE_UPDATEREPORT = 'SAVE_UPDATEREPORT';
export const SET_UPDATEREPORT = 'SET_UPDATEREPORT';

export const saveUpdateReport = (payload) => dispatch =>
  dispatch({
    type: SAVE_UPDATEREPORT,
    payload
  });

export const setUpdateReport = (payload) => dispatch =>
  dispatch({
    type: SET_UPDATEREPORT,
    payload
  });
