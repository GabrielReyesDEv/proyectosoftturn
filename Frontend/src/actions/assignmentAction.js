export const SAVE_ASSIGNMENT = 'SAVE_ASSIGNMENT';
export const SET_ASSIGNED = 'SET_ASSIGNED';
export const SET_RESPONSABLE = 'SET_RESPONSABLE';

export const saveAssignment = (payload) => dispatch =>
  dispatch({
    type: SAVE_ASSIGNMENT,
    payload
  });

export const setAssigned = (payload) => dispatch =>
  dispatch({
    type: SET_ASSIGNED,
    payload
  });

export const setResponsable = (payload) => dispatch =>
  dispatch({
    type: SET_RESPONSABLE,
    payload
  });