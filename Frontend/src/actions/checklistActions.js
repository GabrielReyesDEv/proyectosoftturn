import axios from 'utils/axios';
import moment from 'moment';

const SERVER_URL = 'http://localhost:3001/api';

export const SET_ASIGNATED = 'SET_ASIGNATED';
export const SAVE_USERSDISPACHER = 'SAVE_USERSDISPACHER';
export const SET_ERROR = 'SET_ERROR';
export const SAVE_ALERT = 'SAVE_ALERT';
export const UPDATE_CKLIST = 'UPDATE_CKLIST';
export const SET_STATE_MODAL = 'SET_STATE_MODAL';
export const SAVE_CKLIST = 'SAVE_CKLIST';
export const SET_USERS = 'SET_USERS';
export const SAVE_CKFORMAT = 'SAVE_CKFORMAT';
export const SAVE_FORMAT_TEXT = 'SAVE_FORMAT_TEXT';

export const saveAlert = (payload) => ({
  type: SAVE_ALERT,
  payload,
});

export const saveCkList = (payload) => ({
  type: SAVE_CKLIST,
  payload,
});

export const setAsignated = (payload) => dispatch => dispatch({
  type: SET_ASIGNATED,
  payload,
});

export const setStateModal = (payload) => ({
  type: SET_STATE_MODAL,
  payload,
});

export const setError = (payload) => ({
  type: SET_ERROR,
  payload,
});

export const setUserDIspacher = (payload) => {
  return {
    type: SAVE_USERSDISPACHER,
    payload,
  }
};

export const updateCkList = (payload) => ({
  type: UPDATE_CKLIST,
  payload,
});

export const setCkFormat = (payload) => ({
  type: SAVE_CKFORMAT,
  payload,
});

export const saveFormatText = (payload) => ({
  type: SAVE_FORMAT_TEXT,
  payload,
});

export const setUsers = (payload) => ({
  type: SET_USERS,
  payload,
});

export const newConfigRequest = (token, payload, id) => {
  const alerta = {
    mensaje: 'CheckList creado con exito!!',
    variant: 'success',
  };
  return (dispatch) => {
    axios({
      url: `${SERVER_URL}/checklistconfs/upsert/${id}`,
      headers: { Authorization: `Bearer ${token}` },
      method: 'put',
      data: payload,
    })
      .then((data) => dispatch(saveAlert(alerta)))
      .catch((err) => dispatch(setError(err)));
  };
};

export const getUsersDispacher = (token, rol, grupo) => {
  return (dispatch) => {
    axios({
      url: `${SERVER_URL}/users/pagination?roles.role=${rol}&roles.area=${grupo}&limit=100&page=1`,
      headers: { Authorization: `Bearer ${token}` },
      method: 'get',
    }).then(({ data }) => dispatch(setUserDIspacher(data.data)))
      .catch((err) => dispatch(setError(err)));
  };
};

export const updateCheckList2 = (token, id, payload, link = '/overview') => {
  
  // console.log('ID check', ids.idChecklist);
  // console.log('ID OTH', ids.idOTH);
  return (dispatch) => {
    let alerta = '';

    const alertaInicio = {
      mensaje: 'Actividad iniciada',
      variant: 'success',
    };
    const alertaFin = {
      mensaje: 'Actividad terminada',
      variant: 'success',
    };

    if (payload.estadoChecklist === 'Terminado') {
      alerta = alertaFin;
    } else {
      alerta = alertaInicio;
    }
    axios({
      url: `${SERVER_URL}/checklistconfs/${id}`,
      headers: { Authorization: `Bearer ${token}` },
      method: 'put',
      data: payload,

    })
      .then(() => dispatch(updateCkList(payload)))
      .then((data) => {
        if (link === '/') {
          dispatch(saveAlert(alerta));
        } else {
          dispatch(saveAlert(alerta));
          window.location.href = link;
        }
      })
      .catch((err) => dispatch(setError(err)));
  };
};

export const getCkList = (token, id) => {
  return (dispatch) => {
    axios({
      url: `${SERVER_URL}/checklistconfs/${id}`,
      headers: { Authorization: `Bearer ${token}` },
      method: 'get',
    })
      .then(({ data }) => dispatch(saveCkList(data.data)))
      .catch((err) => dispatch(setError(err)));
  };
};

export const cancelCheckList = (token, id, data, link = '/') => {
  return (dispatch) => {
    const alerta = {
      mensaje: 'Checklist Cancelado con exito!!',
      variant: 'success',
    };
    axios({
      url: `${SERVER_URL}/checklistconfs/${id}`,
      headers: { Authorization: `Bearer ${token}` },
      method: 'put',
      data: { updateAt: moment().format('YYYY-MM-DD HH:mm'), role: 'Gestion', estadoChecklist: 'Cancelado', log: data },
    })
      .then(({ data }) => {
        dispatch(saveAlert(alerta));
        window.location.href = link;
      })
      .catch((err) => dispatch(setError(err)));
  };
};

export const updateCheckList = (token, ids, payload, link = '/') => {

  return (dispatch) => {
    const alerta = {
      mensaje: 'Checklist Asignado con exito!!',
      variant: 'success',
    };
    axios({
      url: `${SERVER_URL}/checklistconfs/${ids.idChecklist}`,
      headers: { Authorization: `Bearer ${token}` },
      method: 'put',
      data: payload.dataCheckList,

    })
      .then((data) => {
        dispatch(saveAlert(alerta));
        window.location.href = link;
      })
      .catch((err) => dispatch(setError(err)));
  };
};

export const saveCkFormat = (token, id, payload, link = '/') => {
  // console.log('ID check', ids.idChecklist);
  // console.log('Data', payload);
  return (dispatch) => {
    axios({
      url: `${SERVER_URL}/checklistFormat/${id}`,
      headers: { Authorization: `Bearer ${token}` },
      method: 'put',
      data: payload,

    })
    .then((data) => {
      if (link === '/') {
        //console.log('Formato Guardado');
      } else {
        //console.log('Formato Guardado');
        window.location.href = link;
      }
    })
    .catch((err) => dispatch(setError(err)));
  };
};

export const setUserByRol = (token, rol, grupo) => {
  return (dispatch) => {
    axios({
      url: `${SERVER_URL}/users/pagination?roles.role=${rol}&roles.area=${grupo}&limit=180&page=1`,
      headers: { Authorization: `Bearer ${token}` },
      method: 'get',
    }).then(({ data }) => dispatch(setUsers(data.data)))
      .catch((err) => dispatch(setError(err)));
  };
};

export const updateOth = (token, idOTH, payload, link = '') => {
  // console.log(idOTH);
  return (dispatch) => {
    const alerta = {
      mensaje: 'Se actualizo la OTH con exito!!',
      variant: 'success',
    };
    axios({
      url: `${SERVER_URL}/oths/${idOTH}`,
      headers: { Authorization: `Bearer ${token}` },
      method: 'put',
      data: payload,

    })
    .then((data) => {
      if (link === '') {
        //console.log('OTH Creada Exitosamente');
      } else {
        dispatch(saveAlert(alerta));
        window.location.href = link;
      }
    })
    .catch((err) => dispatch(setError(err)));
  };
};

export const getCkFormat = (token, id) => {
  // console.log('ID check', ids.idChecklist);
  return (dispatch) => {
    axios({
      url: `${SERVER_URL}/checklistFormat/${id}`,
      headers: { Authorization: `Bearer ${token}` },
      method: 'get',
    })
    .then(({ data }) => dispatch(setCkFormat(data.data)))
    .catch((err) => dispatch(setError(err)));
  }
};