export const USER_EDIT = 'USER_EDIT';
export const ROLE_EDIT = 'ROLE_EDIT';

export const editUser = (payload) => dispatch => {

  return dispatch({
    type: USER_EDIT,
    payload
  });
  
}

export const editRole = (payload) => dispatch => {

  return dispatch({
    type: ROLE_EDIT,
    payload
  });
    
}