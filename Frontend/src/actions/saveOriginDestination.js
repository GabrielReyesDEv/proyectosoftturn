export const SAVE_ORIGIN_DESTINATION = 'SAVE_ORIGIN_DESTINATION';

export const saveOriginDestination = (payload) => dispatch =>
  dispatch({
    type: SAVE_ORIGIN_DESTINATION,
    payload
  });