import React, { Suspense, useState, useEffect } from 'react';
import { renderRoutes } from 'react-router-config';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { LinearProgress, IconButton } from '@material-ui/core';

import { NavBar, TopBar } from './components';
import { useSnackbar } from 'notistack';
import CloseIcon from '@material-ui/icons/Close';

import Cookies from 'js-cookie';
import { closeSession } from 'utils/logoutUtils';
import io from 'socket.io-client';
import { saveColor } from 'colorUtils';

let socket;

const useStyles = makeStyles(() => ({
  root: {
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    overflow: 'hidden'
  },
  topBar: {
    zIndex: 2,
    position: 'relative'
  },
  container: {
    display: 'flex',
    flex: '1 1 auto',
    overflow: 'hidden'
  },
  navBar: {
    zIndex: 3,
    width: 256,
    minWidth: 256,
    flex: '0 0 auto'
  },
  content: {
    overflowY: 'auto',
    flex: '1 1 auto'
  }
}));

const Dashboard = props => {
  const { route } = props;
  console.log('route: ', route);

  const classes = useStyles();
  const [openNavBarMobile, setOpenNavBarMobile] = useState(false);

  // const [name, setName] = useState('');
  // const [room, setRoom] = useState('');
  // const [users, setUsers] = useState('');
  const [notification, setNotification] = useState('');
  const [notifications, setNotifications] = useState([]);
  const [colorDashboard, setColorDashboard] = useState('');
  const [rolesUser, setRolesUser] = useState('');
  const ENDPOINT = 'http://localhost:4000/';
  const roomId = Cookies.get('identification');
  const nameId = Cookies.get('firstName');

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();  

  // useEffect(() => {
  //   let mounted = true;
  //   const init = () => {
  //     if (mounted) {
  //       const roles = [];
  //       if (JSON.parse(Cookies.get('roles')).find((x) => x.role === 'Administrador') !== undefined) roles.push('admin');
  //       if (JSON.parse(Cookies.get('roles')).find((x) => x.role === 'Supervisor') !== undefined) roles.push('super');
  //       if (JSON.parse(Cookies.get('roles')).find((x) => x.role === 'Tecnico') !== undefined) roles.push('tec');
  //       setRolesUser(roles);
  //       console.log('rolesss: ', roles);
  //     }
  //   }
  //   init();
  //   return () => {
  //     mounted = false;
  //   };
  // });

  useEffect(() => {
    const name = nameId
    const room = roomId

    socket = io(ENDPOINT);
    if (Cookies.get('roles') === undefined || localStorage.getItem('jwt-token') === null || JSON.parse(Cookies.get('roles')).length === 0){
      if (Cookies.get('roles') === undefined || (Cookies.get('roles') !== 'undefined' && (JSON.parse(Cookies.get('roles'))).length === 0)) closeSession();
      window.location.href = '/auth/login';
    } else {
      const roles = [];
      if (JSON.parse(Cookies.get('roles')).find((x) => x.role === 'Administrador') !== undefined) roles.push('admin');
      if (JSON.parse(Cookies.get('roles')).find((x) => x.role === 'Supervisor') !== undefined) roles.push('super');
      if (JSON.parse(Cookies.get('roles')).find((x) => x.role === 'Tecnico') !== undefined) roles.push('tec');
      setRolesUser(roles);
      console.log('rolesss: ', roles);
      if (JSON.parse(Cookies.get('roles')).find((x) => x.role === 'Administrador') !== undefined && window.location.pathname === '/overview') {
        setColorDashboard('#59595b');
        saveColor('admin');
      }  else if (JSON.parse(Cookies.get('roles')).find((x) => x.role === 'Supervisor') !== undefined && window.location.pathname === '/overview') {
        setColorDashboard('#9cc700');
        saveColor('super');
      } else if (JSON.parse(Cookies.get('roles')).find((x) => x.role === 'Tecnico') !== undefined && window.location.pathname === '/overview') {
        setColorDashboard('#f78516');
      }
    }
    // setRoom(room);
    // setName(name);

    socket.emit('join', { name, room }, (error) => {
      if(error) {
        console.log(error);
        // if (error.includes('taken')) {
        //   alert('Tienes otra sesion iniciada')
        //   history.push('/auth/login');
        // }
      }
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  
  useEffect(() => {
    socket.on('notification', notification => {

      const handleDismiss = () => {
        closeSnackbar();
      };

      const action = (
        <IconButton
          className={classes.expand}
          onClick={handleDismiss}
        >
          <CloseIcon />
        </IconButton>
      );
      enqueueSnackbar(notification.text, {
        variant: 'info',
        action
      });
      setNotifications(msgs => [ ...msgs, notification ]);
    });
    
    socket.on('roomData', ({ users }) => {
      // setUsers(users);
    });

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const sendNotification = (event) => {
    event.preventDefault();

    if(notification) {
      socket.emit('sendNotification', notification, () => setNotification(''));
    }
  }

  const handleNavBarMobileOpen = () => {
    setOpenNavBarMobile(true);
  };

  const handleNavBarMobileClose = () => {
    setOpenNavBarMobile(false);
  };

  return (
    <div className={classes.root}>
      {localStorage.getItem('jwt-token') !== null && (
        <>
          <TopBar
            className={classes.topBar}
            colorDashboard={colorDashboard}
            name={nameId}
            rolesUser={rolesUser}
            notification={notification}
            notifications={notifications}
            onOpenNavBarMobile={handleNavBarMobileOpen}
            room={roomId}
            sendNotification={sendNotification}
            setNotification={setNotification}
            setNotifications={setNotifications}
            socket={socket}
          />
          <div className={classes.container}>
            <NavBar
              className={classes.navBar}
              onMobileClose={handleNavBarMobileClose}
              openMobile={openNavBarMobile}
            />
            <main className={classes.content}>
              <Suspense fallback={<LinearProgress colorDashboard={colorDashboard}/>}>
                {renderRoutes(route.routes)}
              </Suspense>
            </main>
          </div>
        </>
      )}
      {/* <ChatBar /> */}
    </div>
  );
};

Dashboard.propTypes = {
  route: PropTypes.object
};

export default Dashboard;