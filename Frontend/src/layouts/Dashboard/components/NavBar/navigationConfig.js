/* eslint-disable react/no-multi-comp */
/* eslint-disable react/display-name */
import React from 'react';
import { colors } from '@material-ui/core';
import BarChartIcon from '@material-ui/icons/BarChart';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import ChatIcon from '@material-ui/icons/ChatOutlined';
import DashboardIcon from '@material-ui/icons/DashboardOutlined';
import FolderIcon from '@material-ui/icons/FolderOutlined';
import HomeIcon from '@material-ui/icons/HomeOutlined';
import SettingsIcon from '@material-ui/icons/SettingsOutlined';
import { useTranslation } from 'react-i18next';
import Cookies from 'js-cookie';

import { Label } from 'components';

const NavigationConfig = () => {
  const { t } = useTranslation();
  let pages = [
    {
      title: t('sidebar.pages.overview'),
      href: '/overview',
      icon: HomeIcon
    },
    {
      title: t('sidebar.pages.dashboards'),
      href: '/dashboards',
      icon: DashboardIcon,
      children: [
        {
          title: 'Default',
          href: '/dashboards/default'
        },
        {
          title: 'Analytics',
          href: '/dashboards/analytics'
        }
      ]
    },
  ];
  if (Cookies.get('roles') !== undefined) {
    const findUser2 = JSON.parse(Cookies.get('roles')).find((x) => x.role === 'Administrador');
    if (findUser2 !== undefined) {
      pages.push(
        {
          title: t('sidebar.pages.management'),
          href: '/Administracion',
          icon: BarChartIcon,
          children: [
            {
              title: 'Proyectos',
              href: '/management/projects'
            },
            {
              title: 'Actividades',
              href: '/management/activities'
            },
            {
              title: 'Tareas',
              href: '/management/tasks'
            },
            {
              title: t('sidebar.pages.management.users'),
              href: '/management/users'
            },
            
          ]
        },
      )
    }
  }
  if (Cookies.get('roles') !== undefined) {
    const findUser = JSON.parse(Cookies.get('roles')).find((x) => x.role === 'Supervisor');
    const findUser2 = JSON.parse(Cookies.get('roles')).find((x) => x.role === 'Administrador');
    if (findUser !== undefined || findUser2 !== undefined) {
      pages.push(
        {
          title: t('sidebar.pages.reports'),
          href: '/Reportes',
          icon: BarChartIcon,
          children: [
            {
              title: 'Reportes Supervisores',
              href: '/management/projects'
            },
            {
              title: 'Reportes Tecnicos',
              href: '/management/activities'
            },
          ]
        },
      )
    }
  }
  pages.push(
    {
      title: 'Turnos',
      href: '/tasks',
      icon: FolderIcon,
      children: [
        {
          title: 'Backlog',
          href: '/tasks/backlog'
        },
        {
          title: 'Todas',
          href: '/tasks'
        }
      ]
    },
    {
      title: 'Chat',
      href: '/chat',
      icon: ChatIcon,
      label: () => (
        <Label
          color={colors.red[500]}
          shape="rounded"
        >
          4
        </Label>
      )
    },
    {
      title: 'Calendario',
      href: '/calendar',
      icon: CalendarTodayIcon
    },
    {
      title: 'Settings',
      href: '/settings',
      icon: SettingsIcon,
      children: [
        {
          title: 'General',
          href: '/settings/general'
        }
      ]
    }
  )
  return [
    {
      title: t('sidebar.pages.title'),
      pages: pages.length > 0 ? pages: 
        {
          title: t('sidebar.pages.overview'),
          href: '/overview',
          icon: HomeIcon
        },
    }
  ];
};

export default NavigationConfig;
