/* eslint-disable no-unused-vars */
import React, { useState, useRef, useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/styles';
import getAuthorization from 'utils/getAuthorization';
import axios from 'utils/axios';
import {
  AppBar,
  Badge,
  Button,
  IconButton,
  Toolbar,
  Hidden,
  colors
} from '@material-ui/core';
import NotificationsIcon from '@material-ui/icons/NotificationsOutlined';
import InputIcon from '@material-ui/icons/Input';
import MenuIcon from '@material-ui/icons/Menu';
import useRouter from 'utils/useRouter';
import { NotificationsPopover } from 'components';
import { closeSession } from 'utils/logoutUtils';
import { logout } from 'actions';
import Cookies from 'js-cookie';

const TopBar = props => {
  const { rolesUser, colorDashboard, socket, room, name, notification, notifications, setNotification, sendNotification, setNotifications, onOpenNavBarMobile, className, ...rest } = props;
  const useStyles = makeStyles(theme => ({
    root: {
      boxShadow: 'none',
      // backgroundColor: '#008fd5'
      backgroundColor: colorDashboard,
    },
    flexGrow: {
      flexGrow: 1
    },
    search: {
      backgroundColor: 'rgba(255,255,255, 0.1)',
      borderRadius: 4,
      flexBasis: 300,
      height: 36,
      padding: theme.spacing(0, 2),
      display: 'flex',
      alignItems: 'center'
    },
    searchIcon: {
      marginRight: theme.spacing(2),
      color: 'inherit'
    },
    searchInput: {
      flexGrow: 1,
      color: 'inherit',
      '& input::placeholder': {
        opacity: 1,
        color: 'inherit'
      }
    },
    searchPopper: {
      zIndex: theme.zIndex.appBar + 100
    },
    searchPopperContent: {
      marginTop: theme.spacing(1)
    },
    trialButton: {
      marginLeft: theme.spacing(2),
      color: theme.palette.white,
      backgroundColor: colors.green[600],
      '&:hover': {
        backgroundColor: colors.green[900]
      }
    },
    trialIcon: {
      marginRight: theme.spacing(1)
    },
    notificationsButton: {
      marginLeft: theme.spacing(1)
    },
    notificationsBadge: {
      backgroundColor: colors.orange[600]
    },
    languageSelect: {
      color: theme.palette.white
    },
    logoutButton: {
      marginLeft: theme.spacing(1)
    },
    logoutIcon: {
      marginRight: theme.spacing(1)
    },
    icon: {
      color: '#ffff',
      fontSize: '24px',
      fontFamily: 'Roboto',
      fontWeight: 500,
      lineHeight: '28px',
      letterSpacing: '-0.06px'
    }
  }));
  const classes = useStyles();
  const { history } = useRouter();
  const dispatch = useDispatch();

  const notificationsRef = useRef(null);
  const [openNotifications, setOpenNotifications] = useState(false);

  useEffect (() => {
    let mounted = true;
    
    if (mounted) {
      if (Cookies.get('roles') !== undefined ) {
        // const findUser = JSON.parse(Cookies.get('roles')).find((x) => x.role === 'Administrador');
        const admArray = window.location.pathname.split('/')[1];
        // console.log('admArray: ', admArray === 'management');
        // console.log('findUser === undefined: ', findUser === undefined);
        console.log('rolesss222: ', rolesUser);
        if (rolesUser.indexOf('admin') === -1 && admArray === 'management') {
          console.log('entraaa');
          window.location.href = '/overview';
        }
      }
    }

    return () => {
      mounted = false;
    };
  })

  useEffect(() => {
    let mounted = true;
    const fetchNotifications = async () => {
      const body = {
        limit: 6,
        page: 1,
        'eq': [{ 'field': 'view', 'values': [false] }],
        ownerId: room,
        columnsNeeded: {
          'title': 1,
          'type': 1,
          'typeId' : 1,
          'created_at': 1,
          'view': 1,
        },
        'sort': {'column': 'created_at', 'value': -1},
      };
      axios.post('/notifications/pagination', body, getAuthorization()).then(response => {
        if (mounted) {
          setNotifications(response.data.data)
        }
      });
      // if (Cookies.get('roles') !== undefined ) {
      //   // const findUser = JSON.parse(Cookies.get('roles')).find((x) => x.role === 'Administrador');
      //   const admArray = window.location.pathname.split('/')[1];
      //   const valRol = await rolesUser.indexOf('admin') === -1;
      //   const findUser = JSON.parse(Cookies.get('roles')).find((x) => x.role === 'Administrador');
      //   console.log('Cookies.get(roles) !== undefined : ', Cookies.get('roles') !== undefined );
      //   console.log('Cookies.get(roles) : ', Cookies.get('roles') );
      //   console.log('rolesss222: ', rolesUser);
      //   console.log('admArray: ', admArray);
      //   console.log('valRol: ', valRol);
      //   console.log('findUser: ', findUser);
      //   if (findUser === undefined && admArray === 'management') {
      //     console.log('entraaa');
      //     window.location.href = '/overview';
      //   }
      // }
    };

    fetchNotifications();

    return () => {
      mounted = false;
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleLogout = async () => {
    try {
      dispatch(logout());
      history.push('/auth/login');
      await closeSession();
      socket.disconnect();
    } catch (error) {
      console.error(error);
    }
  };

  const handleNotificationsOpen = () => {
    setOpenNotifications(true);
  };

  const handleNotificationsClose = () => {
    setOpenNotifications(false);
  };

  return (
    <AppBar
      {...rest}
      className={clsx(classes.root, className)}
      color="primary"
      // color={nameColorDashboard}
    >
      <Toolbar>
        <RouterLink to="/">
          <div className={classes.icon}>SOFTURN</div>
        </RouterLink>
        <div className={classes.flexGrow} />
        <IconButton
          className={classes.notificationsButton}
          color="inherit"
          onClick={handleNotificationsOpen}
          ref={notificationsRef}
        >
          <Badge
            badgeContent={notifications.length}
            classes={{ badge: classes.notificationsBadge }}
          >
            <NotificationsIcon />
          </Badge>
        </IconButton>
        <Button
          className={classes.logoutButton}
          color="inherit"
          onClick={handleLogout}
        >
          <InputIcon className={classes.logoutIcon} />
        </Button>
        <Hidden lgUp>
          <IconButton
            color="inherit"
            onClick={onOpenNavBarMobile}
          >
            <MenuIcon />
          </IconButton>
        </Hidden>
      </Toolbar>
      <NotificationsPopover
        anchorEl={notificationsRef.current}
        name={name}
        notification={notification}
        notifications={notifications}
        onClose={handleNotificationsClose}
        open={openNotifications}
        room={room}
        sendNotification={sendNotification}
        setNotification={setNotification}
      />
    </AppBar>
  );
};

TopBar.propTypes = {
  className: PropTypes.string,
  onOpenNavBarMobile: PropTypes.func
};

export default TopBar;
