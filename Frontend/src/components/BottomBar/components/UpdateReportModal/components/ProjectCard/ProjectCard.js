import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Avatar,
  Card,
  CardContent,
  Typography,
} from '@material-ui/core';

import getInitials from 'utils/getInitials';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    flexWrap: 'wrap',
    marginBottom: theme.spacing(2)
  },
  content: {
    height: '54px',
    padding: theme.spacing(2),
    flexGrow: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
      flexWrap: 'wrap'
    },
    '&:last-child': {
      paddingBottom: theme.spacing(2)
    }
  },
  header: {
    maxWidth: '57%',
    width: '36%',
    display: 'flex',
    [theme.breakpoints.down('sm')]: {
      marginBottom: theme.spacing(2),
      flexBasis: '100%'
    }
  },
  avatar: {
    marginRight: theme.spacing(2)
  },
  stats: {
    maxWidth: '100%',
    padding: theme.spacing(1),
    [theme.breakpoints.down('sm')]: {
      flexBasis: '60%'
    }
  },
  actions: {
    padding: theme.spacing(1),
    [theme.breakpoints.down('sm')]: {
      flexBasis: '50%'
    }
  }
}));

const ProjectCard = props => {
  const { project, className, ...rest } = props;

  const classes = useStyles();

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardContent className={classes.content}>
        <div className={classes.header}>
          <Avatar
            alt="Author"
            className={classes.avatar}
            src={project.area}
          >
            {getInitials(project.author?.split('#')[0])}
          </Avatar>
          <div>
            <Typography variant="body2">Id Proyecto</Typography>
            <Typography variant="h6">
              {project.projectId}
            </Typography>
            {/* <Typography variant="body2">
              por {' '}
              <Link //Leonel: retirar o agregar ruta de direccionamiento en link
                color="textPrimary"
                component={RouterLink}
                to="/management/users"
                variant="h6"
              >
                {project.author?.split('#')[0]}
              </Link>
            </Typography> */}
          </div>
        </div>
        <div className={classes.stats}>
          <Typography variant="h6">
            {''}
            {project.type}
          </Typography>
          <Typography variant="body2">Tipo de Trabajo</Typography>
        </div>
        <div className={classes.stats}>
          <Typography variant="h6">{project.status}</Typography>
          <Typography variant="body2">Estado del proyecto</Typography>
        </div>
      </CardContent>
    </Card>
  );
};

ProjectCard.propTypes = {
  className: PropTypes.string,
  project: PropTypes.object.isRequired
};

export default ProjectCard;
