import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Button,
  CircularProgress,
  LinearProgress,
  Dialog,
  DialogContent,
  TextField,
  Typography,
  colors
} from '@material-ui/core';
import axios from 'utils/axios';
import getAuthorization from 'utils/getAuthorization';
import { useHistory } from 'react-router';
// import getInitials from 'utils/getInitials';

const useStyles = makeStyles(theme => ({
  header: {
    padding: theme.spacing(3),
    maxWidth: 720,
    margin: '0 auto'
  },
  content: {
    padding: theme.spacing(0, 2),
    maxWidth: 720,
    margin: '5% auto'
  },
  helperText: {
    textAlign: 'right',
    marginRight: 0
  },
  author: {
    margin: theme.spacing(4, 0),
    display: 'flex'
  },
  avatar: {
    marginRight: theme.spacing(2)
  },
  actions: {
    backgroundColor: colors.grey[100],
    padding: theme.spacing(2),
    display: 'flex',
    justifyContent: 'center'
  },
  loader: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  applyButton: {
    color: theme.palette.white,
    backgroundColor: colors.green[600],
    '&:hover': {
      backgroundColor: colors.green[900]
    }
  }
}));

const Application = props => {
  const history = useHistory()
  const { author, open, onClose,  className, valueSelect, role, route, effect, ...rest } = props;
  const [progress, setProgress] = useState({ cuantity: 0, show:false });
  const [value, setValue] = useState('');
  const [executors, setExecutors] = useState(null);

  const classes = useStyles();

  const handleChange = event => {
    console.log(event)
    event.persist();
    setValue(event.target.value);
  };

  const onApply = async () => {
    try {
      console.log(valueSelect)
      setProgress({...progress, show: true});
      let acumulate = 0;
      const user = value.split('#');
      for (let i = 0; i < valueSelect.length; i++) {
        
        const petition = await axios.put(`${route}${valueSelect[i]._id}`,
          {
            field: '_id',
            author: value,
          },
          getAuthorization());
          if (petition.status === 200) {
            acumulate = acumulate + (100/valueSelect.length);
            setProgress({show: true, cuantity: acumulate});
            acumulate >= 100 && setTimeout(function(){ history.go('management/projects') }, 1000);
          }
      }
    } catch (error) {
      
    // }
    // console.log('values: ', value);
    // console.log('valueSelect: ', valueSelect);
    }
  };

  const getUsers = async () => {
    try {
      console.log(role);
      console.log(valueSelect)

      const petition = await axios.post('/users/pagination', {
        "roles": {
          "$elemMatch": role,
        }
      }, getAuthorization());
      if (petition.status === 200) {
        setExecutors(petition.data.data[0].data);
      }
        
    } catch (e) {
      console.error(e);
    }
  };

  useEffect(() => { open && getUsers(); }, [open]);

  return (
    <Dialog
      maxWidth="xl"
      onClose={onClose}
      open={open}
    >
      <DialogContent
        {...rest}
        className={clsx(classes.root, className)}
      >
        <div className={classes.header}>
          <Typography
            align="center"
            className={classes.title}
            gutterBottom
            variant="h3"
          >
            Asignar Tarea
          </Typography>
          <Typography
            align="center"
            className={classes.subtitle}
            variant="subtitle2"
          >
            Selecciona un {role.role} de la lista.
          </Typography>
        </div>
        <div className={classes.content}>
          { executors ? (<TextField
              fullWidth
              label="Ejecutores"
              name="executors"
              onChange={event =>
                handleChange(event)
              }
              select
              // eslint-disable-next-line react/jsx-sort-props
              SelectProps={{ native: true }}
              value={value.name}
              defaultValue=''
              variant="outlined"
            >
              <option
                value=""
                disabled
              />
              {executors.map(option => (
                <option
                  key={option.identification}
                  value={`${option.firstName} ${option.lastName}#${option.identification}`}
                >
                  {`${option.firstName} ${option.lastName}`}
                </option>
              ))}
            </TextField>) : (
              <div className={classes.loader}>
                <CircularProgress color="secondary" />
                <Typography>Buscando...</Typography>
              </div>
            )
          }
        </div>
        {progress.show && (
          <LinearProgress
            value={progress.cuantity}
            variant="determinate"
          />
        )}
        <div className={classes.actions}>
          <Button
            className={classes.applyButton}
            onClick={onApply}
            variant="contained"
          >
            Asignar
          </Button>
        </div>
      </DialogContent>
    </Dialog>
  );
};

Application.propTypes = {
  author: PropTypes.object ,
  className: PropTypes.string,
  // onApply: PropTypes.func,
  onChange: PropTypes.func,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired
};

export default Application;
