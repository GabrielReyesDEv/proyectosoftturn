import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { Drawer, Grid, Typography, Button, Hidden } from '@material-ui/core';
import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';
import { Application, UpdateReportModal } from './components';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(2)
  },
  actions: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    '& > * + *': {
      marginLeft: theme.spacing(2)
    }
  },
  buttonIcon: {
    marginRight: theme.spacing(1)
  }
}));

const BottomBar = props => {
  const {
    className,
    effect,
    fetchExecutors,
    onClose,
    onAcept,
    onChange,
    onMarkPaid,
    onMarkUnpaid,
    onCancel,
    onDelete,
    role,
    route,
    selected,
    selectedProjects,
    title,
    ...rest
  } = props;

  const classes = useStyles();
  const open = selected.length > 0;

  const [openApplication, setOpenApplication] = useState(false);

  const handleApplicationOpen = () => {
    setOpenApplication(true);
  };

  const handleApplicationClose = () => {
    setOpenApplication(false);
  };

  return (
    <Drawer
      anchor="bottom"
      open={open}
      // eslint-disable-next-line react/jsx-sort-props
      PaperProps={{ elevation: 1 }}
      variant="persistent"
    >
      <div
        {...rest}
        className={clsx(classes.root, className)}
      >
        <Grid
          alignItems="center"
          container
          spacing={2}
        >
          <Hidden smDown>
            <Grid
              item
              md={3}
            >
              <Typography
                color="textSecondary"
                variant="subtitle1"
              >
                {selected.length} seleccionados
              </Typography>
            </Grid>
          </Hidden>
          <Grid
            item
            md={6}
            xs={12}
          >
            <div className={classes.actions}>              
              <Button onClick={handleApplicationOpen}>
                <CheckIcon className={classes.buttonIcon} />
                {title}
              </Button>      
              <Button onClick={onClose}>
                <CloseIcon className={classes.buttonIcon} />
                Cancelar
              </Button>
            </div>
          </Grid>
          { openApplication && title === 'Asignar Actividades' && (
            <Application
              onApply={handleApplicationClose}
              onClose={handleApplicationClose}
              open={openApplication}
              role={role}
              route={route}
              valueSelect={selectedProjects}
            />
          )}
          { openApplication && title === 'Reporte de Actualización' && (
            <UpdateReportModal
              onApply={handleApplicationClose}
              onClose={handleApplicationClose}
              open={openApplication}
              projects={selectedProjects}
            />
          )}
        </Grid>
      </div>
    </Drawer>
  );
};

BottomBar.propTypes = {
  className: PropTypes.string,
  onClose: PropTypes.func,
  onDelete: PropTypes.func,
  onAcept: PropTypes.func,
  onMarkPaid: PropTypes.func,
  onMarkUnpaid: PropTypes.func,
  role: PropTypes.object,
  route: PropTypes.string,
  selected: PropTypes.array.isRequired,
  selectedProjects: PropTypes.array.isRequired,
  title: PropTypes.string,
};

export default BottomBar;
