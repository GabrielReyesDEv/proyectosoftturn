import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import {
  Button,
  Collapse,
  Divider,
  Drawer,
  TextField,
  Typography,
  InputLabel,
  Select,
  Checkbox,
  MenuItem,
  ListItemText,
  FormLabel,
  FormGroup,
} from '@material-ui/core';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import CloseIcon from '@material-ui/icons/Close';
import DeleteIcon from '@material-ui/icons/DeleteOutlined';
import { isEmpty, keysIn, groupBy } from 'lodash';

const useStyles = makeStyles(theme => ({
  root: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  drawer: {
    width: 420,
    maxWidth: '100%'
  },
  header: {
    padding: theme.spacing(2, 1),
    display: 'flex',
    justifyContent: 'space-between'
  },
  buttonIcon: {
    marginRight: theme.spacing(1)
  },
  content: {
    padding: theme.spacing(0, 3),
    flexGrow: 1
  },
  contentSection: {
    padding: theme.spacing(2, 0)
  },
  contentSectionHeader: {
    display: 'flex',
    justifyContent: 'space-between',
    cursor: 'pointer'
  },
  contentSectionContent: {},
  formGroup: {
    padding: theme.spacing(2, 0)
  },
  fieldGroup: {
    display: 'flex',
    alignItems: 'center'
  },
  field: {
    marginTop: 0,
    marginBottom: 0
  },
  flexGrow: {
    flexGrow: 1
  },
  addButton: {
    marginLeft: theme.spacing(1)
  },
  tags: {
    marginTop: theme.spacing(1)
  },
  minAmount: {
    marginRight: theme.spacing(3)
  },
  maxAmount: {
    marginLeft: theme.spacing(3)
  },
  radioGroup: {},
  actions: {
    padding: theme.spacing(3),
    '& > * + *': {
      marginTop: theme.spacing(2)
    }
  },
  statusField: {
    marginRight: '18px',
  }
}));

const Filter = props => {
  const { states, open, onClose, onFilter, className, ...rest } = props;

  const classes = useStyles();

  const initialValues = {
    status: [],
    createAt: ['', ''],
    updateAt: ['', ''],
  };

  const [expandProject, setExpandProject] = useState(true);
  const [values, setValues] = useState({ ...initialValues });

  const handleClear = () => {
    setValues({ ...initialValues });
  };

  const handleFieldChange = (event, field, value, index = '') => {
    event.persist && event.persist();
    if(index === '') {
      setValues(values => ({
        ...values,
        [field]: value
      }));
    } else {
      const auxDate = values.[field];
      auxDate[index] = value;
      setValues(values => ({
        ...values,
        [field]: auxDate
      }));
    }
  };

  const handleToggleProject = () => {
    setExpandProject(expandProject => !expandProject);
  };

  const handleSubmit = event => {
    event.preventDefault();
    onFilter && onFilter(values);
  };

  const dataStates = states;
  const optionValues = keysIn(groupBy(dataStates, 'projectStatus'));

  return (
    <Drawer
      anchor="right"
      classes={{ paper: classes.drawer }}
      onClose={onClose}
      open={open}
      variant="temporary"
    >
      <form
        {...rest}
        className={clsx(classes.root, className)}
        onSubmit={handleSubmit}
      >
        <div className={classes.header}>
          <Button
            onClick={onClose}
            size="small"
          >
            <CloseIcon className={classes.buttonIcon} />
            Close
          </Button>
        </div>
        <div className={classes.content}>
          <div className={classes.contentSection}>
            <div
              className={classes.contentSectionHeader}
              onClick={handleToggleProject}
            >
              <Typography variant="h5">Project</Typography>
              {expandProject ? <ExpandLessIcon /> : <ExpandMoreIcon />}
            </div>
            <Divider />
            <Collapse in={expandProject}>
              <div className={classes.contentSectionContent}>
                <div className={classes.formGroup}>
                  <div className={classes.fieldGroup}>
                    <InputLabel
                      className={classes.statusField}
                      htmlFor="select-multiple-chip"
                    >
                     State
                    </InputLabel>
                    <Select
                      className={clsx(classes.field, classes.flexGrow)}
                      multiple
                      onChange={(event) => {
                        handleFieldChange(event, 'status', event.target.value)
                      }}
                      renderValue={(selected) => {
                        const renderItems = selected.map((item) => {
                          return isEmpty(item) ? '(Blank)' : item;
                        });
                        return renderItems.join(', ');
                      }}
                      value={values.status}
                    >
                      {optionValues.map((item) => (
                        <MenuItem
                          key={item}
                          value={item}
                        >
                          <Checkbox
                            checked={values.status.indexOf(item) > -1}
                            color="primary"
                          />
                          <ListItemText
                            primary={isEmpty(item) ? '(Blank)' : item}
                          />
                        </MenuItem>
                      ))}
                    </Select>
                  </div>
                </div>
                <div className={classes.formGroup}>
                  <FormLabel>Project Creation Date</FormLabel>
                  <FormGroup row>
                    <TextField
                      InputLabelProps={{
                        shrink: true,
                      }}
                      id="startDate"
                      label="Start"
                      onChange={(event) => {
                        handleFieldChange(event, 'createAt', event.target.value, 0)
                      }}
                      style={{ width: '45%', marginRight: '5%' }}
                      type="date"
                      value={values.createAt[0] || ''}
                    />
                    <TextField
                      InputLabelProps={{
                        shrink: true,
                      }}
                      id="endDate"
                      label="Finish"
                      onChange={(event) => {
                        handleFieldChange(event, 'createAt', event.target.value, 1)
                      }}
                      style={{ width: '45%', marginRight: '5%' }}
                      type="date"
                      value={values.createAt[1] || ''}
                    />
                  </FormGroup>
                </div>
                <div className={classes.formGroup}>
                  <FormLabel>Project Update Date</FormLabel>
                  <FormGroup row>
                    <TextField
                      InputLabelProps={{
                        shrink: true,
                      }}
                      id="startDate"
                      label="Start"
                      onChange={(event) => {
                        handleFieldChange(event, 'updateAt', event.target.value, 0)
                      }}
                      style={{ width: '45%', marginRight: '5%' }}
                      type="date"
                      value={values.updateAt[0] || ''}
                    />
                    <TextField
                      InputLabelProps={{
                        shrink: true,
                      }}
                      id="endDate"
                      label="Finish"
                      onChange={(event) => {
                        handleFieldChange(event, 'updateAt', event.target.value, 1)
                      }}
                      style={{ width: '45%', marginRight: '5%' }}
                      type="date"
                      value={values.updateAt[1] || ''}
                    />
                  </FormGroup>
                </div>
              </div>
            </Collapse>
          </div>
        </div>
        <div className={classes.actions}>
          <Button
            fullWidth
            onClick={handleClear}
            variant="contained"
          >
            <DeleteIcon className={classes.buttonIcon} />
            Clear
          </Button>
          <Button
            color="primary"
            fullWidth
            type="submit"
            variant="contained"
          >
            Apply filters
          </Button>
        </div>
      </form>
    </Drawer>
  );
};

Filter.propTypes = {
  className: PropTypes.string,
  onClose: PropTypes.func,
  onFilter: PropTypes.func,
  open: PropTypes.bool.isRequired,
  states: PropTypes.object,
};

export default Filter;
