import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { Drawer, Grid, Typography, Button, Hidden } from '@material-ui/core';
import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';

import { Application } from './components';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(2)
  },
  actions: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    '& > * + *': {
      marginLeft: theme.spacing(2)
    }
  },
  buttonIcon: {
    marginRight: theme.spacing(1)
  }
}));

const TableEditBar = props => {
  const {
    selected,
    className,
    data,
    fetchUsers,
    onCancel,
    onMarkPaid,
    onMarkUnpaid,
    onDelete,
    ...rest
  } = props;

  const classes = useStyles();
  const open = selected.length > 0;

  const [openApplication, setOpenApplication] = useState(false);

  const handleApplicationOpen = () => {
    setOpenApplication(true);
  };

  const handleApplicationClose = () => {
    setOpenApplication(false);
  };

  return (
    <Drawer
      anchor="bottom"
      open={open}
      // eslint-disable-next-line react/jsx-sort-props
      PaperProps={{ elevation: 1 }}
      variant="persistent"
    >
      <div
        {...rest}
        className={clsx(classes.root, className)}
      >
        <Grid
          alignItems="center"
          container
          spacing={2}
        >
          <Hidden smDown>
            <Grid
              item
              md={3}
            >
              <Typography
                color="textSecondary"
                variant="subtitle1"
              >
                {selected.length} seleccionado{selected.length > 1 ? 's' : ''}
              </Typography>
            </Grid>
          </Hidden>
          <Grid
            item
            md={6}
            xs={12}
          >
            <div className={classes.actions}>              
              <Button onClick={handleApplicationOpen}>
                <CheckIcon className={classes.buttonIcon} />
                Asignar Actividades
              </Button>
              {onCancel && (
                <Button onClick={onCancel}>
                  <CloseIcon className={classes.buttonIcon} />
                  Cancelar
                </Button>
              )}            
            </div>
          </Grid>
          <Application
            onApply={handleApplicationClose}
            onClose={handleApplicationClose}
            open={openApplication}
            seldected={selected}
            fetchUsers={fetchUsers}
            data={data}
          />
        </Grid>
      </div>
    </Drawer>
  );
};

TableEditBar.propTypes = {
  className: PropTypes.string,
  data: PropTypes.object,
  fetchUsers: PropTypes.func,
  onDelete: PropTypes.func,
  onMarkPaid: PropTypes.func,
  onMarkUnpaid: PropTypes.func,
  selected: PropTypes.array.isRequired
};

export default TableEditBar;
