import * as actionTypes from 'actions';

const updatereport = (state = {}, action) => {
  switch (action.type) {
    case actionTypes.SAVE_UPDATEREPORT: {
      return {
        ...state,
        show: action.payload
      };
    }
    case actionTypes.SET_UPDATEREPORT: {
      return {
        ...state,
        UpdatedReport: action.payload
      };
    }
    default: {
      return state;
    }
  }
};

export default updatereport;