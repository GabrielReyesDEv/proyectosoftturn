import * as actionTypes from 'actions';
import Cookies from 'js-cookie';
import { getToken } from 'tokenUtils';

const initialState = {
  loggedIn: getToken() ? true : false,
  user: {
    first_name: Cookies.get('firstName'),
    last_name: Cookies.get('lastName'),
    email: Cookies.get('email'),
    identification: Cookies.get('identification'),
    bio: Cookies.get('workPosition'),
    role: Cookies.get('roles')
  }
};

const sessionReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SESSION_LOGIN: {
      return {
        ...state,
        user: action.payload,
        loggedIn: true,
      };
    }

    case actionTypes.SESSION_LOGOUT: {
      return {
        ...state,
        loggedIn: false,
        user: {
          role: 'GUEST'
        }
      };
    }

    default: {
      return state;
    }
  }
};

export default sessionReducer;
