import * as actionTypes from 'actions';

const alert = (state = null, action) => {
  switch (action.type) {
    case actionTypes.CREATE_ALERT:
    case actionTypes.SAVE_ALERT: {
      return action.payload;
    }

    default: {
      return state;
    }
  }
};

export default alert;