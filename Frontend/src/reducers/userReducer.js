import * as actionTypes from 'actions';

const initialState = {
  isValid: false,
  values: {},
  touched: {},
  errors: {},
  roles:[]
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.USER_EDIT: {
      return {
        ...state,
        ...action.payload,
      };
    }

    case actionTypes.ROLE_EDIT: {
      return {
        ...state,
        roles: [
          ...action.payload,
        ],
      };
    }

    default: {
      return state;
    }
  }
};

export default userReducer;
