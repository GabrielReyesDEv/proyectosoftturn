import { combineReducers } from 'redux';

import sessionReducer from './sessionReducer';
import tasksReducer from './tasksReducer';
import serviceKickOff  from './serviceKickoff';
import originDestination from './originDestination';
import formKickoff from './formKickoff';
import userReducer from './userReducer';
import checkListReducer from './checkListReducer';
import alertReducer from './saveAlert';
import assignmentReducer from './assignment';
import updateReportReducer from './updatereport';

const rootReducer = combineReducers({
  session: sessionReducer,
  tasks: tasksReducer,
  serviceReducer: serviceKickOff,
  destinationOriginReducer: originDestination,
  formKickoff: formKickoff,
  newUser: userReducer,
  checklistState: checkListReducer,
  alert: alertReducer,
  assignment: assignmentReducer,
  updatereport: updateReportReducer,
});

export default rootReducer;
