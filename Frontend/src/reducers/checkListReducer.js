import * as actionTypes from 'actions';

const checkListReducer = (state = {}, action) => {
  switch (action.type) {

    case actionTypes.SAVE_ALERT:
      return {
        ...state,
        alert: action.payload,

      };

    case actionTypes.SET_ASIGNATED:
      return {
        ...state,
        asignated: action.payload,
      };

    case actionTypes.SET_ERROR:
      return {
        ...state,
        error: action.payload,
      };

    case actionTypes.SET_USERS:
      return {
        ...state,
        users: action.payload,
      };
    
    case actionTypes.SET_STATE_MODAL:
      return {
        ...state,
        modalOpen: action.payload,
      };

    case actionTypes.SAVE_USERSDISPACHER:
      return {
        ...state,
        usersDispacher: action.payload,
      };

    case actionTypes.SAVE_CKLIST:
      return {
        ...state,
        CkList: action.payload,
      };

    case actionTypes.SAVE_CKFORMAT:
      return {
        ...state,
        Ckformat: action.payload,
      };
    
    case actionTypes.SAVE_FORMAT_TEXT:
      return {
        ...state,
        formatText: action.payload,
      };
  
    case actionTypes.UPDATE_CKLIST:
      return {
        ...state,
        CkList: { ...state.CkList,
          estadoChecklist: action.payload.estadoChecklist,
          log: action.payload.log },
      };
  
    default:
      return state;
  }
};

export default checkListReducer;