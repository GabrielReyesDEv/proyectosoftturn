import * as actionTypes from 'actions';

const sessionReducer = (state = {}, action) => {
  switch (action.type) {
    case actionTypes.SELECTED_TASKS: {
      return {
        ...state,
        selectedTasks: action.payload
      };
    }
    case actionTypes.TASKS_STATUS: {
      return {
        ...state,
        taskStatus: action.payload
      };
    }

    default: {
      return state;
    }
  }
};

export default sessionReducer;
