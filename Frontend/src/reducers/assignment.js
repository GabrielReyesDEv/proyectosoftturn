import * as actionTypes from 'actions';

const assignment = (state = {}, action) => {
  switch (action.type) {
    case actionTypes.SAVE_ASSIGNMENT: {
      return {
        ...state,
        show: action.payload
      };
    }
    case actionTypes.SET_ASSIGNED: {
      return {
        ...state,
        Assigned: action.payload
      };
    }
    case actionTypes.SET_RESPONSABLE: {
      return {
        ...state,
        responsable: action.payload
      };
    }
    default: {
      return state;
    }
  }
};

export default assignment;