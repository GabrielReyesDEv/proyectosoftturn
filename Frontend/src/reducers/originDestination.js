import * as actionTypes from 'actions';

const originDestinatio = (state = {}, action) => {
  switch (action.type) {
    case actionTypes.SAVE_ORIGIN_DESTINATION: {
      return {
        originDestination: action.payload
      };
    }

    default: {
      return state;
    }
  }
};

export default originDestinatio;
