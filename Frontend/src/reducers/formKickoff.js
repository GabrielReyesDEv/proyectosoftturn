import * as actionTypes from 'actions';

const formKickoff = (state = {}, action) => {
  switch (action.type) {
    case actionTypes.SAVE_FORM_KICKOFF: {
      return {
        ...state,
        formKickoff: {
          ...state.formKickoff,
          ...action.payload}
      };
    }
    case actionTypes.SAVE_CHECK_KICKOFF: {
      return {
        ...state,
        checkKickOff: action.payload
      };
    }

    case actionTypes.SAVE_INPUTS_OTPS: {
      return {
        ...state,
        inputsOtp: action.payload
      };
    }

    case actionTypes.SAVE_OTP: {
      return {
        ...state,
        otpInfo: action.payload
      };
    }

    default: {
      return state;
    }
  }
};

export default formKickoff;
