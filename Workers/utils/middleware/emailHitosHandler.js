require('dotenv').config();
var nodemailer = require('nodemailer');
const hbs = require('nodemailer-express-handlebars');
const HitoService = require('../../services/hitos');
// email sender function

const hitoService = new HitoService();
const getHito = async (id) => {
    // return await hitoService.getHito( id );
    const resolve = await hitoService.getHito( id )

    return resolve;
}

/* const observaciones = (val) => {switch(val) {
    case 'Vista Obra Civil (VOC)':
        return val.visitaObraCivil.observacionesVoc;
    case 'Visita EjecucionObra civil (EOC)':
        return val.visitaEjecucionObraCivil.observacionesVeoc;
    case 'Empalmes (EM)':
        return val.empalmesEm.observacionesEm;
    case 'Entrega Servicio':
        return val.entregaServicio.observacionesEntregaServicio;
    case 'Pendiente Cliente':
        return val.observacionesGenrales;
    }
} */

const showImages = (actual) => {
    switch(actual) {
    case 'Vista Obra Civil (VOC)':
        return [1];
    case 'Visita EjecucionObra civil (EOC)':
        return [1, 2];
    case 'Empalmes (EM)':
        return [1, 2, 3];
    case 'Entrega Servicio':
        return [1, 2, 3, 4];
    default:
        return [];
    }
}

exports.sendEmail = function(res, req, data, info){
    var template = '';
    var timeline = '';
    var otpIds = [];
    var x = 0;
    for (const otp of data) {
        getHito( otp.otpId ).then((hito) => {(hito)
            // const obs = observaciones(hito.actividadActual);
            // console.log('hito: ',hito); 
            // console.log('data: ',data);
            // console.log('info: ',info);

            otpIds.push(otp.otpId);

            template += `
                    <tr>
                        <td width="156" nowrap="" valign="bottom" style="width:78.0pt; border:solid windowtext 1.0pt; border-top:none; padding:0cm 3.5pt 0cm 3.5pt; height:60.0pt">
                            <p class="x_MsoNormal" align="right" style="text-align:right"><span style="font-size:10.0pt; font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;; color:black">${otp.otpId}<u></u><u></u></span></p>
                        </td>
                        <td width="154" nowrap="" valign="bottom" style="width:77.0pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt; padding:0cm 3.5pt 0cm 3.5pt; height:60.0pt">
                            <p class="x_MsoNormal" align="right" style="text-align:right"><span style="font-size:10.0pt; font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;; color:black">${otp.cliente.codigoServicio}<u></u><u></u></span></p>
                        </td>
                        <td width="124" nowrap="" valign="bottom" style="width:62.05pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt; padding:0cm 3.5pt 0cm 3.5pt; height:60.0pt">
                            <p class="x_MsoNormal"><span style="font-size:10.0pt; font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;; color:black">${otp.ciudad}
                        <u></u><u></u></span></p>
                        </td>
                        <td width="218" valign="bottom" style="width:108.75pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt; padding:0cm 3.5pt 0cm 3.5pt; height:60.0pt">
                            <p class="x_MsoNormal"><span style="font-size:10.0pt; font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;; color:black">${otp.direccion}<u></u><u></u></span></p>
                        </td>
                        <td width="150" valign="bottom" style="width:75.0pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt; padding:0cm 3.5pt 0cm 3.5pt; height:60.0pt">
                            <p class="x_MsoNormal"><span style="font-size:10.0pt; font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;; color:black">${hito.visitaObraCivil.noAplicaVoc ? 'NO APLICA VISITA OBRA CIVIL' : `${hito.visitaObraCivil.estadoVoc} <br>${hito.visitaObraCivil.fechaCompromisoVoc}` }<u></u><u></u></span></p>
                        </td>
                        <td width="170" valign="bottom" style="width:85.15pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt; padding:0cm 3.5pt 0cm 3.5pt; height:60.0pt">
                            <p class="x_MsoNormal"><span style="font-size:10.0pt; font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;; color:black">${hito.visitaEjecucionObraCivil.noAplicaVeoc ? 'NO APLICA OBRA CIVIL' : `${hito.visitaEjecucionObraCivil.estadoVeoc} <br>${hito.visitaEjecucionObraCivil.fechaCompromisoVeoc}` }
                            <u></u><u></u></span></p>
                        </td>
                        <td width="150" valign="bottom" style="width:75.0pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt; padding:0cm 3.5pt 0cm 3.5pt; height:60.0pt">
                            <p class="x_MsoNormal"><span style="font-size:10.0pt; font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;; color:black">&nbsp; ${hito.empalmesEm.noAplicaEm ? 'NO APLICA EMPALMES' : `${hito.empalmesEm.estadoEm} <br>${hito.empalmesEm.fechaCompromisoEm}` }
                            <u></u><u></u></span></p>
                        </td>
                        <td width="178" valign="bottom" style="width:89.0pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt; padding:0cm 3.5pt 0cm 3.5pt; height:60.0pt">
                            <p class="x_MsoNormal"><span style="font-size:10.0pt; font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;; color:black">&nbsp; ${hito.entregaServicio.noAplicaEntregaServicio ? 'NO APLICA ENTREGA SERVICIO' : `${hito.entregaServicio.estadoEntregaServicio} <br>${hito.entregaServicio.fechaEntregaServicio}` }
                            <u></u><u></u></span></p>
                        </td>
                        <td width="360" valign="bottom" style="width:180.0pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt; padding:0cm 3.5pt 0cm 3.5pt; height:60.0pt">
                            <p class="x_MsoNormal"><span style="font-size:10.0pt; font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;; color:black">${hito.entregaServicio.observacionesEntregaServicio ? hito.entregaServicio.observacionesEntregaServicio : 
                                                                                                                                                        hito.visitaObraCivil.observacionesVoc ? hito.visitaObraCivil.observacionesVoc :
                                                                                                                                                        hito.visitaEjecucionObraCivil.observacionesVeoc ? hito.visitaEjecucionObraCivil.observacionesVeoc :
                                                                                                                                                        hito.empalmesEm.observacionesEm ? hito.empalmesEm.observacionesEm : 
                                                                                                                                                        hito.entregaServicio.observacionesGenrales ? hito.entregaServicio.observacionesGenrales : 'Sin Observaciones' }
                            <u></u><u></u></span></p>
                        </td>
                    </tr>`;

            timeline += `
                    <div dir="ltr">
                        <ul style="font-family:Calibri,Helvetica,sans-serif; margin-bottom:0cm">
                            <li style="list-style-type: none; display: flex; flex-wrap: nowrap;">
                            ${hito.actividadActual != 'Pendiente Cliente' ? '' : `<img src="https://zolid.s3-us-west-2.amazonaws.com/reporteActualizacion/pendienteCliente.png" width="auto" height="40">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;` }
                            <span style="font-size:15.0pt; font-family:&quot;Berlin Sans FB&quot;,sans-serif; color:black; background:white">OT&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            <span style="font-size:15.0pt; color:black; border:none windowtext 1.0pt; padding:0cm">
                            ${otp.otpId} ${otp.direccion}</span></li>
                        </ul>
                        <p style="font-family:Calibri,sans-serif; margin:0cm 0cm 0.0001pt 36pt; font-size:11pt; line-height:normal">
                            <span style="font-size:12.0pt; font-family:&quot;Times New Roman&quot;,serif"></span>
                        </p>
                        <br>
                        <table>
                            <tr>
                            ${ hito.visitaObraCivil.noAplicaVoc ? '' : ((hito.visitaObraCivil.estadoVoc != 'Fecha tentativa' && showImages(hito.actividadActual).includes(1)) ? 
                                                                                                                                    `<td align="center"><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">Visita Obra Civil</span></b><br><img src="https://zolid.s3-us-west-2.amazonaws.com/reporteActualizacion/VisitaObraCivil_verde.png"  width="auto" height="110"><br><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">${hito.visitaObraCivil.fechaCompromisoVoc}</span></b></td>`
                                                                                                                                    : `<td align="center"><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">Visita Obra Civil</span></b><br><img src="https://zolid.s3-us-west-2.amazonaws.com/reporteActualizacion/VisitaObraCivil_gris.png"  width="auto" height="110"><br><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">${hito.visitaObraCivil.fechaCompromisoVoc}</span></b></td>` )}
                                                                                                                                   
                                ${ hito.visitaEjecucionObraCivil.noAplicaVeoc ? '' : (hito.visitaEjecucionObraCivil.estadoVeoc == 'Fecha tentativa' && showImages(hito.actividadActual).includes(2)) ? 
                                                                                        
                                                                                                                                        `<td align="center"><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">Ejecución Obra Civil</span></b><br><img src="https://zolid.s3-us-west-2.amazonaws.com/reporteActualizacion/ejecucionObraCivil_verde.png"  width="auto" height="110"><br><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">${hito.visitaEjecucionObraCivil.fechaCompromisoVeoc}</span></b></td>` 
                                                                                                                                        : 
                                                                                                                                        `<td align="center"><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">Ejecución Obra Civil</span></b><br><img src="https://zolid.s3-us-west-2.amazonaws.com/reporteActualizacion/ejecucionObraCilvil_gris.png"  width="auto" height="110"><br><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">${hito.visitaEjecucionObraCivil.fechaCompromisoVeoc}</span></b></td>`
                                                                                                                                    }
                                ${ hito.empalmesEm.noAplicaEm ? '' : ((hito.empalmesEm.estadoEm == 'Fecha tentativa'  && showImages(hito.actividadActual).includes(3)) ? 
                                                                                                        `<td align="center"><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">Empalmes</span></b><br><img src="https://zolid.s3-us-west-2.amazonaws.com/reporteActualizacion/Empalme_verde.png"  width="auto" height="110"><br><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">${hito.empalmesEm.fechaCompromisoEm}</span></b></td>`
                                                                                                        :
                                                                                                        `<td align="center"><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">Empalme</span></b><br><img src="https://zolid.s3-us-west-2.amazonaws.com/reporteActualizacion/Empalme_gris.png"  width="auto" height="110"><br><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">${hito.empalmesEm.fechaCompromisoEm}</span></b></td>`
                                                                                                                        )}
                                ${ hito.entregaServicio.noAplicaEntregaServicio ? '' : ((hito.entregaServicio.estadoEntregaServicio == 'Fecha tentativa'  && showImages(hito.actividadActual).includes(4)) ? 
                                                                                                                                        `<td align="center"><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">Entrega Servicio</span></b><br><img src="https://zolid.s3-us-west-2.amazonaws.com/reporteActualizacion/entregaServicio_verde.png"  width="auto" height="110"><br><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">${hito.entregaServicio.fechaEntregaServicio}</span></b></td>`
                                                                                                                                        :
                                                                                                                                        `<td align="center"><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">Entrega Servicio</span></b><br><img src="https://zolid.s3-us-west-2.amazonaws.com/reporteActualizacion/entregaServicio_gris.png"  width="auto" height="110"><br><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">${hito.entregaServicio.fechaEntregaServicio}</span></b></td>`
                                                                                                                                    )}
                                
                            </tr>
                        </table>
                    </div>
                    <br><br>`;
        
        }).then(() => {
            x++;
        if (x == data.length) {
            var mailOptions = {
                from: 'Remitente',
                to: info.contacto.email,
                subject: `REPORTE DE ACTUALIZACION DE ACTIVIDADES ${data[0].cliente.servicio.toUpperCase()} - ${info.cliente.toUpperCase()} / OT ${otpIds.join(' - ')}`,
                text: 'Prueba Test',
                template: 'hitos',
                context: {
                    data: info,
                    template,
                    timeline
                }
            };


            var transporter = nodemailer.createTransport({
                // service: 'Gmail',
                host: 'smtp.gmail.com',
                port: 465,
                secure: true,
                auth: {
                    user: process.env.EMAIL,
                    pass: process.env.EMAIL_PASSWORD
                }
            });

            transporter.use('compile', hbs({
            viewEngine: {
                extName: '.handlebars',
                partialsDir: __dirname + '../../../views/',
                layoutsDir:  __dirname + '../../../views/',
                defaultLayout : 'hitos',
            },
            viewPath:  __dirname + '../../../views/'
            }));

            // Enviamos el email
            transporter.sendMail(mailOptions, function(error, info2){
                if (error){
                    console.log(error, info2); //eslint-disable-line
                    res.status(500).send(error.message);
                } else {
                    res.status(200).jsonp(req.body);
                    res.writeHead(301);
                    res.end();
                }
            });

            }    
        });
    };



};
