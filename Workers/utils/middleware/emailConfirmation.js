require('dotenv').config();
var nodemailer = require('nodemailer');
const hbs = require('nodemailer-express-handlebars');
// const handlerHelpers = require('../helpers/handlerbars');


exports.sendEmail = async function(data, email){
  try {
    
    const transporter = nodemailer.createTransport({
      service: 'gmail',
      host: 'smtp.gmail.com',
      port: 587,
      secure: false,
      auth: {
          user: process.env.EMAIL,
          pass: process.env.EMAIL_PASSWORD,
      }
    });
  
    var mailOptions = {
      from: process.env.EMAIL,
      to: email,
      subject:'Notificación de Cargue',
      text: 'Prueba Test',
      template: 'layout'
    };

    transporter.use('compile', hbs({
      viewEngine: {
        extName: '.handlebars',
        partialsDir: __dirname + '/../../views/',
        layoutsDir:  __dirname + '/../../views/',
        defaultLayout : 'layout',
        // helpers: handlerHelpers
      },
      viewPath:  __dirname + '/../../views/'
    }));

    const result = await  transporter.sendMail(mailOptions);

    return result;

  } catch (error) {
    console.log('error: ', error);
  }
};
