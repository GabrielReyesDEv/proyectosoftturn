require('dotenv').config();
var nodemailer = require('nodemailer');
const hbs = require('nodemailer-express-handlebars');
const handlerHelpers = require('../helpers/handlerbars');
// email sender function
exports.sendEmail = function(res, req, data, email){
// Definimos el transporter
const mailOpt = () => { 
    switch (data.idServicio) {
        case ('1'):
        return ['InstalacionServicioTelefoniaFijaPublicaSIP-InternetDedicado', `Reporte de Inicio de Actividades CLiente ${data.reporteInicio.nombre_cliente_val} OTP ${data.otpId} Instalación Servicio Telefonía Pública SIP - Internet Dedicado`];
        case ('2'): 
        return ['InternetDedicado', `Reporte de Inicio de Actividades Cliente ${data.reporteInicio.nombre_cliente_val} OTP ${data.otpId}  Instalación Servicio Internet Dedicado`];
        case ('3'):
        return ['InstalacionServicioMPLSAvanzadoIntranet', `Reporte de Inicio de Actividades Cliente ${data.reporteInicio.nombre_cliente_val} OTP ${data.otpId}  Instalación Servicio MPLS Avanzando Intranet` ];
        case ('4'): 
        return ['InstalacionServicioMPLSAvanzadoSolucionIntrtanetVariosPuntos',`Reporte de Inicio de Actividades Cliente ${data.reporteInicio.nombre_cliente_val} OTP ${data.otpId} Instalación Servicio MPLS Avanzado Intranet - Varios Puntos` ];
        case ('5'):
        return ['InstalacionServicioMLPSAvanzadoIntranetBackupUltimaMillaNDS2', `Reporte de Inicio de Actividades Cliente ${data.reporteInicio.nombre_cliente_val} OTP ${data.otpId} Instalación Servicio MPLS Avanzado Intranet con Backup de Ultima Milla - NDS 2`];
        case ('6'): 
        return ['InstalaciónServicioMPLSAvanzadoIntranetBackupUltimaMillaRouterNDS1', `Reporte de Inicio de Actividades Cliente ${data.reporteInicio.nombre_cliente_val} OTP ${data.otpId} Instalación Servicio MPLS Avanzado Intranet con Backup de Última Milla y Router - NDS1`];
        case ('7'): 
        return ['InstalacionMPLSAvanzadoSolucionExtranet', `Reporte de Inicio de Actividades Cliente ${data.reporteInicio.nombre_cliente_val} OTP ${data.otpId} Instalación Servicio MPLS Avanzado Extranet`];
        case ('8'): 
        return ['Backend_MPLS', ];
        case ('9'): 
        return ['MPLS_Avanzado_con_Componente_Datacenter_Claro', ];
        case ('10'): 
        return ['InstalacionServicioMLPSTransaccionalSolucion3G', `Reporte de Inicio de Actividades Cliente ${data.reporteInicio.nombre_cliente_val} OTP ${data.otpId} Instalación Servicios MPLS Transaccional 3G`];
        case ('11'): 
        return ['InstalacionServicioAeropuertoelDoradoOpain',`Reporte de Inicio de Actividades Cliente ${data.reporteInicio.nombre_cliente_val} OTP ${data.otpId} Instalación Servicios Aeropuerto El Dorado Opain`];
        case ('12'): 
        return ['CambioEquiposServicio', `Reporte de Inicio de Actividades Cliente ${data.reporteInicio.nombre_cliente_val} OTP ${data.otpId} Cambio de Equipos Servicio ${data.reporteInicio.servicio}` ];
        case ('13'): 
        return ['CambioServicioTelefoniaPublicaLineaBasicaALineaE1', `Reporte de Inicio de Actividades Cliente ${data.reporteInicio.nombre_cliente_val} OTP ${data.otpId} Cambio de Servicio Telefonía Fija Pública Línea Básica a Línea E1`];
        case ('14'): 
        return ['CambioDeServicioTelefoniaFijaPublicaLineaSIPaPBXDistribuidaLineaSIP', `Reporte de Inicio de Actividades Cliente ${data.reporteInicio.nombre_cliente_val} OTP ${data.otpId} Cambio de Servicio Telefonía Fija Pública Línea SIP a PBX Distribuida Línea SIP`];
        case ('15'):
        return ['TrasladoExternoServicio',`Reporte de Inicio de Actividades Cliente ${data.reporteInicio.nombre_cliente_val} OTP ${data.otpId} Traslado Externo Servicio ${data.reporteInicio.servicio}`];
        case ('16'):
        return ['TrasladoInternoServicio',`Reporte de Inicio de Actividades Cliente ${data.reporteInicio.nombre_cliente_val} OTP ${data.otpId} Traslado Interno Servicio ${data.reporteInicio.traslado_servicio}`];
        case ('17'): 
        return ['InstalacionServicioSolucionesAdministradasComunicacionesUnificadasPBXadministrada', `Reporte de Inicio de Actividades Cliente ${data.reporteInicio.nombre_cliente_val} OTP ${data.otpId} Instalación Servicio Soluciones Administradas Comunicaciones Unificadas - PBX Administrada`];
        case ('18'): 
        return ['InstalacionServicioTelefoniaFijaPBXDistribuidaLineaE1', `Reporte de Inicio de Actividades Cliente ${data.reporteInicio.nombre_cliente_val} OTP ${data.otpId} Instalación Servicio Telefonía Fija PBX Distribuida Línea E1` ];
        case ('19'): 
        return ['InstalacionTelefoniaFijaPBXDistribuidaLineaSIP', `Reporte de Inicio de Actividades Cliente ${data.reporteInicio.nombre_cliente_val} OTP ${data.otpId} Instalación Servicio Telefonía Fija PBX Distribuida Línea SIP`];
        case ('20'): 
        return ['InstalacionServicioTelefoniaFijaPBXDistribuidaLineaSIPGatewayVoz', `Reporte de Inicio de Actividades Cliente ${data.reporteInicio.nombre_cliente_val} OTP ${data.otpId} Instalación Servicio Telefonía Fija PBX Distribuida Línea SIP - Gateaway de Voz` ];
        case ('21'): 
        return ['InstalacionTelefoniaPublicaBasica-InternetDedicado', `Reporte de Inicio de Actividades Cliente ${data.reporteInicio.nombre_cliente_val} OTP ${data.otpId} Instalación Telefonía Pública Básica - Internet Dedicado`];
        case ('22'): 
        return ['AmpliacionServicioCambioUltimaMilla', `Reporte de Inicio de Actividades Cliente ${data.reporteInicio.nombre_cliente_val} OTP ${data.otpId} Ampliación de Servicio ${data.reporteInicio.servicio} con Cambio de Última Milla`];
        case ('23'):
        return ['AmpliacionServicioCambioEquipo', `Reporte de Inicio de Actividades Cliente ${data.reporteInicio.nombre_cliente_val} OTP ${data.otpId} Ampliación de Servicio ${data.reporteInicio.servicio} con Cambio de Equipo`];
        case ('24'):
        return ['InstalacionServicioPLEthernet',`Reporte de Inicio de Actividades Cliente ${data.reporteInicio.nombre_cliente_val} OTP ${data.otpId} Instalación Servicio PL Ethernet`];
        case ('25'):
        return ['Procesos_ampliacion_servicios', ];
        case ('30'): 
        return ['AmpliacionServiciosCambioEquipo',`Reporte de Inicio de Actividades Cliente ${data.reporteInicio.nombre_cliente_val} OTP ${data.otpId} Ampliacion de Servicio ${data.reporteInicio.servicio} con Cambio de Equipo`];
        case ('31'): 
        return ['InstalacionServicioMPLSTransaccionalFO',`Reporte de Inicio de Actividades Cliente ${data.reporteInicio.nombre_cliente_val} OTP ${data.otpId} Instalación Servicio MPLS Transaccional FO`];
        case ('32'): 
        return ['',''];
        case ('33'): 
        return ['InstalacionServicioMPLSTransaccionalHFC',`Reporte de Inicio de Actividades Cliente ${data.reporteInicio.nombre_cliente_val} OTP ${data.otpId} Instalación Servicio MPLS Transaccional HFC`];
        case ('34'):
        return ['InstalacionServicioTelefoniaPublicaSIP-InternetDedicadoEmpresarial', `Reporte de Inicio de Actividades Cliente ${data.reporteInicio.nombre_cliente_val} OTP ${data.otpId} Instalación Servicio Telefonia Publica  SIP - Internet  Dedicado Empresarial`];
        case('35'):
        return ['InstalacionServicioTelefoniaPublicaLineaBasica(Analoga)',`Reporte de Inicio de Actividades Cliente ${data.reporteInicio.nombre_cliente_val} OTP ${data.otpId} Instalación Servicio Telefonia Publica Linea Basica`];
        case('36'):
        return ['InstalacionServicioTelefoniaPublicaLineaE1',`Reporte de Inicio de Actividades Cliente ${data.reporteInicio.nombre_cliente_val} OTP ${data.otpId} Instalación Servicio Telefonia Publica Linea E1`];
        case('37'):
        return ['',``];
        case('38'):
        return ['',``];
    default:
        return ['Telefonia_publica_lineas_sip', ]
    ;
    }
}

    var mailOptions = {
        from: 'Remitente',
        to: email,
        subject: mailOpt()[1],
        text: 'Prueba Test',
        template: mailOpt()[0],
        context: {
            data
        }
    };


    var transporter = nodemailer.createTransport({
        // service: 'Gmail',
        host: 'smtp.gmail.com',
        port: 465,
        secure: true,
        auth: {
            user: process.env.EMAIL,
            pass: process.env.EMAIL_PASSWORD
        }
    });

transporter.use('compile', hbs({
  viewEngine: {
    extName: '.handlebars',
    partialsDir: __dirname + '../../../views/',
    layoutsDir:  __dirname + '../../../views/',
    defaultLayout : mailOpt()[0],
    helpers: handlerHelpers
  },
  viewPath:  __dirname + '../../../views/'
}));

// Enviamos el email
transporter.sendMail(mailOptions, function(error, info){
    if (error){
        console.log(error, info); //eslint-disable-line
        res.status(500).send(error.message);
    } else {
        console.log("Email sent"); //eslint-disable-line
        //res.status(200).jsonp(req.body);
        res.writeHead(301);
        res.end();
    }
});
};
