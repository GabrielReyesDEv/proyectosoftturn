/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
const { Worker } = require('node-resque');
require('dotenv').config();
const OSS = require('ali-oss');
const fs = require('fs');
const axios = require('axios');
const { getXlsxStream } = require('xlstream');
var Email = require('./utils/middleware/emailConfirmation');


const path = require('path');
const { async } = require('rxjs');
const { log } = require('console');

const fileName = path.resolve(__dirname, 'import/cargue.xlsx')

var totalUsers = {};

async function acumulate(obj) {
  if (Object.keys(obj).length > 0) {
    Object.keys(obj).map(title => {
      totalUsers[title]
        ? totalUsers[title] = {
          taskConf: totalUsers[title].taskConf
            ? totalUsers[title].taskConf + (obj[title].taskConf || 0)
            : obj[title].taskConf || 0,
          taskKO: totalUsers[title].taskKO
            ? totalUsers[title].taskKO + (obj[title].taskKO || 0)
            : obj[title].taskKO || 0 
          }
        : totalUsers[title] = obj[title]
    })
  }
}
async function processChunk(group) {
  try {
    const consult = await axios({
    url: `${process.env.API_URL}/api/activities/importUpdt`,
    method: 'post',
    data: group,
    });
    return consult.data.data.totalUsers;
  } catch (e) {
    console.error('Failure!');
    console.error('Import error: ', e);
    console.error(e && e.response && e.response.status);
  }
}

async function getEmails() {
  try {
    const consult = await axios.post(
      `${process.env.API_URL}/api/email/getByRole`,
      {
        'query': {
          "roles": {
            "$elemMatch": {
              'role': 'Coordinator',
              'group': 'BO',
              'area': 'Nivel 1',
            }  
          }
        },
        'filters':{
          "_id":0,
          "email": 1,
        }
    });
    if (consult.status === 200) {

         const mails = [];
         consult.data.data.map((arr) => {
           return mails.push(arr.email);
         });

        const emailInfo = Email.sendEmail('info.otps', mails);
        return emailInfo;
    }
  } catch (e) {
    console.error('Failure! email');
    console.error('Email error: ', e);
    console.error(e && e.response && e.response.status);
  }
}

async function boot() {
  const connectionDetails = {
    pkg: "ioredis",
    host: process.env.REDIS_HOST,
    password: null,
    port: process.env.REDIS_PORT,
    database: 0,
  };

  const jobs = {
    saveImportDataToLocal: {
      
      perform: async () => {
        totalUsers = {};
        const filePath = './import/cargue.xlsx';
        if (fs.existsSync(filePath)) {
          fs.unlink(filePath, function (err) {
            if (err) throw err;
            console.log('File deleted!');
          }); 
        }
        const client = new OSS({
          region: process.env.ALI_OSS_REGION,
          accessKeyId: process.env.ALI_ACCESS_KEY_ID,
          accessKeySecret: process.env.ALI_ACCESS_KEY_SECRET,
          bucket: process.env.ALI_BUCKET
        });
        var file = fs.createWriteStream(filePath);
        const result = await client.getStream('zolid_new/cargue.xlsx');

        result.stream.pipe(file).on('close', async function() {
          (async () => {
            const stream = await getXlsxStream({
                filePath: fileName,
                sheet: 0,
            });
            let document = [];
            let header = [];
            let counter = 0;
            let group = 0
            let total = 0


            stream.on('data', async x => {
              counter += 1
              total += 1
              // console.log("arr", x.raw.arr)
              if (total == 1){
                  header = x.raw.arr
              } else {
                let rowData = {};
                x.raw.arr.forEach(function(rowVal, colNum){
                  rowData[header[colNum]] = rowVal
                });
                Object.keys(rowData).length !== 0 && (document.push(rowData));
                if (counter == 500){
                  counter = 0
                  group++
                  const data = document
                  document = []
                  stream.pause()
                  const consult = await processChunk(data);
                  await acumulate(consult);
                  stream.resume();
                }
              }
              
            });
            stream.on('end', async () => {
              if (counter < 500 && counter > 0) {
                const consult = await processChunk(document);
                await acumulate(consult);
                await getEmails();
              }
              if (fs.existsSync(filePath)) {
                fs.unlink(filePath, function (err) {
                  if (err) throw err;
                  console.log('File deleted!');
                }); 
              }
              try {
                for (let i = 0; i < Object.keys(totalUsers).length; i++) {
                  let index = Object.keys(totalUsers)[i];
                  // await axios({
                  //   url: 'http://localhost:5000/api/notifications/toClient',
                  //   method: 'post',
                  //   data: {
                  //     to: index,
                  //     msg: `cargue finalizado. Tareas de conf: ${totalUsers[index].taskConf}, Tareas de KO: ${totalUsers[index].taskKO}`,
                  //   },
                  // });                  
                }
                await axios({
                  url: 'http://localhost:5000/api/notifications/toClient',
                  method: 'post',
                  data: {
                    to: 'mainRoom',
                    msg: 'Cargue terminado',
                  },
                });
              } catch (error) {
                console.log('error: ', error)
              } 
            })
        })();
          return 'true'
        });
      }
    }
  }
  const worker = new Worker(
    { connection: connectionDetails, queues: ["default"] },
    jobs
  );
  await worker.connect();
  worker.start();
}

boot()