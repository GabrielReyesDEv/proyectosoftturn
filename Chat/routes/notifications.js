const express = require('express');
const passport = require('passport');
const NotificationsService = require('../services/notifications');
// const cacheResponse = require('../utils/cacheResponse');
// const { FIVE_MINUTES_IN_SECONDS } = require('../utils/time');

// JWT Strategy
// require('../utils/auth/strategies/jwt')

// const {
//     createActivitySchema
// } = require('../utils/schemas/activity');

// const validationHandler = require('../utils/middleware/validationHandler');

function notificationsApi(app, io) {
    const router = express.Router();
    app.use('/api/notifications', router);
    const notificationsService = new NotificationsService();

    router.post('/toClient',
        passport.authenticate('jwt', { session: false } ),
        async function (req, res, next) {
        const { body: connectionNotifications } = req;
        try {
            const createNotification = await notificationsService.newNotification(connectionNotifications, io);

            res.status(201).json({
                data: { createNotification },
                message: 'activity created'
            });

        } catch (err) {
            next(err);
        }
    });
}

module.exports = notificationsApi;