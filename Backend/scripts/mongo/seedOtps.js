// DEBUG=app:* node scripts/mongo/seedOtps.js

const chalk = require('chalk')
const debug = require('debug')('app:scripts:otps')
const MongoLib = require('../../lib/mongo')
const { otpsMock } = require('../../utils/mocks/otps')

async function seedOtps () {
  try {
    const mongoDB = new MongoLib()

    const promises = otpsMock.map(async otp => {
      await mongoDB.create('otps', otp)
    })

    await Promise.all(promises)
    debug(chalk.green(`${promises.length} otps have been created succesfully`))
    return process.exit(0)
  } catch (error) {
    debug(chalk.red(error))
    process.exit(1)
  }
}

seedOtps()
