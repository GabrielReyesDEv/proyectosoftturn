//DEBUG=app:* node scripts/mongo/seedApiKeys.js

const chalk = require('chalk')
const crypto = require('crypto')
const debug = require('debug')('app:scripts:api-keys')
const MongoLib = require('../../lib/mongo')

const adminScopes = [
  'signin:auth',
  'signup:auth',
  'read:tasks',
  'create:tasks',
  'update:tasks',
  'delete:tasks',
  'read:projects',
  'create:projects',
  'update:projects',
  'delete:projects',
  'read:activities',
  'create:activities',
  'update:activities',
  'delete:activities',
  'read:users',
  'create:users',
  'update:users',
  'delete:users',
  'read:areas',
  'create:areas',
  'update:areas',
  'delete:areas',
  'read:notifications',
  'create:notifications',
  'update:notifications',
  'delete:notifications',
]

const coordinatorScopes = [
  'signin:auth',
  'signup:auth',
  'read:tasks',
  'create:tasks',
  'update:tasks',
  'read:projects',
  'create:projects',
  'update:projects',
  'read:activities',
  'create:activities',
  'update:activities',
  'read:users',
  'create:users',
  'update:users',
  'read:areas',
  'create:areas',
  'update:areas',
  'read:notifications',
  'create:notifications',
  'update:notifications',
]

const publicScopes = [
  'signin:auth',
  'signup:auth',
  'read:tasks',
  'read:projects',
  'read:activities',
  'read:notifications'
]

const apiKeys = [
  {
    token: generateRandomToken(),
    scopes: adminScopes
  },
  {
    token: generateRandomToken(),
    scopes: coordinatorScopes
  },
  {
    token: generateRandomToken(),
    scopes: publicScopes
  }
]

function generateRandomToken() {
  const buffer = crypto.randomBytes(32)
  return buffer.toString('hex')
}

async function seedApiKeys() {
  try {
    const mongoDB = new MongoLib();

    const promises = apiKeys.map(async apiKey => {
      await mongoDB.create('api-keys', apiKey)
    })

    await Promise.all(promises)
    debug(chalk.green(`${promises.length} api-keys have been generated succesfully`))
    return process.exit(0)
  } catch (error) {
    debug(chalk.red(error))
    process.exit(1)
  }
}

seedApiKeys()