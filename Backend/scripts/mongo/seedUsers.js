// DEBUG=app:* node scripts/mongo/seedUsers.js

const bcrypt = require('bcrypt')
const chalk = require('chalk')
const debug = require('debug')
const MongoLib = require('../../lib/mongo')
const { config } = require('../../config/index')

const users = [
  {
    email: 'root@undefined.sh',
    name: 'ROOT',
    password: config.defaultAdminPassword,
    isAdmin: true
  },
  {
    email: 'user1@undefined.sh',
    name: 'user1',
    password: config.defaultUserPassword
  },
  {
    email: 'user2@undefined.sh',
    name: 'user2',
    password: config.defaultUserPassword
  },
]

async function createUser(mongoDB, user) {
  const { firstName, lastName, email, password, isAdmin } = user
  const hashedPassword = await bcrypt.hash(password, 10)

  const userId = await mongoDB.create('users', {
    firstName,
    lastName,
    email,
    password: hashedPassword,
    isAdmin: Boolean(isAdmin)
  })

  return userId
}

async function seedUsers() {
  try {
    const mongoDB = new MongoLib()

    const promises = users.map(async user => {
      const userId = await createUser(mongoDB, user)
      debug(chalk.green('User created with id:', userId))
    })

    await Promise.all(promises)
    return process.exit(0)
  } catch(error) {
    debug(chalk.red(error))
    process.exit(1)
  }
}

seedUsers()