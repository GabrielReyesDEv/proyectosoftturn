const joi = require('@hapi/joi');

const notificationIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);

const notificationSchema = {
  title: joi
    .string()
    .max(500)
    .required(),
  type: joi
    .string()
    .valid("Projects", "Activities", "Tasks", "Notifications")
    .max(100)
    .required(),
  typeId: joi
    .number()
    .required(),
  view: joi
    .boolean()
    .required(),
  ownerId: joi
    .string()
    .max(100)
    .required(),
  owner: joi
    .string()
    .max(500)
    .required(),
  author: joi
    .string()
    .max(500)
    .required(),
  updated_at: joi
    .string()
    .max(500)
    .required(),
  field: joi
    .string()
    .max(500),
};

const createNotificationSchema = {
  ...notificationSchema
};

const updateNotificationSchema = {
  view: joi
    .boolean()
    .required(),
  updated_at: joi
    .string()
    .max(500)
    .required(),
    field: joi
  .string()
  .max(500),
};

module.exports = {
  notificationIdSchema,
  createNotificationSchema,
  updateNotificationSchema
};