const joi = require('@hapi/joi');

const taskIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);

const createTaskSchema = {
  area: joi
    .string()
    .max(100)
    .required(),
  group: joi
    .string()
    .max(100)
    .required(),
  role: joi
    .string()
    .valid("Administrator", "Coordinator", "Dispatcher", "Executor")
    .required(),
  type: joi
    .string()
    .valid("Primary", "Calendar", "Secondary")
    .required(),
  message: joi
    .string()
    .max(100)
    .required(),
  author: joi
    .string()
    .max(500)
    .required(),
  deadlineDays: joi.number().required(),
  deadlineDate: joi.string().required(),
  logs: joi
    .array().items(joi.object({
      status: joi
        .string()
        .max(100)
        .required(),
      description: joi.string().max(500).required(),
      author: joi.string().max(500).required(),
      reason: joi.string(),
      createdAt: joi.string().required()
    }))
    .required(),
    activityId: joi.number(),
    projectId: joi.number(),
    priority: joi.number(),
    status: joi.string().max(100),
    zolidStatus: joi.string().max(100),
    createdAt: joi.string().required(),
    updatedAt: joi.string().required(),
    orderType: joi.string(),
};


const updateTaskSchema = {
  message: joi
    .string()
    .max(100)
    .required(),
  author: joi
    .string()
    .max(500)
    .required(),
  deadlineDays: joi.number(),
  deadlineDate: joi.string(),
  logs: joi
    .array().items(joi.object({
      status: joi
        .string()
        .max(100)
        .required(),
      description: joi.string().max(500).required(),
      author: joi.string().max(500).required(),
      reason: joi.string(),
      createdAt: joi.string().required()
    }))
    .required(),
  data: joi.object(),
  field: joi.string(),
  updatedAt: joi.string(),
  zolidStatus: joi.string(),
  hide: joi.boolean(),
  finishDate: joi.string(),
  role: joi
    .string()
    .valid("Administrator", "Coordinator", "Dispatcher", "Executor")
    .required(),
};

module.exports = {
  taskIdSchema,
  createTaskSchema,
  updateTaskSchema
};