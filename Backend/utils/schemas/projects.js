const joi = require('@hapi/joi');

const projectIdSchema = joi.string();
const projectStringSchema = joi.string();
const projectStringRequiredSchema = joi.string().required();
const projectNumberSchema = joi.number();
const projectObjectSchema = joi.object();
const projectArraySchema = joi.array();

const projectSchema = {
  projectId: projectStringRequiredSchema,
  client: projectStringSchema,
  city: projectStringSchema,
  author: projectStringSchema,
  createdAt: projectStringSchema,
  updatedAt: projectStringSchema,
  type: projectStringSchema,
  status: projectStringSchema,
  priority: projectNumberSchema,
  programmingDate: projectStringSchema,
  finishDate: projectStringSchema,
  resolucion1: projectStringSchema,
  resolucion2: projectStringSchema,
  resolucion3: projectStringSchema,
  resolucion4: projectStringSchema,
  members: projectArraySchema,
  commitmentDate: projectStringSchema,
  department: projectStringSchema,
  description: projectStringSchema,
  family: projectStringSchema,
  group: projectStringSchema,
  incidentCity: projectStringSchema,
  incidentDate: projectStringSchema,
  mrc: projectStringSchema,
  orderType: projectStringSchema,
  product: projectStringSchema,
  segment: projectStringSchema,
  service: projectStringSchema,
  serviceCode: projectStringSchema,
  sourceAddress: projectStringSchema,
  destinationAddress: projectStringSchema,
  authorNotify:  projectStringSchema,
};

const createProjectSchema = {
  ...projectSchema
};

const updateProjectSchema = {
  reporteActualizacion:projectObjectSchema,
};

module.exports = {
  projectIdSchema,
  createProjectSchema,
  updateProjectSchema
};