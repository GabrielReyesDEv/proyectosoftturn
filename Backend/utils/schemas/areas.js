const joi = require('@hapi/joi');

const areaIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);

const areaSchema = {
};

const createAreaSchema = {
  ...areaSchema
};

const updateAreaSchema = {
};

module.exports = {
  areaIdSchema,
  createAreaSchema,
  updateAreaSchema
};