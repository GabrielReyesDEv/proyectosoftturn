const joi = require('@hapi/joi');

const othIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);
const othZteIdSchema = joi.string();
const othEstadoSchema = joi.object({
    nombre: joi.string(),
    fechaHora: joi.string(),
    observacion: joi.string(),
    },
    { 
    nombre: joi.string(),
    fechaHora: joi.string(),
    observacion: joi.string(),
    },
    { 
    nombre: joi.string(),
    fechaHora: joi.string(),
    observacion: joi.string(),
    },
    { 
    nombre: joi.string(),
    fechaHora: joi.string(),
    observacion: joi.string(),
});
const othClienteSchema = joi.string();
const othIdPlatformSchema = joi.string();
const othTipoSchema = joi.string();
const othIdOtpSchema = joi.string();
const othFamiliaSchema = joi.string();
const othGrupoObejtivoSchema = joi.string();
const othSegmentoSchema = joi.string();
const othNivelAtencionSchema = joi.string();
const othCiudadSchema = joi.string();
const othDepartamentoSchema = joi.string();
const othGrupoSchema = joi.string();
const othConsultoComercialSchema = joi.string();
const othGrupo2Schema = joi.string();
const othConsultorPostventaSchema = joi.string();
const othProyInstalacionSchema = joi.string();
const othIngResponsableSchema = joi.string();
const othIdEnlaceSchema = joi.string();
const othAliasEnlanceSchema = joi.string();
const othProductoSchema = joi.string();
const othTiempoIncidenteSchema = joi.string();
const othTiempoEstadoSchema = joi.string();
const othAnoIngresoEstadoSchema = joi.string();
const othMesIngresoEstadoSchema = joi.string();
const othFechaIngresoEstadoSchema = joi.string();
const othUsuarioAsignadoSchema = joi.string();
const othGrupoAsignadoSchema = joi.string();
const othIngenieroProvisioningSchema = joi.string();
const othCargoArriendoSchema = joi.string();
const othCargoMensualSchema = joi.string();
const othMontoMonedaLocalArriendoSchema = joi.string();
const othMontoMonedaLocalCargoMensualSchema = joi.string();
const othCargoObraCivilSchema = joi.string();
const othDescripcionSchema = joi.string();
const othDireccionOrigenSchema = joi.string();
const othCiudadIncidenteSchema = joi.string();
const othDireccionDestinoSchema = joi.string();
const othCiudadIncidente3Schema = joi.string();
const othFechaRealizacionSchema = joi.string();
const othResolucion1Schema = joi.string();
const othResolucion2Schema = joi.string();
const othResolucion3Schema = joi.string();
const othResolucion4Schema = joi.string();
const othFechaCreacionOtHijaSchema = joi.string();
const othProveedorUltimaMillaSchema = joi.string();
const othUsuarioAsignado4Schema = joi.string();
const othResolucion15Schema = joi.string();
const othResolucion26Schema = joi.string();
const othResolucion37Schema = joi.string();
const othResolucion48Schema = joi.string();
const othFecActualizacionOnyxHijaSchema = joi.string();
const othTipoTranscurridoSchema = joi.string();
const othFechaInsercionZolidSchema = joi.string();
const othFechaActualSchema = joi.string();
const othEstadoModSchema = joi.string();
const othNObservacionCierreSchema = joi.string();
const othCEmailSchema = joi.string();
const othLastsendSchema = joi.string();
const othFechaModSchema = joi.string();
const othFechaCompromisoSchema = joi.string();
const othUserOnyx = joi.string();
const othBFlagSchema = joi.string();
const othActualizadoSchema = joi.string();
const othServicioSchema = joi.string();
const othCreateAtSchema = joi.string();
const othUpdateAtSchema = joi.string();

const createOthSchema ={
    zteId: othZteIdSchema,
    othId: othIdPlatformSchema,
    estado: othEstadoSchema,
    cliente: othClienteSchema,
    tipo: othTipoSchema,  
    idOTP: othIdOtpSchema,  
    familia: othFamiliaSchema,  
    grupoObejtivo: othGrupoObejtivoSchema,  
    segmento: othSegmentoSchema,  
    nivelAtencion: othNivelAtencionSchema,  
    ciudad: othCiudadSchema,  
    departamento: othDepartamentoSchema,  
    grupo: othGrupoSchema,  
    consultoComercial: othConsultoComercialSchema,  
    grupo2: othGrupo2Schema,  
    consultorPostventa: othConsultorPostventaSchema,  
    proyInstalacion: othProyInstalacionSchema,  
    ingResponsable: othIngResponsableSchema,  
    idEnlace: othIdEnlaceSchema,  
    aliasEnlance: othAliasEnlanceSchema,  
    producto: othProductoSchema,  
    tiempoIncidente: othTiempoIncidenteSchema,  
    tiempoEstado: othTiempoEstadoSchema,  
    anoIngresoEstado: othAnoIngresoEstadoSchema,  
    mesIngresoEstado: othMesIngresoEstadoSchema,  
    fechaIngresoEstado: othFechaIngresoEstadoSchema,  
    usuarioAsignado: othUsuarioAsignadoSchema,  
    grupoAsignado: othGrupoAsignadoSchema,  
    ingenieroProvisioning: othIngenieroProvisioningSchema,  
    cargoArriendo: othCargoArriendoSchema,  
    cargoMensual: othCargoMensualSchema,  
    montoMonedaLocalArriendo: othMontoMonedaLocalArriendoSchema,  
    montoMonedaLocalCargoMensual: othMontoMonedaLocalCargoMensualSchema,  
    cargoObraCivil: othCargoObraCivilSchema,  
    descripcion: othDescripcionSchema,  
    direccionOrigen: othDireccionOrigenSchema,  
    ciudadIncidente: othCiudadIncidenteSchema,  
    direccionDestino: othDireccionDestinoSchema,  
    ciudadIncidente3: othCiudadIncidente3Schema,  
    fechaRealizacion: othFechaRealizacionSchema,  
    resolucion1: othResolucion1Schema,  
    resolucion2: othResolucion2Schema,  
    resolucion3: othResolucion3Schema,  
    resolucion4: othResolucion4Schema,  
    fechaCreacionOtHija: othFechaCreacionOtHijaSchema,  
    proveedorUltimaMilla: othProveedorUltimaMillaSchema,  
    usuarioAsignado4: othUsuarioAsignado4Schema,  
    resolucion15: othResolucion15Schema,  
    resolucion26: othResolucion26Schema,  
    resolucion37: othResolucion37Schema,  
    resolucion48: othResolucion48Schema,   
    fecActualizacionOnyxHija: othFecActualizacionOnyxHijaSchema,  
    tipoTranscurrido: othTipoTranscurridoSchema,  
    fechaInsercionZolid: othFechaInsercionZolidSchema,  
    fechaActual: othFechaActualSchema,  
    estadoMod: othEstadoModSchema,  
    nObservacionCierre: othNObservacionCierreSchema,  
    cEmail: othCEmailSchema,  
    lastsend: othLastsendSchema,  
    fechaMod: othFechaModSchema,  
    fechaCompromiso: othFechaCompromisoSchema, 
    userOnyx: othUserOnyx, 
    bFlag: othBFlagSchema,  
    actualizado: othActualizadoSchema,  
    servicio: othServicioSchema,
    createAt: othCreateAtSchema,
    updateAt: othUpdateAtSchema,
};

const updateOthSchema = {
    zteId: othZteIdSchema,
    estado: othEstadoSchema,
    cliente: othClienteSchema,
    tipo: othTipoSchema,  
    idOTP: othIdOtpSchema,  
    familia: othFamiliaSchema,  
    grupoObejtivo: othGrupoObejtivoSchema,  
    segmento: othSegmentoSchema,  
    nivelAtencion: othNivelAtencionSchema,  
    ciudad: othCiudadSchema,  
    departamento: othDepartamentoSchema,  
    grupo: othGrupoSchema,  
    consultoComercial: othConsultoComercialSchema,  
    grupo2: othGrupo2Schema,  
    consultorPostventa: othConsultorPostventaSchema,  
    proyInstalacion: othProyInstalacionSchema,  
    ingResponsable: othIngResponsableSchema,  
    idEnlace: othIdEnlaceSchema,  
    aliasEnlance: othAliasEnlanceSchema,  
    producto: othProductoSchema,  
    tiempoIncidente: othTiempoIncidenteSchema,  
    tiempoEstado: othTiempoEstadoSchema,  
    anoIngresoEstado: othAnoIngresoEstadoSchema,  
    mesIngresoEstado: othMesIngresoEstadoSchema,  
    fechaIngresoEstado: othFechaIngresoEstadoSchema,  
    usuarioAsignado: othUsuarioAsignadoSchema,  
    grupoAsignado: othGrupoAsignadoSchema,  
    ingenieroProvisioning: othIngenieroProvisioningSchema,  
    cargoArriendo: othCargoArriendoSchema,  
    cargoMensual: othCargoMensualSchema,  
    montoMonedaLocalArriendo: othMontoMonedaLocalArriendoSchema,  
    montoMonedaLocalCargoMensual: othMontoMonedaLocalCargoMensualSchema,  
    cargoObraCivil: othCargoObraCivilSchema,  
    descripcion: othDescripcionSchema,  
    direccionOrigen: othDireccionOrigenSchema,  
    ciudadIncidente: othCiudadIncidenteSchema,  
    direccionDestino: othDireccionDestinoSchema,  
    ciudadIncidente3: othCiudadIncidente3Schema,  
    fechaRealizacion: othFechaRealizacionSchema,  
    resolucion1: othResolucion1Schema,  
    resolucion2: othResolucion2Schema,  
    resolucion3: othResolucion3Schema,  
    resolucion4: othResolucion4Schema,  
    fechaCreacionOtHija: othFechaCreacionOtHijaSchema,  
    proveedorUltimaMilla: othProveedorUltimaMillaSchema,  
    usuarioAsignado4: othUsuarioAsignado4Schema,  
    resolucion15: othResolucion15Schema,  
    resolucion26: othResolucion26Schema,  
    resolucion37: othResolucion37Schema,  
    resolucion48: othResolucion48Schema,    
    fecActualizacionOnyxHija: othFecActualizacionOnyxHijaSchema,  
    tipoTranscurrido: othTipoTranscurridoSchema,  
    fechaInsercionZolid: othFechaInsercionZolidSchema,  
    fechaActual: othFechaActualSchema,  
    estadoMod: othEstadoModSchema,  
    nObservacionCierre: othNObservacionCierreSchema,  
    cEmail: othCEmailSchema,  
    lastsend: othLastsendSchema,  
    fechaMod: othFechaModSchema,  
    fechaCompromiso: othFechaCompromisoSchema,
    userOnyx: othUserOnyx,  
    bFlag: othBFlagSchema,  
    actualizado: othActualizadoSchema,  
    servicio: othServicioSchema,
    updateAt: othUpdateAtSchema,
};

module.exports = {
    othIdSchema,
    createOthSchema,
    updateOthSchema
};