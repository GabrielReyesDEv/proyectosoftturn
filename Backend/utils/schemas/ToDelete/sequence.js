const joi = require('@hapi/joi');

const sequenceIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);
const nameCollectionSchema = joi.string();
const valueCollectionSchema = joi.number();

const createSequenceSchema = {
  name: nameCollectionSchema.required(),
  seq: 1,
  lastLoaded: joi.string(),
};

const updateSequenceSchema = {
  name: nameCollectionSchema.required(),
  seq: valueCollectionSchema.required(),
  lastLoaded: joi.string(),
};

module.exports = {
  sequenceIdSchema,
  createSequenceSchema,
  updateSequenceSchema
}
