const joi = require('@hapi/joi');

const activityIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);
// const othZteIdSchema = joi.string();
// const othEstadoSchema = joi.object({
//     nombre: joi.string(),
//     fechaHora: joi.string(),
//     observacion: joi.string(),
//     },
//     { 
//     nombre: joi.string(),
//     fechaHora: joi.string(),
//     observacion: joi.string(),
//     },
//     { 
//     nombre: joi.string(),
//     fechaHora: joi.string(),
//     observacion: joi.string(),
//     },
//     { 
//     nombre: joi.string(),
//     fechaHora: joi.string(),
//     observacion: joi.string(),
// });

const pymesOtpIdSchema = joi.string();
const pymesIngenieroSchema = joi.string();
const pymesClienteSchema = joi.string();
const pymesHoraSchema = joi.string();
const pymesCiudadSchema = joi.string();
const pymesNotasSchema = joi.string();
const pymesEstadoSchema = joi.string();
const pumesRolSchema = joi.string();
const pymesCreateAtSchema = joi.string();
const pymesDiaAgendaSchema = joi.string();
const pymesFechaCalendario = joi.string();

const createActivityPymeschema ={
    otpId: pymesOtpIdSchema,
    ingeniero: pymesIngenieroSchema,
    cliente: pymesClienteSchema,
    hora: pymesHoraSchema,
    ciudad: pymesCiudadSchema,  
    notas: pymesNotasSchema,  
    estado: pymesEstadoSchema,
    rol: pumesRolSchema,
    createAt: pymesCreateAtSchema,  
    diaAgenda: pymesDiaAgendaSchema,  
    fechaCalendario: pymesFechaCalendario,
};

const updateActivityPymesSchema = {
    otpId: pymesOtpIdSchema,
    ingeniero: pymesIngenieroSchema,
    cliente: pymesClienteSchema,
    hora: pymesHoraSchema,
    ciudad: pymesCiudadSchema,  
    notas: pymesNotasSchema,  
    estado: pymesEstadoSchema,  
    rol: pumesRolSchema,
    createAt: pymesCreateAtSchema,  
    diaAgenda: pymesDiaAgendaSchema,  
    fechaCalendario: pymesFechaCalendario,
};

module.exports = {
    activityIdSchema,
    createActivityPymeschema,
    updateActivityPymesSchema
};