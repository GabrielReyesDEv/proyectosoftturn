const joiBase = require('@hapi/joi');
const joiDate = require('@hapi/joi-date');

const joi = joiBase.extend(joiDate);

const inventoryForecastIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);
const anoSchema = joi.number();
const mesSchema = joi.number();
const forecastSchema = joi.number();
const idProyectoSchema = joi.string();

const createinventoryForecastSchema = {
    mes: anoSchema.required(),
    ano: mesSchema.required(),
    forecast: forecastSchema.required(),
    idProyecto: idProyectoSchema,
};

const updateinventoryForecastSchema = {
    mes: anoSchema,
    ano: mesSchema,
    forecast: forecastSchema,
    idProyecto: idProyectoSchema,
};

module.exports = {
    inventoryForecastIdSchema,
    createinventoryForecastSchema,
    updateinventoryForecastSchema
};