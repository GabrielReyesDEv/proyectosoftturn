const joi = require('@hapi/joi');

const activityZteIdSchema = joi.string();
const activityActionSchema = joi.string();
const activityOwnerNameSchema = joi.string();
const activityOwnerIdSchema = joi.number();
const activityCreatedAtSchema = joi.string();
const activityOTSchema = joi.string();
const activityOtTypeSchema = joi.string();
const activityServiceSchema = joi.string();
const activityOtpIdSchema = joi.string();

const createActivitySchema = {
  zteId: activityZteIdSchema,
  action: activityActionSchema.required(),
  ownerName: activityOwnerNameSchema.required(),
  ownerId: activityOwnerIdSchema.required(),
  createdAt: activityCreatedAtSchema.required(),
  oT: activityOTSchema.required(),
  otType: activityOtTypeSchema.required(),
  service: activityServiceSchema.required(),
  otpId: activityOtpIdSchema.required(),
};

module.exports = {
  createActivitySchema
}
