const joi = require('@hapi/joi');

const hitoIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);
const hitoZteIdSchema = joi.string();
const hitoOtpIdSchema = joi.string();
const hitoActividadActualSchema = joi.string();
const hitoObservacionesGeneralesSchema = joi.string();
const hitoFechaModificacionSchema = joi.string();//agregado
const hitoUsuarioSesionSchema = joi.string();//agregado
  const hitoVocSchema = joi.object({//VISITA OBRA CIVIL (VOC)
    fechaCompromisoVoc: joi.string(),
    estadoVoc: joi.string(),
    observacionesVoc: joi.string(),
    noAplicaVoc: joi.boolean(),
  });
  const hitoVeocSchema = joi.object({//VISITA EJECUCION OBRA CIVIL (VEOC)
    fechaCompromisoVeoc: joi.string(),
    estadoVeoc: joi.string(),
    observacionesVeoc: joi.string(),
    noAplicaVeoc: joi.boolean(),
  });
  const hitoEmSchema = joi.object({//EMPALMES (EM)
    fechaCompromisoEm: joi.string(),
    estadoEm: joi.string(),
    observacionesEm: joi.string(),
    noAplicaEm: joi.boolean(),
  });
  const hitoEsSchema = joi.object({//ENTREGA SERVICIO (ES)
    fechaEntregaServicio: joi.string(),
    estadoEntregaServicio: joi.string(),
    observacionesEntregaServicio: joi.string(),
    noAplicaEntregaServicio: joi.boolean(),
  });
  const hitoPcSchema = joi.object({//PENDIENTE CLIENTE (PC)
    fechaCompromisoPc: joi.string(),
    estadoPc: joi.string(),
    observacionesPc: joi.string(),
    noAplicaPc: joi.boolean(),
  });

const createhitoSchema = {
  zteId: hitoZteIdSchema,
  otpId: hitoOtpIdSchema ,
  observacionesGenrales: hitoObservacionesGeneralesSchema,
  actividadActual: hitoActividadActualSchema,
  observacionesGenerales: hitoObservacionesGeneralesSchema,
  fechaModificacion: hitoFechaModificacionSchema,
  usuarioSesion: hitoUsuarioSesionSchema,
  visitaObraCivil: hitoVocSchema,
  visitaEjecucionObraCivil: hitoVeocSchema,
  empalmesEm: hitoEmSchema,
  entregaServicio: hitoEsSchema,
  pendienteCliente: hitoPcSchema,
};

const updatehitoSchema = {
  //otpId: hitoOtpIdSchema ,
  observacionesGenrales: hitoObservacionesGeneralesSchema,
  actividadActual: hitoActividadActualSchema,
  observacionesGenerales: hitoObservacionesGeneralesSchema,
  fechaModificacion: hitoFechaModificacionSchema,
  usuarioSesion: hitoUsuarioSesionSchema,
  visitaObraCivil: hitoVocSchema,
  visitaEjecucionObraCivil: hitoVeocSchema,
  empalmesEm: hitoEmSchema,
  entregaServicio: hitoEsSchema,
  pendienteCliente: hitoPcSchema,
};

module.exports = {
  hitoIdSchema,
  createhitoSchema,
  updatehitoSchema
}
