const joi = require('@hapi/joi');

const departamentosIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);
const departamentoIdSchema = joi.string();
const departamentoNombreSchema = joi.string();

const createdepartamentoSchema = {
  idDep: departamentoIdSchema,
  nombreDep: departamentoNombreSchema,
};

const updatedepartamentoSchema = {
  idDep: departamentoIdSchema,
  nombreDep: departamentoNombreSchema,
};

module.exports = {
  departamentosIdSchema,
  createdepartamentoSchema,
  updatedepartamentoSchema
}
