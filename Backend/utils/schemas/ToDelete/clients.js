const joi = require('@hapi/joi');

const clientsIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);
const clientsZteIdSchema = joi.string();
const clientsIdUserSchema = joi.string();
const clientsIdOnyxSchema = joi.string();
const clientsDireccionSchema = joi.string();
const clientsCorreoSchema = joi.string();
const clientsTelefonoSchema = joi.string();
const clientsClasificacionSchema = joi.string();
const clientsTipoOficinaSchema = joi.string();
const clientsProyectosSchema = joi.string();

const createClientsSchema = {
  zteId: clientsZteIdSchema,
  clientsId: clientsIdSchema,
  idUser: clientsIdUserSchema.required(),
  idOnyx: clientsIdOnyxSchema.required(),
  direccion: clientsDireccionSchema,
  correo: clientsCorreoSchema,
  telefono: clientsTelefonoSchema,
  clasificacion: clientsClasificacionSchema,
  tipoOficina: clientsTipoOficinaSchema,
  proyectos: clientsProyectosSchema,
};

const updateClientsSchema = {
  zteId: clientsZteIdSchema,
  idUser: clientsIdUserSchema,
  idOnyx: clientsIdOnyxSchema,
  direccion: clientsDireccionSchema,
  correo: clientsCorreoSchema,
  telefono: clientsTelefonoSchema,
  clasificacion: clientsClasificacionSchema,
  tipoOficina: clientsTipoOficinaSchema,
  proyectos: clientsProyectosSchema,
};

module.exports = {
clientsIdSchema,
createClientsSchema,
updateClientsSchema
}