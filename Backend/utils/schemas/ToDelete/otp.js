const joi = require('@hapi/joi');

  const otpIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);
  const otpZteIdSchema = joi.string()
  const otpIdFieldSchema = joi.number();
  const otpResponsableSchema = joi.object({
    idUsuario:joi.string(),
    nombre:joi.string(),
    userOnyx: joi.string()
  });
 
  const otpClienteSchema = joi.object({
    nombre:joi.string(),
    trabajo:joi.string(),
    servicio:joi.string(),
    codigoServicio: joi.string(),
  });
 
  const otpEstadoSchema = joi.object({
    nombre:joi.string(),
    fecha:joi.string(),
    descripcion:joi.string(),
  });
  
  const otpeFechasSchema = joi.object({
    creacion:joi.string(),
    compromiso:joi.string(),
    programacion:joi.string(),
    actualizacion:joi.string(),
  });

  const otpLogRepActSchema = joi.array();
  
  const otpOnyxSchema = joi.string();
  const otpNombreSedeSchema = joi.string();
  const otpCiudadSchema = joi.string();
  const otpDepartamentoSchema = joi.string();
  const otpDireccionSchema = joi.string();
  const otpClasificacionSchema = joi.string();
  const otpTipoOficinaSchema = joi.string();
  const otpListaObservacionesSchema = joi.string();
  const otpObservacionSchema = joi.string();
  const otpFinalizoSchema = joi.string();
  const otpUltimoReporteSchema = joi.string();
  const otpCerradoZolidSchema = joi.string();
  const otpNombreResponsableSchema = joi.string();
  const otpCreateAtSchema = joi.string();
  const otpUpdateAtSchema = joi.string();

const createOtpSchema = {
  zteId: otpZteIdSchema,
  otpId: otpIdFieldSchema,
  responsable: otpResponsableSchema,
  cliente: otpClienteSchema,
  estado: otpEstadoSchema,
  fechas: otpeFechasSchema,
  logReporteActualizacion: otpLogRepActSchema,
  onyx: otpOnyxSchema,
  nombreSede: otpNombreSedeSchema,
  ciudad: otpCiudadSchema,
  departamento: otpDepartamentoSchema,
  direccion: otpDireccionSchema,
  clasificacion: otpClasificacionSchema,
  tipoOficina: otpTipoOficinaSchema,
  listaObservaciones: otpListaObservacionesSchema,
  observacion: otpObservacionSchema,
  finalizo: otpFinalizoSchema,
  ultimoReporte: otpUltimoReporteSchema,
  cerradoZolid: otpCerradoZolidSchema,
  nombreResponsable: otpNombreResponsableSchema, 
  createAt: otpCreateAtSchema,
  updateAt: otpUpdateAtSchema,
};

const updateOtpSchema = {
  zteId: otpZteIdSchema,
  otpId: otpIdFieldSchema,
  responsable: otpResponsableSchema,
  cliente: otpClienteSchema,
  estado: otpEstadoSchema,
  fechas: otpeFechasSchema,
  logReporteActualizacion: otpLogRepActSchema,
  onyx: otpOnyxSchema,
  nombreSede: otpNombreSedeSchema,
  ciudad: otpCiudadSchema,
  departamento: otpDepartamentoSchema,
  direccion: otpDireccionSchema,
  clasificacion: otpClasificacionSchema,
  tipoOficina: otpTipoOficinaSchema,
  listaObservaciones: otpListaObservacionesSchema,
  observacion: otpObservacionSchema,
  finalizo: otpFinalizoSchema,
  ultimoReporte: otpUltimoReporteSchema,
  cerradoZolid: otpCerradoZolidSchema,
  nombreResponsable:otpNombreResponsableSchema,
  createAt: otpCreateAtSchema,
  updateAt: otpUpdateAtSchema,
};

module.exports = {
  otpIdSchema,
  createOtpSchema,
  updateOtpSchema
}