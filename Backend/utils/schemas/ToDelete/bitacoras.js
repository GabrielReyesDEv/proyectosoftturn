const joi = require('@hapi/joi');

const bitacoraIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);
const bitacoraZteIdSchema = joi.string();
const bitacorasExecutorIdSchema = joi.string();
const bitacorasRoleSchema = joi.string();
const bitacorasOtpIdSchema = joi.string();
const bitacorasOthIdSchema = joi.string();
const bitacorasTypeOfOthSchema = joi.string();
const bitacorasTypeOfServiceSchema = joi.string();
const bitacorasStateSchema = joi.string();
const bitacorasReasonSchema = joi.string();
const bitacorasAssignDateSchema = joi.string();
const bitacorasInitialDateSchema = joi.string();
const bitacorasEndDateSchema = joi.string();

const createBitacoraSchema = {
  zteId: bitacoraZteIdSchema,
  bitacoraId: bitacoraIdSchema,
  executorId: bitacorasExecutorIdSchema .required(), 
  role: bitacorasRoleSchema .required(), 
  otpId: bitacorasOtpIdSchema .required(), 
  othId: bitacorasOthIdSchema , 
  typeOfOth: bitacorasTypeOfOthSchema , 
  typeOfService: bitacorasTypeOfServiceSchema , 
  state: bitacorasStateSchema .required(), 
  reason: bitacorasReasonSchema , 
  assignDate: bitacorasAssignDateSchema .required(), 
  initialDate: bitacorasInitialDateSchema , 
  endDate: bitacorasEndDateSchema ,
};

const updateBitacoraSchema = {
  zteId: bitacoraZteIdSchema,
  executorId: bitacorasExecutorIdSchema .required(), 
  role: bitacorasRoleSchema .required(), 
  otpId: bitacorasOtpIdSchema .required(), 
  othId: bitacorasOthIdSchema , 
  typeOfOth: bitacorasTypeOfOthSchema , 
  typeOfService: bitacorasTypeOfServiceSchema , 
  state: bitacorasStateSchema .required(), 
  reason: bitacorasReasonSchema , 
  assignDate: bitacorasAssignDateSchema .required(), 
  initialDate: bitacorasInitialDateSchema , 
  endDate: bitacorasEndDateSchema ,
};

module.exports = {
  bitacoraIdSchema,
  createBitacoraSchema,
  updateBitacoraSchema
};