const joi = require('@hapi/joi');

const userIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);
const userZteIdSchema = joi.string();
const createUserSchema = {
  identification: joi.string(),
  zteId: userZteIdSchema,
  firstName: joi.string().max(100).required(),
  lastName: joi.string().max(100).required(),
  email: joi.string().email().required(),
  password: joi.string().required(),
  roles: joi.array().items(joi.object({
    title: joi.string(),
    value: joi.number(),
    group: joi.string(),
    userOnyx: joi.string(), 
  })),
  telefono: joi.number(),
  terms: joi.string()

}

const updateUserSchema = {
  identification: joi.string(),
  zteId: userZteIdSchema,
  firstName: joi.string().max(100),
  lastName: joi.string().max(100),
  email: joi.string().email(),
  password: joi.string(),
  roles: joi.array().items(joi.object({
    title: joi.string(),
    value: joi.number(),
    group: joi.string(),
    userOnyx: joi.string(), 
  })),
  telefono: joi.number(),
  terms: joi.string(),
  fechaContrasena: joi.date(),
  actContrasena: joi.boolean(),

}

module.exports = {
  userIdSchema,
  createUserSchema,
  updateUserSchema
}