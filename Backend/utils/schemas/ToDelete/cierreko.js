const joi = require('@hapi/joi');

const cierrekoIdschema = joi.string().regex(/^[0-9a-fA-F]{24}$/);
const cierrekoZteIdSchema = joi.string();
const cierrekoOtpIdSchema = joi.string();
const cierrekoOthIdSchema = joi.string();
const cierrekoIdServicio = joi.string();
const cierrekoTipoServicio = joi.string();
const cierrekolineaBaseSchema = joi.object();
const cierrekoReporteInicioSchema = joi.object();
const cierrekoSchema = joi.object();
const cierrekoCreateAtSchema = joi.string();
const cierrekoUpdateAtSchema = joi.string();

const createCierrekoSchema = {
  zteId: cierrekoZteIdSchema,
  otpId: cierrekoOtpIdSchema,
  othId: cierrekoOthIdSchema,
  idServicio: cierrekoIdServicio,
  tipoServicio: cierrekoTipoServicio,
  lineaBase: cierrekolineaBaseSchema.required(),
  reporteInicio: cierrekoReporteInicioSchema.required(), 
  cierreKO: cierrekoSchema.required(),
  createAt: cierrekoCreateAtSchema,
  updateAt: cierrekoUpdateAtSchema,
};

const updateCierrekoSchema = {
  zteId: cierrekoZteIdSchema,
  otpId: cierrekoOtpIdSchema,
  othId: cierrekoOthIdSchema,
  idServicio: cierrekoIdServicio,
  tipoServicio: cierrekoTipoServicio,
  lineaBase: cierrekolineaBaseSchema,
  reporteInicio: cierrekoReporteInicioSchema, 
  cierreKO: cierrekoSchema.required(),
  createAt: cierrekoCreateAtSchema,
  updateAt: cierrekoUpdateAtSchema,
};

module.exports = {
  cierrekoIdschema,
  createCierrekoSchema,
  updateCierrekoSchema
};