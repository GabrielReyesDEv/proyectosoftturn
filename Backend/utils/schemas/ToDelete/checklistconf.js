const joi = require('@hapi/joi');

const checklistconfIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);
const checklistconfRoleSchema = joi.string();
const checklistconfEstadoChecklist = joi.string();
const checklistconfUsuarioAsignado = joi.object();
const checklistconfLog = joi.array().items(joi.object({
  nombreUsuario: joi.string(),
  idUsuario: joi.string(),
  role: joi.object(),
  estado: joi.string(),
  razon: joi.string(),
  fechaInicio: joi.string(),
  fechaAsignacion: joi.string(),
  fechaFinalizacion: joi.string()
}))
const checklistconfNovedadSchema = joi.string()
const checklistconfZteIdSchema = joi.string();
const checklistconfUpdateAt = joi.string();
const checklistconfCreatedAt = joi.string();
const checklistconfOthIdSchema = joi.string();
const checklistconfOtpIdSchema = joi.string();
const checklistconfClienteSchema = joi.string();
const checklistconfSaveAddressSchema = joi.boolean();
const checklistconfCambioCpEoInstalacionSchema = joi.string();
const checklistconfTipoDemarcadorAInstalarSchema = joi.string();
const checklistconfDocumentacionParaConfiguracionSchema = joi.array();
const checklistconfFechaRequeridaConfiguracionSchema = joi.string();
const checklistconfIngenieroIsSchema = joi.string();
const checklistconfMarcaCpeSchema = joi.string();
const checklistconfNodoEntregaUltimaMillaSchema = joi.string();
const checklistconfObservacionesSchema = joi.string();
const checklistconfOtpAsociadasSchema = joi.string();
const checklistconfOtpEntregaUltimaMillaSchema = joi.string();
const checklistconfProveedorUltimaMillaSchema = joi.string();
const checklistconfRespuestaFactibilidadSchema = joi.string();
const checklistconfSoportesRequeridosSchema = joi.array();
const checklistconfTipoServicioSchema = joi.string();
const checklistconfOthResponsableSchema = joi.string();
const checklistconfTelefonoSchema = joi.string();
const checklistconfIngenieroNivel2Schema = joi.string();
const checklistconfTelefonoNivel2Schema = joi.string();
const checklistconfIngenieroNivel3Schema = joi.string();
const checklistconfTelefonoNivel3Schema = joi.string();


const createChecklistconfSchema = {
  zteId: checklistconfZteIdSchema,
  role:checklistconfRoleSchema,
  estadoChecklist: checklistconfEstadoChecklist,
  novedad: checklistconfNovedadSchema,
  usuarioAsignado: checklistconfUsuarioAsignado,
  othResponsable: checklistconfOthResponsableSchema,
  log: checklistconfLog,
  checklistconfId: checklistconfIdSchema,
  createdAt: checklistconfCreatedAt,
  updateAt: checklistconfUpdateAt,
  othId: checklistconfOthIdSchema,
  otpId: checklistconfOtpIdSchema,
  cliente: checklistconfClienteSchema,
  saveAddress: checklistconfSaveAddressSchema,
  cambioCPEoInstalacion: checklistconfCambioCpEoInstalacionSchema,
  tipoDemarcadorAInstalar: checklistconfTipoDemarcadorAInstalarSchema,
  documentacionParaConfiguracion: checklistconfDocumentacionParaConfiguracionSchema,
  fechaRequeridaConfiguracion: checklistconfFechaRequeridaConfiguracionSchema,
  ingenieroIS: checklistconfIngenieroIsSchema,
  marcaCPE: checklistconfMarcaCpeSchema,
  nodoEntregaUltimaMilla: checklistconfNodoEntregaUltimaMillaSchema,
  observaciones: checklistconfObservacionesSchema,
  otpAsociadas: checklistconfOtpAsociadasSchema,
  otpEntregaUltimaMilla: checklistconfOtpEntregaUltimaMillaSchema,
  proveedorUltimaMilla: checklistconfProveedorUltimaMillaSchema,
  respuestaFactibilidad: checklistconfRespuestaFactibilidadSchema,
  soportesRequeridos: checklistconfSoportesRequeridosSchema,
  tipoServicio: checklistconfTipoServicioSchema,
  telefono: checklistconfTelefonoSchema,
  ingenieroNivel2: checklistconfIngenieroNivel2Schema,
  telefonoNivel2: checklistconfTelefonoNivel2Schema,
  ingenieroNivel3: checklistconfIngenieroNivel3Schema,
  telefonoNivel3: checklistconfTelefonoNivel3Schema,
};

const updateChecklistconfSchema = {
  zteId: checklistconfZteIdSchema,
  role:checklistconfRoleSchema,
  othResponsable: checklistconfOthResponsableSchema,
  usuarioAsignado: checklistconfUsuarioAsignado,
  novedad: checklistconfNovedadSchema,
  log: checklistconfLog,
  estadoChecklist: checklistconfEstadoChecklist,
  saveAddress: checklistconfSaveAddressSchema,
  othId: checklistconfOthIdSchema,
  otpId: checklistconfOtpIdSchema,
  cliente: checklistconfClienteSchema,
  createdAt: checklistconfCreatedAt,
  updateAt: checklistconfUpdateAt,
  cambioCPEoInstalacion: checklistconfCambioCpEoInstalacionSchema,
  tipoDemarcadorAInstalar: checklistconfTipoDemarcadorAInstalarSchema,
  documentacionParaConfiguracion: checklistconfDocumentacionParaConfiguracionSchema,
  fechaRequeridaConfiguracion: checklistconfFechaRequeridaConfiguracionSchema,
  ingenieroIS: checklistconfIngenieroIsSchema,
  marcaCPE: checklistconfMarcaCpeSchema,
  nodoEntregaUltimaMilla: checklistconfNodoEntregaUltimaMillaSchema,
  observaciones: checklistconfObservacionesSchema,
  otpAsociadas: checklistconfOtpAsociadasSchema,
  otpEntregaUltimaMilla: checklistconfOtpEntregaUltimaMillaSchema,
  proveedorUltimaMilla: checklistconfProveedorUltimaMillaSchema,
  respuestaFactibilidad: checklistconfRespuestaFactibilidadSchema,
  soportesRequeridos: checklistconfSoportesRequeridosSchema,
  tipoServicio: checklistconfTipoServicioSchema,
  telefono: checklistconfTelefonoSchema,
  ingenieroNivel2: checklistconfIngenieroNivel2Schema,
  telefonoNivel2: checklistconfTelefonoNivel2Schema,
  ingenieroNivel3: checklistconfIngenieroNivel3Schema,
  telefonoNivel3: checklistconfTelefonoNivel3Schema,
};

module.exports = {
  checklistconfIdSchema,
  createChecklistconfSchema,
  updateChecklistconfSchema
};