const joi = require('@hapi/joi');

const samiaIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);
const activoIdSchema = joi.string();
const activoEstadoSchema = joi.string();

const createsamiaSchema = {
  idActivo: activoIdSchema,
  estado: activoEstadoSchema,
};

const updatesamiaSchema = {
  idActivo: activoIdSchema,
  estado: activoEstadoSchema,
};

module.exports = {
  samiaIdSchema,
  createsamiaSchema,
  updatesamiaSchema
}
