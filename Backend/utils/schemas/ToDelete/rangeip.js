const joi = require('@hapi/joi');

const rangeipIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);
const rangeipTypeIdSchema = joi.string();
const rangeipTypeSchema = joi.string();
const rangeipFromSchema = joi.string();
const rangeipToSchema = joi.string();
const rangeipCreateAtSchema = joi.string();
const rangeipUpdateAtSchema = joi.string();
const rangeipMaskSchema = joi.array();

const createrangeipSchema = {
  typeId: rangeipTypeIdSchema,
  type: rangeipTypeSchema,
  from: rangeipFromSchema,
  to: rangeipToSchema,
  createAt: rangeipCreateAtSchema,
  updateAt: rangeipUpdateAtSchema,
  mask: rangeipMaskSchema,
};

const updaterangeipSchema = {
  typeId: rangeipTypeIdSchema,
  type: rangeipTypeSchema,
  from: rangeipFromSchema,
  to: rangeipToSchema,
  createAt: rangeipCreateAtSchema,
  updateAt: rangeipUpdateAtSchema,
  mask: rangeipMaskSchema,
};

module.exports = {
  rangeipIdSchema,
  createrangeipSchema,
  updaterangeipSchema
}
