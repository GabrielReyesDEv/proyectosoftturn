const joi = require('@hapi/joi');

const datosSchema = joi.string();
const datosSchemaFormat = joi.object();

const updateChecklistFormSchema = {
  othId: datosSchema,
  createdBy: datosSchema,
  updatedAt: datosSchema,
  createdAt: datosSchema,
  format: datosSchemaFormat,
};

module.exports = {
  updateChecklistFormSchema,
};