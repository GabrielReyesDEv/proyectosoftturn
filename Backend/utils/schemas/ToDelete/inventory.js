const joiBase = require('@hapi/joi');
const joiDate = require('@hapi/joi-date');

const joi = joiBase.extend(joiDate);

const inventoryIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);
const inventoryZteIdSchema = joi.string();
const projectNameSchema = joi.string();
const agreementNumberSchema = joi.string();
const batchNumberSchema = joi.string();
const descriptionSchema = joi.string();
const quantitySchema = joi.number();
const sapCodeSchema = joi.number();
const deliverWaySchema = joi.string();
const destinationSchema = joi.string();
const statusSchema = joi.string().valid('Stock Local', 'Transito internacional', 'En Fabrica');
const manufacturingCompletionSchema = joi.date();
const shippingDateSchema = joi.date();
const transportationDocumentSchema = joi.string();
const packageNumberSchema = joi.number();
const grossWeightSchema = joi.number();
const dateArrivalColombiaSchema = joi.date();
const dataIngresoAInventarioSchema = joi.date();
const customsCompletionDateSchema = joi.date();
const iinventoryEntryDateSchema = joi.date();
const poSchema = joi.array().items(joi.object({
    poNumber: joi.number(),
    deliveredQuantity: joi.number(),
    deliveryDate: joi.date(),
    forecastDate: joi.date(),
}));
const idProyectoSchema = joi.string();

const createinventorySchema ={
    zteId: inventoryZteIdSchema,
    projectName: projectNameSchema.required(),
    agreementNumber: agreementNumberSchema.required(),  
    batchNumber: batchNumberSchema,  
    description: descriptionSchema.required(),  
    quantity: quantitySchema.required(),  
    sapCode: sapCodeSchema.required(),  
    deliverWay: deliverWaySchema.required(),  
    destination: destinationSchema,  
    status: statusSchema.required(),
    manufacturingCompletion: manufacturingCompletionSchema.required(),
    shippingDate: shippingDateSchema,
    transportationDocument: transportationDocumentSchema,
    packageNumber: packageNumberSchema,
    grossWeight: grossWeightSchema,
    dateArrivalColombia: dateArrivalColombiaSchema,
    customsCompletionDate: customsCompletionDateSchema,
    iinventoryEntryDate: iinventoryEntryDateSchema,
    po: poSchema.default([]),
    idProyecto: idProyectoSchema,
};

const updateinventorySchema = {
    zteId: inventoryZteIdSchema,
    projectName: projectNameSchema,
    agreementNumber: agreementNumberSchema,  
    batchNumber: batchNumberSchema,  
    description: descriptionSchema,  
    quantity: quantitySchema,  
    sapCode: sapCodeSchema,  
    deliverWay: deliverWaySchema,  
    destination: destinationSchema,  
    status: statusSchema,
    manufacturingCompletion: manufacturingCompletionSchema,
    shippingDate: shippingDateSchema,
    transportationDocument: transportationDocumentSchema,
    packageNumber: packageNumberSchema,
    grossWeight: grossWeightSchema,
    dateArrivalColombia: dateArrivalColombiaSchema,
    customsCompletionDate: customsCompletionDateSchema,
    iinventoryEntryDate: iinventoryEntryDateSchema,
    po: poSchema,
    ingresoAInventario: dataIngresoAInventarioSchema,
    idProyecto: idProyectoSchema,
};

module.exports = {
    inventoryIdSchema,
    createinventorySchema,
    updateinventorySchema
};