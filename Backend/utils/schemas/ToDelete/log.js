const joi = require('@hapi/joi');

const logZteIdSchema = joi.string();
const logUserSchema = joi.object({
  name: joi.string(),
  id: joi.string()
});
const logOTPSchema = joi.string();
const logOTHSchema = joi.string();
const logDescriptionSchema = joi.string();
const logTypeSchema = joi.string();
const logIdRegisterSchema = joi.string();
const logIdCreatedAtSchema = joi.string();

const createLogSchema = {
  zteId: logZteIdSchema,
  user: logUserSchema.required(),
  OTP: logOTPSchema.required(),
  OTH: logOTHSchema.required(),
  Descripcion: logDescriptionSchema.required(),
  type: logTypeSchema.required(),
  idRegister: logIdRegisterSchema.required(),
  createdAt: logIdCreatedAtSchema.required(),
};

module.exports = {
  createLogSchema
}