
const joi = require('@hapi/joi');

const { taskIdSchema } = require('./tasks');
const { userIdSchema } = require('./users');

const userTaskIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);

const createUserTaskSchema = {
  userId: userIdSchema,
  taskId: taskIdSchema
};

module.exports = {
  userTaskIdSchema,
  createUserTaskSchema
};