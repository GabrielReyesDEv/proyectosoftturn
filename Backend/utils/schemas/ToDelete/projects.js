const joi = require('@hapi/joi');

const projectsIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);
//const projectsZteIdSchema = joi.string();
const projectsoNameSchema = joi.string();
const idProyectoSchema = joi.number();
const nombreProyectoSchema = joi.string();
const numeroContratoSchema = joi.string();
const unidadesContratoSchema = joi.string();
const finalizacionContratoSchema = joi.string();

const createProjectsSchema ={
  //zteId: projectsZteIdSchema,
  projectsId: projectsIdSchema,
  idProyecto: idProyectoSchema,
  nombre: projectsoNameSchema,
  nombreProyecto: nombreProyectoSchema,
  numeroContrato: numeroContratoSchema,
  unidadesContrato: unidadesContratoSchema,
  finalizacionContrato: finalizacionContratoSchema,

};

const updateProjectsSchema = {
  //zteId: projectsZteIdSchema,
  projectsId: projectsIdSchema,
  idProyecto: idProyectoSchema,
  nombre: projectsoNameSchema,
  nombreProyecto: nombreProyectoSchema,
  numeroContrato: numeroContratoSchema,
  unidadesContrato: unidadesContratoSchema,
  finalizacionContrato: finalizacionContratoSchema,
};

module.exports = {
  projectsIdSchema,
  createProjectsSchema,
  updateProjectsSchema
  }