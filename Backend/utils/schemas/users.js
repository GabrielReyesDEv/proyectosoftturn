const joi = require('@hapi/joi');

const userIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);

const userSchema = {
  firstName: joi
    .string()
    .max(100)
    .required(),
  lastName: joi
    .string()
    .max(100)
    .required(),
  identification: joi
    .number()
    .required(),
  phone: joi
    .number()
    .required(),
  workPosition: joi
    .string()
    .max(500)
    .required(),
  email: joi
    .string()
    .email()
    .required(),
  password: joi.string().required(),
  status: joi.string().valid("Active", "Inactive", "Pending"),
  roles: joi
    .array().items(joi.object({
      role: joi.string().max(100),
      area: joi.string().max(100)
    }))
    .required(),
  group: joi.string().required(),
  verified: joi.boolean(),
  createdAt: joi.string().required()
};

const createUserSchema = {
  ...userSchema
};

const createProviderUserSchema = {
  ...userSchema,
  apiKeyToken: joi.string().required()
};

module.exports = {
  userIdSchema,
  createUserSchema,
  createProviderUserSchema
};