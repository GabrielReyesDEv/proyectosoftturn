const joi = require('@hapi/joi');

const activityIdSchema = joi.string();
const activityStringSchema = joi.string();
const activityStringRequiredSchema = joi.string().required();
const activityNumberSchema = joi.number();
const activityArraySchema = joi.array();

const activitySchema = {
  activityId: activityStringRequiredSchema,
  projectId: activityStringRequiredSchema,
  author: activityStringSchema,
  status: activityStringSchema,
  priority: activityNumberSchema,
  createdAt: activityStringSchema,
  updatedAt: activityStringSchema,
  resolucion15: activityStringSchema,
  resolucion26: activityStringSchema,
  resolucion37: activityStringSchema,
  resolucion48: activityStringSchema,
  members: activityArraySchema,
  area: activityStringSchema,
  userOnyx: activityStringSchema,
  authorNotify:  activityStringSchema,
};

const createActivitySchema = {
  ...activitySchema
};

const updateActivitySchema = {
};

module.exports = {
  activityIdSchema,
  createActivitySchema,
  updateActivitySchema
};