require('dotenv').config();
var nodemailer = require('nodemailer');
const hbs = require('nodemailer-express-handlebars');
const handlerHelpers = require('../helpers/handlerbars');

// email sender function
exports.sendEmail = async function(res, req, data, email){

       console.log('data para el correo ', data);
// Definimos el transporter
const mailOpt = () => {
    switch (data.serviceType) {
        case ('Internet Dedicado Empresarial'): //ok
           return ['InstalacionServicioTelefoniaFijaPublicaSIP-InternetDedicado', `Reporte de Inicio de Actividades CLiente ${data.form.initialReport.client} OTP ${data.form.basicData.projectId} Instalación Servicio Telefonía Pública SIP - Internet Dedicado`]
        case ('Internet Dedicado'): //ok
               return ['InternetDedicado', `Reporte de Inicio de Actividades Cliente ${data.form.initialReport.client} OTP ${data.form.basicData.projectId}  Instalación Servicio Internet Dedicado`];
        case ('PL ETHERNET'): //ok
               return ['InstalacionServicioPLEthernet',`Reporte de Inicio de Actividades Cliente ${data.form.initialReport.client} OTP ${data.form.basicData.projectId} Instalación Servicio PL Ethernet`];
        case ('Instalación Servicio Telefonia Fija PBX Distribuida Linea E1'): //ok
               return ['InstalacionServicioTelefoniaFijaPBXDistribuidaLineaE1', `Reporte de Inicio de Actividades Cliente ${data.form.initialReport.client} OTP ${data.form.basicData.projectId} Instalación Servicio Telefonía Fija PBX Distribuida Línea E1`];
        case ('Instalación Servicio Telefonia Fija PBX Distribuida Linea SIP'): //ok
               return ['InstalacionTelefoniaFijaPBXDistribuidaLineaSIP', `Reporte de Inicio de Actividades Cliente ${data.form.initialReport.client} OTP ${data.form.basicData.projectId} Instalación Servicio Telefonía Fija PBX Distribuida Línea SIP`];
        case ('Instalación Servicio Telefonia Fija PBX Distribuida Linea SIP con Gateway de Voz'): //ok
               return ['InstalacionServicioTelefoniaFijaPBXDistribuidaLineaSIPGatewayVoz', `Reporte de Inicio de Actividades Cliente ${data.form.initialReport.client} OTP ${data.form.basicData.projectId} Instalación Servicio Telefonía Fija PBX Distribuida Línea SIP - Gateaway de Voz`];
        case ('Instalación Telefonía Publica Básica - Internet Dedicado'): //ok revisar por que no tiene el apartado de ciudades y varios campos no coinciden
               return ['InstalacionTelefoniaPublicaBasica-InternetDedicado', `Reporte de Inicio de Actividades Cliente ${data.form.initialReport.client} OTP ${data.form.basicData.projectId} Instalación Telefonía Pública Básica - Internet Dedicado`];
        case ('PRIVATE LINE'): //revisar por que no tiene sentido 
               return ['AmpliacionServicioCambioEquipo',`Reporte de Inicio de Actividades Cliente ${data.form.initialReport.client} OTP ${data.form.basicData.projectId} Ampliacion de Servicio ${data.reporteInicio.servicio} con Cambio de Equipo`];
        case ('MPLS Avanzado Intranet'): //revisar por que no tiene sentido 
               return ['InstalacionServicioMPLSAvanzadoIntranet', `Reporte de Inicio de Actividades Cliente ${data.form.initialReport.client} OTP ${data.form.basicData.projectId}  Instalación Servicio MPLS Avanzando Intranet`];
        case ('MPLS Avanzado Intranet - Varios Puntos'):
               return ['InstalacionServicioMPLSAvanzadoSolucionIntrtanetVariosPuntos',`Reporte de Inicio de Actividades Cliente ${data.form.initialReport.client} OTP ${data.form.basicData.projectId} Instalación Servicio MPLS Avanzado Intranet - Varios Puntos`];
        case ('MPLS Avanzado Intranet con Backup de Ultima Milla - NDS 2'):
               return ['InstalacionServicioMLPSAvanzadoIntranetBackupUltimaMillaNDS2', `Reporte de Inicio de Actividades Cliente ${data.form.initialReport.client} OTP ${data.form.basicData.projectId} Instalación Servicio MPLS Avanzado Intranet con Backup de Ultima Milla - NDS 2`];
        case ('MPLS Avanzado Intranet con Backup de Ultima Milla y Router - NDS1'):
               return ['InstalaciónServicioMPLSAvanzadoIntranetBackupUltimaMillaRouterNDS1', `Reporte de Inicio de Actividades Cliente ${data.form.initialReport.client} OTP ${data.form.basicData.projectId} Instalación Servicio MPLS Avanzado Intranet con Backup de Última Milla y Router - NDS1`];
        case ('MPLS Avanzado Extranet'):
               return ['InstalacionMPLSAvanzadoSolucionExtranet', `Reporte de Inicio de Actividades Cliente ${data.form.initialReport.client} OTP ${data.form.basicData.projectId} Instalación Servicio MPLS Avanzado Extranet`];
        case ('Backend MPLS'):
               return ['Backend_MPLS', `Reporte de Inicio de Actividades Cliente ${data.form.initialReport.client} OTP ${data.form.basicData.projectId} Instalación Servicio MPLS Backend`];
        case ('MPLS Avanzado con Componente Datacenter Claro'):
               return ['MPLS_Avanzado_con_Componente_Datacenter_Claro', `Reporte de Inicio de Actividades Cliente ${data.form.initialReport.client} OTP ${data.form.basicData.projectId} Instalación Servicio MPLS Avanzado con Componente Datacenter Claro`];
        case ('MPLS Transaccional 3G'):
               return ['InstalacionServicioMLPSTransaccionalSolucion3G', `Reporte de Inicio de Actividades Cliente ${data.form.initialReport.client} OTP ${data.form.basicData.projectId} Instalación Servicios MPLS Transaccional 3G`];
        case ('Punta DATACENTER'): //No hay formato de correo
               return [''];
        case ('LAN ADMINISTRADA'): //revisar ya que el formato no esta terminado
               return ['InstalaciónServicioMPLSTransaccionalFO',`Reporte de Inicio de Actividades Cliente ${data.form.initialReport.client} OTP ${data.form.basicData.projectId} Instalación Servicio MPLS Transaccional FO`];
        case ('SOLUCIONES ADMINISTRATIVAS - COMUNICACIONES UNIFICADAS PBX ADMINISTRADA'):
               return ['InstalacionServicioSolucionesAdministradasComunicacionesUnificadasPBXadministrada', `Reporte de Inicio de Actividades Cliente ${data.form.initialReport.client} OTP ${data.form.basicData.projectId} Instalación Servicio Soluciones Administradas Comunicaciones Unificadas - PBX Administrada`];
        case ('Traslado Externo Servicio'):
               return ['TrasladoExternoServicio',`Reporte de Inicio de Actividades Cliente ${data.form.initialReport.client} OTP ${data.form.basicData.projectId} Traslado Externo Servicio ${data.reporteInicio.servicio}`];
        case ('Traslado Interno Servicio'):
               return ['TrasladoInternoServicio',`Reporte de Inicio de Actividades Cliente ${data.form.initialReport.client} OTP ${data.form.basicData.projectId} Traslado Interno Servicio ${data.reporteInicio.traslado_servicio}`];
        case ('Cambio de Equipos Servicio'):
               return ['CambioEquiposServicio', `Reporte de Inicio de Actividades Cliente ${data.form.initialReport.client} OTP ${data.form.basicData.projectId} Cambio de Equipos Servicio ${data.reporteInicio.servicio}`];
        case ('Cambio de Servicio Telefonia Fija Pública Linea Basica a Linea E1'):
               return ['CambioServicioTelefoniaPublicaLineaBasicaALineaE1', `Reporte de Inicio de Actividades Cliente ${data.form.initialReport.client} OTP ${data.form.basicData.projectId} Cambio de Servicio Telefonía Fija Pública Línea Básica a Línea E1`];
        case ('Cambio de Servicio Telefonia Fija Pública Linea SIP a PBX Distribuida Linea SIP'):
               return ['CambioDeServicioTelefoniaFijaPublicaLineaSIPaPBXDistribuidaLineaSIP', `Reporte de Inicio de Actividades Cliente ${data.form.initialReport.client} OTP ${data.form.basicData.projectId} Cambio de Servicio Telefonía Fija Pública Línea SIP a PBX Distribuida Línea SIP`];
        case ('Cambio de Última Milla'):
               return ['AmpliacionServicioCambioUltimaMilla', `Reporte de Inicio de Actividades Cliente ${data.form.initialReport.client} OTP ${data.form.basicData.projectId} Ampliación de Servicio ${data.reporteInicio.servicio} con Cambio de Última Milla`];
        case ('Cambio de Equipo'):
               return ['AmpliacionServicioCambioEquipo', `Reporte de Inicio de Actividades Cliente ${data.form.initialReport.client} OTP ${data.form.basicData.projectId} Ampliación de Servicio ${data.reporteInicio.servicio} con Cambio de Equipo`];
        default:
          return '';
      }
}

    try{


        const transporter = nodemailer.createTransport({
            service: 'gmail',
            host: 'smtp.gmail.com',
            port: 587,
            secure: false,
            auth: {
                user: process.env.EMAIL,
                pass: process.env.EMAIL_PASSWORD,
            }
          });
    
        var mailOptions = {
            from: process.env.EMAIL,
            to: email,
            subject: mailOpt()[1],
            text: 'Prueba Test',
            template: mailOpt()[0],
            context: {
                data
            }
        };
    
        transporter.use('compile', hbs({
        viewEngine: {
            extName: '.handlebars',
            partialsDir: __dirname + '../../../views/',
            layoutsDir:  __dirname + '../../../views/',
            defaultLayout : mailOpt()[0],
            helpers: handlerHelpers
        },
        viewPath:  __dirname + '../../../views/'
        }));
    
        // Enviamos el email
        const result = await  transporter.sendMail(mailOptions);

        console.log('Se envio el correo');

        return result;

        } catch (error) {
            console.log('error', error);
            console.log('Error nos e envio el correo');
        }
    };