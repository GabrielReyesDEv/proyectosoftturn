require('dotenv').config();
const openvpnmanager = require('node-openvpn');
//const handlerHelpers = require('../helpers/handlerbars');
// email sender function
exports.vpnConnect = function(res, req, data){
// Definimos el transporter
const opts = {
    host: data.host, // normally '127.0.0.1', will default to if undefined
    port: 1337, //port openvpn management console
    timeout: 1500, //timeout for connection - optional, will default to 1500ms if undefined
    logpath: 'log.txt' //optional write openvpn console output to file, can be relative path or absolute
  };
  const auth = {
    user: data.user,
    pass: data.pass,
  };
  //const openvpn = openvpnmanager.connect(opts)

  const openvpn = openvpnmanager.connect(opts, function(error, info){
    if (error){
        console.log(error, info); //eslint-disable-line
        res.status(500).send(error.message);
    } else {
        console.log("Connect to VPN"); //eslint-disable-line
        //res.status(200).jsonp(req.body);
        res.writeHead(301);
        res.end();
    }
});

    openvpn.on('connected', () => {
        openvpnmanager.authorize(auth);
      });
   
//   // will be emited on successful interfacing with openvpn instance
//   openvpn.on('connected', () => {
//     openvpnmanager.authorize(auth);
//   });
   
//   // emits console output of openvpn instance as a string
//   openvpn.on('console-output', output => {
//     console.log(output)
//   });
   
//   // emits console output of openvpn state as a array
//   openvpn.on('state-change', state => {
//     console.log(state)
//   });
   
//   // emits console output of openvpn state as a string
//   openvpn.on('error', error => {
//     console.log(error)
//   });
   
//   // get all console logs up to this point
//   openvpnmanager.getLog(console.log)
   
//   // and finally when/if you want to
//   openvpnmanager.disconnect();
   
//   // emits on disconnect
//   openvpn.on('disconnected', () => {
//     // finally destroy the disconnected manager 
//     openvpnmanager.destroy()
//   });

};
