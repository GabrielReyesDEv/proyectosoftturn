require('dotenv').config();
var nodemailer = require('nodemailer');
const hbs = require('nodemailer-express-handlebars');
const ProjectService = require('../../services/projects');
// email sender function

const projectService = new ProjectService();
const getHito = async (query, filters) => {
    // return await hitoService.getHito( id );
    const resolve = await projectService.findProject( query, filters )
    return resolve[0];
}

/* const observaciones = (val) => {switch(val) {
    case 'Vista Obra Civil (VOC)':
        return val.visitaObraCivil.observacionesVoc;
    case 'Visita EjecucionObra civil (EOC)':
        return val.visitaEjecucionObraCivil.observacionesVeoc;
    case 'Empalmes (EM)':
        return val.empalmesEm.observacionesEm;
    case 'Entrega Servicio':
        return val.entregaServicio.observacionesEntregaServicio;
    case 'Pendiente Cliente':
        return val.observacionesGenrales;
    }
} */

const showImages = (actual) => {
    switch(actual) {
    case 'Vista Obra Civil (VOC)':
        return [1];
    case 'Visita EjecucionObra civil (EOC)':
        return [1, 2];
    case 'Empalmes (EM)':
        return [1, 2, 3];
    case 'Entrega Servicio':
        return [1, 2, 3, 4];
    default:
        return [];
    }
}

exports.sendEmail = function(res, req, data, info){
    var template = '';
    var timeline = '';
    var otpIds = [];
    var x = 0;
    for (const obj of data) {

        getHito({'projectId': obj.projectId}).then((project) => {
            // const obs = observaciones(hito.actividadActual);
            // console.log('actividades: ',project.hitos); 
            // console.log('data: ',data);
            // console.log('info: ',info);

            otpIds.push(project.projectId);

            template += `
                    <tr>
                        <td width="156" nowrap="" valign="bottom" style="width:78.0pt; border:solid windowtext 1.0pt; border-top:none; padding:0cm 3.5pt 0cm 3.5pt; height:60.0pt">
                            <p class="x_MsoNormal" align="right" style="text-align:right"><span style="font-size:10.0pt; font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;; color:black">${project.projectId}<u></u><u></u></span></p>
                        </td>
                        <td width="154" nowrap="" valign="bottom" style="width:77.0pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt; padding:0cm 3.5pt 0cm 3.5pt; height:60.0pt">
                            <p class="x_MsoNormal" align="right" style="text-align:right"><span style="font-size:10.0pt; font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;; color:black">${project.serviceCode}<u></u><u></u></span></p>
                        </td>
                        <td width="124" nowrap="" valign="bottom" style="width:62.05pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt; padding:0cm 3.5pt 0cm 3.5pt; height:60.0pt">
                            <p class="x_MsoNormal"><span style="font-size:10.0pt; font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;; color:black">${project.city}
                        <u></u><u></u></span></p>
                        </td>
                        <td width="218" valign="bottom" style="width:108.75pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt; padding:0cm 3.5pt 0cm 3.5pt; height:60.0pt">
                            <p class="x_MsoNormal"><span style="font-size:10.0pt; font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;; color:black">${project.sourceAddress}<u></u><u></u></span></p>
                        </td>
                        <td width="150" valign="bottom" style="width:75.0pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt; padding:0cm 3.5pt 0cm 3.5pt; height:60.0pt">
                            <p class="x_MsoNormal"><span style="font-size:10.0pt; font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;; color:black">${project.hitos.actividades[0].noAplica ? 'NO APLICA VISITA OBRA CIVIL' : `${project.hitos.actividades[0].estado} <br>${project.hitos.actividades[0].fechaCompromiso}` }<u></u><u></u></span></p>
                        </td>
                        <td width="170" valign="bottom" style="width:85.15pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt; padding:0cm 3.5pt 0cm 3.5pt; height:60.0pt">
                            <p class="x_MsoNormal"><span style="font-size:10.0pt; font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;; color:black">${project.hitos.actividades[1].noAplica ? 'NO APLICA OBRA CIVIL' : `${project.hitos.actividades[1].estado} <br>${project.hitos.actividades[1].fechaCompromiso}` }
                            <u></u><u></u></span></p>
                        </td>
                        <td width="150" valign="bottom" style="width:75.0pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt; padding:0cm 3.5pt 0cm 3.5pt; height:60.0pt">
                            <p class="x_MsoNormal"><span style="font-size:10.0pt; font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;; color:black">&nbsp; ${project.hitos.actividades[2].noAplica ? 'NO APLICA EMPALMES' : `${project.hitos.actividades[2].estado} <br>${project.hitos.actividades[2].fechaCompromiso}` }
                            <u></u><u></u></span></p>
                        </td>
                        <td width="178" valign="bottom" style="width:89.0pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt; padding:0cm 3.5pt 0cm 3.5pt; height:60.0pt">
                            <p class="x_MsoNormal"><span style="font-size:10.0pt; font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;; color:black">&nbsp; ${project.hitos.actividades[3].noAplica ? 'NO APLICA ENTREGA SERVICIO' : `${project.hitos.actividades[3].estado} <br>${project.hitos.actividades[3].fechaCompromiso}` }
                            <u></u><u></u></span></p>
                        </td>
                        <td width="360" valign="bottom" style="width:180.0pt; border-top:none; border-left:none; border-bottom:solid windowtext 1.0pt; border-right:solid windowtext 1.0pt; padding:0cm 3.5pt 0cm 3.5pt; height:60.0pt">
                            <p class="x_MsoNormal"><span style="font-size:10.0pt; font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;; color:black">${project.hitos.actividades[3].observaciones ? project.hitos.actividades[3].observaciones : 
                                                                                                                                                        project.hitos.actividades[0].observaciones ? project.hitos.actividades[0].observaciones :
                                                                                                                                                        project.hitos.actividades[1].observaciones ? project.hitos.actividades[1].observaciones :
                                                                                                                                                        project.hitos.actividades[2].observaciones ? project.hitos.actividades[2].observaciones : 
                                                                                                                                                        project.hitos.observacionesGenrales ? project.hitos.observacionesGenrales : 'Sin Observaciones' }
                            <u></u><u></u></span></p>
                        </td>
                    </tr>`;

            timeline += `
                    <div dir="ltr">
                        <ul style="font-family:Calibri,Helvetica,sans-serif; margin-bottom:0cm">
                            <li style="list-style-type: none; display: flex; flex-wrap: nowrap;">
                            ${project.hitos.actividadActual != 'Pendiente Cliente' ? '' : `<img src="https://telmex.oss-ap-southeast-1.aliyuncs.com/ReporteActualizacion/pendienteCliente.png" alt="pendiente cliente" width="auto" height="40">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;` }
                            <span style="font-size:15.0pt; font-family:&quot;Berlin Sans FB&quot;,sans-serif; color:black; background:white">OT&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            <span style="font-size:15.0pt; color:black; border:none windowtext 1.0pt; padding:0cm">
                            ${project.projectId} ${project.destinationAddress}</span></li>
                        </ul>
                        <p style="font-family:Calibri,sans-serif; margin:0cm 0cm 0.0001pt 36pt; font-size:11pt; line-height:normal">
                            <span style="font-size:12.0pt; font-family:&quot;Times New Roman&quot;,serif"></span>
                        </p>
                        <br>
                        <table>
                            <tr>
                            ${ project.hitos.actividades[0].noAplica ? '' : ((project.hitos.actividades[0].estado != 'Fecha tentativa' && showImages(project.hitos.actividadActual).includes(1)) ? 
                                                                                                                                    `<td align="center"><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">Visita Obra Civil</span></b><br><img src="https://telmex.oss-ap-southeast-1.aliyuncs.com/ReporteActualizacion/VisitaObraCivil_verde.png" alt="visita obra civil" width="auto" height="110"><br><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">${project.hitos.actividades[0].fechaCompromiso}</span></b></td>`
                                                                                                                                    : `<td align="center"><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">Visita Obra Civil</span></b><br><img src="https://telmex.oss-ap-southeast-1.aliyuncs.com/ReporteActualizacion/VisitaObraCivil_gris.png" alt="visita obra civil" width="auto" height="110"><br><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">${project.hitos.actividades[0].fechaCompromiso}</span></b></td>` )}
                                                                                                                                
                                                                                                        ${ project.hitos.actividades[1].noAplica ? '' : (project.hitos.actividades[1].estado == 'Fecha tentativa' && showImages(project.hitos.actividadActual).includes(2)) ? 

                                                                                                                                        `<td align="center"><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">Ejecución Obra Civil</span></b><br><img src="https://telmex.oss-ap-southeast-1.aliyuncs.com/ReporteActualizacion/ejecucionObraCivil_verde.png" alt="ejecucion obra civil" width="auto" height="110"><br><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">${project.hitos.actividades[1].fechaCompromiso}</span></b></td>` 
                                                                                                                                        : 
                                                                                                                                        `<td align="center"><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">Ejecución Obra Civil</span></b><br><img src="https://telmex.oss-ap-southeast-1.aliyuncs.com/ReporteActualizacion/ejecucionObraCilvil_gris.png" alt="ejecucion obra civil"  width="auto" height="110"><br><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">${project.hitos.actividades[1].fechaCompromiso}</span></b></td>`
                                                                                                                                    }
                                                                                                        ${ project.hitos.actividades[2].noAplica ? '' : ((project.hitos.actividades[2].estado == 'Fecha tentativa'  && showImages(project.hitos.actividadActual).includes(3)) ? 
                                                                                                        `<td align="center"><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">Empalmes</span></b><br><img src="https://telmex.oss-ap-southeast-1.aliyuncs.com/ReporteActualizacion/Empalme_verde.png" alt="empalme"  width="auto" height="110"><br><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">${project.hitos.actividades[2].fechaCompromiso}</span></b></td>`
                                                                                                        :
                                                                                                        `<td align="center"><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">Empalme</span></b><br><img src="https://telmex.oss-ap-southeast-1.aliyuncs.com/ReporteActualizacion/Empalme_gris.png" alt="empalme" width="auto" height="110"><br><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">${project.hitos.actividades[2].fechaCompromiso}</span></b></td>`
                                                                                                                        )}
                                                                                                        ${ project.hitos.actividades[3].noAplica ? '' : ((project.hitos.actividades[3].estado == 'Fecha tentativa'  && showImages(project.hitos.actividadActual).includes(4)) ? 
                                                                                                                                        `<td align="center"><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">Entrega Servicio</span></b><br><img src="https://telmex.oss-ap-southeast-1.aliyuncs.com/ReporteActualizacion/entregaServicio_verde.png" alt="entrega servicio" width="auto" height="110"><br><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">${project.hitos.actividades[3].fechaCompromiso}</span></b></td>`
                                                                                                                                        :
                                                                                                                                        `<td align="center"><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">Entrega Servicio</span></b><br><img src="https://telmex.oss-ap-southeast-1.aliyuncs.com/ReporteActualizacion/entregaServicio_gris.png" alt="entrega servicio" width="auto" height="110"><br><b style="font-size:12pt; font-style:inherit; font-variant-ligatures:inherit; font-variant-caps:inherit"><span style="font-size:14.0pt; font-family:&quot;Arial Narrow&quot;,sans-serif">${project.hitos.actividades[3].fechaCompromiso}</span></b></td>`
                                                                                                                                    )}
                                
                            </tr>
                        </table>
                    </div>
                    <br><br>`;
        
        }).then(() => {
            x++;
        if (x == data.length) {

            var transporter = nodemailer.createTransport({
                service: 'gmail',
                host: 'smtp.gmail.com',
                port: 587,
                secure: false,
                auth: {
                    user: process.env.EMAIL,
                    pass: process.env.EMAIL_PASSWORD,
                },
            });

            var mailOptions = {
                from: process.env.EMAIL,
                to: info.contacto.email,
                subject: `REPORTE DE ACTUALIZACION DE ACTIVIDADES ${data[0].service && data[0].service.toUpperCase()} - ${info.cliente.toUpperCase()} / OT ${otpIds.join(' - ')}`,
                text: 'Prueba Test',
                template: 'hitos',
                context: {
                    data: info,
                    template,
                    timeline
                }
            };

            transporter.use('compile', hbs({
            viewEngine: {
                extName: '.handlebars',
                partialsDir: __dirname + '../../../views/',
                layoutsDir:  __dirname + '../../../views/',
                defaultLayout : 'hitos',
            },
            viewPath:  __dirname + '../../../views/'
            }));

            // Enviamos el email
            transporter.sendMail(mailOptions, function(error, info2){
                if (error){
                    console.log(error, info2); //eslint-disable-line
                    res.status(500).send(error.message);
                } else {
                    res.status(200).jsonp(req.body);
                    res.writeHead(301);
                    res.end();
                }
            });

            }    
        });
    }

};
