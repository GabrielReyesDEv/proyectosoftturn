
module.exports = {
  ifCond: (options) => {
    return (options.hash.expected === options.hash.val) ? options.fn(this) : options.inverse(this);
  },
  includeElement: (options) => {
    return (options.hash.val.includes(options.hash.expected)) ? options.fn(this) : options.inverse(this);
  }
}