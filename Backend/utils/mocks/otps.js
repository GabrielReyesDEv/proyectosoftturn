const otpsMock = [
  {
    "idUser": "123", //k_id_user
    "onyx": "123", //id_cliente_onyx
    "nombreSede": "test", 
    "ciudad": "test",
    "departamento": "test",
    "direccion": "test",
    "clasificacion": "test",
    "tipoOficina": "test",
    "cliente": "test", //n_nombre_cliente
    "ordenTrabajo": "test", //orden_trabajo
    "servicio": "test", //servicio
    "estado": "test", //estado_orden_trabajo
    "fechaCreacion": "test", //fecha_creacion
    "fechaCompromiso": "test", //fecha_compromiso
    "fechaProgramacion": "test", // fecha_programacion
    "listaObservaciones": "test", //lista_observaciones
    "observacion": "test", //observacion
    "fechaActualizacion": "test", //fecha_actualizacion
    "usuarioActualizacion": "test", //usuario_actualizacion
    "finalizo": "test", //finalizo
    "ultimoReporte": "test", //ultimo_envio_reporte
    "cerradoZolid": "test", //cerrado_zolid
    "nombreResponsable": "test",
    "oth": [
      {
        "_id": "test",
        "tipo": "test",
        "estado": "test",
        "grupo": "test",
        "updatedAt": "test",
        "asignado": "test",
        "fechaCompromiso": "test"
      },
      {
        "_id": "test",
        "tipo": "test",
        "estado": "test",
        "grupo": "test",
        "updatedAt": "test",
        "asignado": "test",
        "fechaCompromiso": "test"
      }
    ]
  },

  {
    "idUser": "456", //k_id_user
    "onyx": "456", //id_cliente_onyx
    "nombreSede": "test", 
    "ciudad": "test",
    "departamento": "test",
    "direccion": "test",
    "clasificacion": "test",
    "tipoOficina": "test",
    "cliente": "test", //n_nombre_cliente
    "ordenTrabajo": "test", //orden_trabajo
    "servicio": "test", //servicio
    "estado": "test", //estado_orden_trabajo
    "fechaCreacion": "test", //fecha_creacion
    "fechaCompromiso": "test", //fecha_compromiso
    "fechaProgramacion": "test", // fecha_programacion
    "listaObservaciones": "test", //lista_observaciones
    "observacion": "test", //observacion
    "fechaActualizacion": "test", //fecha_actualizacion
    "usuarioActualizacion": "test", //usuario_actualizacion
    "finalizo": "test", //finalizo
    "ultimoReporte": "test", //ultimo_envio_reporte
    "cerradoZolid": "test", //cerrado_zolid
    "nombreResponsable": "test",
    "oth": [
      {
        "_id": "test",
        "tipo": "test",
        "estado": "test",
        "grupo": "test",
        "updatedAt": "test",
        "asignado": "test",
        "fechaCompromiso": "test"
      },
      {
        "_id": "test",
        "tipo": "test",
        "estado": "test",
        "grupo": "test",
        "updatedAt": "test",
        "asignado": "test",
        "fechaCompromiso": "test"
      }
    ]
  }
  

]

module.exports = { otpsMock };