const MongoLib = require('../lib/mongo');

class ProjectService {
	constructor() {
		this.collection = 'projects';
		this.mongoDB = new MongoLib();
	}
	async getProjects( tags ) {
    const projects = await this.mongoDB.getAllPaginated(this.collection, tags);
    return projects || [];
  }

  async getProject({ projectId }) {
    const project = await this.mongoDB.get(this.collection, projectId);
    return project || {};
  }

  async getProjectsPaginated( tags ) {
    const result = await this.mongoDB.match(this.collection, tags);
    return result;
  }

  async createProject({ project }) {
    const createProjectId = await this.mongoDB.create(this.collection, project);
    return createProjectId;
  }

  async updateProject({ projectId, project } = {}) {
    const updatedProjectId = await this.mongoDB.update(
      this.collection,
      projectId,
      project
    );
    return updatedProjectId;
  }

  async deleteProject({ projectId }) {
    const deletedProjectId = await this.mongoDB.delete(this.collection, projectId);
    return deletedProjectId;
  }

  async findProject(query, filters = {}) {
    const users = await this.mongoDB.getAll(this.collection, query, filters);
    return users;
  }
}

module.exports = ProjectService