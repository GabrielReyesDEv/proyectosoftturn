const MongoLib = require('../lib/mongo');
// const ZteId = require('../services/zteId');

class CierrekoService {
    constructor() {
        this.collection = 'tasks';
        this.mongoDB = new MongoLib();
    }
    
    // async getcierresko( tags ){
    //     // const query = tags && { tags: {$in: tags}};
    //     const cierreko = await this.mongoDB.getAll(this.collection, tags);
    //     return cierreko || [];
    // }

    // async getcierreskoPaginated( tags ) {
    //     const cierreko = await this.mongoDB.getPagination(this.collection, tags);
    //     return cierreko || [];
    // }

    async getcierreko(tags) {
        const result = await this.mongoDB.match(this.collection, tags);
        return result;
    }

    // async createCierreko({ cierreko }) {
    //     const ZteIdService = new ZteId();
    //     cierreko.zteId = await ZteIdService.nextVal(this.collection);
    //     const cierrekoId = await this.mongoDB.create(this.collection, cierreko);
    //     return cierrekoId;
    // } 

    // async updateCierreko(tags) {
    //     const updatecierrekoId = await this.mongoDB.updateGeneral(this.collection, tags.collection.id, tags.query, tags.collection.filter);
    //     return updatecierrekoId;
    // }

    // async deleteCierreko({ cierrekoId }) {
    //     const deletedCierreokId = await this.mongoDB.delete(this.collection, cierrekoId)
    //     return deletedCierreokId;
    // }

    // async getMails() {
    //     const deletedCierreokId = await this.mongoDB.getMails('users')
    //     return deletedCierreokId;
    // }

}

module.exports = CierrekoService