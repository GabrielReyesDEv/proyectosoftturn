const MongoLib = require('../lib/mongo');

class zteId {
    constructor() {
        this.mongoDB = new MongoLib();
    }
    
    async nextVal(name) {
        
        var bulkUpdateOpsHitos = [];
        const collect = 'sequence';
         
          bulkUpdateOpsHitos.push({ 
    
          "updateOne": {
            "filter": { "name": name },
            "update": { "$inc": {   
              "seq": 1
            }
          },
          "upsert": true
        } 
    
        });
        
          // eslint-disable-next-line no-unused-vars
          var createHitoId = await this.mongoDB.updates(collect, bulkUpdateOpsHitos);
          var res = await this.mongoDB.getSeq(collect,name);
    
        return (JSON.stringify(res.seq));
     }
}


module.exports = zteId