const MongoLib = require('../lib/mongo');

class AreaService {
	constructor() {
		this.collection = 'areas';
		this.mongoDB = new MongoLib();
	}
	async getAreas( tags ) {
    const areas = await this.mongoDB.getAllPaginated(this.collection, tags);
    return areas || [];
  }

  async getAreasPaginated( tags ) {
    const result = await this.mongoDB.match(this.collection, tags);
    return result;
  }

  async getArea({ areaId }) {
    const area = await this.mongoDB.get(this.collection, areaId);
    return area || {};
  }

  async createArea({ area }) {
    const createAreaId = await this.mongoDB.create(this.collection, area);
    return createAreaId;
  }

  async updateArea({ areaId, area } = {}) {
    const updatedAreaId = await this.mongoDB.update(
      this.collection,
      areaId,
      area
    );
    return updatedAreaId;
  }

  async deleteArea({ areaId }) {
    const deletedAreaId = await this.mongoDB.delete(this.collection, areaId);
    return deletedAreaId;
  }
}

module.exports = AreaService