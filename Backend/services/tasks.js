const MongoLib = require('../lib/mongo');
const moment = require('moment');

class TaskService {
	constructor() {
		this.collection = 'tasks';
		this.mongoDB = new MongoLib();
	}
	async getTasks( tags ) {
    const tasks = await this.mongoDB.getAllPaginated(this.collection, tags);
    return tasks || [];
  }

  async getTasksPaginated( tags ) {
    const result = await this.mongoDB.match(this.collection, tags);
    return result;
  }

  async getArrayObject(project) {
    const createProjectId = await this.mongoDB.getArrayCollection(this.collection, project);
    return createProjectId;
} 

  async getTask({ taskId }) {
    const task = await this.mongoDB.get(this.collection, taskId);
    return task || {};
  }

  async createTask({ task }) {
    const createTaskId = await this.mongoDB.create(this.collection, task);
    return createTaskId;
  }

  async updateTask({ taskId, task } = {}) {
    const updatedTaskId = await this.mongoDB.update(
      this.collection,
      taskId,
      task
    );
    return updatedTaskId;
  }

  async deleteTask({ taskId }) {
    const deletedTaskId = await this.mongoDB.delete(this.collection, taskId);
    return deletedTaskId;
  }
}

module.exports = TaskService