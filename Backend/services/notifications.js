const MongoLib = require('../lib/mongo');
const axios = require('axios');

class NotificationService {
	constructor() {
		this.collection = 'notifications';
		this.mongoDB = new MongoLib();
	}
	async getNotifications( tags ) {
    const notifications = await this.mongoDB.getAllPaginated(this.collection, tags);
    return notifications || [];
  }

  async getNotification({ notificationId }) {
    const notification = await this.mongoDB.get(this.collection, notificationId);
    return notification || {};
  }

  async getNotificationsPaginated( tags ) {
    const result = await this.mongoDB.match(this.collection, tags);
    return result;
  }

  async createNotification({ notification, room }) {
    const createNotificationId = await this.mongoDB.create(this.collection, notification);
    await axios.post('http://localhost:5000/api/notifications', room);
    return createNotificationId;
  }

  async updateNotification({ notificationId, notification } = {}) {
    const updatedNotificationId = await this.mongoDB.update(
      this.collection,
      notificationId,
      notification
    );
    return updatedNotificationId;
  }

  async deleteNotification({ notificationId }) {
    const deletedNotificationId = await this.mongoDB.delete(this.collection, notificationId);
    return deletedNotificationId;
  }
}

module.exports = NotificationService