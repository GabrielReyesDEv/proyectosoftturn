const MongoLib = require('../lib/mongo');

class SequenceService {
	constructor() {
		this.collection = 'sequences';
		this.mongoDB = new MongoLib();
	}
	async getSequences({ tags }) {
    const query = tags && { tags: { $in: tags } };
    const sequences = await this.mongoDB.getAll(this.collection, query);
    return sequences || [];
  }

  async getSequence({ sequenceId }) {
    const sequence = await this.mongoDB.get(this.collection, sequenceId);
    return sequence || {};
  }

  async createSequence({ sequence }) {
    const createSequenceId = await this.mongoDB.create(this.collection, sequence);
    return createSequenceId;
  }

  async updateSequence({ sequenceId, sequence } = {}) {
    const updatedSequenceId = await this.mongoDB.update(
      this.collection,
      sequenceId,
      sequence
    );
    return updatedSequenceId;
  }

  async deleteSequence({ sequenceId }) {
    const deletedSequenceId = await this.mongoDB.delete(this.collection, sequenceId);
    return deletedSequenceId;
  }
}

module.exports = SequenceService