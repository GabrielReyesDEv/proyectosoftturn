const MongoLib = require('../lib/mongo');
const moment = require('moment');
const ZteId = require('./zteId');
const momentTimezone = require('moment-timezone');
const { DevOpsGuru } = require('aws-sdk');

var totalUsers = {};

var bulkUpdateOpsOths = [];
var bulkUpdateOpsOtps = [];
var bulkUpdateOpsTask = [];

const projectObject = { NRO_OT_ONYX: 'projectId', USUARIO_ASIGNADO: 'author', SEGMENTO: 'segment', GRUPO: 'group', FAMILIA: 'family', PRODUCTO: 'product', DESCRIPCION: 'description', RESOLUCION_1: 'resolucion1', RESOLUCION_2: 'resolucion2', RESOLUCION_3: 'resolucion3', RESOLUCION_4: 'resolucion4', CIUDAD_INCIDENTE: 'incidentCity', CIUDAD_INCIDENTE3: 'incidentCity', NOMBRE_CLIENTE: 'client', ORDEN_TRABAJO: 'type', SERVICIO: 'service', ID_ENLACE: 'serviceCode', ESTADO_ORDEN_TRABAJO: 'status', FECHA_CREACION: 'createdAt', TIEMPO_INCIDENTE: 'incidentDate', FECHA_COMPROMISO: 'commitmentDate', FECHA_PROGRAMACION: 'programmingDate', CIUDAD: 'city', DEPARTAMENTO: 'department', DIRECCION_DESTINO: 'destinationAddress', DIRECCION_ORIGEN: 'sourceAddress', FECHA_REALIZACION: 'finishDate', MONTO_MONEDA_LOCAL_CARGO_MENSUAL: 'mrc'}
const activityObject = { NRO_OT_ONYX: 'projectId', OT_HIJA: 'type', ID_ORDEN_TRABAJO_HIJA: 'activityId', ESTADO_ORDEN_TRABAJO_HIJA: 'status', FECHA_CREACION_OT_HIJA: 'createdAt', RESOLUCION_15: 'resolucion15', RESOLUCION_26: 'resolucion26', RESOLUCION_37: 'resolucion37', RESOLUCION_48: 'resolucion48', FEC_ACTUALIZACION_ONYX_HIJA: 'updatedAt', USUARIO_ASIGNADO4: 'userOnyx', OTH_HIJA: 'type'}
class ActivityService {
	constructor() {
		this.collection = 'activities';
		this.mongoDB = new MongoLib();
	}
	async getActivities( tags ) {
    const activities = await this.mongoDB.getAllPaginated(this.collection, tags);
    return activities || [];
  }

  async getActivity({ activityId }) {
    const activity = await this.mongoDB.get(this.collection, activityId);
    return activity || {};
  }

  async getActivititiesPaginated( tags ) {
    const result = await this.mongoDB.match(this.collection, tags);
    return result;
  }

  async createActivity({ activity }) {
    const createActivityId = await this.mongoDB.create(this.collection, activity);
    return createActivityId;
  }

  async updateActivity({ activityId, activity } = {}) {
    const updatedActivityId = await this.mongoDB.update(
      this.collection,
      activityId,
      activity
    );
    return updatedActivityId;
  }

  async deleteActivity({ activityId }) {
    const deletedActivityId = await this.mongoDB.delete(this.collection, activityId);
    return deletedActivityId;
  }
getObjectValue(obj, type, value, users) {
  switch (type) {
    case 'USUARIO_ASIGNADO': {
        const userOnyx = (value.replace(/ /g, "")).toUpperCase();
        const array = users.map((user) => {
          return user.roles.map((role) => { return { ...role, firstName: user.firstName, lastName: user.lastName, _id: user.identification }})
        })
        let all17 = array.reduce((prev, next) => prev.concat(next), []);
        const author = all17.find(user => user.userOnyx === userOnyx)
        return { members: author.contacts, author: author && `${author.firstName} ${author.lastName}#${author._id}` }
    }
    case 'FECHA_REALIZACION':
    case 'FECHA_CREACION':
    case 'TIEMPO_INCIDENTE':
    case 'FECHA_COMPROMISO':
    case 'FECHA_PROGRAMACION':
    case 'FEC_ACTUALIZACION_ONYX_HIJA':
    case 'FECHA_CREACION_OT_HIJA':
      if (moment(value, 'YYYY-MM-DD HH:mm', true).isValid()) {
        return { [obj[type]]: value }
      } else {
        return { [obj[type]]: (moment((value - 25568) *  86400 *  1000).format("YYYY-MM-DD HH:mm")).toLocaleString()}
      }
    default: {
      return { [obj[type]]: (value && value !== '') && value.trim() }
    }
  }
}

updateMany(doc, users, filter, obj, areas) {

  let areaInfo

  if (filter === 'projectId' && (doc['ESTADO_ORDEN_TRABAJO'] !== '3- Terminada' || doc['ESTADO_ORDEN_TRABAJO'] !== 'Cancelada')) {

    const filtered = areas.filter((area) => area.kpi )
    const array = filtered.map((area) => {
      return area.kpi.map((kpi) => { return { name: kpi, area: area.name, group: area.group, status: area.kpiStatus }})
    })
    let all17 = array.reduce((prev, next) => prev.concat(next), []);
    const kpi = all17.find(kpi => doc.SERVICIO.match(kpi.name))

    
    if (kpi) {
      const userOnyx = (doc['USUARIO_ASIGNADO'].replace(/ /g, "")).toUpperCase();
      const array2 = users.map((user) => {
        return user.roles.map((role) => { return { ...role, firstName: user.firstName, lastName: user.lastName, _id: user.identification }})
      })
      let all172 = array2.reduce((prev, next) => prev.concat(next), []);
      const author = all172.find(user => user.userOnyx === userOnyx)

      let fechas2 = {}

      if (moment(doc['FECHA_CREACION'], 'YYYY-MM-DD HH:mm', true).isValid()) {
        fechas2['deadlineDate'] = doc['FECHA_CREACION']
        fechas2['createdAt'] = doc['FECHA_CREACION']
      } else {
        fechas2['deadlineDate'] = (moment((doc['FECHA_CREACION'] - 25568) *  86400 *  1000).add(kpi.status[0].days, 'days').format("YYYY-MM-DD HH:mm")).toLocaleString()
        fechas2['createdAt'] = (moment((doc['FECHA_CREACION'] - 25568) *  86400 *  1000).format("YYYY-MM-DD HH:mm")).toLocaleString()
      }

      const data = {
        author: author && `${author.coordinator}` ,
        orderType: kpi.name,
        area: kpi.area,
        group: kpi.group,
        priority: 0,
        type: 'Primary',
        status: kpi.status[0].type,
        role: kpi.status[0].role,
        zolidStatus: kpi.status[0].status[0].name,
        deadlineDays: kpi.status[0].days,
        logs: [
          { //agregar el usuario asignado
            status: kpi.status[0].status[0].name,
            description: `Tarea asignada a ${kpi.status[0].role} ${author.coordinator.split('#')[0]} otpId: ${doc['NRO_OT_ONYX']}`,
            createdAt: momentTimezone().tz('America/Bogota').format("YYYY-MM-DD HH:mm"),
            author: 'zolid#null'
          }
        ],
        updatedAt: momentTimezone().tz('America/Bogota').format("YYYY-MM-DD HH:mm"),
        ...fechas2
      }

      bulkUpdateOpsTask.push({ 
        "updateOne": {
          "filter": { "$and": [{ "projectId" : `${doc.NRO_OT_ONYX}`}, { "activityId": null}] },
          "update": { "$set": {...data} },
          "upsert": true, 
        }
      });
    }
  }

  if (filter === 'activityId' && (doc['ESTADO_ORDEN_TRABAJO'] !== '3- Terminada' || doc['ESTADO_ORDEN_TRABAJO'] !== 'Cancelada')) {
    const array = areas.map((area) => {
      return area.activities.map((activity) => { return { name: activity, area: area.name, group: area.group, tasks: area.tasks }})
    })
    let all17 = array.reduce((prev, next) => prev.concat(next), []);
    const kpi = all17.find(activity => doc.OT_HIJA.includes(activity.name));
    
    if (kpi) {
      let userOnyx = (doc['USUARIO_ASIGNADO4'].replace(/ /g, "")).toUpperCase();
      let array2 = users.map((user) => {
        return user.roles.map((role) => { return { ...role, firstName: user.firstName, lastName: user.lastName, _id: user.identification }})
      })
      let all172 = array2.reduce((prev, next) => prev.concat(next), []);
      let author = all172.find(user => user.userOnyx === userOnyx)
      if (!author) {
        userOnyx = (doc['USUARIO_ASIGNADO'].replace(/ /g, "")).toUpperCase();
        const array21 = users.map((user) => {
          return user.roles.map((role) => { return { ...role, firstName: user.firstName, lastName: user.lastName, _id: user.identification }})
        })
        let all1722 = array21.reduce((prev, next) => prev.concat(next), []);
        author = all1722.find(user => user.userOnyx === userOnyx)
      }
      const toDo = kpi.tasks.filter((task) => task.order === 0)
      toDo.map((val) => {

        areaInfo = kpi.area
        let fechas = {}

        if (moment(doc['FECHA_CREACION_OT_HIJA'], 'YYYY-MM-DD HH:mm', true).isValid()) {
          fechas['deadlineDate'] = doc['FECHA_CREACION_OT_HIJA']
          fechas['createdAt'] = doc['FECHA_CREACION_OT_HIJA']
        } else {
          fechas['deadlineDate'] = (moment((doc['FECHA_CREACION_OT_HIJA'] - 25568) *  86400 *  1000).add(val.days, 'days').format("YYYY-MM-DD HH:mm")).toLocaleString()
          fechas['createdAt'] = (moment((doc['FECHA_CREACION_OT_HIJA'] - 25568) *  86400 *  1000).format("YYYY-MM-DD HH:mm")).toLocaleString()
        }
      const data = {
        author: author && `${author.firstName} ${author.lastName}#${author._id}`,
        orderType: kpi.name,
        area: kpi.area,
        group: kpi.group,
        priority: 0,
        type: 'Primary',
        status: val.type,
        role: val.role,
        zolidStatus: 'Created',
        deadlineDays: val.days,
        logs: [
          { 
            status: val.type,
            description: `Tarea asignada a ${val.role} ${author.firstName} ${author.lastName} othId: ${doc['ID_ORDEN_TRABAJO_HIJA']}`,
            createdAt: momentTimezone().tz('America/Bogota').format("YYYY-MM-DD HH:mm"),
            author: 'zolid#null'
          }
        ],
        updatedAt: momentTimezone().tz('America/Bogota').format("YYYY-MM-DD HH:mm"),
        ...fechas
      }

      bulkUpdateOpsTask.push({ 
        "updateOne": {
          "filter": { "$and": [{ "projectId" : `${doc.NRO_OT_ONYX}`, "status" : data.status}, { "activityId": `${doc['ID_ORDEN_TRABAJO_HIJA']}`}] },
          "update": { "$set": data },
          "upsert": true, 
        }
      });
    })
    }
  }
  let data = {}
  for (let key in doc) {
    const value = (typeof(doc[key]) == 'number') ? JSON.stringify(doc[key]) : doc[key]
    const result = this.getObjectValue(obj, key, value, users)
    data = {
      ...data,
      ...result,
    }
    delete data.undefined
  }
  return {
    "filter": { [filter]: data[filter]},
    "update": { "$set": {...data, area: areaInfo, priority: 0, updatedAt: momentTimezone().tz('America/Bogota').format("YYYY-MM-DD HH:mm") } },
    "upsert": true,
  }
}
  async upsertOts( ots, users, areas = [] ) {
    totalUsers = {};    
    var createOthId = {};
    var createOtpId = {};
    var createTask = {};
    const collectione = 'projects';
    const collectionTwo = 'tasks';
    for (let i=0; i < ots.length; i++) {
      let data = ots[i];
      if (Object.keys(data).length !== 0) {
        bulkUpdateOpsOths.push({ 
          "updateMany": this.updateMany(data, users, 'activityId', activityObject, areas)
        });
        bulkUpdateOpsOtps.push({ 
          "updateMany": this.updateMany(data, users, 'projectId', projectObject, areas)
        });
      }
    }
    createOthId = await this.mongoDB.updates(this.collection, bulkUpdateOpsOths); 
    createOtpId = await this.mongoDB.updates(collectione, bulkUpdateOpsOtps);
    createTask = bulkUpdateOpsTask.length > 0 ? await this.mongoDB.updates(collectionTwo, bulkUpdateOpsTask) : '';
    const ultimoCargue = {
      lastLoaded: momentTimezone().tz('America/Bogota').format("YYYY-MM-DD HH:mm") //tz('Asia/Hong_Kong')
    };
    var lastCargue = await this.mongoDB.update('sequence', 'projects', { ...ultimoCargue, field: 'name' });
    return  { createOthId, createOtpId, createTask, lastCargue, totalUsers }; 
  }

}

module.exports = ActivityService