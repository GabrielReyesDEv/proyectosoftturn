const MongoLib = require('../lib/mongo');
const { ObjectId } = require('mongodb');
const bcrypt = require('bcrypt');

var bulkUpdateOpsUsers = [];

class UsersService {
  constructor() {
    this.collection = 'users';
    this.mongoDB = new MongoLib();
  }

  async getUsers( tags ) {
    const users = await this.mongoDB.getAllPaginated(this.collection, tags);
    return users || [];
  }

  async getUsersPaginated( tags ) {
    const result = await this.mongoDB.match(this.collection, tags);
    return result;
  }

  async getUser({ email }) {
    const [user] = await this.mongoDB.getAll(this.collection, { email });
    return user;
  }

  async findUsers(query, filters = {}) {
    const users = await this.mongoDB.getAll(this.collection, query, filters);
    return users;
  }

  async getSingleUser({ userId }) {
    const user = await this.mongoDB.get(this.collection, userId);
    return user || {};
  }

  async getUserRole({ identification }) {
    const user = await this.mongoDB.getUserRole(this.collection, { identification });
    return user[0];
  }

  async createUser({ user }) {
    const { password } = user;
    const hashedPassword = await bcrypt.hash(password, 10);

    const createUserId = await this.mongoDB.create(this.collection, {
      ...user,
      password: hashedPassword
    });

    return createUserId;
  }

  async getOrCreateUser({ user }) {
    const queriedUser = await this.getUser({ email: user.email });

    if (queriedUser) {
      return queriedUser;
    }

    await this.createUser({ user });
    return await this.getUser({ email: user.email });
  }

  async updateUser({ userId, user } = {}) {
    if (user.password) user.password = await bcrypt.hash(user.password, 10);
    const updatedUserId = await this.mongoDB.update(
      this.collection,
      userId,
      user
    );
    return updatedUserId;
  }

  async deleteUser({ userId }) {
    const deletedUserId = await this.mongoDB.delete(this.collection, userId);
    return deletedUserId;
  }

  async upsertUsers( users ) {
    var createUserId = {};
    for (let i=0; i < users.length; i++) {
      let data = users[i];
      if (Object.keys(data).length !== 0) {
        bulkUpdateOpsUsers.push({ 
          "updateMany": this.updateManyGeneral( '_id', data._id, data)
        });
      }
    }
    createUserId = await this.mongoDB.updates(this.collection, bulkUpdateOpsUsers); 
    return  { createUserId }; 
  }

  updateManyGeneral(filtro, id, doc) {
    let user = {}
    let roles = [];
    let groups = [];
    user = {
      firstName: doc.firstName,
      lastName: doc.lastName,
      identification: doc.identification,
      email: doc.email,
      password: doc.password,
      createdAt: doc.fechaContrasena,
      verified: doc.actContrasena,
      workPosition: 'No Especificado',
      status: 'Active',
      // groups: ['BO'],
      roles: [],
      "phone": `${doc.telefono}`
      // createdAt: moment().format('DD/MM/YYYY')
    }
    if(doc.roles.length > 0) {
      for (let index = 0; index < doc.roles.length; index++) {
        if (doc.roles[index].title === 'Ingeniero Gestion') {
          roles.push({
            role: 'Coordinator',
            area: 'Nivel 1',
            group: 'BO',
            userOnyx: doc.roles[index].userOnyx,
            "coordinator": "Edgar Tapia#1085897976",
            "contacts": [{
                "name": `${doc.firstName} ${doc.lastName}`,
                "mail": doc.email,
                "phone": `${doc.telefono}`
            }, {
                "name": "Melani Viveros",
                "mail": "melani.viverosm.ext@claro.com.co",
                "phone": "3102129290"
            }, {
                "name": "Cristian Ramirez",
                "mail": "cristian.ramirezr.ext@claro.com.co",
                "phone": "3102852816"
            }]
          }) 
          if (groups.indexOf('BO') === -1) groups.push('BO');
        } else if (doc.roles[index].group === 'PYMES') {
          roles.push({
            role: doc.roles[index].title.indexOf(' ') !== -1 ? doc.roles[index].title.split(' ')[0] : doc.roles[index].title,
            area: 'Pymes',
            group: 'BO',
          })
          if (groups.indexOf('BO') === -1) groups.push('BO');
        } else if (doc.roles[index].group === 'Samia') {
          roles.push({
            role: doc.roles[index].title.indexOf(' ') !== -1 ? doc.roles[index].title.split(' ')[0] : doc.roles[index].title,
            area: doc.roles[index].group,
            group: 'Samia',
          })
          if (groups.indexOf('Samia') === -1) groups.push('Samia');
        }
        else {
          roles.push({
            role: doc.roles[index].title.indexOf(' ') !== -1 ? doc.roles[index].title.split(' ')[0] : doc.roles[index].title,
            area: doc.roles[index].group,
            group: 'BO',
          })
          if (groups.indexOf('BO') === -1) groups.push('BO');
        }  
      }
      user = {...user, roles, groups};
    };
    return {
        "filter": { [filtro]: ( filtro === '_id') ? ObjectId(id) : id },
        "update": { "$set": user
        },
        "upsert": true, 
      }

}

}

module.exports = UsersService;