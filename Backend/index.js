const express = require('express');
const cors = require("cors");
const helmet = require('helmet');
const app = express();

const { config } = require('./config/index');

const authApi = require('./routes/auth');
const usersApi = require('./routes/users');
const tasksApi = require('./routes/tasks.js');
const projectsApi = require('./routes/projects.js');
const activitiesApi = require('./routes/activities.js');
const notificationsApi = require('./routes/notifications.js');
const areasApi = require('./routes/areas.js');
const sequencesApi = require('./routes/sequences.js');
const s3Api = require('./routes/s3.js');
const functionalitiesApi = require('./routes/functionalities.js');
const emailApi = require('./routes/email.js');

const {
  logErrors,
  wrapErrors,
  errorHandler
} = require('./utils/middleware/errorHandlers.js');

const notFoundHandler = require('./utils/middleware/notFoundHandler');

const bodyParser = require('body-parser');
app.use(bodyParser.json({limit: "180mb" }));
app.use(bodyParser.urlencoded({limit: "180mb" , extended: true, parameterLimit:90000}));

// cors
app.use(cors());

// body parser
app.use(express.json());
app.use(helmet());

// routes
authApi(app);
usersApi(app);
tasksApi(app);
projectsApi(app);
activitiesApi(app);
notificationsApi(app);
areasApi(app);
sequencesApi(app);
emailApi(app);
s3Api(app);
functionalitiesApi(app);

// Catch 404
app.use(notFoundHandler);

// Errors middleware
app.use(logErrors);
app.use(wrapErrors);
app.use(errorHandler);

app.listen(config.port, function() {
  console.log(`Listening http://localhost:${config.port}`);
});
