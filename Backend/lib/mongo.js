const { MongoClient, ObjectId } = require('mongodb');
const { config } = require('../config');
const moment = require('moment');

const USER = encodeURIComponent(config.dbUser);
const PASSWORD = encodeURIComponent(config.dbPassword);
const DB_NAME = config.dbName;
let MONGO_URI

if (config.dev) {
	// MONGO_URI = `mongodb://mongo_db:a4b3c2d1@cluster0-shard-00-00-rentc.mongodb.net:27017,cluster0-shard-00-01-rentc.mongodb.net:27017,cluster0-shard-00-02-rentc.mongodb.net:27017/${DB_NAME}?authSource=admin&replicaSet=Cluster0-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true`;
  MONGO_URI = `mongodb://${USER}:${PASSWORD}@${config.dbHost}/${DB_NAME}?authSource=admin&replicaSet=Cluster0-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true`;
  // MONGO_URI = `mongodb://Gabriel:999Reyes@cluster0-shard-00-00-1gvdo.mongodb.net:27017,cluster0-shard-00-01-1gvdo.mongodb.net:27017,cluster0-shard-00-02-1gvdo.mongodb.net:27017/${DB_NAME}?authSource=admin&replicaSet=Cluster0-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true`;
  console.log('hola');
} else {
  MONGO_URI = `mongodb+srv://${USER}:${PASSWORD}@${config.dbHost}/${DB_NAME}?retryWrites=true&w=majority`;
}

class MongoLib {
  constructor() {
    this.client = new MongoClient(MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true });
    this.dbName = DB_NAME;
  }

  connect() {
    if (!MongoLib.connection) {
      MongoLib.connection = new Promise((resolve, reject) => {
        this.client.connect(err => {
          if (err) {
            reject(err);
          }

          console.log('Connected succesfully to mongo');
          resolve(this.client.db(this.dbName));
        });
      });
    }

    return MongoLib.connection;
  }

  getAll(collection, query, filters = {}) {
    return this.connect().then(db => {
      return db
        .collection(collection)
        .find(query)
        .project(filters)
        .toArray();
    });
  }


  getAllPaginated(collection, query) {
    const limit = parseInt(query.limit)
    const page = parseInt(query.page)

    delete query.limit
    delete query.page
    return this.connect().then(db => {
    return db.collection(collection)
      .aggregate([
        { "$facet": {
          "totalData": [
            { "$match": query || {}},
            { "$count": "filterTotal"},
          ],
          "data": [
            { "$match": query ? query : 0},
            { "$skip": (limit * page) - limit },
            { "$limit": limit },
          ],
          "totalCount": [
            { "$group": 
              {
                "_id": null,
                "count": { "$sum": 1 },
              },
            },
          ]
        }}
      ]).toArray()
    })  
  }

  updates(collection, bulkUpdateOps) {
    return this.connect().then(db => {
        return db.collection(collection).bulkWrite(bulkUpdateOps)
    }).then(result => result);
  }

  aggregate(collection, id, filter, group) {
    return this.connect().then(db => {
        return db.collection(collection).aggregate([
            { "$match": { [filter]: id } },
            { "$project": group },
            { "$sort": { natural: -1 } }
        ]).toArray();
    })
  }

  match(collection, query) {
    const { limit, page, sort, columnsNeeded } = query;
    delete query.limit
    delete query.page
    delete query.sort
    delete query.columnsNeeded
    let match = {};
    let totalData = [];
    let data = [];

    if(query.or && query.or.length !== 0) {
        let array = [];
        query.or.map((numberField) => {
            array.push({[numberField.field]: { $regex: new RegExp(numberField.value, 'gi') }});
        })
        match['$or'] = array;
        delete query.or;
    }
    
    if(query.and && query.and.length !== 0) {
        let array = [];
        query.and.map((numberField) => {
          if (typeof(numberField.value) === 'object' && numberField.value.length >=1 && moment(numberField.value[0], 'YYYY-MM-DD', true).isValid() === false) {
              array.push({[numberField.field]: { $in: numberField.value }});
          }
          if (typeof(numberField.value) === 'object' && numberField.value.length ===1 && moment(numberField.value[0], 'YYYY-MM-DD', true).isValid() === true){
              array.push({[numberField.field]: { $regex: new RegExp(numberField.value[0], 'gi') }});
          }
          if (typeof(numberField.value) === 'object' && numberField.value.length ===2 && moment(numberField.value[0], 'YYYY-MM-DD', true).isValid() === true){
              if (numberField.value[0] === numberField.value[1]) {
                  array.push({[numberField.field]: { $regex: new RegExp(numberField.value[0], 'gi') }});
              } else {
                  array.push({[numberField.field]: { $gte: numberField.value[0], $lte: moment(numberField.value[1]).add(1, 'd').locale('es').format('YYYY-MM-DD') }});
              }
          } 
          if (typeof(numberField.value) !== 'object'){
              array.push({[numberField.field]: { $regex: new RegExp(numberField.value, 'gi') }});
          }
        })
        match['$and'] = array;
        delete query.and;
    }
    if(query.compare) {
        query.compare.map((numberField) => {
            match[numberField.field[0]] = { [numberField.field[1]]: numberField.values};
        })
        delete query.compare;
    }
    if(query.eq) {
      query.eq.map((numberField) => {
          if(numberField.field === '_id') {
            match[numberField.field] = { $eq: ObjectId(numberField.values)};
          } else {
            match[numberField.field] = { $in: numberField.values};
          }
      })
      delete query.eq;
    }
    
    if(query.filterFechas) {
        query.filterFechas.map((numberField) => {
            if((numberField.field != 'null')){
                if((numberField.values[0] != 'null') && (numberField.values[1] != 'null')){
                    if (numberField.values[0] === numberField.values[1]) {
                        match[numberField.field] = { $regex: new RegExp(numberField.values[0], 'gi') }
                    } else {
                        match[numberField.field] = { $gte: numberField.values[0], $lt: moment(numberField.values[1]).add(1, 'd').locale('es').format('YYYY-MM-DD') }; //$lt: moment(numberField.values[1]).add(1, 'd').locale('es').format('YYYY-MM-DD') $lte: numberField.values[1]
                    }
                }
                if((numberField.values[0] != 'null') && (numberField.values[1] == 'null')){
                    match[numberField.field] = { $gte: numberField.values[0]};
                }
                if((numberField.values[0] == 'null') && ((numberField.values[1] != 'null'))){
                    match[numberField.field] = { $lte: numberField.values[1]};
                }
            }
        })
        delete query.filterFechas;
    }

    const properties = ["neq", "eq", "gte", "lte", "gt", "lt", "or"];
    const objectProperties = { 'neq': '$nin','eq': '$in', 'gte': '$gte', 'lte': '$lte', 'gt': '$gt', 'lt': '$lt'}
    for (var i = 0; i < properties.length; i++ ) {
      if (properties[i] in query) {
        query[properties[i]].map((val) => {
          (val.field === '_id') ? (
            match[val.field] = ObjectId(val.values[0])
          ) : (
            match[val.field] = {
              [objectProperties[properties[i]]]: val.values
            }
          ) 
        })
        delete query[properties[i]];
      }
    } 

    for (let key in query){
        
        if ( typeof query[key] === 'object' && query[key].$elemMatch ){ 
            match[key] = query[key];
        } else {
            match[key] = { $regex: new RegExp(query[key], 'gi') };
        }
    }
    totalData.push({ $match :  match })
    totalData.push({ "$count": "filterTotal"})
    data.push({ $match :  match })
    if ( columnsNeeded ) {
        data.push({ '$project': columnsNeeded})
    }
    if (limit && page) {
        data.push({ '$skip': (limit * page) - limit})
        data.push({ '$limit': limit })
    }
    if ( sort ) {
      data.push({ '$sort': {[sort.column]: sort.value} })
    }

    return this.connect().then(db => {
        return db.collection(collection)
        .aggregate([
            { "$facet": {
                "totalData": totalData,
                  "data": data,
                  "totalCount": [
                    { "$group": {
                      "_id": null,
                      "count": { "$sum": 1 },
                    },
                    },
                  ]
              }
            }
        ]).toArray()
    })
  }


  getArrayCollection(collection, query) {
    const { limit, page, sort, filter1 } = query;
    delete query.limit
    delete query.page
    delete query.sort
    delete query.filter1
    delete query.columnsNeeded
    let match = {};

    if(query.eq) {
      query.eq.map((numberField) => {
        match[numberField.field] = { $in: numberField.values};
      })
      delete query.eq;
    }

    if(query.neq) {
      query.neq.map((numberField) => {
          match[numberField.field] = { $nin: numberField.values};
      })
      delete query.neq;
  }

    const mergeObjects = ['$$lg'];

    if ( query.mergeObjects){ 
        query.mergeObjects.map((mergeObject) => {
            mergeObjects.push(mergeObject)
        })
        delete query.mergeObjects;
    }

    for (let key in query){
        if ( typeof query[key] === 'object' && query[key].$elemMatch ){ 
            match[key] = query[key];
        } else {
            match[key] = { $regex: new RegExp(query[key], 'gi') };
        }
    }

    return this.connect().then(db => {
        return db.collection(collection)
        .aggregate([
            {$match: match},
            { $project:
                { [filter1]:
                   {
                     $map:
                        {
                          input: `${'$'.concat(filter1)}`,
                          as: "lg",
                          in: { $mergeObjects: mergeObjects }
                        }
                   }
                }
             },
            {$group:{
                _id: null,
                selectedTags: { $push: `${'$'.concat(filter1)}` }     
             }},
             {$project: {
                 selectedTags: { $reduce: {
                     input: "$selectedTags",
                     initialValue: [],
                     in: {$setUnion : ["$$value", "$$this"]}
                 }}
             }},
             { $unwind: "$selectedTags" },
             { '$sort': sort !== undefined ? {[`selectedTags.${sort.column}`]: parseInt(sort.value)} : {_id: 1}},
             { "$group": {
                      "_id": null,
                      "count": { "$sum": 1 },
                      "data": { $push: "$selectedTags"},
                    },
              },
             {$project: {
                 total: {
                     data: "$data",
                     count: "$count"
                  }
             }},
             { $unwind: "$total" },
             { $project: { 
                 "total.count": 1, 
                 data: { $slice: [ "$total.data", parseInt(((limit * page) - limit)) , parseInt(limit) ] },
              }},
        ]).toArray()
    })
  }

  getUserRole(collection, { identification }) {
    return this.connect().then(db => {
      return db.collection(collection).aggregate([
        {$match: { identification }},
        { $project:
            { roles:
               {
                 $map:
                    {
                      input: "$roles",
                      as: "rl",
                      in: { $mergeObjects: [ "$$rl", { role: "$role"}, { area: "$area"}, { group: "$group" }  ] }
                    }
               }
            }
         },
        {$group:{
            _id: null,
            selectedTags: { $push: '$roles' }     
         }},
         {$project: {
             selectedTags: { $reduce: {
                 input: "$selectedTags",
                 initialValue: [],
                 in: {$setUnion : ["$$value", "$$this"]}
             }}
         }},
         { $unwind: "$selectedTags" },
         { "$group": {
                "_id": "null",
                "areas": {$addToSet: '$selectedTags.area'},
                "groups": {$addToSet: '$selectedTags.group'},
                "roles": {$addToSet: '$selectedTags.role'},
            },
        }
        ]).toArray();
    });
  }

  get(collection, id) {
    return this.connect().then(db => {
      return db.collection(collection).findOne({ _id: ObjectId(id) });
    });
  }

  create(collection, data) {
    return this.connect()
      .then(db => {
        return db.collection(collection).insertOne(data);
      })
      .then(result => result.insertedId);
  }

  update(collection, id, data) {
    const field = data.field
    delete data.field
    return this.connect()
      .then(db => {
        return db
          .collection(collection)
          .updateOne({ [field]: ( field === '_id') ? ObjectId(id) : id }, (data['$push'] ||  data['$pull']) ? data : { $set: data }, { upsert: true });
      })
      .then(result => result.upsertedId || id);
  }

  delete(collection, id) {
    return this.connect()
      .then(db => {
        return db.collection(collection).deleteOne({ _id: ObjectId(id) });
      })
      .then(() => id);
  }
}

module.exports = MongoLib;