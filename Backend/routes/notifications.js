const express = require('express');
const passport = require('passport');
const NotificationsService = require('../services/notifications');

const {
  notificationIdSchema,
  createNotificationSchema,
  updateNotificationSchema
} = require('../utils/schemas/notifications');

const validationHandler = require('../utils/middleware/validationHandler');
const scopesValidationHandler = require('../utils/middleware/scopesValidationHandler');

const cacheResponse = require('../utils/cacheResponse');
const {
  FIVE_MINUTES_IN_SECONDS,
  SIXTY_MINUTES_IN_SECONDS
} = require('../utils/time');

// JWT strategy
require('../utils/auth/strategies/jwt');

function notificationsApi(app) {
  const router = express.Router();
  app.use('/api/notifications', router);

  const notificationsService = new NotificationsService();

  router.get(
		'/',
		passport.authenticate('jwt', { session: false }),
		scopesValidationHandler(['read:notifications']),
		async function(req, res, next) {
    cacheResponse(res, FIVE_MINUTES_IN_SECONDS);
    const tags = req.query;
    const page = tags.page;
    const limit = tags.limit;

    try {
      const notifications = await notificationsService.getNotifications( tags );
      const filterTotal = notifications[0].totalData[0] ? notifications[0].totalData[0].filterTotal : 0;

      res.status(200).json({
        page: parseInt(page),
        per_page: parseInt(limit),
        total: filterTotal,
        total_pages: Math.ceil(filterTotal / parseInt(limit)),
        data: notifications[0].data,
        message: 'notifications listed'
      });
    } catch (err) {
      next(err);
    }
  });

  router.get(
		'/:notificationId',
		passport.authenticate('jwt', { session: false }),
		scopesValidationHandler(['read:notifications']),
    validationHandler({ notificationId: notificationIdSchema }, 'params'),
    async function(req, res, next) {
      cacheResponse(res, SIXTY_MINUTES_IN_SECONDS);
      const { notificationId } = req.params;

      try {
        const notifications = await notificationsService.getNotification({ notificationId });

        res.status(200).json({
          data: notifications,
          message: 'notification retrieved'
        });
      } catch (err) {
        next(err);
      }
    }
  );

  router.post('/pagination', passport.authenticate('jwt', { session: false }), async function (req, res, next) {
    const query = req.body;
    const page = query.page;
    const limit = query.limit;
    try {
        const data = await notificationsService.getNotificationsPaginated(query);
        const filterTotal = data[0].totalData[0] ? data[0].totalData[0].filterTotal : 0;
        res.status(200).json({
            page: parseInt(page),
            per_page: parseInt(limit),
            total: filterTotal,
            total_pages: Math.ceil(filterTotal / parseInt(limit)),
            data: data[0].data,
            message: 'notifications retrived'
        });

    } catch (err) {
        next(err);
    }
  }); 

  router.post(
		'/',
		passport.authenticate('jwt', { session: false }),
    scopesValidationHandler(['create:notifications']),
		validationHandler(createNotificationSchema),
		async function(req, res, next) {
    const { body: notification } = req;
    try {
      const createdNotificationId = await notificationsService.createNotification({ notification });

      res.status(201).json({
        data: createdNotificationId,
        message: 'notification created'
      });
    } catch (err) {
      next(err);
    }
  });

  router.put(
		'/:notificationId',
		passport.authenticate('jwt', { session: false }),
    scopesValidationHandler(['update:notifications']),
    validationHandler({ notificationId: notificationIdSchema }, 'params'),
    validationHandler(updateNotificationSchema),
    async function(req, res, next) {
      const { notificationId } = req.params;
      const { body: notification } = req;

      try {
        const updatedNotificationId = await notificationsService.updateNotification({
          notificationId,
          notification
        });

        res.status(200).json({
          data: updatedNotificationId,
          message: 'notification updated'
        });
      } catch (err) {
        next(err);
      }
    }
  );

  router.delete(
		'/:notificationId',
		passport.authenticate('jwt', { session: false }),
    scopesValidationHandler(['delete:notifications']),
    validationHandler({ notificationId: notificationIdSchema }, 'params'),
    async function(req, res, next) {
      const { notificationId } = req.params;

      try {
        const deletedNotificationId = await notificationsService.deleteNotification({ notificationId });

        res.status(200).json({
          data: deletedNotificationId,
          message: 'notification deleted'
        });
      } catch (err) {
        next(err);
      }
    }
  );
}

module.exports = notificationsApi;