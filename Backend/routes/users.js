const express = require('express');
const passport = require('passport');
const UsersService = require('../services/users');

const {
  userIdSchema,
  createUserSchema,
  updateUserSchema
} = require('../utils/schemas/users');

const validationHandler = require('../utils/middleware/validationHandler');
const scopesValidationHandler = require('../utils/middleware/scopesValidationHandler');

const cacheResponse = require('../utils/cacheResponse');
const {
  FIVE_MINUTES_IN_SECONDS,
  SIXTY_MINUTES_IN_SECONDS
} = require('../utils/time');

// JWT strategy
require('../utils/auth/strategies/jwt');

function usersApi(app) {
  const router = express.Router();
  app.use('/api/users', router);

  const usersService = new UsersService();

  router.get(
		'/',
		passport.authenticate('jwt', { session: false }),
		scopesValidationHandler(['read:users']),
		async function(req, res, next) {
    cacheResponse(res, FIVE_MINUTES_IN_SECONDS);
    const tags = req.query;
    const page = tags.page;
    const limit = tags.limit;
    try {
      const users = await usersService.getUsers( tags );
      const filterTotal = users[0].totalData[0] ? users[0].totalData[0].filterTotal : 0;

      res.status(200).json({
        page: parseInt(page),
        per_page: parseInt(limit),
        total: filterTotal,
        total_pages: Math.ceil(filterTotal / parseInt(limit)),
        data: users[0].data,
        message: 'users listed'
      });
    } catch (err) {
      next(err);
    }
  });

  router.get(
		'/info',
		passport.authenticate('jwt', { session: false }),
		scopesValidationHandler(['read:users']),
		async function(req, res, next) {
    cacheResponse(res, FIVE_MINUTES_IN_SECONDS);
    const tags = req.query;

    try {
      const user = await usersService.getUserRole( tags );

      res.status(200).json({
        data: user,
        message: 'user listed'
      });
    } catch (err) {
      next(err);
    }
  });

  router.get(
		'/:userId',
		passport.authenticate('jwt', { session: false }),
		scopesValidationHandler(['read:users']),
    validationHandler({ userId: userIdSchema }, 'params'),
    async function(req, res, next) {
      cacheResponse(res, SIXTY_MINUTES_IN_SECONDS);
      const { userId } = req.params;

      try {
        const users = await usersService.getSingleUser({ userId });

        res.status(200).json({
          data: users,
          message: 'user retrieved'
        });
      } catch (err) {
        next(err);
      }
    }
  );

  router.post(
		'/',
		passport.authenticate('jwt', { session: false }),
    scopesValidationHandler(['create:users']),
		validationHandler(createUserSchema),
		async function(req, res, next) {
    const { body: user } = req;
    try {
      const createdUserId = await usersService.createUser({ user });

      res.status(201).json({
        data: createdUserId,
        message: 'user created'
      });
    } catch (err) {
      next(err);
    }
  });

  router.post('/pagination', passport.authenticate('jwt', { session: false }), async function (req, res, next) {
    const query = req.body;
    const page = query.page;
    const limit = query.limit;

    try {
        const data = await usersService.getUsersPaginated(query);
        const filterTotal = data[0].totalData[0] ? data[0].totalData[0].filterTotal : 0;
        const total = data[0].totalCount[0] ? data[0].totalCount[0].count : 0;
        res.status(200).json({
            page: parseInt(page),
            per_page: parseInt(limit),
            total: filterTotal,
            total_pages: Math.ceil(filterTotal / parseInt(limit)),
            totalRegisters: total,
            data,
            message: 'tasks retrived'
        });

    } catch (err) {
        next(err);
    }
  }); 

  router.post('/bySomeGeneralCreateMany', 
  passport.authenticate('jwt', { session: false } ),
  /*scopesValidationHandler(['create:hitos']),*/
  //validationHandler(createsamiaSchema), 
  async function (req, res, next) {
          const query = req.body;
          //const collection = req.query;

      try {
          const data = await usersService.upsertUsers(query);

          res.status(200).json({
              data: data,
              message: 'data upserted'
          });

      } catch (err) {
          next(err);
      }
  }); 

  router.put(
		'/:userId',
		passport.authenticate('jwt', { session: false }),
    scopesValidationHandler(['update:users']),
    validationHandler({ userId: userIdSchema }, 'params'),
    validationHandler(updateUserSchema),
    async function(req, res, next) {
      const { userId } = req.params;
      const { body: user } = req;

      try {
        const updatedUserId = await usersService.updateUser({
          userId,
          user
        });

        res.status(200).json({
          data: updatedUserId,
          message: 'user updated'
        });
      } catch (err) {
        next(err);
      }
    }
  );

  router.delete(
		'/:userId',
		passport.authenticate('jwt', { session: false }),
    scopesValidationHandler(['delete:users']),
    validationHandler({ userId: userIdSchema }, 'params'),
    async function(req, res, next) {
      const { userId } = req.params;

      try {
        const deletedUserId = await usersService.deleteUser({ userId });

        res.status(200).json({
          data: deletedUserId,
          message: 'user deleted'
        });
      } catch (err) {
        next(err);
      }
    }
  );
}

module.exports = usersApi;