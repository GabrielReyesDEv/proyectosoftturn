const express = require('express');
const bodyParser = require('body-parser');
const multer = require('multer');
var aliOssStorage = require('multer-ali-oss');

require('dotenv').config();

require('../utils/auth/strategies/jwt')

function s3Api(app) {
    app.use(bodyParser.json());

    var uploadFile = multer( {
      storage: aliOssStorage({
        config: {
          region: process.env.ALI_OSS_REGION,
          accessKeyId: process.env.ALI_ACCESS_KEY_ID,
          accessKeySecret: process.env.ALI_ACCESS_KEY_SECRET,
          bucket: process.env.ALI_BUCKET
        },
        acl: 'public-read',
        filename: function (req, file, cb) {
          const name = file.originalname
          cb(null, `/zolid_new/cargue.${name.split('.').slice(-1)[0] }`);
        }
      })
    })

    const router = express.Router();
    app.use('/api/s3', router);
  
    router.post('/import', uploadFile.single('file'), function (req, res, next) {
      try {
        res.status(200).json({
          success : 1,
          file: {
            uploaded: 1,
            name: req.file.name,
            url: req.file.url
            // name: req.file.key,
            // url: req.file.location,
          }
        });
      } catch (err) {
          next(err);
      }
    });
  }
module.exports = s3Api;
