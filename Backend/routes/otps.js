const express = require('express');
const passport = require('passport');
const OtpsService = require('../services/otps');
const General = require('../services/general');
// const cacheResponse = require('../utils/cacheResponse');
// const { FIVE_MINUTES_IN_SECONDS, SIXTY_MINUTES_IN_SECONDS } = require('../utils/time');

// JWT Strategy
require('../utils/auth/strategies/jwt')

const {
    otpIdSchema,
    createOtpSchema,
    updateOtpSchema
} = require('../utils/schemas/otp');

const validationHandler = require('../utils/middleware/validationHandler');
const scopesValidationHandler = require('../utils/middleware/scopesValidationHandler');

function otpsApi(app) {
    const router = express.Router();
    app.use('/api/otps', router);
    const generalServices = new General('otps');
    const otpsService = new OtpsService();

    router.get('/', passport.authenticate('jwt', { session: false } ),
        scopesValidationHandler(['read:otps']), async function (req, res, next) {
        // cacheResponse(res, FIVE_MINUTES_IN_SECONDS);
        const tags = req.query;
        try {
            const otps = await otpsService.getOtps( tags );
            res.status(200).json({
                data: otps,
                message: 'otps listed'
            });

        } catch (err) {
            next(err);
        }
    });

    router.get('/bySome', passport.authenticate('jwt', { session: false }),
        /* scopesValidationHandler(['read:otps']), */ 
        /* validationHandler({ otpId: otpIdSchema }, 'params'), */ 
        async function (req, res, next) {
            // cacheResponse(res, SIXTY_MINUTES_IN_SECONDS);
            const tags = req.query;

            // eslint-disable-next-line no-prototype-builtins
            if(!tags.hasOwnProperty('by')) {
                tags.by='otpId';
                }
                
            try {
                const otp = await otpsService.getOtpGen( tags.id, tags.by );

                res.status(200).json({
                    data: otp,
                    message: 'otp retrived'
                });


            } catch (err) {
                next(err);
            }
        });

    router.get('/pagination', passport.authenticate('jwt', { session: false }), async function (req, res, next) {
        // cacheResponse(res, FIVE_MINUTES_IN_SECONDS);
        const tags = req.query;
        const page = tags.page;
        const limit = tags.limit;
        try {
            const otps = await otpsService.getOtpsPaginated( tags );
            const filterTotal = otps[0].totalData[0] ? otps[0].totalData[0].filterTotal : 0;
            res.status(200).json({
                page: parseInt(page),
                per_page: parseInt(limit),
                total: filterTotal,
                total_pages: Math.ceil(filterTotal / parseInt(limit)),
                data: otps[0].data,
                totalRegisters: otps[0].totalCount[0].count,
                message: 'otps listed'
            });

        } catch (err) {
            next(err);
        }
    });

    router.post('/pagination', passport.authenticate('jwt', { session: false }), async function (req, res, next) {
        const query = req.body;
        const page = query.page;
        const limit = query.limit;
        try {
            const data = await generalServices.multipurpose(query);
            const filterTotal = data[0].totalData[0] ? data[0].totalData[0].filterTotal : 0;
            const total = data[0].totalCount[0] ? data[0].totalCount[0].count : 0;
            res.status(200).json({
                page: parseInt(page),
                per_page: parseInt(limit),
                total: filterTotal,
                total_pages: Math.ceil(filterTotal / parseInt(limit)),
                totalRegisters: total,
                data,
                message: 'data retrived'
            });

        } catch (err) {
            next(err);
        }
    });

    router.get('/otpsGroup',  async function (req, res, next) {
        // cacheResponse(res, SIXTY_MINUTES_IN_SECONDS);
        const tags = req.query;
        const page = tags.page;
        const limit = tags.limit;
        try {
            const otps = await otpsService.getOtpGroup(tags);

            res.status(200).json({
                page: parseInt(page),
                per_page: parseInt(limit),
                data: otps,
                message: 'otps retrieved'
            });

        } catch (err) {
            next(err);
        }
    });

    router.get('/:otpId', 
        passport.authenticate('jwt', { session: false } ), 
        scopesValidationHandler(['read:otps']),
        validationHandler({ otpId: otpIdSchema }, 'params'), async function (req, res, next) {
        // cacheResponse(res, SIXTY_MINUTES_IN_SECONDS);
        const { otpId } = req.params;

        try {
            const otp = await otpsService.getOtp({ otpId });

            res.status(200).json({
                data: otp,
                message: 'otp retrieved'
            });

        } catch (err) {
            next(err);
        }
    });

    router.post('/', 
        passport.authenticate('jwt', { session: false } ),
        scopesValidationHandler(['create:otps']),
        validationHandler(createOtpSchema), async function (req, res, next) {
        const { body: otp } = req;
        try {
            const createdOtpId = await otpsService.createOtp({ otp });

            res.status(201).json({
                data: { createdOtpId, otpId: otp.otpId},
                message: 'otp created'
            });

        } catch (err) {
            next(err);
        }
    });

    router.put('/updateGroup', 
        passport.authenticate('jwt', { session: false } ),
        async function (req, res, next) {
            const { otps, data } = req.body;
            try {
                const updatedOtpId = await otpsService.updateGroup( otps, data);
                res.status(200).json({
                    data: updatedOtpId,
                    message: 'otp update'
                });  
            } catch (err) {
                next(err);
            }
        });

    router.put('/:otpId', 
        passport.authenticate('jwt', { session: false } ),
        scopesValidationHandler(['update:otps']),
        validationHandler({ otpId: otpIdSchema }, 'params'), validationHandler(updateOtpSchema), async function (req, res, next) {
        const { otpId } = req.params;
        const { body: otp } = req;

        try {
            const updatedOtpId = await otpsService.updateOtp({ otpId, otp });

            res.status(200).json({
                data: updatedOtpId,
                message: 'otp update'
            });

        } catch (err) {
            next(err);
        }
    });

    router.delete('/:otpId', 
        passport.authenticate('jwt', { session: false } ),
        scopesValidationHandler(['delete:otps']),
        validationHandler({ otpId: otpIdSchema }, 'params'), async function (req, res, next) {
        const { otpId } = req.params;

        try {
            const deletedOtpId = await otpsService.deleteOtp({ otpId });
            res.status(200).json({
                data: deletedOtpId,
                message: 'otp deleted'
            });

        } catch (err) {
            next(err);
        }
    });
}

module.exports = otpsApi;