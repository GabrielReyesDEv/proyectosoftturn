const express = require('express');
const passport = require('passport');
const { Queue } = require('node-resque');
require('dotenv').config();

// JWT Strategy
require('../utils/auth/strategies/jwt')

function functionalitiesApi(app) {
  const router = express.Router();
  app.use('/api', router);

  router.post('/loadData/functionality', async function (req, res, next) {
    const connectionDetails = {
      pkg: 'ioredis',
      host: process.env.REDIS_HOST,
      // password: 'a4b3c2d1+17',
      //password: null,
      port: process.env.REDIS_PORT,
      database: 0,
      // options: {password: 'a4b3c2d1+17'},
    };
    try {
      const queue = new Queue({ connection: connectionDetails });
      queue.on('error', function(error) {
        console.log(error);  //eslint-disable-line
      });

      await queue.connect();
      await queue.enqueue('default', 'saveImportDataToLocal');

      res.status(201).json({
          message: 'Data upserted!'
      });

    } catch (err) {
        next(err);
    }
    
  });
}


module.exports = functionalitiesApi;