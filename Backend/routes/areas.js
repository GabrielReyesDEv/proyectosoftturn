const express = require('express');
const passport = require('passport');
const AreasService = require('../services/areas');

const {
  areaIdSchema,
  createAreaSchema,
  updateAreaSchema
} = require('../utils/schemas/areas');

const validationHandler = require('../utils/middleware/validationHandler');
const scopesValidationHandler = require('../utils/middleware/scopesValidationHandler');

const cacheResponse = require('../utils/cacheResponse');
const {
  FIVE_MINUTES_IN_SECONDS,
  SIXTY_MINUTES_IN_SECONDS
} = require('../utils/time');

// JWT strategy
require('../utils/auth/strategies/jwt');

function areasApi(app) {
  const router = express.Router();
  app.use('/api/areas', router);

  const areasService = new AreasService();

  router.get(
		'/',
		passport.authenticate('jwt', { session: false }),
		scopesValidationHandler(['read:areas']),
		async function(req, res, next) {
    cacheResponse(res, FIVE_MINUTES_IN_SECONDS);
    const tags = req.query;
    const page = tags.page;
    const limit = tags.limit;

    try {
      const areas = await areasService.getAreas( tags );
      const filterTotal = areas[0].totalData[0] ? areas[0].totalData[0].filterTotal : 0;

      res.status(200).json({
        page: parseInt(page),
        per_page: parseInt(limit),
        total: filterTotal,
        total_pages: Math.ceil(filterTotal / parseInt(limit)),
        data: areas[0].data,
        message: 'areas listed'
      });
    } catch (err) {
      next(err);
    }
  });

  router.get(
		'/:areaId',
		passport.authenticate('jwt', { session: false }),
		scopesValidationHandler(['read:areas']),
    validationHandler({ areaId: areaIdSchema }, 'params'),
    async function(req, res, next) {
      cacheResponse(res, SIXTY_MINUTES_IN_SECONDS);
      const { areaId } = req.params;

      try {
        const areas = await areasService.getArea({ areaId });

        res.status(200).json({
          data: areas,
          message: 'area retrieved'
        });
      } catch (err) {
        next(err);
      }
    }
  );

  router.post('/pagination', passport.authenticate('jwt', { session: false }), async function (req, res, next) {
    const query = req.body;
    const page = query.page;
    const limit = query.limit;
    try {
        const data = await areasService.getAreasPaginated(query);
        const filterTotal = data[0].totalData[0] ? data[0].totalData[0].filterTotal : 0;
        res.status(200).json({
            page: parseInt(page),
            per_page: parseInt(limit),
            total: filterTotal,
            total_pages: Math.ceil(filterTotal / parseInt(limit)),
            data: data[0].data,
            message: 'areas retrived'
        });

    } catch (err) {
        next(err);
    }
  }); 

  router.post(
		'/',
		passport.authenticate('jwt', { session: false }),
    scopesValidationHandler(['create:areas']),
		validationHandler(createAreaSchema),
		async function(req, res, next) {
    const { body: area } = req;
    try {
      const createdAreaId = await areasService.createArea({ area });

      res.status(201).json({
        data: createdAreaId,
        message: 'area created'
      });
    } catch (err) {
      next(err);
    }
  });

  router.put(
		'/:areaId',
		passport.authenticate('jwt', { session: false }),
    scopesValidationHandler(['update:areas']),
    validationHandler({ areaId: areaIdSchema }, 'params'),
    validationHandler(updateAreaSchema),
    async function(req, res, next) {
      const { areaId } = req.params;
      const { body: area } = req;

      try {
        const updatedAreaId = await areasService.updateArea({
          areaId,
          area
        });

        res.status(200).json({
          data: updatedAreaId,
          message: 'area updated'
        });
      } catch (err) {
        next(err);
      }
    }
  );

  router.delete(
		'/:areaId',
		passport.authenticate('jwt', { session: false }),
    scopesValidationHandler(['delete:areas']),
    validationHandler({ areaId: areaIdSchema }, 'params'),
    async function(req, res, next) {
      const { areaId } = req.params;

      try {
        const deletedAreaId = await areasService.deleteArea({ areaId });

        res.status(200).json({
          data: deletedAreaId,
          message: 'area deleted'
        });
      } catch (err) {
        next(err);
      }
    }
  );
}

module.exports = areasApi;