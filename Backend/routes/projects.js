const express = require('express');
const passport = require('passport');
const ProjectsService = require('../services/projects');
const NotificationsService = require('../services/notifications');

const {
  projectIdSchema,
  createProjectSchema,
  updateProjectSchema
} = require('../utils/schemas/projects');

const validationHandler = require('../utils/middleware/validationHandler');
const scopesValidationHandler = require('../utils/middleware/scopesValidationHandler');

const cacheResponse = require('../utils/cacheResponse');
const {
  FIVE_MINUTES_IN_SECONDS,
  SIXTY_MINUTES_IN_SECONDS
} = require('../utils/time');

// JWT strategy
require('../utils/auth/strategies/jwt');

function projectsApi(app) {
  const router = express.Router();
  app.use('/api/projects', router);

  const projectsService = new ProjectsService();
  const notificationsService = new NotificationsService();

  router.get(
		'/',
		passport.authenticate('jwt', { session: false }),
		scopesValidationHandler(['read:projects']),
		async function(req, res, next) {
    cacheResponse(res, FIVE_MINUTES_IN_SECONDS);
    const tags = req.query;
    const page = tags.page;
    const limit = tags.limit;

    try {
      const projects = await projectsService.getProjects( tags );
      const filterTotal = projects[0].totalData[0] ? projects[0].totalData[0].filterTotal : 0;

      res.status(200).json({
        page: parseInt(page),
        per_page: parseInt(limit),
        total: filterTotal,
        total_pages: Math.ceil(filterTotal / parseInt(limit)),
        data: projects[0].data,
        message: 'projects listed'
      });
    } catch (err) {
      next(err);
    }
  });

  router.get(
		'/:projectId',
		passport.authenticate('jwt', { session: false }),
		scopesValidationHandler(['read:projects']),
    validationHandler({ projectId: projectIdSchema }, 'params'),
    async function(req, res, next) {
      cacheResponse(res, SIXTY_MINUTES_IN_SECONDS);
      const { projectId } = req.params;

      try {
        const projects = await projectsService.getProject({ projectId });

        res.status(200).json({
          data: projects,
          message: 'project retrieved'
        });
      } catch (err) {
        next(err);
      }
    }
  );

  router.post(
		'/',
		passport.authenticate('jwt', { session: false }),
    scopesValidationHandler(['create:projects']),
		validationHandler(createProjectSchema),
		async function(req, res, next) {
    const { body: project } = req;
    const authNotify = project.authorNotify;
    const room = {
      room: project.author.split('#')[1],
      message: `Proyecto con Id: ${project.projectId} asignado.`,
    }
    const notification = {
      title: `Proyecto con Id: ${project.projectId} asignado.`,
      type: 'projects',
      typeId: project.projectId,
      projectId: project.projectId,
      owner: project.author.split('#')[0],
      ownerId: project.author.split('#')[1],
      activityId: false,
      view: false,
      created_at: project.createdAt,
      updated_at: project.updatedAt,
      author: authNotify,
    }

    try {
      delete project.authorNotify;
      const createdProjectId = await projectsService.createProject({ project });
      await notificationsService.createNotification({ notification, room });
      res.status(201).json({
        data: createdProjectId,
        message: 'project created'
      });
    } catch (err) {
      next(err);
    }
  });

  router.post('/pagination', passport.authenticate('jwt', { session: false }), async function (req, res, next) {
    const query = req.body;
    const page = query.page;
    const limit = query.limit;
    try {
        const data = await projectsService.getProjectsPaginated(query);
        const filterTotal = data[0].totalData[0] ? data[0].totalData[0].filterTotal : 0;
        res.status(200).json({
            page: parseInt(page),
            per_page: parseInt(limit),
            total: filterTotal,
            total_pages: Math.ceil(filterTotal / parseInt(limit)),
            data: data[0].data,
            message: 'projects retrived'
        });

    } catch (err) {
        next(err);
    }
  }); 

  router.put(
		'/:projectId',
		passport.authenticate('jwt', { session: false }),
    scopesValidationHandler(['update:projects']),
    validationHandler({ projectId: projectIdSchema }, 'params'),
    // validationHandler(updateProjectSchema),
    async function(req, res, next) {
      const { projectId } = req.params;
      const { body: project } = req;

      try {
        const updatedProjectId = await projectsService.updateProject({
          projectId,
          project
        });

        res.status(200).json({
          data: updatedProjectId,
          message: 'project updated'
        });
      } catch (err) {
        next(err);
      }
    }
  );

  router.delete(
		'/:projectId',
		passport.authenticate('jwt', { session: false }),
    scopesValidationHandler(['delete:projects']),
    validationHandler({ projectId: projectIdSchema }, 'params'),
    async function(req, res, next) {
      const { projectId } = req.params;

      try {
        const deletedProjectId = await projectsService.deleteProject({ projectId });

        res.status(200).json({
          data: deletedProjectId,
          message: 'project deleted'
        });
      } catch (err) {
        next(err);
      }
    }
  );
}

module.exports = projectsApi;