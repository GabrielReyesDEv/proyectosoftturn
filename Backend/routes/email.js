const express = require('express');
// const CierreKOService = require('../services/cierreko');
const TasksService = require('../services/tasks');
const UsersService = require('../services/users');

// JWT Strategy
require('../utils/auth/strategies/jwt')
var EmailCtrl = require('../utils/middleware/emailHandler');
var Email = require('../utils/middleware/emailHitosHandler');

function emailApi(app) {
    const router = express.Router();
    app.use('/api/email', router);

    const tasksService = new TasksService();
    const usersService = new UsersService();

    router.post('/',  async function (req, res, next) {
        const query = req.body;
        const email = query.email
        delete query.email

        try {
            const emailInfo = await tasksService.getTasksPaginated( query );
            if (emailInfo[0].data[0].data.serviceType) EmailCtrl.sendEmail(res, req, emailInfo[0].data[0].data, email);
            
            res.status(201).json({
                data: emailInfo,
                message: 'email sent'
            });

        } catch (err) {
            next(err);
        }
        
    });

    router.post('/hitos',  async function (req, res, next) {
        const { body: info } = req;  //info = { correo: {nombre: 'Leonel', cliente: 'claro', entregaServicio: '', email: 'leoneloliveros.co@gmail.com'}, otps: [{},{},{}] }

        try {

            const emailInfo = Email.sendEmail(res, req, info.otps, info.correo);
            res.status(201).json({
                data: emailInfo,
                message: 'Email Reporte Actualizacion sent'
            });

        } catch (err) {
            next(err);
        }
        
    });

    router.post('/getByRole',  async function (req, res, next) {
        const { query, filters } = req.body;
        try {
            const data = await usersService.findUsers(query || {}, filters || {});
            res.status(200).json({
                data,
                message: 'users retrived'
            });
        } catch (err) {
            next(err);
        }
        
    });


}

module.exports = emailApi;