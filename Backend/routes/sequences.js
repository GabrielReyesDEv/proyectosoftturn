const express = require('express');
const passport = require('passport');
const SequencesService = require('../services/sequences');
const cacheResponse = require('../utils/cacheResponse');
const { FIVE_MINUTES_IN_SECONDS, SIXTY_MINUTES_IN_SECONDS } = require('../utils/time');

// JWT Strategy
require('../utils/auth/strategies/jwt')

//const validationHandler = require('../utils/middleware/validationHandler');
const scopesValidationHandler = require('../utils/middleware/scopesValidationHandler');

function sequencesApi(app) {
  const router = express.Router();
  app.use('/api/sequences', router);

  const sequencesService = new SequencesService();

  router.get(
    '/',
    passport.authenticate('jwt', { session: false }),
    scopesValidationHandler(['read:sequences']),
    async function (req, res, next) {
        cacheResponse(res, FIVE_MINUTES_IN_SECONDS);
        const { tags } = req.query;
        try {
            const sequence = await sequencesService.getSequence({ tags });
            res.status(200).json({
                data: sequence,
                message: 'sequence listed'
            });

        } catch (err) {
            next(err);
        }
    }
  );
}

module.exports = sequencesApi;