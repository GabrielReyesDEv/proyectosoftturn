const express = require('express');
const passport = require('passport');
const ActivitiesService = require('../services/activities');
const UsersService = require('../services/users');
const AreasService = require('../services/areas');
const NotificationsService = require('../services/notifications');

const {
  activityIdSchema,
  createActivitySchema,
  updateActivitySchema
} = require('../utils/schemas/activities');

const validationHandler = require('../utils/middleware/validationHandler');
const scopesValidationHandler = require('../utils/middleware/scopesValidationHandler');

const cacheResponse = require('../utils/cacheResponse');
const {
  FIVE_MINUTES_IN_SECONDS,
  SIXTY_MINUTES_IN_SECONDS
} = require('../utils/time');

// JWT strategy
require('../utils/auth/strategies/jwt');

function activitiesApi(app) {
  const router = express.Router();
  app.use('/api/activities', router);

  const activitiesService = new ActivitiesService();
  const areasService = new AreasService();
  const usersService = new UsersService();
  const notificationsService = new NotificationsService();

  router.get(
		'/',
		passport.authenticate('jwt', { session: false }),
		scopesValidationHandler(['read:activities']),
		async function(req, res, next) {
    cacheResponse(res, FIVE_MINUTES_IN_SECONDS);
    const tags = req.query;
    const page = tags.page;
    const limit = tags.limit;

    try {
      const activities = await activitiesService.getActivities( tags );
      const filterTotal = activities[0].totalData[0] ? activities[0].totalData[0].filterTotal : 0;

      res.status(200).json({
        page: parseInt(page),
        per_page: parseInt(limit),
        total: filterTotal,
        total_pages: Math.ceil(filterTotal / parseInt(limit)),
        data: activities[0].data,
        message: 'activities listed'
      });
    } catch (err) {
      next(err);
    }
  });

  router.get(
		'/:activityId',
		passport.authenticate('jwt', { session: false }),
		scopesValidationHandler(['read:activities']),
    validationHandler({ activityId: activityIdSchema }, 'params'),
    async function(req, res, next) {
      cacheResponse(res, SIXTY_MINUTES_IN_SECONDS);
      const { activityId } = req.params;

      try {
        const activities = await activitiesService.getActivity({ activityId });

        res.status(200).json({
          data: activities,
          message: 'activity retrieved'
        });
      } catch (err) {
        next(err);
      }
    }
  );

  router.post('/importUpdt',
    /*scopesValidationHandler(['create:oths']),*/
    /* validationHandler(createOthSchema), */ async function (req, res, next) {
        const { body: ots } = req;
        try {
            const users = await await usersService.getUsersPaginated({ eq: [{ field: 'roles.group', values: ["BO"]}]});
            const areas = await areasService.getAreasPaginated({ eq: [{ field: 'group', values: ["BO"]}]});
            const createOthId = await activitiesService.upsertOts( ots, users[0].data, areas[0].data );
            res.status(201).json({
                data: createOthId,
                message: 'ots created'
            });

        } catch (err) {
            next(err);
        }
    });

  router.post('/pagination', passport.authenticate('jwt', { session: false }), async function (req, res, next) {
    const query = req.body;
    const page = query.page;
    const limit = query.limit;
    try {
        const data = await activitiesService.getActivititiesPaginated(query);
        const filterTotal = data[0].totalData[0] ? data[0].totalData[0].filterTotal : 0;
        res.status(200).json({
            page: parseInt(page),
            per_page: parseInt(limit),
            total: filterTotal,
            total_pages: Math.ceil(filterTotal / parseInt(limit)),
            data: data[0].data,
            message: 'activities retrived'
        });

    } catch (err) {
        next(err);
    }
  }); 

  router.post(
		'/',
		passport.authenticate('jwt', { session: false }),
    scopesValidationHandler(['create:activities']),
		validationHandler(createActivitySchema),
		async function(req, res, next) {
    const { body: activity } = req;
    const authNotify = activity.authorNotify;
    const room = {
      room: activity.author.split('#')[1],
      message: `Actividad con Id: ${activity.activityId} asignada.`,
    }
    const notification = {
      title: `Actividad con Id: ${activity.activityId} asignada.`,
      type: 'activities',
      typeId: activity.activityId,
      projectId: activity.projectId,
      owner: activity.author.split('#')[0],
      ownerId: activity.author.split('#')[1],
      activityId: activity.activityId,
      view: false,
      created_at: activity.createdAt,
      updated_at: activity.updatedAt,
      author: authNotify,
    }
    try {
      delete activity.authorNotify;
      const createdActivityId = await activitiesService.createActivity({ activity });
      await notificationsService.createNotification({ notification, room });

      res.status(201).json({
        data: createdActivityId,
        message: 'activity created'
      });
    } catch (err) {
      next(err);
    }
  });

  router.put(
		'/:activityId',
		passport.authenticate('jwt', { session: false }),
    scopesValidationHandler(['update:activities']),
    validationHandler({ activityId: activityIdSchema }, 'params'),
    // validationHandler(updateActivitySchema),
    async function(req, res, next) {
      const { activityId } = req.params;
      const { body: activity } = req;

      try {
        const updatedActivityId = await activitiesService.updateActivity({
          activityId,
          activity
        });

        res.status(200).json({
          data: updatedActivityId,
          message: 'activity updated'
        });
      } catch (err) {
        next(err);
      }
    }
  );

  router.delete(
		'/:activityId',
		passport.authenticate('jwt', { session: false }),
    scopesValidationHandler(['delete:activities']),
    validationHandler({ activityId: activityIdSchema }, 'params'),
    async function(req, res, next) {
      const { activityId } = req.params;

      try {
        const deletedActivityId = await activitiesService.deleteActivity({ activityId });

        res.status(200).json({
          data: deletedActivityId,
          message: 'activity deleted'
        });
      } catch (err) {
        next(err);
      }
    }
  );
}

module.exports = activitiesApi;