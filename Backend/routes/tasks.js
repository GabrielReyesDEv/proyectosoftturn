const express = require('express');
const passport = require('passport');
const TasksService = require('../services/tasks');
const NotificationsService = require('../services/notifications');

const {
  taskIdSchema,
  createTaskSchema,
  updateTaskSchema
} = require('../utils/schemas/tasks');

const validationHandler = require('../utils/middleware/validationHandler');
const scopesValidationHandler = require('../utils/middleware/scopesValidationHandler');

const cacheResponse = require('../utils/cacheResponse');
const {
  FIVE_MINUTES_IN_SECONDS,
  SIXTY_MINUTES_IN_SECONDS
} = require('../utils/time');

// JWT strategy
require('../utils/auth/strategies/jwt');

function tasksApi(app) {
  const router = express.Router();
  app.use('/api/tasks', router);

  const tasksService = new TasksService();
  const notificationsService = new NotificationsService();

  router.get(
		'/',
		passport.authenticate('jwt', { session: false }),
		scopesValidationHandler(['read:tasks']),
		async function(req, res, next) {
    cacheResponse(res, FIVE_MINUTES_IN_SECONDS);
    const tags = req.query;
    const page = tags.page;
    const limit = tags.limit;

    try {
      const tasks = await tasksService.getTasks( tags );
      const filterTotal = tasks[0].totalData[0] ? tasks[0].totalData[0].filterTotal : 0;

      res.status(200).json({
        page: parseInt(page),
        per_page: parseInt(limit),
        total: filterTotal,
        total_pages: Math.ceil(filterTotal / parseInt(limit)),
        data: tasks[0].data,
        message: 'tasks listed'
      });
    } catch (err) {
      next(err);
    }
  });

  router.get(
		'/:taskId',
		passport.authenticate('jwt', { session: false }),
		scopesValidationHandler(['read:tasks']),
    validationHandler({ taskId: taskIdSchema }, 'params'),
    async function(req, res, next) {
      cacheResponse(res, SIXTY_MINUTES_IN_SECONDS);
      const { taskId } = req.params;

      try {
        const tasks = await tasksService.getTask({ taskId });

        res.status(200).json({
          data: tasks,
          message: 'task retrieved'
        });
      } catch (err) {
        next(err);
      }
    }
  );

  router.post(
		'/',
		passport.authenticate('jwt', { session: false }),
    scopesValidationHandler(['create:tasks']),
		validationHandler(createTaskSchema),
		async function(req, res, next) {
    const { body: task } = req;
    try {
      const createdTaskId = await tasksService.createTask({ task });

      res.status(201).json({
        data: createdTaskId,
        message: 'task created'
      });
    } catch (err) {
      next(err);
    }
  });

  router.post('/pagination',
  passport.authenticate('jwt', { session: false }),
  async function (req, res, next) {
    const query = req.body;
    const page = query.page;
    const limit = query.limit;
    try {
        const data = await tasksService.getTasksPaginated(query);
        const filterTotal = data[0].totalData[0] ? data[0].totalData[0].filterTotal : 0;
        res.status(200).json({
            page: parseInt(page),
            per_page: parseInt(limit),
            total: filterTotal,
            total_pages: Math.ceil(filterTotal / parseInt(limit)),
            data: data[0].data,
            message: 'tasks retrived'
        });

    } catch (err) {
        next(err);
    }
  }); 

  router.post('/extractArrays', 
        passport.authenticate('jwt', { session: false } ),
        async function (req, res, next) {
        const { body: event } = req;
        const page = event.page;
        const limit = event.limit;
        try {
            const createdactivoId = await tasksService.getArrayObject(event);
            const data = createdactivoId.length > 0 && createdactivoId[0].data ? createdactivoId[0].data : [];
            const filterTotal = createdactivoId.length > 0 && createdactivoId[0].total ? createdactivoId[0].total.count : 0;
            const total = createdactivoId.length > 0 && createdactivoId[0].data ? createdactivoId[0].data.length : 0;
            res.status(201).json({
                page: parseInt(page),
                per_page: parseInt(limit),
                total: filterTotal,
                total_pages: Math.ceil(filterTotal / parseInt(limit)),
                totalRegisters: total,
                data: data,
                message: 'registers listed'
            });

        } catch (err) {
            next(err);
        }
    });


  router.put(
		'/:taskId',
		passport.authenticate('jwt', { session: false }),
    scopesValidationHandler(['update:tasks']),
    validationHandler({ taskId: taskIdSchema }, 'params'),
    validationHandler(updateTaskSchema),
    async function(req, res, next) {
      const { taskId } = req.params;
      const { body: task } = req;
      let room = {};
      let notification = {};
      if (task.updateNotify) {
        const authNotify = task.updateNotify;
        room = {
          room: task.author.split('#')[1],
          message: `Tarea con Id Proyecto:${task.projectId} y Id Actividad:${task.activityId} asignada.`,
        }
        notification = {
          title: `Tarea con Id Proyecto:${task.projectId} y Id Actividad:${task.activityId} asignada.`,
          type: 'tasks',
          typeId: taskId,
          projectId: task.projectId,
          owner: task.author.split('#')[0],
          ownerId: task.author.split('#')[1],
          activityId: task.activityId,
          view: false,
          created_at: task.createdAt,
          updated_at: task.updatedAt,
          author: authNotify,
        }
      }
      try {
        if (task.updateNotify) delete task.updateNotify;
        const updatedTaskId = await tasksService.updateTask({
          taskId,
          task
        });
        if (task.updateNotify) await notificationsService.createNotification({ notification, room });

        res.status(200).json({
          data: updatedTaskId,
          message: 'task updated'
        });
      } catch (err) {
        next(err);
      }
    }
  );

  router.delete(
		'/:taskId',
		passport.authenticate('jwt', { session: false }),
    scopesValidationHandler(['delete:tasks']),
    validationHandler({ taskId: taskIdSchema }, 'params'),
    async function(req, res, next) {
      const { taskId } = req.params;
        console.log('taskId: ',  taskId);
      try {
        const deletedTaskId = await tasksService.deleteTask({ taskId });

        res.status(200).json({
          data: deletedTaskId,
          message: 'task deleted'
        });
      } catch (err) {
        next(err);
      }
    }
  );
}

module.exports = tasksApi;