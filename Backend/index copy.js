/* eslint-disable no-useless-escape */
/* eslint-disable no-console */
/* eslint-disable require-atomic-updates */
const express = require('express');
const app = express();
const cors = require("cors");
var SSHClient = require('ssh2').Client;

const { config } = require('./config/index');
const otpsApi =  require('./routes/otps.js');
const logsApi =  require('./routes/logs.js');
const othsApi = require('./routes/oths.js');
const usersApi = require('./routes/users.js');
const authApi = require('./routes/auth');
const clientsApi = require('./routes/clients');
const projectsApi = require('./routes/projects');
const bitacorasApi = require('./routes/bitacoras');
const emailApi = require('./routes/email');
const vpnApi = require('./routes/vpn');
const cierreko = require('./routes/cierreko');
const activityApi = require('./routes/activities');
const checklistconf = require('./routes/checklistconf');
const hitosApi = require('./routes/hito');
const departamentosApi = require('./routes/departamentos');
const samiaApi = require('./routes/samia');
const rangeipApi = require('./routes/rangeip');
const secuenceApi = require('./routes/secuence');
const inventoryApi = require('./routes/inventory');
const migrationApi = require('./routes/migration');
const checklistformfApi = require('./routes/checklistformat');
const s3Api = require('./routes/s3');
const functionalitiesApi =  require('./routes/functionalities');
const pymes = require('./routes/pymes');
const switches = require('./routes/switches');
const procedureLogsApi = require('./routes/procedureLogs.js');
const zValidador = require('./routes/zValidador');
const lineaBaseApi = require('./routes/lineaBase');
const markings = require('./routes/markings');
const calendar = require('./routes/calendar');
const tasks = require('./routes/tasks');
const generalCol = require('./routes/generalCol');
const dashboardApi = require('./routes/dashboard.js');
const { logErrors, wrapErrors,  errorHandler } = require('./utils/middleware/errorHandlers');
//const moment = require('moment');
const momentTimezone = require('moment-timezone');

const notFoundHandler = require('./utils/middleware/notFoundHandler');

const bodyParser = require('body-parser');

app.use(bodyParser.json({limit: "180mb" }));
app.use(bodyParser.urlencoded({limit: "180mb" , extended: true, parameterLimit:90000}));
const axios = require('axios');
// body parser
app.use(express.json());

// Socket.io 
const http = require('http').Server(app); 
const io = require('socket.io')(http);
const ioVPN = require("socket.io-client");
io.on('connection', (socket) => {

  const ioClientVPN = ioVPN.connect("http://172.21.61.22:8000"); // va a ser el de la vpn
  socket.on('request:connection', (data, pymes) => {
    ioClientVPN.emit('request:connection', data, pymes);
  })
  socket.on('request:marking', (data, pymes) => {
    ioClientVPN.emit('request:marking', data, pymes);
  }); 

  ioClientVPN.on('request:message', (data) => {
    socket.emit('request:message', data);
  });

  ioClientVPN.on('request:error', (data) => {
    socket.emit('request:error', data);
  });

  ioClientVPN.on('request:success', (data) => {
    socket.emit('request:success', data);
  });
  
  ioClientVPN.on('request:reset', (data) => {
    socket.emit('request:reset', data);
  }); 

    var conn = new SSHClient();
    var one_connection = true;
    let unavailable_ip = false;
    let selected_network = 'test';
    let commands;
    let step_command = 0;
    let ipInformation;
    let current_command;
    let clean_command;
    let send_command = false;
    let changeNetwork = false;
    let message = '';
    let termino = false;
    let introduction = false;
    let cambio = true;
    let userHendryx = '';
    let passwordHendryx = '';
    let userTacacs = '';
    let passwordTacacs = '';
    let vrfPymes = '';
    let pymesVar = false;
    let i = 1;
    const bloque = [];
    socket.on('get_data', async function(data, pymes) {
      pymesVar = pymes;
      const information = await JSON.parse(data);
      // console.log('information: ', information);
      vrfPymes = information.vrf
      ipInformation = information.ip
      commands = await automatic_commands(ipInformation);
      if (pymes) {
        selected_network = information.pe
      } else {
        selected_network = commands[0].network[1] || commands[0].network[0];
      }
      userHendryx = information.userHendryx;
      passwordHendryx = information.passwordHendryx;
      userTacacs = information.userTacacs;
      passwordTacacs = information.passwordTacacs;
      i = 1;
      console.log('information: ', information,);

      conn.connect({
        host: '200.14.205.9',
        username: userHendryx,
        password: passwordHendryx
      })
    })

    conn.on('ready', function() {

      socket.emit('data', '\r\n** SSH CONNECTION ESTABLISHED **\r\n')
       
      // if (user !== '' && password !== '') {
        conn.shell(function(err, stream) {
          // if (selected_network)
          //   stream.write(`ssh ${selected_network}\n`);

          if (err)
            return socket.emit('data', '\r\n** SSH SHELL ERROR: ' + err.message + ' **\r\n');

          // AQUI NO LLEGA (POR EL MOMENTO)
          socket.on('data', function(data) {
            stream.write(data)
          });

          stream.on('data', async function(d) {
            
            message += d.toString('binary');
            //console.log('#####', message, send_command)
            if (d.toString('binary').includes('hendrix') && selected_network && step_command <= commands.length && cambio ) {
              stream.write(`ssh ${selected_network} -l ${userTacacs}\n`);
              //console.log('entro AQUI2')

              if (clean_command && clean_command.length > 2) {
                    changeNetwork = true
                  }
              one_connection = true;
              cambio = false;
            }

            if (d.toString('binary').includes('password:') && one_connection) {
              stream.write(`${passwordTacacs}\n`);
              one_connection = false
              introduction = true;
              message = ''
            }

            if (d.toString('binary').includes('--More--')) {
              stream.write('\ ');
            }

            if (d.toString('binary').includes('yes/no')) {
              stream.write('yes\n');
            } 


            if (d.toString('binary').includes(`${selected_network}#`) && !unavailable_ip && step_command <= commands.length && !send_command && !termino) {
              if (introduction) {
                message = ''
                introduction = false
                

              }
              //console.log('MESSAGE', message)
              let clean_data = message.replace(`${selected_network}#`,'');
              clean_data = clean_data.replace( /[A-Z]{2}\/\d+\/[A-Z]+\d+\/[A-Z]+\d+:/,'');
              message = clean_data;
              if((message.length > 2 && selected_network.length + 1 < message.length && !(message.includes('Last login')) && !(message.includes('Last switch-over')))) {
                console.log('MESSAGE BEGIN', message, 'MESSAGE END')
                const resp = await expect_result(current_command.not_expected, message)
                bloque.push({
                  step: `${i} de ${commands.length}`,
                  begin: message,
                  command: clean_command,
                });
                message = ''
                if (resp) {
                  if (i === commands.length) {
                    
                    await axios.put(`http://localhost:3000/api/procedureLogs/commands/${ipInformation.ip}`,
                    JSON.stringify({data: {
                      blockCommands: bloque,
                      status: resp ? 'Disponible' : 'No Disponible',
                      createdAt: (momentTimezone().tz('America/Bogota').format("YYYY-MM-DD HH:mm")).toLocaleString(),
                    }}), {
                      headers: {
                          'Content-Type': 'application/json',
                      }
                    }
                    );
                  }
                  socket.emit("disponible", { current: i++, total: commands.length });
                  console.log('disponible', clean_command)
                  unavailable_ip = false;
                  stream.write('\ ');
                } else if (!resp) {
                  unavailable_ip = true;
                  socket.emit('ssh', 'Direccion Ip no disponible');
                  console.log('No disponible')
                  stream.write('\ ');

                  await axios.put(`http://localhost:3000/api/procedureLogs/commands/${ipInformation.ip}`,
                  JSON.stringify({data: {
                    blockCommands: bloque,
                    status: resp ? 'Disponible' : 'No Disponible',
                    createdAt: (momentTimezone().tz('America/Bogota').format("YYYY-MM-DD HH:mm")).toLocaleString(),
                  }}), {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                  }
                  );
                }
                if (step_command == commands.length) {
                  termino = true;
                }
              }

              if (step_command > commands.length) {
                send_command = false;
                termino = true;
              } else {
                send_command = true;
              }
              if (changeNetwork) {
                //console.log('entro AQUI', clean_command, message)
                send_command = false;
                step_command += 1;
                stream.write(`${clean_command}\n`)
                changeNetwork = false;
              } else {
              if (send_command && step_command < commands.length) {
                current_command = commands[step_command];
                send_command = false;
                clean_command = await extract_command(current_command, ipInformation.ip, ipInformation.mask).command;
                if (pymesVar) {
                  clean_command = await vrf_pymes(clean_command, vrfPymes);
                }
                if (current_command.changeNetwork) {  // 
                  cambio = true;
                  selected_network = current_command.network[1] || current_command.network[0];
                  stream.write(`exit\n`);
                  
                } else {
                  //console.log('CLEAN COMMAND', clean_command);
                  stream.write(`${clean_command}\n`);
                  step_command += 1;
                } // 

              }
                // stream.write(`${clean_command}\n`);
                send_command = false;
                
              }
            }

            if (((!(d.toString('binary').includes(`${selected_network}#`))) && (!(introduction))) && changeNetwork && (!(one_connection))) {
              stream.write('\n');
            }

            socket.emit('data', d.toString('binary'));
          }).on('close', function() {
            conn.end();
          });
        });
      // }
    }).on('close', function() {
      socket.emit('data', '\r\n** SSH CONNECTION CLOSED **\r\n');
    }).on('error', function(err) {
      socket.emit('data', '\r\n** SSH CONNECTION ERROR: ' + err.message + ' **\r\n');
    });
});


async function automatic_commands(ip) {
// AQUI EMPIEZA EL ENVIO DE COMANDOS AUTOMATIZADOS //
  try {
    return await axios.post('http://localhost:3000/api/zValidador/pagination', JSON.stringify(
      {
        commandType: ip.type,
        limit: 0,
        page: 1,
        sort: { column: 'step', value: 1 },
      },
    ), {
      headers: {
          'Content-Type': 'application/json',
      }
  }
  )
    .then(({ data }) => {
      return data.data[0].data
    });

  } catch (e) {
    console.error('Failure!');
    console.error(e.response.status);
  }
// AQUI TERMINA EL ENVIO DE COMANDOS AUTOMATIZADOS //
}

function vrf_pymes(command, vrf) {
  return command.replace('VRFPYMES', vrf);
}

function extract_command(data, ip, mask) {
  const xxx_xxx_xxx_yy = /(([X]{1,3}\.){3}[X]{1,3}\/[Y]{1,2})/;
  const xxx_xxx_xxx_xxx = /(([X]{1,3}\.){3}[X]{1,3})/;
  const xxx_xxx_xxx = /(([X]{1,3}\.){3})/;
  const xxx_xxx_xxxzzz = /(([X]{1,3}\.){3}[X]{1,3}[Z]{3})/;
  const xxx_xxx_xxxzz = /(([X]{1,3}\.){3}[X]{1,3}[Z]{2})/;
  const xxx_xxx_xxxz = /(([X]{1,3}\.){3}[X]{1,3}[Z]{1})/;

  if (data.command.match(xxx_xxx_xxx_yy) !== null) {
    const parseado = data.command.replace(xxx_xxx_xxx_yy, `${ip}/${mask}`);
    if (parseado.match(xxx_xxx_xxx) !== null) {
      const regex = /(([0-9]{1,3}\.){3})/;
      const validacion = ip.match(regex);
      console.log(parseado.replace(xxx_xxx_xxx, validacion[0]));
      return { ...data, command: parseado.replace(xxx_xxx_xxx, validacion[0]) };
    }
    console.log(parseado);
    return { ...data, command: parseado };
  } if (data.command.match(xxx_xxx_xxxzzz) !== null) {
    const arr = ip.split('.');
    const numeroSuma = parseInt(arr[arr.length - 1]) + 3;
    const ipNueva = ip.slice(0, -(arr[arr.length - 1].length)) + numeroSuma;
    const parseado = data.command.replace(xxx_xxx_xxxzzz, ipNueva);
    if (parseado.match(xxx_xxx_xxx) !== null) {
      const regex = /(([0-9]{1,3}\.){3})/;
      const validacion = ip.match(regex);
      console.log(parseado.replace(xxx_xxx_xxx, validacion[0]));
      return { ...data, command: parseado.replace(xxx_xxx_xxx, validacion[0]) };
    }
    console.log(parseado);
    return { ...data, command: parseado };
  } if (data.command.match(xxx_xxx_xxxzz) !== null) {
    const arr = ip.split('.');
    const numeroSuma = parseInt(arr[arr.length - 1]) + 2;
    const ipNueva = ip.slice(0, -(arr[arr.length - 1].length)) + numeroSuma;
    const parseado = data.command.replace(xxx_xxx_xxxzz, ipNueva);
    if (parseado.match(xxx_xxx_xxx) !== null) {
      const regex = /(([0-9]{1,3}\.){3})/;
      const validacion = ip.match(regex);
      console.log(parseado.replace(xxx_xxx_xxx, validacion[0]));
      return { ...data, command: parseado.replace(xxx_xxx_xxx, validacion[0]) };
    }
    console.log(parseado);
    return { ...data, command: parseado };
  } if (data.command.match(xxx_xxx_xxxz) !== null) {
    const arr = ip.split('.');
    const numeroSuma = parseInt(arr[arr.length - 1]) + 1;
    const ipNueva = ip.slice(0, -(arr[arr.length - 1].length)) + numeroSuma;
    const parseado = data.command.replace(xxx_xxx_xxxz, ipNueva);
    if (parseado.match(xxx_xxx_xxx) !== null) {
      const regex = /(([0-9]{1,3}\.){3})/;
      const validacion = ip.match(regex);
      console.log(parseado.replace(xxx_xxx_xxx, validacion[0]));
      return { ...data, command: parseado.replace(xxx_xxx_xxx, validacion[0]) };
    }
    console.log(parseado);
    return { ...data, command: parseado };
  }
  if (data.command.match(xxx_xxx_xxx_xxx) !== null) {
    const parseado = data.command.replace(xxx_xxx_xxx_xxx, ip);
    if (parseado.match(xxx_xxx_xxx) !== null) {
      const regex = /(([0-9]{1,3}\.){3})/;
      const validacion = ip.match(regex);
      return { ...data, command: parseado.replace(xxx_xxx_xxx, validacion[0]) };
    }
    return { ...data, command: parseado };
  }
}

function expect_result(expect, response_message) {
  console.log('parse_expect ', expect );
  if (response_message) {
    if (response_message.length < 125) {
      return true
    }
  
    if (response_message.includes('% Invalid')) {
      return true
    }

  }

  if (expect) {
    //console.log(`response ${response_message.length}`, response_message);
    if (expect.includes('date')) {
      if (response_message.match(/([A-Z]{1}[a-z]+\s?){2}\s{2}\d+\s{1}\d+:\d+:\d+\.\d+\s{1}[A-Z]+/)) {
        return true
      }
    }
    if (expect.includes('/30') ) {
      if (response_message) {
        // console.log('includes /30');
        if (response_message.includes('/24') || response_message.includes('/25') || response_message.includes('/26') || response_message.includes('/27') || response_message.includes('/28') || response_message.includes('/29') || response_message.includes('/30')){
          // console.log('includes /302222222');
          return false
        } else {
          // console.log('includes /3033333333');
          return true
        }

      }
    
    } else {
      if (response_message.includes(expect)){
        //Cuando es expected
        return true
      }
    }

  }

  
  return false
};

io.listen(8000);


//cors
//const corsOptions = { origin: config.url };
//app.use(cors(corsOptions));
app.use(cors());
// routes
authApi(app);
otpsApi(app);
othsApi(app);
usersApi(app);
clientsApi(app);
projectsApi(app);
bitacorasApi(app);
logsApi(app);
emailApi(app);
vpnApi(app);
cierreko(app);
activityApi(app);
checklistconf(app);
hitosApi(app);
departamentosApi(app);
samiaApi(app);
rangeipApi(app);
secuenceApi(app);
inventoryApi(app);
migrationApi(app);
checklistformfApi(app);
s3Api(app);
functionalitiesApi(app);
pymes(app);
switches(app);
procedureLogsApi(app);
zValidador(app);
markings(app);
lineaBaseApi(app);
calendar(app);
tasks(app);
generalCol(app);
dashboardApi(app);
// catch 404
app.use(notFoundHandler);

// Error middleware
app.use(logErrors);
app.use(wrapErrors);
app.use(errorHandler);


app.listen(config.port, function() {
    console.log(`Listening http://localhost:${config.port}`); //eslint-disable-line
});
