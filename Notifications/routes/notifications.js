const express = require('express');
const passport = require('passport');
const NotificationsService = require('../services/notifications');
const General = require('../services/general');
// const cacheResponse = require('../utils/cacheResponse');
// const { FIVE_MINUTES_IN_SECONDS } = require('../utils/time');

// JWT Strategy
require('../utils/auth/strategies/jwt')

// const {
//     createActivitySchema
// } = require('../utils/schemas/activity');

// const validationHandler = require('../utils/middleware/validationHandler');

function notificationsApi(app, io) {
    const router = express.Router();
    app.use('/api/notifications', router);
    const notificationsService = new NotificationsService();
    const generalServices = new General('notifications');

    router.post('/pagination', passport.authenticate('jwt', { session: false }), async function (req, res, next) {
        const query = req.body;
        const page = query.page;
        const limit = query.limit;
        try {
            const data = await generalServices.multipurpose(query);
            const filterTotal = data[0].totalData[0] ? data[0].totalData[0].filterTotal : 0;
            const total = data[0].totalCount[0] ? data[0].totalCount[0].count : 0;
            res.status(200).json({
                page: parseInt(page),
                per_page: parseInt(limit),
                total: filterTotal,
                total_pages: Math.ceil(filterTotal / parseInt(limit)),
                totalRegisters: total,
                data,
                message: 'data retrived'
            });

        } catch (err) {
            next(err);
        }
    });

    router.post('/toClient',
        async function (req, res, next) {
        const { body: connectionNotifications } = req;
        try {
            const createNotification = await notificationsService.newNotification(connectionNotifications, io);

            res.status(201).json({
                data: { createNotification },
                message: 'activity created'
            });

        } catch (err) {
            next(err);
        }
    });

    router.post('/',
        async function (req, res, next) {
        const { body: data } = req;
        try {
            //const sendNotification = await notificationsService.newNotification(connectionNotifications, io);
            io.to(data.room).emit('notification', { user: 'Admin', text: data.message });
            res.status(201).json({
                message: 'Notification Sent'
            });

        } catch (err) {
            next(err);
        }
    });
}

module.exports = notificationsApi;