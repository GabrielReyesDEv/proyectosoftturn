const http = require('http');
const express = require('express');
const socketio = require('socket.io');
const cors = require('cors');

const { addUser, removeUser, getUser, getUsersInRoom } = require('./users');

const notificationsApi =  require('./routes/notifications');

const router = require('./router');

const app = express();
const bodyParser = require('body-parser');

app.use(bodyParser.json({limit: "180mb" }));
app.use(bodyParser.urlencoded({limit: "180mb" , extended: true, parameterLimit:90000}));

const server = http.createServer(app);
const io = socketio(server);

app.use(cors());
app.use(router);

io.on('connect', (socket) => {
  socket.on('join', ({ name, room }, callback) => {
    const { error, user } = addUser({ id: socket.id, name, room });

    if(error) return callback(error);

    socket.join(user.room);
    socket.join('general');

    socket.emit('notification', { user: 'admin', text: `${user.name}, Bienvenido a Zolid`});
    socket.broadcast.to(user.room).emit('notification', { user: 'admin', text: `${user.name} inició sesión en otro dispositivo` });

    io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room) });

    callback();
  });

  socket.on('sendNotification', (notification, callback) => {
    const user = getUser(socket.id);

    io.to(user.room).emit('notification', { user: user.name, text: notification });

    callback();
  });

  socket.on('disconnect', () => {
    const user = removeUser(socket.id);
    if(user) {
      io.to(user.room).emit('notification', { user: 'Admin', text: `${user.name} cerró sesión en el otro dispositivo` });
      io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room)});
    }
  })
});

notificationsApi(app, io);

server.listen(4000, () => console.log(`Notifications Server has started.`));

app.listen('5000', function() {
  console.log('Listening http://localhost:5000'); //eslint-disable-line
});