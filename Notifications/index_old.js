/* eslint-disable no-useless-escape */
/* eslint-disable no-console */
/* eslint-disable require-atomic-updates */
const express = require('express');
const app = express();
const cors = require("cors");
const formatMessage = require('./utils/messages');
const {
  userJoin,
  getCurrentUser,
  userLeave,
  getRoomUsers
} = require('./utils/users');

// const { config } = require('./config/index');
const notificationsApi =  require('./routes/notifications');

const bodyParser = require('body-parser');

app.use(bodyParser.json({limit: "180mb" }));
app.use(bodyParser.urlencoded({limit: "180mb" , extended: true, parameterLimit:90000}));
// body parser
app.use(express.json());

// Socket.io 
const http = require('http').Server(app); 
const io = require('socket.io')(http);

io.listen(8001);

const botName = 'Zolid';
io.on('connection', socket => {
    socket.on('joinRoom', (userData) => {
      const currentUser = getCurrentUser(userData.idUser);
      let user = {};
      if (currentUser === undefined) {
        user = userJoin(socket.id, userData.idUser, userData.userName, userData.roomId);     
      } else{
        user = currentUser;
      }

      socket.leave(userData.idUser); 
      socket.join(userData.idUser);
      socket.leave('mainRoom'); 
      socket.join('mainRoom');
  
      // Welcome current user
      if (userData.isLogin === 'login') {
        // socket.emit('message', formatMessage(botName, `Welcome to Zolid ${user.username}!`));
        io.to(user.id).emit('message', formatMessage(user ? user.username : 'Zolid', `Welcome to Zolid ${user.username}!`));
      }
      // else {
      //   socket.emit('message', formatMessage(botName, `${user.username} connected succesfully!`));
      // }
  
      // Broadcast when a user connects
      socket.broadcast
        .to(user.room)
        .emit(
          'message',
          formatMessage(botName, `${user.username} has joined to the application`)
        );
  
      // Send users and room info
      io.to(user.id).emit('roomUsers', {
        room: user.room,
        users: getRoomUsers(user.room)
      });
      io.to('mainRoom').emit('roomUsersMain', {
        room: 'mainRoom',
        users: getRoomUsers('mainRoom')
      });
    });
  
    // Listen for chatMessage
    socket.on('notifyMessage', (data) => {
      const user = getCurrentUser(data.idUser);
        io.to(data.to).emit('message', formatMessage(user ? user.username : 'Zolid', data.msg));
    });

    socket.on('notifyUsers', (data) => {
      const user = getRoomUsers('mainRoom')
      const user2 = getRoomUsers(data.to)
        // io.to(data.to).emit('message', formatMessage('Zolid', user));
    });
  
    // Runs when client disconnects
    socket.on('disconnectUser', (idUser) => {
      const user = userLeave(idUser);
  
      if (user) {
        socket.leave(user.id);
        socket.leave('mainRoom');
        if(socket) socket.disconnect();
        io.to('mainRoom').emit(
          'message',
          formatMessage(botName, `${user.username} has left the application`)
        );
  
        // Send users and room info
        io.to(user.room).emit('roomUsers', {
          room: user.room,
          users: getRoomUsers(user.room)
        });
      }
    });
  });

app.use(cors());
// routes
notificationsApi(app, io);


app.listen('5000', function() {
    console.log('Listening http://localhost:5000'); //eslint-disable-line
});
