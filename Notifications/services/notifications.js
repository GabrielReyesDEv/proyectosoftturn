const MongoLib = require('../../Backend/lib/mongo');

class ActivitiesService {
    constructor() {
        this.collection = 'notifications';
        this.mongoDB = new MongoLib();
    }
    
    async newNotification( connectionNotifications, io ){
        io.sockets.in(connectionNotifications.room).emit('message', connectionNotifications.message);
    }
}

module.exports = ActivitiesService