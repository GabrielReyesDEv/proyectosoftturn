const MongoLib = require('../lib/mongo');
const bcrypt = require('bcrypt');
const moment = require('moment');

class UsersService {
  constructor() {
    this.collection = 'users';
    this.mongoDB = new MongoLib();
  }

  async getUser({ email }) {
    const [ user ] = await this.mongoDB.getAllUser(this.collection, { email });
    return user;
  }

  async getUserPaginated( tags ){
    const user = await this.mongoDB.getPagination(this.collection, tags);
    return user || [];
}


  async getUsers( tags ){
    const users = await this.mongoDB.getAll(this.collection, tags);
    return users || [];
}

  async getUserById( userId ){
    const users = await this.mongoDB.get(this.collection, userId);
    return users || {};
}
  async createUser({ user }) {

    const createUserId = await this.mongoDB.create(this.collection, user);

    return createUserId;
  }

  async register(user) {
    const { firstName, lastName, email, identification, password, phone } = user;
    const hashedPassword = await bcrypt.hash(password, password.length);

    const createUserId = await this.mongoDB.create(this.collection, {
      createdAt: moment().format('DD/MM/YYYY'),
      email,
      firstName,
      identification,
      lastName,
      password: hashedPassword,
      phone,
      status: 'pending',
    });

    return createUserId;
  }

  async updateUserPass({ user }) {
    const { email, password, identification, telefono, fechaContrasena, actContrasena } = user;
    const hashedPassword = await bcrypt.hash(password, 10);

    const updateUserId = await this.mongoDB.updateGeneral(this.collection, email, {
      email,
      password: hashedPassword,
      identification,
      telefono,
      fechaContrasena,
      actContrasena,
    }, 'email');

    return updateUserId;
  }

  async updateUser(id, data) {

    const updateUserId = await this.mongoDB.update(this.collection, id, data);

    return updateUserId;
  }

  async updatePure(id, data) {

    const pureConsult = await this.mongoDB.updatePure(this.collection, id, data);

    return pureConsult;
  }

  async deleteUser(id) {
    const deleteUsers = await this.mongoDB.delete(this.collection, id);

    return deleteUsers;
  }

  async deleteUsers(ids) {
    const deleteUsers = await this.mongoDB.deleteMany(this.collection, ids);

    return deleteUsers;
  }

}

module.exports = UsersService;