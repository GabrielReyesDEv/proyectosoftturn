const MongoLib = require('../lib/mongo');

class General {
    constructor(data) {
        this.collection = data;
        this.mongoDB = new MongoLib();
    }

  async multipurpose(query) {
    const result = await this.mongoDB.match(this.collection, query);
    return result;
  }

  async arrayMatch(query) {
    const result = await this.mongoDB.matchArray(this.collection, query);
    return result;
  } 

}

module.exports = General