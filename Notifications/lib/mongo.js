/* eslint-disable no-console */
const { MongoClient, ObjectId } = require('mongodb');
const { config } = require('../config');
const moment = require('moment');
const momentTimezone = require('moment-timezone');

// const USER = encodeURIComponent(config.dbUser);
// const PASSWORD = encodeURIComponent(config.dbPassword);

const DB_NAME = config.dbName;

// const MONGO_URI = `mongodb://${USER}:${PASSWORD}@${config.dbHost}/${DB_NAME}?retrywrites=true&w=majority`;
const MONGO_URI = `mongodb://mongo_db:a4b3c2d1@cluster0-shard-00-00-rentc.mongodb.net:27017,cluster0-shard-00-01-rentc.mongodb.net:27017,cluster0-shard-00-02-rentc.mongodb.net:27017/${DB_NAME}?authSource=admin&replicaSet=Cluster0-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true`;
class MongoLib {
    constructor() {
        this.client = new MongoClient(MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true } );
        this.dbName = DB_NAME;
    }

    connect() {
        if (!MongoLib.connection) {
            MongoLib.connection = new Promise((resolve, reject) => {
                this.client.connect(err => {
                    if (err) {
                        reject(err)
                    }
                    console.log('Connected succesfully to mongo')
                    resolve(this.client.db(this.dbName));
                });
            });
        }

        return MongoLib.connection;
    }

    getAllCollection(collection) {
        return this.connect().then(db => {
            return db.collection(collection).find().toArray();
        })

    }

    getAll(collection, query) {
        let find_filter = query.filter ? JSON.parse(`{ "${query.filter}": "${query.value}" }`) : undefined;
        let limit = parseInt(query.limit)
        let page = parseInt(query.page)
        let sort = query.sort ?  JSON.parse(`{ "${query.sort}": -1 }`) : undefined;
        return this.connect().then(db => {
            return db.collection(collection).find(find_filter).skip((limit * page) - limit)
            .limit(limit).sort(sort).toArray();
        })
    }
//
    getSeq(collection, query) {
        return this.connect().then(db => {
            return db.collection(collection).findOne({ name: query });
        })
    }

    get(collection, id) {
        return this.connect().then(db => {
            return db.collection(collection).findOne({ _id: ObjectId(id) })
        })
    }

    getBy(collection, query) {
        return this.connect().then(db => {
            return db.collection(collection).findOne(query);
        })
    }

    getProject(collection, id) {
        return this.connect().then(db => {
            return db.collection(collection).findOne({ idProyecto: parseInt(id) });
        })
    }
    
    getGeneral(collection, id, filter) {
        return this.connect().then(db => {
            return db.collection(collection).find({ [filter]: (id) }).sort({$natural: -1}).toArray();
        })
    }

    /* getGeneral(collection, id, filter) {
        var idd = id.toString();
        let find_filter = filter ? JSON.parse(`{ "${filter}": "${idd}" }`) : undefined;
        console.log('num2: ', id);
        console.log('num3: ', typeof idd);
        console.log('num4: ', idd);
        return this.connect().then(db => {
            return db.collection(collection).find({ find_filter }).sort({$natural: -1}).toArray();
        })
    } */
    
    getByOthId(collection, id) {
        return this.connect().then(db => {
            return db.collection(collection).findOne({ activityId: id })
        })
    }
    getLastByOthId(collection, id) {
        return this.connect().then(db => {
            return db.collection(collection).find({activityId: id}).sort({$natural: -1}).limit(1).toArray();
        })
    }

    create(collection, data) {
        return this.connect().then(db => {
            return db.collection(collection).insertOne(data)
        }).then(result => result.insertedId);
    }

    Upsert(collection, id, data) {
        return this.connect().then(db => {
            return db.collection(collection).updateOne({ projectId: id }, { $set: data }, { upsert: true })
        }).then(result => result.upsertedId || id);
    }

    UpsertFormPymes(data,  id, collection) {
        return this.connect().then(db => {
            return db.collection(collection).updateOne({ otp: id }, { $set: data }, { upsert: true })
        }).then(result => result.upsertedId || id);
    }

    UpsertByOth(collection, id, data) {
        return this.connect().then(db => {
            return db.collection(collection).updateOne({ activityId: id }, { $set: data }, { upsert: true })
        }).then(result => result.upsertedId || id);
    }

    updateGeneral(collection, id, data, filter) {
        return this.connect().then(db => {
            return db.collection(collection).updateOne({ [filter]: filter === '_id' ? ObjectId(id) : (id) }, { $set: data }), { upsert: true }
        }).then(result => result.upsertedId || id);
    }

    deleteGeneral(collection, id, filter) {
        return this.connect().then(db => {
            return db.collection(collection).deleteOne({ [filter]: filter === '_id' ? ObjectId(id) : (id) })
        }).then(() => id)
    }

    update(collection, id, data) {
        return this.connect().then(db => {
            return db.collection(collection).updateOne({ _id: ObjectId(id) }, { $set: data }), { upsert: true }
        }).then(result => result.upsertedId || id);
    }

    push(collection, id, data, filter) {
        return this.connect().then(db => {
            return db.collection(collection).update({ [filter]: (id) }, { $push: { commands: data } })
        }).then(result => result.upsertedId || id);
    }

    updatePure(collection, id, data) {
        return this.connect().then(db => {
            return db.collection(collection).update({ _id: ObjectId(id) }, data), { upsert: true }
        }).then(result => result.upsertedId || id);
    }

    getMails(collection) {
        return this.connect().then(db => {
            return db.collection(collection).aggregate([{$match : { 'roles.title' : /Analista/}}, {$group: { _id: '$email'}}]).toArray();
        });
    }
    
    // updateSeq(collection, id, data) {
    //     return this.connect().then(db => {
    //         return db.collection(collection).updateOne({ name: (id) }, { $set: data }), { upsert: true }
    //     }).then(result => result.upsertedId || id);
    // }

    delete(collection, id) {
        return this.connect().then(db => {
            return db.collection(collection).deleteOne({ _id: ObjectId(id) })
        }).then(() => id)
    }

    deleteMany(collection, ids) {
        const data = [];
        for (var i = 0; i < ids.length; i++) {
            data.push(ObjectId(ids[i]));
        }
        return this.connect().then(db => {
            return db.collection(collection).deleteMany({ _id: { $in: data} })
        }).then(() => ids)
    }
    
    deleteManyGeneral(collection, ids, filter) {
        const data = [];
        for (var i = 0; i < ids.length; i++) {
            if(filter=='_id'){
                data.push(ObjectId(ids[i]));
            }else{
                data.push(ids[i]);
            }
        }
        return this.connect().then(db => {
            return db.collection(collection).deleteMany({ [filter]: { $in: data} })
        }).then(() => ids)
    }

    getAllUser(collection, query) {
        return this.connect().then(db => {
            return db.collection(collection).find(query).toArray();
        })
    }

    createMany(collection, data) {
        return this.connect().then(db => {
            return db.collection(collection).insertMany(data)
        }).then(result => result.insertedId);
    }

    updates(collection, bulkUpdateOps) {
        return this.connect().then(db => {
            return db.collection(collection).bulkWrite(bulkUpdateOps)
        }).then(result => result);
    }

    getAllByUser(collection, query) {
        return this.connect().then(db => {
            return db.collection(collection).find({executorId: query.userId, state: query.state}).toArray();
        })
    }

    getUserPymes(collection, query) {
        return this.connect().then(db => {
            return db.collection(collection).find({executorId: query.userId, state: query.state}).toArray();
        })
    }

    getUserPymesName(collection, name) {
        return this.connect().then(db => {
            return db.collection(collection).aggregate([
                {
                    $addFields: {
                        "nombreCompleto": { $concat: [ "$firstName", " ", "$lastName" ] }
                    }      
                },
                {
                    $match: {
                        'nombreCompleto': new RegExp(name, 'gi')
                    }
                }
            ]).toArray()
            })
    }

    getUserGeneral(collection, query) {
        let match = {};
        
        for (let key in query){
            
            if ( typeof query[key] === 'object' && query[key].$elemMatch ){
                match[key] = query[key];
            } else {
                match[key] = { $regex: new RegExp(query[key], 'gi') };
            }
        }

        return this.connect().then(db => {
            return db.collection(collection).aggregate([
                {
                    $addFields: {
                        "nombreCompleto": { $concat: [ "$firstName", " ", "$lastName" ] }
                    }      
                },
                {
                    $match :  match
                }
            ]).toArray()
            })
    }
    

    getPagination(collection, query) {
        let limit = parseInt(query.limit)
        let page = parseInt(query.page)
        delete query.limit
        delete query.page
        return this.connect().then(db => {
            return db.collection(collection)
            .aggregate([
                { "$facet": {
                  "totalData": [
                    { "$match": query || {}},
                    { "$count": "filterTotal"},
                  ],
                  "data": [
                    { "$match": query ? query : 0},
                    { "$skip": (limit * page) - limit },
                    { "$limit": limit },
                  ],
                  "totalCount": [
                    { "$group": {
                      "_id": null,
                      "count": { "$sum": 1 },
                    },
                    },
                  ]
                }}
              ]).toArray()
        })  
    }

    getRolesSupervision(collection) {
        return this.connect().then(db => {
            return db.collection(collection)
            .aggregate([
                { $project:
                    { logs:
                       {
                         $map:
                            {
                              input: "$log",
                              as: "lg",
                              in: { $mergeObjects: [ "$$lg", { projectId: "$otpId"}, { activityId: "$othId"} ] }
                            }
                       }
                    }
                 },
                {$group:{
                    _id: null,
                    selectedTags: { $push: '$logs' }     
                 }},
                 {$project: {
                     selectedTags: { $reduce: {
                         input: "$selectedTags",
                         initialValue: [],
                         in: {$setUnion : ["$$value", "$$this"]}
                     }}
                 }},
            ]).toArray();

        })
    }
    
    getManagementSupervision(collection, query) {
        // let limit = parseInt(query.limit)
        // let page = parseInt(query.page)
        delete query.limit
        delete query.page

        return this.connect().then(db => {
            return db.collection(collection)
            .aggregate([
                { "$facet": {
                    "data": [
                        { "$project": {
                            "_id": 0,
                            "usuarioAsignado": 1,
                            "Pendientes": {$cond: [{$eq: ['$estadoChecklist', 'Asignado']}, {field: ["othId"], value: '$othId'}, '$$REMOVE']},
                            "Ejecutadas": {$cond: [{$eq: ['$estadoChecklist', 'Iniciado']}, {field: ["othId"], value: '$othId'}, '$$REMOVE']},
                            "Terminadas": {$cond: [{$eq: ['$estadoChecklist', 'Terminado']}, {field: ["othId"], value: '$othId'}, '$$REMOVE']},
                            "Rechazadas": {$cond: [{$eq: ['$estadoChecklist', 'Cancelado']}, {field: ["othId"], value: '$othId'}, '$$REMOVE']},
                            "Total": {$cond: [{$or: [{$eq: ['$estadoChecklist', 'Asignado']},
                                {$eq: ['$estadoChecklist', 'Iniciado']},
                                {$eq: ['$estadoChecklist', 'Terminado']},
                                {$eq: ['$estadoChecklist', 'Cancelado']},
                            ]
                            },
                            {field: ["othId"], value: '$othId'},
                            '$$REMOVE'
                            ]
                            },
                        }},
                        { "$group": {
                                "_id": "$usuarioAsignado.id",
                                "name": { $first: "$usuarioAsignado.name" },
                                "SumPendientes": {$push: '$Pendientes'},
                                "SumEjecutadas": {$push: '$Ejecutadas'},
                                "SumTerminadas": {$push: '$Terminadas'},
                                "SumRechazadas": {$push: '$Rechazadas'},
                                "SumTotal": {$push: '$Total'},
                              "count": { "$sum": 1 },
                            },
                        },
                        { "$sort":{'_id':1} },
                    ],
                    "totalData": [
                        { "$project": {
                            "_id": 0,
                            "usuarioAsignado": 1,
                            "Pendientes": {$cond: [{$eq: ['$estadoChecklist', 'Asignado']}, 1, 0]},
                            "Ejecutadas": {$cond: [{$eq: ['$estadoChecklist', 'Iniciado']}, 1, 0]},
                            "Terminadas": {$cond: [{$eq: ['$estadoChecklist', 'Terminado']}, 1, 0]},
                            "Rechazadas": {$cond: [{$eq: ['$estadoChecklist', 'Cancelado']}, 1, 0]},
                        }},
                        { "$group": {
                                "_id": "$usuarioAsignado.id",
                                "name": { $first: "$usuarioAsignado.name" },
                                "SumPendientes": {$sum: '$Pendientes'},
                                "SumEjecutadas": {$sum: '$Ejecutadas'},
                                "SumTerminadas": {$sum: '$Terminadas'},
                                "SumRechazadas": {$sum: '$Rechazadas'},
                              "count": { "$sum": 1 },
                            },
                        },
                        { "$sort":{'_id':1} },
                    ]
                }}
            ]).toArray();
            })
    }

    getManagementSupervisionWithDate(collection, query) {
        // let limit = parseInt(query.limit);
        // let page = parseInt(query.page);
        const date = query.date;
        // console.log('date: ', date)
        delete query.limit
        delete query.page
        delete query.date

        return this.connect().then(db => {
            return db.collection(collection)
            .aggregate([
                { "$facet": {
                    "data": [
                        { "$project": {
                            "_id": 0,
                            "usuarioAsignado": 1,
                            "Pendientes": {$cond: [{$and: [{$eq: ['$estadoChecklist', 'Asignado']},
                                                           {'$regexMatch': {"input": "$createdAt", "regex":  new RegExp(date, 'gi')}}
                                                          ]
                                                   },
                                                   {field: ["othId"], value: '$othId'},
                                                   '$$REMOVE'
                                                  ]
                                          },
                            "Ejecutadas": {$cond: [{$and: [{$eq: ['$estadoChecklist', 'Iniciado']},
                                                           {'$regexMatch': {"input": "$createdAt", "regex": new RegExp(date, 'gi')}}
                                                          ]
                                                   },
                                                   {field: ["othId"], value: '$othId'},
                                                   '$$REMOVE'
                                                  ]
                                          },
                           "Terminadas": {$cond: [{$and: [{$eq: ['$estadoChecklist', 'Terminado']},
                                                           {'$regexMatch': {"input": "$createdAt", "regex": new RegExp(date, 'gi')}}
                                                          ]
                                                   },
                                                   {field: ["othId"], value: '$othId'},
                                                   '$$REMOVE'
                                                  ]
                                          },
                           "Rechazadas": {$cond: [{$and: [{$eq: ['$estadoChecklist', 'Cancelado']},
                                                           {'$regexMatch': {"input": "$createdAt", "regex": new RegExp(date, 'gi')}}
                                                          ]
                                                   },
                                                   {field: ["othId"], value: '$othId'},
                                                   '$$REMOVE'
                                                  ]
                                          },
                            "Total": {$cond: [{$and: [{$or: [{$eq: ['$estadoChecklist', 'Asignado']},
                                                              {$eq: ['$estadoChecklist', 'Iniciado']},
                                                              {$eq: ['$estadoChecklist', 'Terminado']},
                                                              {$eq: ['$estadoChecklist', 'Cancelado']}
                                                            ]
                                                      },
                                              {'$regexMatch': {"input": "$createdAt", "regex": /2020-06-/}}
                                             ]},
                                            {field: ["othId"], value: '$othId'},
                                            '$$REMOVE'
                                             ]
                                     },
                        }},
                        { "$group": {
                                "_id": "$usuarioAsignado.id",
                                "name": { $first: "$usuarioAsignado.name" },
                                "SumPendientes": {$push: '$Pendientes'},
                                "SumEjecutadas": {$push: '$Ejecutadas'},
                                "SumTerminadas": {$push: '$Terminadas'},
                                "SumRechazadas": {$push: '$Rechazadas'},
                                "SumTotal": {$push: '$Total'},
                              "count": { "$sum": 1 },
                            },
                        },
                        { "$sort":{'_id':1} },
                    ],
                    "totalData": [
                        { "$project": {
                            "_id": 0,
                            "usuarioAsignado": 1,
                            "Pendientes": {$cond: [{$eq: ['$estadoChecklist', 'Asignado']}, 1, 0]},
                            "Ejecutadas": {$cond: [{$eq: ['$estadoChecklist', 'Iniciado']}, 1, 0]},
                            "Terminadas": {$cond: [{$eq: ['$estadoChecklist', 'Terminado']}, 1, 0]},
                            "Rechazadas": {$cond: [{$eq: ['$estadoChecklist', 'Cancelado']}, 1, 0]},
                        }},
                        { "$group": {
                                "_id": "$usuarioAsignado.id",
                                "name": { $first: "$usuarioAsignado.name" },
                                "SumPendientes": {$sum: '$Pendientes'},
                                "SumEjecutadas": {$sum: '$Ejecutadas'},
                                "SumTerminadas": {$sum: '$Terminadas'},
                                "SumRechazadas": {$sum: '$Rechazadas'},
                              "count": { "$sum": 1 },
                            },
                        },
                        { "$sort":{'_id':1} },
                    ]
                }}
            ]).toArray();
            })
    }

    getOtpsGroupByUsers(collection, tags) {
        const tipoFecha = tags.fecha;
        const fechaInicio = tags.fechaInicio;
        const fechaFin = tags.fechaFin;
        delete tags.fecha;
        delete tags.fechaInicio;
        delete tags.fechaFin;

        let match  = undefined;
        // if (tags.userOnyx) {
        //    match  =  { "responsable.userOnyx": tags.userOnyx}
        // }

         if(tipoFecha=='null' || (fechaInicio=='null' && fechaFin=='null')){
            if (tags.userOnyx) match  = {'responsable.userOnyx': {$eq: tags.userOnyx}};
        }else{
            if(fechaInicio=='null' && fechaFin!='null'){
                if (tags.userOnyx) match = {'responsable.userOnyx': {$eq: tags.userOnyx}, [tipoFecha]: { $lte: fechaFin }};
            }
            if(fechaInicio!='null' && fechaFin=='null'){
                if (tags.userOnyx) match = {'responsable.userOnyx': {$eq: tags.userOnyx}, [tipoFecha]: { $gte: fechaInicio }};
            }
            if(fechaInicio!='null' && fechaFin!='null'){
                if (tags.userOnyx) match = {'responsable.userOnyx': {$eq: tags.userOnyx}, [tipoFecha]: { '$gte': fechaInicio, '$lte': fechaFin }};
            }
        }

        return this.connect().then(db => {
            return db.collection(collection)
            .aggregate([
                { $match: match ? match : {}},
                { $group: {
                    _id: "$responsable.userOnyx",
                    name: { $first: "$responsable.nombre" },
                    data: { $push: { projectId: '$otpId', fechas: '$fechas', log: { $slice: [ '$logReporteActualizacion', -1 ] }  } } 
                }}]).toArray();
            })
    }

    getInventoryDuration(collection, query) {
        //console.log('query2: ', query);
        return this.connect().then(db => {
            return db.collection(collection)
            .aggregate([
                { $match: { status : "Stock Local", idProyecto: query}},
                {
                    $project: {
                        batchNumber: 1,
                        ingresoAInventario: 1,
                        total: { $subtract: [ '$quantity', { $sum: '$po.deliveredQuantity'} ]}
                    }
                }
            ]).toArray();
        })
    }

    getInventoryEfectivity(collection) {
        return this.connect().then(db => {
            return db.collection(collection)
            .aggregate([
                {$group: {
                    _id: {mes: {$month: "$ingresoAInventario"}, ano:  {$year: "$ingresoAInventario"}}, 
                    disponible: {$sum: '$quantity'}
                }},
                { $sort:{'_id.ano':1, '_id.mes':1} }
            ]).toArray();
        })
    }

    getInventoryForecast(collection, query) {
        let limit = parseInt(query.limit)
        let page = parseInt(query.page)
        delete query.limit
        delete query.page
        return this.connect().then(db => {
            return db.collection(collection)
            .aggregate([
                { "$facet": {
                  "totalData": [
                    { "$match": query || {}},
                    { "$count": "filterTotal"},
                  ],
                  "data": [
                    { "$match": query ? query : 0},
                    { "$skip": (limit * page) - limit },
                    { "$limit": limit },
                  ],
                  "totalCount": [
                    { "$group": {
                      "_id": null,
                      "count": { "$sum": 1 },
                    },
                    },
                  ]
                }}
              ]).toArray()
        })  
    }
    getInventoryPO(collection, query) {
        return this.connect().then(db => {
            return db.collection(collection)
            .aggregate([
                //{ "$match": query || {}},
                { "$match": query ? query : 0},
                     {$group:{
            _id: null,
            selectedTags: { $push: '$po' }      
         }},
         {$project: {
             selectedTags: { $reduce: {
                 input: "$selectedTags",
                 initialValue: [],
                 in: {$setUnion : ["$$value", "$$this"]}
             }}
         }},
         {$unwind: "$selectedTags" },
         {$group:{
            _id: '$selectedTags.poNumber',
            result:  { $sum: '$selectedTags.deliveredQuantity' }  
         }},
        ]).toArray();
    })
}

getInventoryPoSelected(collection, id) {
    return this.connect().then(db => {
        return db.collection(collection)
        .aggregate([
           
                 {$group:{
        _id: null,
        selectedTags: { $push: '$po' }      
     }},
     {$project: {
         selectedTags: { $reduce: {
             input: "$selectedTags",
             initialValue: [],
             in: {$setUnion : ["$$value", "$$this"]}
         }}
     }},
     {$project: {
        items: {
           $filter: {
              input: "$selectedTags",
              as: "item",
              cond: { $eq: [ "$$item.poNumber", id ] }
           }
        }
     }}
    
    ]).toArray();
})
}

    getInventoryEfectivityDelivered(collection) {

                return this.connect().then(db => {
                    return db.collection(collection)
                    .aggregate([
                             {$group:{
                    _id: null,
                    selectedTags: { $push: '$po' }      
                 }},
                 {$project: {
                     selectedTags: { $reduce: {
                         input: "$selectedTags",
                         initialValue: [],
                         in: {$setUnion : ["$$value", "$$this"]}
                     }}
                 }},
                 { $project:
                        { entregado:
                           {
                             $map:
                                {
                                  input: "$selectedTags",
                                  as: "grade",
                                  in: { cantidad: '$$grade.deliveredQuantity', mes: {$month: "$$grade.forecastDate"}, ano: {$year: "$$grade.forecastDate"}}
                                }
                           }
                        }
                     },
                     {$unwind: "$entregado" },
                     {$group: { _id: { mes: "$entregado.mes", ano: '$entregado.ano'}, cantidad: { $sum: '$entregado.cantidad' } }},
                     { $sort:{'_id.ano':1, '_id.mes':1} }
                    ]).toArray();
                })
    }

    getInventoryDashboard(collection, query) {

        return this.connect().then(db => {
            return db.collection(collection)
            .aggregate([
                { "$facet": {
                    "entregado": [
                        { "$match": query || {}},
                        { "$group": {
                            "_id": null,
                            "totalValue": { 
                                "$sum": { "$sum": "$po.deliveredQuantity" } 
                            }
                        } }
                    ],
                    "data": [
                        { "$match": query ? query : 0},
                        { "$group": {
                          "_id": "$status",
                          "response": { "$sum": "$quantity" }
                        }},
                      ],
                    
                  }}
            ]).toArray();
        })
}
    getCounters(collection) {
        return this.connect().then(db => {
            return db.collection(collection)
            .aggregate([
                { "$facet": {
                  "Generada": [
                    { "$match": { "estado.nombre": "1- Generada" }},
                    { "$count": "total"},
                  ],
                  "Ejecucion": [
                    { "$match": { "estado.nombre": "2- En ejecución" }},
                    { "$count": "total"},
                  ],
                  "Terminada": [
                    { "$match": { "estado.nombre": "3- Terminada" }},
                    { "$count": "total"},
                  ],
                  "Cancelada": [
                    { "$match": { "estado.nombre": "Cancelada" }},
                    { "$count": "total"},
                  ]
                }}
              ]).toArray()
        })  
    }

    getCountersPymes(collection, query) {
        const userId = query.userId;
        const rol = query.rol;
        const tipoFecha = query.tipoFecha;
        const fechaInicio = query.fechaInicio;
        const fechaFin = query.fechaFin;
        delete query.userId;
        delete query.rol;
        delete query.tipoFecha;
        delete query.fechaInicio;
        delete query.fechaFin;
        
        let array = [];
        const state = ["Asignada", "Sin Asignar", "Terminada", "Iniciada",["Asignada", "Iniciada", "Sin Asignar"]];

        if(rol=="null" || rol==""){
            for(let i = 0; i < state.length; i++){
                let eq = {};
                eq = {'$match': {'estadio': ''}};
                array.push(eq);
            }
        } else {
        if(rol=="Supervisor Pymes"){
            for(let i = 0; i < state.length; i++){
                let eq = {};
                if(i==4){
                    eq = {'$match': {'estado': { '$in': state[i]}, 'fechaCompromiso': { $lte: momentTimezone().tz('America/Bogota').format('YYYY-MM-DD') }}};
                }
                else if(tipoFecha=='null' || (fechaInicio=='null' && fechaFin=='null')){
                    eq = {'$match': {'estado': state[i]}};
                }else{
                    if(fechaInicio=='null' && fechaFin!='null'){
                        eq = {'$match': {'estado': state[i], [tipoFecha]: { $lte: fechaFin }}};
                    }
                    if(fechaInicio!='null' && fechaFin=='null'){
                        eq = {'$match': {'estado': state[i], [tipoFecha]: { $gte: fechaInicio }}};
                    }
                    if(fechaInicio!='null' && fechaFin!='null'){
                        eq = {'$match': {'estado': state[i], [tipoFecha]: { '$gte': fechaInicio, '$lte': fechaFin }}};
                    }
                }
                array.push(eq);
            }
        }
        if(rol=="Ejecutor Pymes" || rol=="Preconfiguracion" || rol=="Pem" || rol=="Novedad" || rol=="Kickoff"){
            for(let i = 0; i < state.length; i++){
                let eq = {};
                if(i==4){
                    eq = {'$match': {'estado': { '$in': state[i]}, 'fechaCompromiso': { $lte: momentTimezone().tz('America/Bogota').format('YYYY-MM-DD') }, "rol": rol, "ingeniero":userId}};
                }
                if(i==1){
                    if(tipoFecha=='null' || (fechaInicio=='null' && fechaFin=='null')){
                        eq = {'$match': {'estado': state[i], "ingeniero":"Sin Asignar", "rol": rol}};
                    }else{
                        if(fechaInicio=='null' && fechaFin!='null'){
                            eq = {'$match': {'estado': state[i], "ingeniero":"Sin Asignar", "rol": rol, [tipoFecha]: { $lte: fechaFin }}};
                        }
                        if(fechaInicio!='null' && fechaFin=='null'){
                            eq = {'$match': {'estado': state[i], "ingeniero":"Sin Asignar", "rol": rol, [tipoFecha]: { $gte: fechaInicio }}};
                        }
                        if(fechaInicio!='null' && fechaFin!='null'){
                            if (fechaInicio === fechaFin) {
                                eq = {'$match': {'estado': state[i], "ingeniero":"Sin Asignar", "rol": rol, [tipoFecha]: new RegExp(fechaInicio, 'gi') }};
                            } else {
                                eq = {'$match': {'estado': state[i], "ingeniero":"Sin Asignar", "rol": rol, [tipoFecha]: { $gte: fechaInicio, $lte: fechaFin }}};
                            }
                        }
                    }
                } else{
                    if(tipoFecha=='null' || (fechaInicio=='null' && fechaFin=='null')){
                        eq = {'$match': {'estado': state[i], "ingeniero":userId, "rol": rol}};
                    }else{
                        if(fechaInicio=='null' && fechaFin!='null'){
                            eq = {'$match': {'estado': state[i], "ingeniero":userId, "rol": rol, [tipoFecha]: { $lte: fechaFin }}};
                        }
                        if(fechaInicio!='null' && fechaFin=='null'){
                            eq = {'$match': {'estado': state[i], "ingeniero":userId, "rol": rol, [tipoFecha]: { $gte: fechaInicio }}};
                        }
                        if(fechaInicio!='null' && fechaFin!='null'){
                            if (fechaInicio === fechaFin) {
                                eq = {'$match': {'estado': state[i], "ingeniero":userId, "rol": rol, [tipoFecha]: new RegExp(fechaInicio, 'gi') }};
                            } else {
                                eq = {'$match': {'estado': state[i], "ingeniero":userId, "rol": rol, [tipoFecha]: { $gte: fechaInicio, $lte: fechaFin }}};
                            }
                        }
                    }
                }
                array.push(eq);
            }
        }
        if(rol=="Dispatcher Pymes" || rol=="Dispatcher nivel 1" || rol=="Dispatcher nivel 2"){
            for(let i = 0; i < state.length; i++){
                let eq = {};

                if(i==4){
                    eq = {'$match': {'estado': { '$in': state[i]}, 'fechaCompromiso': { $lte: momentTimezone().tz('America/Bogota').format('YYYY-MM-DD') }, "logs": { "$elemMatch": { "rol": "Dispatcher", "userId":userId, "rolInterno": rol } }}};
                }
                else if(i==1){
                    if(rol=="Dispatcher Pymes" || rol=="Dispatcher nivel 1"){
                        if(tipoFecha=='null' || (fechaInicio=='null' && fechaFin=='null')){
                            eq = {'$match': {'estado': state[i], "rol": rol, 'ingeniero': { '$in': [ "Sin Asignar", userId ]}, 'createBy': userId}};
                        }else{
                            if(fechaInicio=='null' && fechaFin!='null'){
                                eq = {'$match': {'estado': state[i], "rol": rol, 'ingeniero': { '$in': [ "Sin Asignar", userId ]}, 'createBy': userId, [tipoFecha]: { $lte: fechaFin }}};
                            }
                            if(fechaInicio!='null' && fechaFin=='null'){
                                eq = {'$match': {'estado': state[i], "rol": rol, 'ingeniero': { '$in': [ "Sin Asignar", userId ]}, 'createBy': userId, [tipoFecha]: { $gte: fechaInicio }}};
                            }
                            if(fechaInicio!='null' && fechaFin!='null'){
                                if (fechaInicio === fechaFin) {
                                    eq = {'$match': {'estado': state[i],"rol": rol, 'ingeniero': { '$in': [ "Sin Asignar", userId ]}, 'createBy': userId, [tipoFecha]: new RegExp(fechaInicio, 'gi') }};
                                } else {
                                    eq = {'$match': {'estado': state[i], "rol": rol, 'ingeniero': { '$in': [ "Sin Asignar", userId ]}, 'createBy': userId, [tipoFecha]: { $gte: fechaInicio, $lte: fechaFin }}};
                                }
                            }
                        }
                    } 
                    // if(rol=="Dispatcher nivel 2"){
                    //     if(tipoFecha=='null' || (fechaInicio=='null' && fechaFin=='null')){
                    //         eq = {'$match': {'estado': state[i], "rol": rol, "ingeniero": userId}};
                    //     }else{
                    //         if(fechaInicio=='null' && fechaFin!='null'){
                    //             eq = {'$match': {'estado': state[i], "rol": rol, "ingeniero": userId, [tipoFecha]: { $lte: fechaFin }}};
                    //         }
                    //         if(fechaInicio!='null' && fechaFin=='null'){
                    //             eq = {'$match': {'estado': state[i], "rol": rol, "ingeniero": userId, [tipoFecha]: { $gte: fechaInicio }}};
                    //         }
                    //         if(fechaInicio!='null' && fechaFin!='null'){
                    //             eq = {'$match': {'estado': state[i], "rol": rol, "ingeniero": userId, [tipoFecha]: { $gte: fechaInicio, $lte: fechaFin }}};
                    //         }
                    //     }
                    // } 
                } else if(i==2){
                    if(tipoFecha=='null' || (fechaInicio=='null' && fechaFin=='null')){
                        eq = {'$match': {'estado': state[i], "logs": { "$elemMatch": { "rol": "Dispatcher", "userId":userId, "rolInterno": rol } }}};
                    }else{
                        if(fechaInicio=='null' && fechaFin!='null'){
                            eq = {'$match': {'estado': state[i], "logs": { "$elemMatch": { "rol": "Dispatcher", "userId":userId, "rolInterno": rol } }, [tipoFecha]: { $lte: fechaFin } }};
                        }
                        if(fechaInicio!='null' && fechaFin=='null'){
                            eq = {'$match': {'estado': state[i], "logs": { "$elemMatch": { "rol": "Dispatcher", "userId":userId, "rolInterno": rol } }, [tipoFecha]: { $gte: fechaInicio } }};
                        }
                        if(fechaInicio!='null' && fechaFin!='null'){
                            if (fechaInicio === fechaFin) {
                                eq = {'$match': {'estado': state[i], "logs": { "$elemMatch": { "rol": "Dispatcher", "userId":userId, "rolInterno": rol } }, [tipoFecha]: new RegExp(fechaInicio, 'gi') }};
                            } else {
                                eq = {'$match': {'estado': state[i], "logs": { "$elemMatch": { "rol": "Dispatcher", "userId":userId, "rolInterno": rol } }, [tipoFecha]: { $gte: fechaInicio, $lte: fechaFin } }};
                            }
                        }
                    }
                } else if(i==3){
                    if(tipoFecha=='null' || (fechaInicio=='null' && fechaFin=='null')){
                        eq = {'$match': {'estado': { $in: [state[0], state[i]]}, "logs": { "$elemMatch": { "rol": "Dispatcher", "userId":userId, "rolInterno": rol } }}};
                    }else{
                        if(fechaInicio=='null' && fechaFin!='null'){
                            eq = {'$match': {'estado': { $in: [state[0], state[i]]}, "logs": { "$elemMatch": { "rol": "Dispatcher", "userId":userId, "rolInterno": rol } }, [tipoFecha]: { $lte: fechaFin } }};
                        }
                        if(fechaInicio!='null' && fechaFin=='null'){
                            eq = {'$match': {'estado': { $in: [state[0], state[i]]}, "logs": { "$elemMatch": { "rol": "Dispatcher", "userId":userId, "rolInterno": rol } }, [tipoFecha]: { $gte: fechaInicio } }};
                        }
                        if(fechaInicio!='null' && fechaFin!='null'){
                            if (fechaInicio === fechaFin) {
                                eq = {'$match': {'estado': { $in: [state[0], state[i]]}, "logs": { "$elemMatch": { "rol": "Dispatcher", "userId":userId, "rolInterno": rol } }, [tipoFecha]: new RegExp(fechaInicio, 'gi') }};
                            } else {
                                eq = {'$match': {'estado': { $in: [state[0], state[i]]}, "logs": { "$elemMatch": { "rol": "Dispatcher", "userId":userId, "rolInterno": rol } }, [tipoFecha]: { $gte: fechaInicio, $lte: fechaFin } }};
                            }
                        }
                    }
                }else{
                    if(tipoFecha=='null' || (fechaInicio=='null' && fechaFin=='null')){
                        eq = {'$match': {'estado': state[i], "ingeniero":userId, "rol": rol}};
                    }else{
                        if(fechaInicio=='null' && fechaFin!='null'){
                            eq = {'$match': {'estado': state[i], "ingeniero":userId, "rol": rol, [tipoFecha]: { $lte: fechaFin } }};
                        }
                        if(fechaInicio!='null' && fechaFin=='null'){
                            eq = {'$match': {'estado': state[i], "ingeniero":userId, "rol": rol, [tipoFecha]: { $gte: fechaInicio } }};
                        }
                        if(fechaInicio!='null' && fechaFin!='null'){
                            if (fechaInicio === fechaFin) {
                                eq = {'$match': {'estado': state[i], "ingeniero":userId, "rol": rol, [tipoFecha]: new RegExp(fechaInicio, 'gi') }};
                            } else {
                                eq = {'$match': {'estado': state[i], "ingeniero":userId, "rol": rol, [tipoFecha]: { $gte: fechaInicio, $lte: fechaFin } }};
                            }
                        }
                    }
                }
                array.push(eq);
            }
        }
    }

        return this.connect().then(db => {
            return db.collection(collection)
            .aggregate([
                { "$facet": {
                    "Asignadas": [
                      array[0],
                      { "$count": "total"},
                    ],
                    "SinAsignar": [
                        array[1],
                      { "$count": "total"},
                    ],
                    "Terminada": [
                        array[2],
                      { "$count": "total"},
                    ],
                    "Iniciada": [
                        array[3],
                      { "$count": "total"},
                    ],
                    "BackLog": [
                        array[4],
                      { "$count": "total"},
                    ]
                  }}
              ]).toArray()
        })  
    }

    getCountersSamia(collection, query) {
        const userId = query.ingeniero;
        const rol = query.rol;
        const tipoFecha = query.tipoFecha;
        const fechaInicio = query.fechaInicio;
        const fechaFin = query.fechaFin;
        delete query.userId;
        delete query.rol;
        delete query.tipoFecha;
        delete query.fechaInicio;
        delete query.fechaFin;
        
        let array = [];
        const state = ["Entrada", "Salidas y Entregas", "Devolucion", "Consumo", "Translado"];

            for(let i = 0; i < state.length; i++){
                let eq = {};
                    if(tipoFecha=='null' || (fechaInicio=='null' && fechaFin=='null')){
                        eq = {'$match': {'estado': state[i], "ingeniero":userId, "rol": rol}};
                    }else{
                        if(fechaInicio=='null' && fechaFin!='null'){
                            eq = {'$match': {'estado': state[i], "ingeniero":userId, "rol": rol, [tipoFecha]: { $lte: fechaFin }}};
                        }
                        if(fechaInicio!='null' && fechaFin=='null'){
                            eq = {'$match': {'estado': state[i], "ingeniero":userId, "rol": rol, [tipoFecha]: { $gte: fechaInicio }}};
                        }
                        if(fechaInicio!='null' && fechaFin!='null'){
                            if (fechaInicio === fechaFin) {
                                eq = {'$match': {'estado': state[i], "ingeniero":userId, "rol": rol, [tipoFecha]: new RegExp(fechaInicio, 'gi') }};
                            } else {
                                eq = {'$match': {'estado': state[i], "ingeniero":userId, "rol": rol, [tipoFecha]: { $gte: fechaInicio, $lte: fechaFin }}};
                            }
                        }
                    }
                array.push(eq);
            }

        return this.connect().then(db => {
            return db.collection(collection)
            .aggregate([
                { "$facet": {
                    "Entradas": [
                      array[0],
                      { "$count": "total"},
                    ],
                    "Salidas": [
                        array[1],
                      { "$count": "total"},
                    ],
                    "Devoluciones": [
                        array[2],
                      { "$count": "total"},
                    ],
                    "Consumos": [
                        array[3],
                      { "$count": "total"},
                    ],
                    "Translados": [
                        array[4],
                      { "$count": "total"},
                    ]
                  }}
              ]).toArray()
        })  
    }

    getPosInfo(collection, id) {
        return this.connect().then(db => {
            return db.collection(collection).findOne({ _id: ObjectId(id) }, { po: 1 });
        })
    }

    getHito(collection, id) {
        return this.connect().then(db => {
            return db.collection(collection).findOne({ projectId: id});
        })
    }

    getActivityPymes(collection, id) {
        return this.connect().then(db => {
            return db.collection(collection).findOne({ projectId: id});
        })
    }

    getFormPymes(collection, id) {
        return this.connect().then(db => {
            return db.collection(collection).findOne({ otp: id});
        })
    }

    createPoInventory(collection, data) {
        return this.connect().then(db => {
            return db.collection(collection).updateOne({ _id: ObjectId(data._id) }, { $push: {po: { poNumber: data.poNumber, deliveredQuantity: parseInt(data.deliveredQuantity), deliveryDate: moment(data.deliveryDate)._d, forecastDate: moment(data.forecastDate)._d
            }} }), { upsert: true }
        }).then(result => result.upsertedId || data);
    }

    deletePoInventory(collection, data) {
        return this.connect().then(db => {
            return db.collection(collection).updateOne({ _id: ObjectId(data._id) }, { $pull: {po: { poNumber: data.poNumber }} }), { upsert: true }
        }).then(result => result.upsertedId || data);
    }

    updatePoInventory(collection, data) {
        return this.connect().then(db => {
            return db.collection(collection).updateOne({ _id: ObjectId(data._id), "po.poNumber": data.poNumber }, { $set: { "po.$.deliveredQuantity" : parseInt(data.deliveredQuantity),  "po.$.deliveryDate": moment(data.deliveryDate)._d,  "po.$.forecastDate": moment(data.forecastDate)._d
        } })
        }).then(result => result.upsertedId || data);
    }

    /* GENERACION ESPECIFICA DE LLAMADOS A BASE DE DATOS */

    match(collection, query) {
        const { limit, page, sort, columnsNeeded } = query;
        delete query.limit
        delete query.page
        delete query.sort
        delete query.columnsNeeded
        let addFields = {};
        let match = {};
        let totalData = [];
        let data = [];
        if (query.numbersFieldSeach) {
            query.numbersFieldSeach.map((numberField) => {
                addFields[`${numberField.key}Str`] = {$toString: `$${numberField.key}`}
                match[`${numberField.key}Str`] = { $regex: new RegExp(numberField.value, 'gi') };
            })
            totalData.push({$addFields: addFields});
            data.push({$addFields: addFields});
            delete query.numbersFieldSeach;
        };

        if (query.datesFieldSearch) {
            query.datesFieldSearch.map((numberField) => {
                match[numberField.field] = { $gte: numberField.gte, $lte: numberField.lt };
            })
            delete query.datesFieldSearch;
        };

        if (query.lookup) {
            data.push({
                $lookup: {
                    from: query.lookup.from,
                    localField:  query.lookup.localField,
                    foreignField:  query.lookup.foreignField,
                    as: query.lookup.as
                }
            })
            delete query.lookup;
        }

        if(query.or && query.or.length !== 0) {
            let array = [];
            query.or.map((numberField) => {
                array.push({[numberField.field]: { $regex: new RegExp(numberField.value, 'gi') }});
            })
            match['$or'] = array;
            delete query.or;
        }
        
        if(query.and && query.and.length !== 0) {
            let array = [];
            query.and.map((numberField) => {
                    if (typeof(numberField.value) === 'object' && numberField.value.length >=1 && moment(numberField.value[0], 'YYYY-MM-DD', true).isValid() === false) {
                        array.push({[numberField.field]: { $in: numberField.value }});
                    }
                    if (typeof(numberField.value) === 'object' && numberField.value.length ===1 && moment(numberField.value[0], 'YYYY-MM-DD', true).isValid() === true){
                        array.push({[numberField.field]: { $regex: new RegExp(numberField.value[0], 'gi') }});
                    }
                    if (typeof(numberField.value) === 'object' && numberField.value.length ===2 && moment(numberField.value[0], 'YYYY-MM-DD', true).isValid() === true){
                        if (numberField.value[0] === numberField.value[1]) {
                            array.push({[numberField.field]: { $regex: new RegExp(numberField.value[0], 'gi') }});
                        } else {
                            array.push({[numberField.field]: { $gte: numberField.value[0], $lte: moment(numberField.value[1]).add(1, 'd').locale('es').format('YYYY-MM-DD') }});
                        }
                    } 
                    if (typeof(numberField.value) !== 'object'){
                        array.push({[numberField.field]: { $regex: new RegExp(numberField.value, 'gi') }});
                    }
            })
            match['$and'] = array;
            delete query.and;
        }

        if(query.neq) {
            query.neq.map((numberField) => {
                // numberField.values.map((value) => {
                //     match[numberField.field] = { $regex: new RegExp(`^((?!${value}).)*$`, 'gm') };
                // })
                match[numberField.field] = { $nin: numberField.values};
            })
            delete query.neq;
        }

        if(query.compare) {
            query.compare.map((numberField) => {
                match[numberField.field[0]] = { [numberField.field[1]]: numberField.values};
            })
            delete query.compare;
        }

        if(query.eq) {
            query.eq.map((numberField) => {
                match[numberField.field] = { $in: numberField.values};
            })
            delete query.eq;
        }

        if(query.gte) {
            query.gte.map((numberField) => {
                match[numberField.field] = { $gte: numberField.values};
            })
            delete query.gte;
        }

        if(query.lte) {
            query.lte.map((numberField) => {
                match[numberField.field] = { $lte: numberField.values};
            })
            delete query.lte;
        }

        if(query.gt) {
            query.gt.map((numberField) => {
                match[numberField.field] = { $gt: numberField.values};
            })
            delete query.gt;
        }

        if(query.lt) {
            query.lt.map((numberField) => {
                match[numberField.field] = { $lt: numberField.values};
            })
            delete query.lt;
        }
        
        if(query.filterFechas) {
            query.filterFechas.map((numberField) => {
                if((numberField.field != 'null')){
                    if((numberField.values[0] != 'null') && (numberField.values[1] != 'null')){
                        if (numberField.values[0] === numberField.values[1]) {
                            match[numberField.field] = { $regex: new RegExp(numberField.values[0], 'gi') }
                        } else {
                            match[numberField.field] = { $gte: numberField.values[0], $lt: moment(numberField.values[1]).add(1, 'd').locale('es').format('YYYY-MM-DD') }; //$lt: moment(numberField.values[1]).add(1, 'd').locale('es').format('YYYY-MM-DD') $lte: numberField.values[1]
                        }
                    }
                    if((numberField.values[0] != 'null') && (numberField.values[1] == 'null')){
                        match[numberField.field] = { $gte: numberField.values[0]};
                    }
                    if((numberField.values[0] == 'null') && ((numberField.values[1] != 'null'))){
                        match[numberField.field] = { $lte: numberField.values[1]};
                    }
                }
            })
            delete query.filterFechas;
        }

        for (let key in query){
            
            if ( typeof query[key] === 'object' && query[key].$elemMatch ){ 
                match[key] = query[key];
            } else {
                match[key] = { $regex: new RegExp(query[key], 'gi') };
            }
        }
        totalData.push({ $match :  match })
        totalData.push({ "$count": "filterTotal"})
        data.push({ $match :  match })
        if ( columnsNeeded ) {
            data.push({ '$project': columnsNeeded})
        }
        if (limit && page) {
            data.push({ '$skip': (limit * page) - limit})
            data.push({ '$limit': limit })
        }
        if ( sort ) {
            data.push({ '$sort': {[sort.column]: sort.value} })
        }

        return this.connect().then(db => {
            return db.collection(collection)
            .aggregate([
                { "$facet": {
                    "totalData": totalData,
                      "data": data,
                      "totalCount": [
                        { "$group": {
                          "_id": null,
                          "count": { "$sum": 1 },
                        },
                        },
                      ]
                  }
                }
            ]).toArray()
        })
    }

    matchArray(collection, query) {
        //let find_filter = query.filter ? JSON.parse(`{ "${query.filter}": "${query.value}" }`) : undefined;
        let limit = parseInt(query.limit)
        let page = parseInt(query.page)
        let sort = query.sort ?  JSON.parse(`{ "${query.sort}": -1 }`) : undefined;
        let elementMatch = undefined;
        if(query.match){
            elementMatch = {log: { $elemMatch: query.match }};
        }
        return this.connect().then(db => {
            return db.collection(collection).find(elementMatch).skip((limit * page) - limit)
            .limit(limit).sort(sort).toArray();
        })
    }

    countersGestion(collection, userId) {
        return this.connect().then(db => {
            return db.collection(collection)
            .aggregate([
                { "$facet": {
                  "verificacion": [
                    { "$match": {"usuarioAsignado.id": userId, "role": "Dispatcher", "estadoChecklist": "Asignado"}},
                    { "$count": "total"},
                  ],
                  "Pendientes": [
                    { "$match": { "usuarioAsignado.id": userId, "estadoChecklist": "Asignado"}},
                    { "$count": "total"},
                  ],
                  "Ejecucion": [
                    { "$match": { "usuarioAsignado.id": userId, "log.role.nombre": "Ejecutor", "estadoChecklist": "Iniciado" }},
                    { "$count": "total"},
                  ],
                  "Terminadas": [
                    { "$match": { "usuarioAsignado.id": userId, "role": "Gestion", "estadoChecklist": "Terminado" }},
                    { "$count": "total"},
                  ],
                  "Cancelada": [
                    { "$match": { "usuarioAsignado.id": userId, "role": "Gestion", "estadoChecklist": "Cancelado" }},
                    { "$count": "total"},
                  ]
                }}
            ]).toArray()
        })  
    }

    countersDispatcher(collection, userId) {
        return this.connect().then(db => {
            return db.collection(collection)
            .aggregate([
                { "$facet": {
                     
                  "Pendientes": [
                    { "$match": { log: { $elemMatch: { "idUsuario": userId, "estado": 'Asignado' } }, "role": "Dispatcher" }},
                    { "$count": "total"},
                  ],
                  "Asignados": [
                    { "$match": { log: { $elemMatch: { "idUsuario": userId, "role.nombre": "Dispatcher" } }, "estadoChecklist": "Asignado", "role": "Ejecutor"}},
                    { "$count": "total"},
                  ],
                  "Iniciadas": [
                    { "$match": { log: { $elemMatch: { "idUsuario": userId, 'role.nombre': 'Dispatcher' } }, "estadoChecklist": "Iniciado", "role": "Ejecutor" }},
                    { "$count": "total"},
                  ],    
                  "Terminadas": [
                    { "$match": { log: { $elemMatch: { "idUsuario": userId, "estado": 'Terminado' } }, "role": "Gestion", "estadoChecklist": "Terminado" }},
                    { "$count": "total"},
                  ],
                  "Cancelada": [
                    { "$match": { log: { $elemMatch: { "idUsuario": userId, "role.nombre": "Dispatcher" } }, "role": "Gestion", "estadoChecklist": "Cancelado" }},
                    { "$count": "total"},
                  ]
                }}
            ]).toArray()
        })  
    }
    
    countersEjecutor(collection, userId) {
        return this.connect().then(db => {
            return db.collection(collection)
            .aggregate([
                { "$facet": {
                  "Pendientes": [
                    { "$match": { log: { $elemMatch: { "idUsuario": userId, 'role.nombre': 'Ejecutor', "estado": 'Asignado' } }, "role": "Ejecutor" }},
                    { "$count": "total"},
                  ],
                  "Iniciados": [
                    { "$match": { log: { $elemMatch: { 'idUsuario': userId, 'role.nombre': 'Ejecutor', 'estado': 'Iniciado' } }, "estadoChecklist": "Iniciado", "role": "Ejecutor" }},
                    { "$count": "total"},
                  ],
                  "Terminadas": [
                    { "$match": { log: { $elemMatch: { "idUsuario": userId, "estado": 'Terminado' } }, "role": "Gestion", "estadoChecklist": "Terminado" }},
                    { "$count": "total"},
                  ],
                  "Cancelada": [
                    { "$match": { "log.idUsuario": userId, "estadoChecklist": "Cancelado" }},
                    { "$count": "total"},
                  ]
                }}
            ]).toArray()
        })  
    }

    countersKo(collection, query) {
        const userId = query.userId;
        let tipoFecha = query.fecha;
        const fechaInicio = query.fechaInicio;
        const fechaFin = query.fechaFin;
        delete query.userId
        delete query.tipoFecha
        delete query.fechaInicio
        delete query.fechaFin

        if(tipoFecha === 'fechas.actualizacion'){
            tipoFecha='fecActualizacionOnyxHija';
        }else if(tipoFecha === 'fechas.compromiso'){
            tipoFecha='fechaCompromiso';
        }else if(tipoFecha === 'fechas.programacion'){
            tipoFecha='fechaRealizacion';
        }else{//fechas.creacion
            tipoFecha='fechaCreacionOtHija';
        }

        let array = [];
        const state = [{"$nin": ["Cerrada", "Cancelada", "Cerrado", "Cancelado"]}, "Cerrad", "Cancelad"];

            for(let i = 0; i < state.length; i++){
                let eq = {};
                if(tipoFecha=='null' || (fechaInicio=='null' && fechaFin=='null')){
                    if(i===1 || i===2){
                        eq = { "$match": { "estado.nombre": new RegExp(state[i], 'gi'), "userOnyx": userId, "tipo": /Kick/ }}
                    } else{
                        eq = { "$match": { "estado.nombre": state[i], "userOnyx": userId, "tipo": /Kick/ }}
                    }
                }else{
                    if(fechaInicio=='null' && fechaFin!='null'){
                        if(i===1 || i===2){
                            eq = {'$match': {"estado.nombre": new RegExp(state[i], 'gi'), "userOnyx": userId, "tipo": /Kick/, [tipoFecha]: { $lte: fechaFin }}};
                        } else{
                            eq = {'$match': {"estado.nombre": state[i], "userOnyx": userId, "tipo": /Kick/, [tipoFecha]: { $lte: fechaFin }}};
                        }
                    }
                    if(fechaInicio!='null' && fechaFin=='null'){
                        if(i===1 || i===2){
                            eq = {'$match': {"estado.nombre": new RegExp(state[i], 'gi'), "userOnyx": userId, "tipo": /Kick/, [tipoFecha]: { $gte: fechaInicio }}};
                        } else{
                            eq = {'$match': {"estado.nombre": state[i], "userOnyx": userId, "tipo": /Kick/, [tipoFecha]: { $gte: fechaInicio }}};
                        }
                    }
                    if(fechaInicio!='null' && fechaFin!='null'){
                        if(i===1 || i===2){
                            eq = {'$match': {"estado.nombre": new RegExp(state[i], 'gi'), "userOnyx": userId, "tipo": /Kick/, [tipoFecha]: { '$gte': fechaInicio, '$lte': fechaFin }}};
                        } else{
                            eq = {'$match': {"estado.nombre": state[i], "userOnyx": userId, "tipo": /Kick/, [tipoFecha]: { '$gte': fechaInicio, '$lte': fechaFin }}};
                        }
                    }
                }
                array.push(eq);
            }

        return this.connect().then(db => {
            return db.collection(collection)
            .aggregate([
                { "$facet": {
                  "Generada": [
                    array[0],
                    { "$count": "total"},
                  ],
                  "Terminada": [
                    array[1],
                    { "$count": "total"},
                  ],
                  "Cancelada": [
                    array[2],
                    { "$count": "total"},
                  ]
                }}
              ]).toArray()
        })  
    }

    getManagementSupervisionPymes(collection) {
        //const userId = query.userId;
        //const rol = query.rol;
        // const tipoFecha = query.tipoFecha;
        // const fechaInicio = query.fechaInicio;
        // const fechaFin = query.fechaFin;
        //delete query.userId;
        //delete query.rol;
        // delete query.tipoFecha;
        // delete query.fechaInicio;
        // delete query.fechaFin;

        let array = [];
        let array2 = [];
        let eq = {};
        let eq2 = {};
        //let auxFecha = {};
        const state = ["Pem", "Preconfiguracion", "Novedad", "Kickoff"];

        const match = {'$match': {'$and':[
            {'rol': { '$in': state}}, 
            {'estado':{ '$in': ['Asignada', 'Iniciada']}}
            ]}
            };

        for(let i = 0; i < state.length; i++){

            //if(tipoFecha=='null' || (fechaInicio=='null' && fechaFin=='null')){
                    eq = {$cond: [{$eq: ['$tipoServicio', state[i]]}, {field: ["otpId"], value: '$otpId'}, '$$REMOVE']};
                    eq2 = {$cond: [{$eq: ['$tipoServicio', state[i]]}, 1, 0]};
            // }else{
            //     if(fechaInicio=='null' && fechaFin!='null'){
            //         auxFecha = {$lte: [tipoFecha, fechaFin]};
            //     }
            //     if(fechaInicio!='null' && fechaFin=='null'){
            //         auxFecha = {$gte: [tipoFecha, fechaInicio]};
            //     }
            //     if(fechaInicio!='null' && fechaFin!='null'){
            //         auxFecha = {$and: [{$gte: [tipoFecha, fechaInicio]}, 
            //                 {$lte: [tipoFecha, fechaFin]}
            //                 ]
            //         };
            //     }
            //     eq = {$cond: [{$and: [{$eq: ['$tipoServicio', state[i]]},
            //                             auxFecha
            //                         ]
            //                     },
            //                     {field: ["otpId"], value: '$otpId'}, '$$REMOVE']};

            //     eq2 = {$cond: [{$and: [{$eq: ['$tipoServicio', state[i]]},
            //                     auxFecha
            //                         ]
            //                     }, 1, 0]};
            // }
            array.push(eq);
            array2.push(eq2);
        }
        //if(tipoFecha=='null' || (fechaInicio=='null' && fechaFin=='null')){
            eq = {$cond: [{$or: [
                                 {$eq: ['$tipoServicio', state[0]]},
                                 {$eq: ['$tipoServicio', state[1]]},
                                 {$eq: ['$tipoServicio', state[2]]},
                                 {$eq: ['$tipoServicio', state[3]]}
                                ]
                            },
                            {field: ["otpId"], value: '$otpId'}, '$$REMOVE']};
            eq2 = {$cond: [{$or: [
                                {$eq: ['$tipoServicio', state[0]]},
                                {$eq: ['$tipoServicio', state[1]]},
                                {$eq: ['$tipoServicio', state[2]]},
                                {$eq: ['$tipoServicio', state[3]]}
                                ]
                            },
                            1, 0]};
            
        // }else{
        //     eq = {$cond: [{$and: [{$or: 
        //                                 [{$eq: ['$tipoServicio', state[0]]},
        //                                  {$eq: ['$tipoServicio', state[1]]},
        //                                  {$eq: ['$tipoServicio', state[2]]},
        //                                  {$eq: ['$tipoServicio', state[3]]}
        //                                 ]
        //                             },
        //                             auxFecha
        //                         ]},
        //                         {field: ["otpId"], value: '$otpId'}, '$$REMOVE']};
        //     eq2 = {$cond: [{$and: [{$or: 
        //                             [{$eq: ['$tipoServicio', state[0]]},
        //                              {$eq: ['$tipoServicio', state[1]]},
        //                              {$eq: ['$tipoServicio', state[2]]},
        //                              {$eq: ['$tipoServicio', state[3]]}
        //                             ]
        //                         },
        //                         auxFecha
        //                     ]},
        //                     1, 0]};
        // }
        array.push(eq);
        array2.push(eq2);

        return this.connect().then(db => {
            return db.collection(collection)
            .aggregate([
                { "$facet": {
                    "data": [
                        match,
                        { "$project": {
                            "_id": 0,
                            "ingeniero": 1,
                            "nombreIng": 1,
                            "Pem": array[0],
                            "Pre": array[1],
                            "Nov": array[2],
                            "Kick": array[3],
                            "Total": array[4],
                        }},
                        { "$group": {
                                "_id": "$ingeniero",
                                "name": { $first: "$nombreIng" },
                                "SumPem": {$push: '$Pem'},
                                "SumPreconf": {$push: '$Pre'},
                                "SumNovedad": {$push: '$Nov'},
                                "SumKickoff": {$push: '$Kick'},
                                "SumTotal": {$push: '$Total'},
                              "count": { "$sum": 1 },
                            },
                        },
                        { "$sort":{'_id':1} },
                    ],
                    "totalData": [
                        match,
                        { "$project": {
                            "_id": 0,
                            "ingeniero": 1,
                            "nombreIng": 1,
                            "Pem": array2[0],
                            "Pre": array2[1],
                            "Nov": array2[2],
                            "Kick": array2[3],
                            "Total": array2[4],
                        }},
                        { "$group": {
                                "_id": "$ingeniero",
                                "name": { $first: "$nombreIng" },
                                "SumPem": {$sum: '$Pem'},
                                "SumPreconf": {$sum: '$Pre'},
                                "SumNovedad": {$sum: '$Nov'},
                                "SumKickoff": {$sum: '$Kick'},
                                "SumTotal": {$sum: '$Total'},
                              "count": { "$sum": 1 },
                            },
                        },
                        { "$sort":{'_id':1} },
                    ]
                }}
            ]).toArray();
            })
    }

    getRolesSupervisionPymes(collection, query) {
        const rol = query.rol;
        const tipoFecha = query.tipoFecha;
        const fechaInicio = query.fechaInicio;
        const fechaFin = query.fechaFin;
        delete query.rol;
        delete query.tipoFecha;
        delete query.fechaInicio;
        delete query.fechaFin;
        let eq={};

        if(rol==='Pem' || rol==='Preconfiguracion' || rol==='Novedad' || rol==='Kickoff'){
            if(tipoFecha=='null' || (fechaInicio=='null' && fechaFin=='null')){
                eq={'$match': {'selectedTags2.rol': { '$in': ['Pem', 'Preconfiguracion', 'Novedad', 'Kickoff']}}};
            }else{
                if(fechaInicio=='null' && fechaFin!='null'){
                    eq= {'$match': {'$and':[
                        {'selectedTags2.rol': { '$in': ['Pem', 'Preconfiguracion', 'Novedad', 'Kickoff']}}, 
                        {[('selectedTags2.').concat(tipoFecha)]:{'$lte': fechaFin}}
                        ]}
                        };
                }
                if(fechaInicio!='null' && fechaFin=='null'){
                    eq= {'$match': {'$and':[
                        {'selectedTags2.rol': { '$in': ['Pem', 'Preconfiguracion', 'Novedad', 'Kickoff']}}, 
                        {[('selectedTags2.').concat(tipoFecha)]:{'$gte': fechaInicio}}
                        ]}
                        };
                }
                if(fechaInicio!='null' && fechaFin!='null'){
                    eq= {'$match': {'$and':[
                        {'selectedTags2.rol': { '$in': ['Pem', 'Preconfiguracion', 'Novedad', 'Kickoff']}}, 
                        {[('selectedTags2.').concat(tipoFecha)]:{'$lte': fechaFin}},
                        {[('selectedTags2.').concat(tipoFecha)]:{'$gte': fechaInicio}}
                        ]}
                        };
                }
        }
        }else{
            if(tipoFecha=='null' || (fechaInicio=='null' && fechaFin=='null')){
                eq={'$match': {'selectedTags2.rol': rol}};
            }else{
                if(fechaInicio=='null' && fechaFin!='null'){
                    eq= {'$match': {'$and':[
                        {'selectedTags2.rol': rol}, 
                        {[('selectedTags2.').concat(tipoFecha)]:{'$lte': fechaFin}}
                        ]}
                        };
                }
                if(fechaInicio!='null' && fechaFin=='null'){
                    eq= {'$match': {'$and':[
                        {'selectedTags2.rol': rol}, 
                        {[('selectedTags2.').concat(tipoFecha)]:{'$gte': fechaInicio}}
                        ]}
                        };
                }
                if(fechaInicio!='null' && fechaFin!='null'){
                    eq= {'$match': {'$and':[
                        {'selectedTags2.rol': rol}, 
                        {[('selectedTags2.').concat(tipoFecha)]:{'$lte': fechaFin}},
                        {[('selectedTags2.').concat(tipoFecha)]:{'$gte': fechaInicio}}
                        ]}
                        };
                }
            }
         }

        return this.connect().then(db => {
            return db.collection(collection)
            .aggregate([
                { $project:
                    { logs:
                       {
                         $map:
                            {
                              input: "$logs",
                              as: "lg",
                              in: { $mergeObjects: [ "$$lg", 
                                  {estadoFinal: "$estado"}, 
                                  {rolFinal: "$rol"},
                                  {fechaCalendario: "$fechaCalendario"},
                                  {fechaCompromiso: "$fechaCompromiso"},
                                  ] }
                            }
                       }
                    }
                 },
                {$group:{
                    _id: null,
                    selectedTags: { $push: '$logs' }
                        
                 }},
                 {$project: {
                     selectedTags2: { $reduce: {
                         input: "$selectedTags",
                         initialValue: [],
                         in: {$setUnion : ["$$value", "$$this"]}
                     }}
                 }},
                 {$unwind: "$selectedTags2" },
                 eq,
                 { "$group": {
                                "_id": "$selectedTags2.userId",
                                "name": { $first: "$selectedTags2.createBy" },
                                'logs': {$push: {
                                        "idOtp": "$selectedTags2.idOtp",
                                        "action": "$selectedTags2.action",
                                        "createAt": "$selectedTags2.createAt",
                                        "createBy": "$selectedTags2.createBy",
                                        "estado": "$selectedTags2.estado",
                                        "userId": "$selectedTags2.userId",
                                        "rol": "$selectedTags2.rol",
                                        "rolFinal": "$selectedTags2.rolFinal",
                                        "estadoFinal": "$selectedTags2.estadoFinal",
                                        "fechaCalendario": "$selectedTags2.fechaCalendario",
                                        "fechaCompromiso": "$selectedTags2.fechaCompromiso",
                                    }},
                            },
                        }
            ]).toArray();

        })
    }

    getExportPymesForms(collection, query) {
        const tipoFecha = query.filterFechas[0].field;
        const fechaInicio = query.filterFechas[0].values[0];
        const fechaFin = query.filterFechas[0].values[1];
        const estado = query.estado;

        delete query.tipoFecha;
        delete query.fechaInicio;
        delete query.fechaFin;
        delete query.estado;

        let array = [];

        const form = ["Pem", "Preconfiguracion", "Novedad", "Kickoff"];

        for(let i = 0; i < form.length; i++){
            let eq = {};
            if(tipoFecha=='null' || (fechaInicio=='null' && fechaFin=='null')){
                eq = { "$match": { "estado": estado, "tipoServicio": form[i] }};
            }else{
                if(fechaInicio=='null' && fechaFin!='null'){
                    eq = {'$match': {"estado": estado, "tipoServicio": form[i], [tipoFecha]: { $lte: fechaFin }}};
                }
                if(fechaInicio!='null' && fechaFin=='null'){
                    eq = {'$match': {"estado": estado, "tipoServicio": form[i], [tipoFecha]: { $gte: fechaInicio }}};
                }
                if(fechaInicio!='null' && fechaFin!='null'){
                    if (fechaInicio === fechaFin) {
                        eq = {'$match': {'estado': estado, "tipoServicio": form[i], [tipoFecha]: new RegExp(fechaInicio, 'gi') }};
                    } else {
                        eq = {'$match': {"estado": estado, "tipoServicio": form[i], [tipoFecha]: { '$gte': fechaInicio, '$lte': fechaFin }}};
                    }
                }
            }
            array.push(eq);
        }
        return this.connect().then(db => {
            return db.collection(collection)
            .aggregate([
                { "$facet": {
          "PEM": [
            array[0],
            {
                 $lookup:
                   {
                     from: "pem",
                     localField: "otpId",
                     foreignField: "otp",
                     as: "form"
                   }
              }
          ],
          "Preconf": [
            array[1],
            {
                 $lookup:
                   {
                     from: "preconfiguracion",
                     localField: "otpId",
                     foreignField: "otp",
                     as: "form"
                   }
              }
          ],
          "Novedad": [
            array[2],
            {
                 $lookup:
                   {
                     from: "novedad",
                     localField: "otpId",
                     foreignField: "otp",
                     as: "form"
                   }
              }
          ],
          "Kickoff": [
            array[3],
            {
                 $lookup:
                   {
                     from: "kickoff",
                     localField: "otpId",
                     foreignField: "otp",
                     as: "form"
                   }
              }
          ]
        }
        
        
        }
    ]).toArray();

        })
    }

    getCountersGeneral(collection, query) {
        const tipoFecha = query.tipoFecha;
        const fechaInicio = query.fechaInicio;
        const fechaFin = query.fechaFin;
        const valores = query.valores.split(';');
        const campo = query.campo;
        const keys = query.keys && query.keys.split(';');
        const values = query.values && query.values.split(';');

        delete query.tipoFecha;
        delete query.fechaInicio;
        delete query.fechaFin;
        delete query.valores;
        delete query.campo;

        let facetObjects = {};
        let obj = {};

        if(keys && values && values.length > 0 && values.length === keys.length){
            for(let i=0; i < values.length; i++){
                obj = { ...obj,
                    [keys[i]]: values[i],
                }
            }
        }
        for(let i = 0; i < valores.length; i++){
            let eq = {};
            if(tipoFecha=='null' || (fechaInicio=='null' && fechaFin=='null')){
                if(keys && values && values.length > 0 && values.length === keys.length){
                    for(let i=0; i < values.length; i++){
                        obj = { ...obj,
                            [keys[i]]: values[i],
                        }
                    }
                    obj = { ...obj,
                        [campo]: valores[i],
                    }
                    eq = { "$match": obj};
                } else {
                    eq = { "$match": { [campo]: valores[i] }};
                }
            }else{
                if(fechaInicio=='null' && fechaFin!='null'){
                    if(keys && values && values.length > 0 && values.length === keys.length){
                        for(let i=0; i < values.length; i++){
                            obj = { ...obj,
                                [keys[i]]: values[i],
                            }
                        }
                        obj = { ...obj,
                            [campo]: valores[i],
                            [tipoFecha]: { $lte: fechaFin },
                        }
                        eq = { "$match": obj};
                    } else {
                        eq = { "$match": { [campo]: valores[i], [tipoFecha]: { $lte: fechaFin } }};
                    }
                }
                if(fechaInicio!='null' && fechaFin=='null'){
                    if(keys && values && values.length > 0 && values.length === keys.length){
                        for(let i=0; i < values.length; i++){
                            obj = { ...obj,
                                [keys[i]]: values[i],
                            }
                        }
                        obj = { ...obj,
                            [campo]: valores[i],
                            [tipoFecha]: { $gte: fechaInicio },
                        }
                        eq = { "$match": obj};
                    } else {
                        eq = { "$match": { [campo]: valores[i], [tipoFecha]: { $gte: fechaInicio } }};
                    }
                }
                if(fechaInicio!='null' && fechaFin!='null'){
                    if (fechaInicio === fechaFin) {
                        if(keys && values && values.length > 0 && values.length === keys.length){
                            for(let i=0; i < values.length; i++){
                                obj = { ...obj,
                                    [keys[i]]: values[i],
                                }
                            }
                            obj = { ...obj,
                                [campo]: valores[i],
                                [tipoFecha]: new RegExp(fechaInicio, 'gi'),
                            }
                            eq = { "$match": obj};
                        } else {
                            eq = { "$match": { [campo]: valores[i], [tipoFecha]: new RegExp(fechaInicio, 'gi') }};
                        }
                    } else {
                        if(keys && values && values.length > 0 && values.length === keys.length){
                            for(let i=0; i < values.length; i++){
                                obj = { ...obj,
                                    [keys[i]]: values[i],
                                }
                            }
                            obj = { ...obj,
                                [campo]: valores[i],
                                [tipoFecha]: { '$gte': fechaInicio, '$lte': fechaFin },
                            }
                            eq = { "$match": obj};
                        } else {
                            eq = { "$match": { [campo]: valores[i], [tipoFecha]: { '$gte': fechaInicio, '$lte': fechaFin } }};
                        }
                    }
                }
            }
            facetObjects = {
                ...facetObjects,
                [valores[i]]: [eq, { "$count": "total"}],
            }
        }

        return this.connect().then(db => {
            return db.collection(collection)
            .aggregate([
                { "$facet": facetObjects
            }
            ]).toArray()
        })  
    }

    getReportGeneralSamia(collection, query) {
        const tipoFecha = query.tipoFecha;
        const fechaInicio = query.fechaInicio;
        const fechaFin = query.fechaFin;

        delete query.tipoFecha;
        delete query.fechaInicio;
        delete query.fechaFin;

            let eq = '';

            if(tipoFecha!=='null' && (fechaInicio!=='null' || fechaFin!=='null')){
                    eq = {'$match': {[tipoFecha]: { $lte: fechaFin }}};
                if(fechaInicio!='null' && fechaFin=='null'){
                    eq = {'$match': {[tipoFecha]: { $gte: fechaInicio }}};
                }
                if(fechaInicio!='null' && fechaFin!=='null'){
                    if (fechaInicio === fechaFin) {
                        eq = {'$match': {[tipoFecha]: new RegExp(fechaInicio, 'gi') }};
                    } else {
                        eq = {'$match': {[tipoFecha]: { '$gte': fechaInicio, '$lte': fechaFin }}};
                    }
                }
            }

            const camposCompartidos = [
            'departamento', 
            'bodega',
            'otp',
            'oth',
            'observaciones',
            'estado',
            'createdAt',
            'updatedAt',
            'zteId',
            ];

            const camposEntrada = [
                'prc',
                'propiedad',
                'cliente',
                'remision',
            ];

            const camposSalidaEntrega = [
                'fechaEntrega',
                'poyectoEspecial',
                'tipo',
            ];

            const camposDevolucion = [
                'tipoDevolucion',
            ];

            // const camposConsumo = [
            //     'quienInstalo',
            // ];

            const camposTranslado = [
                'fechaTranslado',
            ];

            const camposMateriales = [
                'idMaterial',
                'serial',
                'ubicacion',
                'cantidad',
                'codigoMaterial',
                'tipoMaterial',
                'estadoMaterial',
                'descripcion',
                'unidad',
            ];

        let merge1 = [];
        let merge2 = [];
        let group2 = {};
        merge1.push("$$mat");
        merge1.push({responsableNombre: "$responsable.nombre"});
        merge1.push({responsableId: "$responsable.id"});
        if (collection==='entradasSamia') {
            merge1.push({solicitanteNombre: "$solicitante.nombre"});
            merge1.push({solicitanteId: "$solicitante.id"});
            merge1.push({quienRecibeNombre: "$quienRecibe.nombre"});
            merge1.push({quienRecibeId: "$quienRecibe.id"});
            merge2.push("$$elem");
            merge2.push({responsableNombre: "$selectedTags2.responsableNombre"});
            merge2.push({responsableId: "$selectedTags2.responsableId"});
            merge2.push({solicitanteNombre: "$selectedTags2.solicitanteNombre"});
            merge2.push({solicitanteId: "$selectedTags2.solicitanteId"});
            merge2.push({quienRecibeNombre: "$selectedTags2.quienRecibeNombre"});
            merge2.push({quienRecibeId: "$selectedTags2.quienRecibeId"});
            group2 = {
                ...group2,
                "_id": "$elementos2.idMaterial",
                "responsableNombre": { $first: "$elementos2.responsableNombre" },
                "responsableId": { $first: "$elementos2.responsableId" },
                "solicitanteNombre": { $first: "$elementos2.solicitanteNombre" },
                "solicitanteId": { $first: "$elementos2.solicitanteId" },
                "quienRecibeNombre": { $first: "$elementos2.quienRecibeNombre" },
                "quienRecibeId": { $first: "$elementos2.quienRecibeId" },
            }
        } else {
            merge1.push({"idConsulta": { $concat: [ "$zteId", "|", "$$mat.idMaterial" ] }});
            group2 = {
                ...group2,
                "_id": "$selectedTags2.idConsulta",
            }
        }
        for (let i = 0; i<camposCompartidos.length; i++) {
            merge1.push({[camposCompartidos[i]]:`$${camposCompartidos[i]}`});
            if (collection==='entradasSamia') {
                merge2.push({[camposCompartidos[i]]: `$selectedTags2.${camposCompartidos[i]}`});
                group2 = {
                    ...group2,
                    [camposCompartidos[i]]: {$first: `$elementos2.${camposCompartidos[i]}`},
                }
                if(i+1 === camposCompartidos.length){
                    for(let i2=0; i2< camposEntrada.length; i2++){
                        merge1.push({[camposEntrada[i2]]:`$${camposEntrada[i2]}`});
                        merge2.push({[camposEntrada[i2]]: `$selectedTags2.${camposEntrada[i2]}`});
                        group2 = {
                            ...group2,
                            [camposEntrada[i2]]: {$first: `$elementos2.${camposEntrada[i2]}`},
                        }
                    }
                    for(let i3=0; i3< camposMateriales.length; i3++){
                        if (camposMateriales[i3] === 'estadoMaterial') {
                            merge2.push({[camposMateriales[i3]]: '$$elem.estado'});
                        } else {
                            merge2.push({[camposMateriales[i3]]: `$selectedTags2.${camposMateriales[i3]}`});
                        }
                        group2 = {
                            ...group2,
                            [camposMateriales[i3]]: {$first: `$elementos2.${camposMateriales[i3]}`},
                        }
                    }
                }
            } else {
                group2 = {
                    ...group2,
                    [camposCompartidos[i]]: {$first: `$selectedTags2.${camposCompartidos[i]}`},
                    responsableNombre: {$first: '$selectedTags2.responsableNombre'},
                    responsableId: {$first: '$selectedTags2.responsableId'},
                }
                let arrAux = [];
                if (collection==='salidasSamia' || collection==='entregasSamia') {
                    arrAux=camposSalidaEntrega;
                } else if (collection==='devolucionesSamia') {
                    arrAux=camposDevolucion;
                } else if (collection==='transladosSamia') {
                    arrAux=camposTranslado;
                }
                if(i+1 === camposCompartidos.length){
                    for(let i4=0; i4< arrAux.length; i4++){
                        merge1.push({[arrAux[i4]]:`$${arrAux[i4]}`});
                        group2 = {
                            ...group2,
                            [arrAux[i4]]: {$first: `$selectedTags2.${arrAux[i4]}`},
                        }
                    }
                    for(let i5=0; i5< camposMateriales.length; i5++){
                        group2 = {
                            ...group2,
                            [camposMateriales[i5]]: {$first: `$selectedTags2.${camposMateriales[i5]}`},
                        }
                    }
                }
            }
        }


        if (collection==='entradasSamia' || collection==='devolucionesSamia' || collection==='consumosSamia') {
            merge1.push({'fechaEntrada': '$fechaEntrada'});
            if (collection==='entradasSamia') {
                merge2.push({fechaEntrada: '$selectedTags2.fechaEntrada'});
                group2 = {
                    ...group2,
                    'fechaEntrada': {$first: '$elementos2.fechaEntrada'},
                }
            }
            if (collection==='consumosSamia') {
                merge1.push({'quienInstaloNombre': '$quienInstalo.nombre'});
                merge1.push({'quienInstaloId': '$quienInstalo.id'});
                group2 = {
                    ...group2,
                    'quienInstaloNombre': {$first: '$selectedTags2.quienInstaloNombre'},
                    'quienInstaloId': {$first: '$selectedTags2.quienInstaloId'},
                    'fechaEntrada': {$first: '$selectedTags2.fechaEntrada'},
                }
            }
            if (collection==='devolucionesSamia') {
                group2 = {
                    ...group2,
                    'fechaEntrada': {$first: '$selectedTags2.fechaEntrada'},
                }
            }
        }

        if(collection!=='entradasSamia' && collection!=='consumosSamia'){
            merge1.push({'quienEntregaNombre': '$quienEntrega.nombre'});
            merge1.push({'quienEntregaId': '$quienEntrega.id'});
            merge1.push({'quienRecibeNombre': '$quienRecibe.nombre'});
            merge1.push({'quienRecibeId': '$quienRecibe.id'});
            group2 = {
                ...group2,
                'quienEntregaNombre': {$first: '$selectedTags2.quienEntregaNombre'},
                'quienEntregaId': {$first: '$selectedTags2.quienEntregaId'},
                'quienRecibeNombre': {$first: '$selectedTags2.quienRecibeNombre'},
                'quienRecibeId': {$first: '$selectedTags2.quienRecibeId'},
            }
        }



        let ConsultaFinal = [];

        const project1 = {
            $project:
                { materiales:
                   {
                     $map:
                        {
                          input: "$materiales",
                          as: "mat",
                          in: { $mergeObjects: merge1 }
                        }
                   }
                }
        }

        const group1 = {
            $group:{
                _id: null,
                selectedTags: { $push: '$materiales' }
                    
             }
        };

        const project2 = {
            $project: {
                selectedTags2: { $reduce: {
                    input: "$selectedTags",
                    initialValue: [],
                    in: {$setUnion : ["$$value", "$$this"]}
                }}
            }
        };

        const unwind1 = {$unwind: "$selectedTags2" };

        if (eq !== '') {
            ConsultaFinal.push(eq);
        }

        ConsultaFinal.push(project1);
        ConsultaFinal.push(group1);
        ConsultaFinal.push(project2);
        ConsultaFinal.push(unwind1);

        if (collection==='entradasSamia') {
            const project3 = {
                $project:
                    { elementos2:
                       {
                         $map:
                            {
                              input: "$selectedTags2.elementos",
                              as: "elem",
                              in: { $mergeObjects: merge2 
                                }
                            }
                       }
                    }
            };

            ConsultaFinal.push(project3);
            ConsultaFinal.push({$unwind: "$elementos2" });
            ConsultaFinal.push({"$group": group2 });

        } else {
            ConsultaFinal.push({"$group": group2 });
        }

        return this.connect().then(db => {
            return db.collection(collection)
            .aggregate(ConsultaFinal).toArray();

        })

    }

    // countersKo(collection, userId) {
    //     return this.connect().then(db => {
    //         return db.collection(collection)
    //         .aggregate([
    //             { "$facet": {
    //               "Generada": [
    //                 { "$match": {"estado.nombre": {"$nin": ["Cerrada", "Cancelada"]}, "userOnyx": userId, "tipo": /Kick/}},
    //                 { "$count": "total"},
    //               ],
    //               "Terminada": [
    //                 { "$match": { "estado.nombre": "Cerrada", "userOnyx": userId, "tipo": /Kick/ }},
    //                 { "$count": "total"},
    //               ],
    //               "Cancelada": [
    //                 { "$match": { "estado.nombre": "Cancelada", "userOnyx": userId, "tipo": /Kick/ }},
    //                 { "$count": "total"},
    //               ]
    //             }}
    //           ]).toArray()
    //     })  
    // }


}

module.exports = MongoLib;